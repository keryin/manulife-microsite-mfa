let autoprefixer = require('gulp-autoprefixer');
let batchReplace = require('gulp-batch-replace');
let browserSync = require('browser-sync');
let cleanCSS = require('gulp-clean-css');
let clearRequire = require('clear-require');
let version = require('gulp-version-number');
let dataPlugin = require('gulp-data');
let debug = require('gulp-debug');
let del = require('del');
let flatten = require('gulp-flatten');
let fs = require('fs');
let gulp = require('gulp');
let gulpSequence = require('gulp-sequence');
let handlebars = require('gulp-compile-handlebars');
let hash_src = require("gulp-hash-src");
let jade = require('gulp-jade');
let moment = require('moment');
let notifier = require('node-notifier');
let path = require('path');
let plumber = require('gulp-plumber');
let reload = browserSync.reload;
let rename = require('gulp-rename');
let replace = require('gulp-replace');
let sass = require('gulp-sass');
let sassGlob = require('gulp-sass-glob');
let sourcemaps = require('gulp-sourcemaps');
let template = require('gulp-template');
// let uglify = require('gulp-uglify');
let uglify = require('gulp-uglify-es').default;

let username = require('username');
let vinylPaths = require('vinyl-paths');
let webpack = require('gulp-webpack');
let webpackstream = require('webpack-stream');

//hack for making data.js fallback tree system
global.__base = __dirname + '/';
global.moment = require('moment');

//https://hooks.slack.com/services/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
var slack = require('gulp-slack')({
    url: 'https://hooks.slack.com/services/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',
    channel: '#studio_automations',
    icon_url: 'https://s3-eu-west-1.amazonaws.com/studio.gf.staging/Novia/images/general/novia_logo.svg'
});


var DB = {
    data: {
        en: "./pug/data/data.js",
        agency: {
            general: {
                en: "./pug/all/agency/general/en/data/data.js",
            },
            ipaadvisory: {
                en: "./pug/all/agency/ipaadvisory/en/data/data.js",
            },

        },
        agent: {
            general: {
                en: "./pug/all/agent/general/en/data/data.js",
            },
            // greenapple: {
            //     en: "./pug/all/agent/greenapple/en/data/data.js",
            // }

        }
    },
    handlebar: {
        en: "./pug/data/handleBarsData.js",
        agency: {
            general: {
                en: "./pug/all/agency/general/en/data/handleBarsData.js",
            },
            ipaadvisory: {
                en: "./pug/all/agency/ipaadvisory/en/data/handleBarsData.js",
            },
        },
        agent: {
            general: {
                en: "./pug/all/agent/general/en/data/handleBarsData.js",
            },
            // greenapple: {
            //     en: "./pug/all/agent/greenapple/en/data/handleBarsData.js",
            // }
        }
    }
};

//Add versioning to the js,css refer this for details https://www.npmjs.com/package/gulp-version-number
var versionConfig = {
    value: moment().format("YYYYMMDDHHmmss"),
    append: {
      key: 'v',
      to: ['css', 'js'],
    },
};


// var dataEN = require('./pug/all/agency/en/data/data.js');
var dataAgencyGeneralEN = require('./pug/all/agency/general/en/data/data.js');

// var dataAgencyEN = require('./pug/all/agency/en/data/data.js');
// var dataAgentEN = require('./pug/all/agent/en/data/data.js');

// var handleBarsDataEN = require('./pug/all/agency/en/data/handleBarsData.js');
var handleBarsDataAgencyGeneralEN = require('./pug/all/agency/general/en/data/handleBarsData.js');

// var handleBarsDataAgencyEN = require('./pug/all/agency/en/data/handleBarsData.js');
// var handleBarsDataAgentEN = require('./pug/all/agent/en/data/handleBarsData.js');



/** production environment **/
// const S3_BUCKET_PRODUCTION = '//s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/production/mls-mfa/';
const S3_BUCKET_PRODUCTION = '//s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/staging/mls-mfa/';
const S3_BUCKET_STAGING = '//d4kcggv2s2ep2.cloudfront.net/manulife-website/xyz/';
const S3_BUCKET_STAGING_NO_HASH = '//s3-eu-west-1.amazonaws.com/studio.gf.staging/manulife-website/staging/mls-mfa/';


// ------------------------------------------------ Variables for replacer
let THISPROJECTNAME = 'Manulife';
let currentProject = 'Manulife';


// gulp.task('slack-gulp', function () {
//     username().then(username => {
//         slack('Project: '+currentProject+'\n Task: Gulp\n User: ' + username)
//     });
// });

// gulp.task('slack-gulp-st', function () {
//     username().then(username => {
//         slack('Project: '+currentProject+'\n Task: Gulp st\n User: ' + username)
//     });
// });

// gulp.task('slack-gulp-pt', function () {
//     username().then(username => {
//         slack('Project: '+currentProject+'\n Task: Gulp pt\n User: ' + username)
//     });
// });



// ------------------------------------------------------------------------------------------------------ Tasks
// ------------------------------------------------ Utilities
gulp.task('clean:all', function (cb) {
    del([
        'build-staging',
        'build-production',
        'local',
        'build-test',
        'build'
    ]);
    cb();
});

gulp.task('build-notification', function (cb) {
    notifier.notify({
        title: 'Done:',
        message: 'Building',
        sound: 'Blow' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
    });

    cb();
});

//fix this to make dynamic with walker inside
gulp.task('cleanNodeCache', function (cb) {

    // clearRequire('./pug/data/data.js');
    // clearRequire('./pug/data/handleBarsData.js');
    // clearRequire('./pug/all/agency/en/data/data.js');
    // clearRequire('./pug/all/agent/en/data/data.js');
    // clearRequire('./pug/all/agency/en/data/handleBarsData.js');
    // clearRequire('./pug/all/agent/en/data/handleBarsData.js');
    //
    // dataAgencyGeneralEN = require('./pug/data/data.js');
    // handleBarsDataAgencyGeneralEN = require('./pug/data/handleBarsData.js');
    //
    // dataAgencyEN = require('./pug/all/agency/en/data/data.js');
    // dataAgentEN = require('./pug/all/agent/en/data/data.js');
    // handleBarsDataAgencyEN = require('./pug/all/agency/en/data/handleBarsData.js');
    // handleBarsDataAgentEN = require('./pug/all/agent/en/data/handleBarsData.js');

    cb();

});


gulp.task('timestamp-create-staging', function () {

    return gulp.src('./build-staging/**/*')
    .pipe(plumber())
    .pipe(debug({title: '----timestamp staging:', minimal: true}))
    .pipe(replace('momentDateStampObject', moment().format("YYYY/MM/DD/ HH-mm-ss")))
    .pipe(gulp.dest('./build-staging/'));

});

gulp.task('timestamp-create-production', function () {
    gulp.src('./build-staging/**/*')
    .pipe(plumber())
    .pipe(debug({title: '----timestamp staging:', minimal: true}))
    .pipe(replace('momentDateStampObject', moment().format("YYYY/MM/DD/ HH-mm-ss")))
    .pipe(gulp.dest('./build-staging/'));

    return gulp.src('./build-production/**/*')
    .pipe(plumber())
    .pipe(debug({title: '----timestamp production:', minimal: true}))
    .pipe(replace('momentDateStampObject', moment().format("YYYY/MM/DD/ HH-mm-ss")))
    .pipe(gulp.dest('./build-production/'));
});



gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: './',
            serveStaticOptions: {
                extensions: ['html']
            }
        },
        port: 9085,
        ui: {
            port: 9086
        },
        // open: "external",
        // browser: "google chrome",
        startPath: "/local/"
    });
});


// function serverLocal(path) {
//     return function () {
//         browserSync.init({
//             server: {
//                 baseDir: './',
//                 serveStaticOptions: {
//                     extensions: ['html']
//                 }
//             },
//             port: 9066,
//             ui: {
//                 port: 9067
//             },
//             open: "external",
//             browser: "google chrome",
//             startPath: "/local/"
//         });
//     };
// }
//
// gulp.task('server-local-agency-general-en', serverLocal('agency/general/en/index.html'));



gulp.task('server-staging', function () {
    browserSync.init({
        server: {
            baseDir: './',
            serveStaticOptions: {
                extensions: ['html']
            }
        },
        port: 9036,
        ui: {
            port: 9037
        },
        open: "external",
        browser: "google chrome",
        startPath: "/build-staging/html/agency/en/"
    });
});

gulp.task('server-reload', function (cb) {
    reload();
    cb();
});


// ------------------------------------------------ Pug


// siteMap object is used for fixing [HTMLs names], [attribute href], [attribute page]
// for converting urls from
// when you change page name, please change it here also
// in future I will create functions to compare real files and this object, and find missing files
let siteMap = {
    "redemption" : { origin: "redemption", prefix: "women-we-know"},
    "simple-tips-for-creating-sustainable-financial-habits-for-successful-women" : { origin: "simple-tips-for-creating-sustainable-financial-habits-for-successful-women", prefix: "insights"},
    "easy-ways-to-identify-your-money-personality" : { origin: "easy-ways-to-identify-your-money-personality", prefix: "insights"},
    "see-how-the-savings-grow" : { origin: "see-how-the-savings-grow", prefix: "insights"},
    "investing-strategies-for-every-speed" : { origin: "investing-strategies-for-every-speed", prefix: "insights"},
    "do-i-need-an-early-stage-critical-illness-plan" : { origin: "do-i-need-an-early-stage-critical-illness-plan", prefix: "insights"},
    "do-i-need-a-personal-accident-plan-if-i-work-in-an-office" : { origin: "do-i-need-a-personal-accident-plan-if-i-work-in-an-office", prefix: "insights"},
    "tips-for-financial-security-from-inspirational-women" : { origin: "tips-for-financial-security-from-inspirational-women", prefix: "insights"},
    "is-critical-illness-insurance-necessary" : { origin: "is-critical-illness-insurance-necessary", prefix: "insights"},
    "critical-illness-insurance-for-elderly-parents" : { origin: "critical-illness-insurance-for-elderly-parents", prefix: "insights"},
    "how-to-stay-active-and-healthy-while-at-home" : { origin: "how-to-stay-active-and-healthy-while-at-home", prefix: "insights"},
    "five-ways-to-teach-children-the-importance-of-financial-responsibility" : { origin: "five-ways-to-teach-children-the-importance-of-financial-responsibility", prefix: "insights"},
    "five-easy-ways-to-change-your-life-from-stressful-to-successful" : { origin: "five-easy-ways-to-change-your-life-from-stressful-to-successful", prefix: "insights"},
    "five-tips-for-preparing-for-unexpected-circumstances" : { origin: "five-tips-for-preparing-for-unexpected-circumstances", prefix: "insights"},
    "covid-19-benefits-at-a-glance" : { origin: "covid-19-benefits-at-a-glance", prefix: "insights"},
    "3-ways-to-better-prepare-ourselves-against-accidental-death" : { origin: "3-ways-to-better-prepare-ourselves-against-accidental-death", prefix: "insights"},
    "manulife-survey-1-out-of-3-critical-illness-patients-have-used-up-most-or-all-their-savings" : { origin: "manulife-survey-1-out-of-3-critical-illness-patients-have-used-up-most-or-all-their-savings", prefix: "insights"},
    "covid-19-special-s1million-support-fund-for-our-customers" : { origin: "covid-19-special-s1million-support-fund-for-our-customers", prefix: "insights"},
    "manulife-plan-coverage-covid-19" : { origin: "manulife-plan-coverage-covid-19", prefix: "insights"},
    "new-benefits-to-cover-you-for-covid-19" : { origin: "new-benefits-to-cover-you-for-covid-19", prefix: "insights"},
    "be-unbroken" : { origin: "be-unbroken", prefix: "insights"},
    "a-tale-of-gifts-and-greetings" : { origin: "a-tale-of-gifts-and-greetings", prefix: "insights"},
    "share-your-feels" : { origin: "share-your-feels", prefix: "insights"},
    "8-ways-to-spring-clean-your-finances-before-chinese-new-year" : { origin: "8-ways-to-spring-clean-your-finances-before-chinese-new-year", prefix: "insights"},
    "finding-your-financial-path" : { origin: "finding-your-financial-path", prefix: "insights"},
    "5-ways-to-stamp-out-stress-snacking-at-work" : { origin: "5-ways-to-stamp-out-stress-snacking-at-work", prefix: "insights"},
    "nine-most-important-things-to-consider-when-planning-for-2020" : { origin: "nine-most-important-things-to-consider-when-planning-for-2020", prefix: "insights"},
    "free-gifts-to-give-this-holiday-season" : { origin: "free-gifts-to-give-this-holiday-season", prefix: "insights"},
    "do-this-not-that" : { origin: "do-this-not-that", prefix: "insights"},
    "the-beginners-meditation-checklist" : { origin: "the-beginners-meditation-checklist", prefix: "insights"},
    "10-ways-to-reduce-stress" : { origin: "10-ways-to-reduce-stress", prefix: "insights"},
    "know-your-body" : { origin: "know-your-body", prefix: "insights"},
    "refocusing-your-everyday-worries" : { origin: "refocusing-your-everyday-worries", prefix: "insights"},
    "the-real-behind-faceapp" : { origin: "the-real-behind-faceapp", prefix: "insights"},
    "avoiding-assumptions-about-ci-insurance" : { origin: "avoiding-assumptions-about-ci-insurance", prefix: "insights"},
    "know-what-you-have" : { origin: "know-what-you-have", prefix: "insights"},    
    "financial-planning-skills" : { origin: "financial-planning-skills", prefix: "insights"},
    "how-to-inflation-proof-your-savings" : { origin: "how-to-inflation-proof-your-savings", prefix: "insights"},
    "self-care-guide-for-mothers" : { origin: "self-care-guide-for-mothers", prefix: "insights"},
    "why-should-retirement-planning-start-as-early-as-possible" : { origin: "why-should-retirement-planning-start-as-early-as-possible", prefix: "insights"},
    "how-to-save-money-in-3-unconventional-steps" : { origin: "how-to-save-money-in-3-unconventional-steps", prefix: "insights"},
    "4-unconventional-investments-that-can-make-your-life-better" : { origin: "4-unconventional-investments-that-can-make-your-life-better", prefix: "insights"},
    "fathers-day-special-financial-planning-tips-from-dads": { origin: "fathers-day-special-financial-planning-tips-from-dads", prefix: "insights"},
    "building-financial-discipline-in-singapore-your-4-step-guide": { origin: "building-financial-discipline-in-singapore-your-4-step-guide", prefix: "insights" },
    "why-gig-workers-should-focus-on-retirement-savings" : { origin: "why-gig-workers-should-focus-on-retirement-savings", prefix: "insights"},
    "getting-real-about-baby-blues" : { origin: "getting-real-about-baby-blues", prefix: "insights"},
    "wedding-planning-guide-a-reality-check" : { origin: "wedding-planning-guide-a-reality-check", prefix: "insights"},
    "am-i-saving-less-than-most-singaporeans" : { origin: "am-i-saving-less-than-most-singaporeans", prefix: "insights"},
    "automatic-vs-manual-transmission-turbo-boosting-your-pension" : { origin: "automatic-vs-manual-transmission-turbo-boosting-your-pension", prefix: "insights"},
    "beat-the-fine-print-in-insurance" : { origin: "beat-the-fine-print-in-insurance", prefix: "insights"},
    "can-i-afford-a-bigger-place" : { origin: "can-i-afford-a-bigger-place", prefix: "insights"},
    "financial-lessons-you-can-learn-from-watching-the-world-cup" : { origin: "financial-lessons-you-can-learn-from-watching-the-world-cup", prefix: "insights"},
    "financial-planner-advice" : { origin: "financial-planner-advice", prefix: "insights"},
    "financial-planning-101-guide" : { origin: "financial-planning-101-guide", prefix: "insights"},
    "giving-my-kids-the-best-shot-at-perusing-their-dreams" : { origin: "giving-my-kids-the-best-shot-at-perusing-their-dreams", prefix: "insights"},
    "having-financial-safeguard-when-critical-illness-strikes" : { origin: "having-financial-safeguard-when-critical-illness-strikes", prefix: "insights"},
    "how-do-millennials-approach-retirement" : { origin: "how-do-millennials-approach-retirement", prefix: "insights"},
    "how-the-gig-economy-is-shaping-the-future-of-work" : { origin: "how-the-gig-economy-is-shaping-the-future-of-work", prefix: "insights"},
    "planning-ahead-in-the-gig-economy" : { origin: "planning-ahead-in-the-gig-economy", prefix: "insights"},
    "how-to-bridge-your-critical-illness-protection-gap" : { origin: "how-to-bridge-your-critical-illness-protection-gap", prefix: "insights"},
    "i-dont-want-to-retire" : { origin: "i-dont-want-to-retire", prefix: "insights"},
    "is-leaving-money-in-a-regular-savings-account-a-bad-idea" : { origin: "is-leaving-money-in-a-regular-savings-account-a-bad-idea", prefix: "insights"},
    "keep-savings-accessible" : { origin: "keep-savings-accessible", prefix: "insights"},
    "planning-earlier-retirement" : { origin: "planning-earlier-retirement", prefix: "insights"},
    "starting-a-new-family-4-changes-consider-financial-planning" : { origin: "starting-a-new-family-4-changes-consider-financial-planning", prefix: "insights"},
    "the-5-steps-to-help-your-parents-plan-their-retirement" : { origin: "the-5-steps-to-help-your-parents-plan-their-retirement", prefix: "insights"},
    "the-5-things-to-know-before-meeting-your-financial-representative" : { origin: "the-5-things-to-know-before-meeting-your-financial-representative", prefix: "insights"},
    "the-5-unconventional-ways-to-protect-yourself-against-inflation" : { origin: "the-5-unconventional-ways-to-protect-yourself-against-inflation", prefix: "insights"},
    "the-6-factors-to-think-about-when-building-your-retirement-fund" : { origin: "the-6-factors-to-think-about-when-building-your-retirement-fund", prefix: "insights"},
    "the-6-steps-to-buying-your-first-home" : { origin: "the-6-steps-to-buying-your-first-home", prefix: "insights"},
    "5-everyday-items-that-cost-more-than-term-life-insurance" : { origin: "5-everyday-items-that-cost-more-than-term-life-insurance", prefix: "insights"},
    "6-retirement-planning-mistakes-to-avoid" : { origin: "6-retirement-planning-mistakes-to-avoid", prefix: "insights"},
    "what-should-i-do-with-the-bonus-i-just-received" : { origin: "what-should-i-do-with-the-bonus-i-just-received", prefix: "insights"},
    "your-personal-needs": { origin: "your-personal-needs", prefix: "services"},
    "your-business-needs": { origin: "your-business-needs", prefix: "services"},
    "family-protection": { origin: "family-protection", prefix: "your-personal-needs"},
    "services-health": { origin: "services-health", prefix: "your-personal-needs"},
    "child-education-funding": { origin: "child-education", prefix: "your-personal-needs"},
    "savings-and-investment": { origin: "savings-and-investment", prefix: "your-personal-needs"},
    "retirement-planning": { origin: "retirement-planning", prefix: "your-personal-needs"},
    "home-protection": { origin: "home-protection", prefix: "your-personal-needs"},
    "legacy-planning": { origin: "legacy-planning", prefix: "your-personal-needs"},
    "business-succession-planning": { origin: "business-succession-planning", prefix: "your-business-needs"},
    "employee-benefit-schemes": { origin: "employee-benefit-schemes", prefix: "your-business-needs"},
    "keyman-protection": { origin: "keyman-protection", prefix: "your-business-needs"},
    "credit-protection-planning": { origin: "credit-protection-planning", prefix: "your-business-needs"},
    "your-health-is-wealth": { origin: "your-health-is-wealth", prefix: "events"},
    "career-opportunity-program-2019": { origin: "career-opportunity-program-2019", prefix: "events"},
    "career-opportunity-program-2020": { origin: "career-opportunity-program-2020", prefix: "events"},
    "term-life": { origin: "term-life", prefix: "life"},
    "whole-life": { origin: "whole-life", prefix: "life"},
    "manu-protect-term": { origin: "manu-protect-term", prefix: "life"},
    "direct-manu-assure-term": { origin: "direct-manu-assure-term", prefix: "life"},
    "manu-protect-decreasing": { origin: "manu-protect-decreasing", prefix: "life"},
    "manulife-ready-income": { origin: "manulife-ready-income", prefix: "life"},
    "ready-lifeincome": { origin: "ready-lifeincome", prefix: "life"},
    "life-ready-plus": { origin: "life-ready-plus", prefix: "life"},
    "direct-manu-assure-life": { origin: "direct-manu-assure-life", prefix: "life"},
    "medical-and-hospitalisation": { origin: "medical-and-hospitalisation", prefix: "health"},
    "accident": { origin: "accident", prefix: "health"},
    "critical-illness": { origin: "critical-illness", prefix: "health"},
    "maternity": { origin: "maternity", prefix: "health"},
    "manu-care": { origin: "manu-care", prefix: "health"},
    "ready-protect": { origin: "ready-protect", prefix: "health"},
    "ready-mummy": { origin: "ready-mummy", prefix: "health"},
    "ready-complete-care": { origin: "ready-complete-care", prefix: "health"},
    "critical-selectcare": { origin: "critical-selectcare", prefix: "health"},
    "manulifespring": { origin: "manulifespring", prefix: "save"},
    "ready-builder": { origin: "ready-builder", prefix: "save"},
    "manulife-goal": { origin: "manulife-goal", prefix: "save"},
    "manulife-boost-x": { origin: "manulife-boost-x", prefix: "save"},
    "savings-plans": { origin: "savings-plans", prefix: "save"},
    "education": { origin: "education", prefix: "save"},
    "retirement": { origin: "retirement", prefix: "save"},
    "manu-wealth-secure": { origin: "manu-wealth-secure", prefix: "save"},
    "retire-ready-plus": { origin: "retire-ready-plus", prefix: "save"},
    "manulife-educate": { origin: "manulife-educate", prefix: "save"},
    "ready-payout-plus": { origin: "ready-payout-plus", prefix: "save"},
    "manuinvestduo": { origin: "manuinvestduo", prefix: "invest"},
    "invest-ready-wealth": { origin: "invest-ready-wealth", prefix: "invest"},
    "invest-ready-protect": { origin: "invest-ready-protect", prefix: "invest"},
    "manu-link-investor": { origin: "manu-link-investor", prefix: "invest"},
    "manu-link-enrich": { origin: "manu-link-enrich", prefix: "invest"},
    "heirloom": { origin: "heirloom", prefix: "signature"},
    "signature-life": { origin: "signature-life", prefix: "signature"},
    "signature-income": { origin: "signature-income", prefix: "signature"},
    "signature-indexed-universal-life": { origin: "signature-indexed-universal-life", prefix: "signature"},
    "global-medical": { origin: "global-medical", prefix: "signature"},
}



let siteMapBatchReplace = [];


gulp.task('rename-delete', function () {
    console.log('-----------siteMap', siteMap);

    Object.values(siteMap).map(function(key) {
        console.log('siteMapBatchReplace', siteMapBatchReplace);
        siteMapBatchReplace.push([`page="${key.origin}"`, `page="${key.prefix}/${key.origin}"`]);
        siteMapBatchReplace.push([`href="${key.origin}"`, `href="/${key.prefix}/${key.origin}"`]);
    });

    return gulp.src(['build-staging/html/**/*.html', 'build-production/html/**/*.html'], {base: "./"})
        .pipe(vinylPaths(del))
        .pipe(rename(function (path) {
            console.log('siteMap[path.basename]', siteMap[path.basename]);
            if( siteMap[path.basename] ){
                path.basename = siteMap[path.basename].prefix + "$#" + siteMap[path.basename].origin;
            }
            return path;
        }))
        .pipe(batchReplace(siteMapBatchReplace))
        .pipe(gulp.dest('.'))
});


// log file paths in the stream
gulp.task('log', function () {
    return gulp.src(['./pug/**/*.jade', './pug/**/*.pug', './pug/**/*.js'])
    .pipe(vinylPaths(function (paths) {
        console.log('Paths:', paths);
        return Promise.resolve();
    }));
});


gulp.task('delete2', function () {

    return new Promise(function (resolve, reject) {
        let vp = vinylPaths();

        gulp.src('build-staging/html/**/*.html', {base: "./"})
        .pipe(vp)
        .pipe(rename(function (path) {
            path.basename = path.basename.replace("insights", 'insights$#insights');
            return path;
        }))
        .pipe(gulp.dest('.'))
        .on('end', function () {
            del(vp.paths).then(resolve).catch(reject);
        });
    });
});


let replaceForNormalizeLinks = [
    ['../../../../../', '........'],
    ['../../../../', '........'],
    ['../../../', '........'],
    ['../../', '........'],
    ['../', '........'],
]

function fileClassification({file='', theme='', project='', language='', dataFile=''}) {
    consoleFunctionDestructing({file: file.path, theme, project, language, dataFile});
    let path = file.path.replace('/articles$#/', '');
    let pathArray = path.replace('/Users/', '').split('/');

    let pageDetailTheme = path.indexOf('/agent/') > 0 ? 'page-theme-agent' : 'page-theme-agency';
    let pageDetailProject = typeof project !== '' ? ' page-project-' + project : '';
    let pageDetailLang = 'page-lang-' + language;
    let pageDetailName = 'page-name-' + pathArray.slice(-1)[0].replace('.pug', '');
    let pageDetailType = '';

    let pathArrayLength = pathArray.length;
    let pathArrayPositionOfPages = pathArray.indexOf('pages') + 1;
    let thereIsFolderAfterPages = pathArrayPositionOfPages + 1 != pathArrayLength;
    if(thereIsFolderAfterPages){
        pageDetailType = 'page-type-' + pathArray[pathArrayPositionOfPages];
    }
    return {
        relativePath: path,
        relativeLang: language,
        relativePageDetails: pageDetailTheme + " " + pageDetailLang + " " + pageDetailName + " " + pageDetailType + " " + pageDetailProject,
    }
}

// function consoleFunctionDestructing({file='', theme='', project='', language='', dataFile='', realPath=''}) {
//     console.log('...arguments', arguments);
// }

function consoleFunctionDestructing() {
    console.log('...arguments', arguments);
}

function compileLocalPug({theme='', project='', language='', dataFile=''}) {
    return function () {
        let realPath = `${theme ? theme + "/" : ""}${project ? project + "/" : ""}${language ? language + "/" : ""}`;

        consoleFunctionDestructing({realPath, theme, project, language, dataFile})

        let thisData = require('./pug/all/' + realPath + 'data/data.js');
        let thisHandleBarsData = require('./pug/all/' + realPath + 'data/handleBarsData.js');

        if(dataFile) {
            thisData = dataFile;
        }

        return gulp.src('./pug/all/' + realPath + '**/*.pug')
            .pipe(plumber())
            .pipe(debug({title: '----compile-local ' + realPath + ':', minimal: true}))
            .pipe(dataPlugin(function (file) {
                return thisData
            }))
            .pipe(dataPlugin(function (file) {
                return fileClassification({file, theme, project, language, dataFile});
            }))
            .pipe(jade({
                pretty: true,
                basedir: 'pug'
            }))
            .pipe(handlebars(thisHandleBarsData))
            .pipe(flatten())
            .pipe(replace(language + "$#.html", 'index.html'))
            .pipe(replace(language + "$#", ''))
            .pipe(replace("/" + language + "/", ''))
            .pipe(rename(function (opt) {
                opt.basename = opt.basename.replace(language + "$#", '');
                return opt;
            }))
            .pipe(batchReplace(replaceForNormalizeLinks))
            .pipe(replace('........', '../'))
            .pipe(replace('href="/', 'href="'))
            .pipe(gulp.dest('./local/'));
    };
}

// gulp.task('compile-local-en', compileLocalPug('agent', 'en'));

gulp.task('compile-local-agency-general-en', compileLocalPug({theme:'agency', project: 'general', language:'en'}));



function compileStagingPug({theme='', project='', language='', dataFile=''}) {
    return function () {
        let realPath = `${theme ? theme + "/" : ""}${project ? project + "/" : ""}${language ? language + "/" : ""}`;
        consoleFunctionDestructing({realPath, theme, project, language, dataFile})

        let loadedDataFile = require(dataFile);

        return gulp.src('./pug/all/' + realPath + '**/*.pug')
            .pipe(plumber())
            .pipe(debug({title: '----compile-build-staging ' + realPath + ':', minimal: true}))
            .pipe(dataPlugin(function (file) {
                return loadedDataFile
            }))
            .pipe(dataPlugin(function (file) {
                return fileClassification({file, theme, project, language, dataFile});
            }))
            .pipe(jade({
                pretty: true,
                basedir: 'pug'
            }))
            .pipe(flatten())
            .pipe(batchReplace(replaceForNormalizeLinks))
            .pipe(replace('........', '../'))
            .pipe(replace('<link rel="stylesheet" href="../tools/css/tools.css"/>', ''))
            .pipe(replace('<script src="../tools/js/tools-bundle.js" type="text/javascript"></script>', ''))
            .pipe(replace('../', S3_BUCKET_STAGING_NO_HASH))
            .pipe(replace('content="index, follow"', 'content="noindex, nofollow"'))
            .pipe(gulp.dest('./build-staging/html/' + realPath));
    };
}

// gulp.task('compile-build-staging-agency-en', compileStagingPug('agency', 'en', DB.data.agency.general.en));
// gulp.task('compile-build-staging-agent-en', compileStagingPug('agent', 'en', DB.data.agent.general.en));
gulp.task('compile-build-staging-agency-general-general-en', compileStagingPug({theme:'agency', project: 'general', language:'en', dataFile: DB.data.agency.general.en}));
gulp.task('compile-build-staging-agency-general-ipaadvisory-en', compileStagingPug({theme:'agency', project: 'ipaadvisory', language:'en', dataFile: DB.data.agency.ipaadvisory.en}));
gulp.task('compile-build-staging-agent-general-en', compileStagingPug({theme:'agent', project: 'general', language:'en', dataFile: DB.data.agent.general.en}));
// gulp.task('compile-build-staging-agent-greenapple-en', compileStagingPug({theme:'agent', project: 'greenapple', language:'en', dataFile: DB.data.agent.greenapple.en}));




function compileProductionPug({theme='', project='', language='', dataFile=''}) {
    return function () {
        let realPath = `${theme ? theme + "/" : ""}${project ? project + "/" : ""}${language ? language + "/" : ""}`;
        consoleFunctionDestructing({realPath, theme, project, language, dataFile})

        let loadedDataFile = require(dataFile);

        return gulp.src('./pug/all/' + realPath + '**/*.pug')
            .pipe(plumber())
            .pipe(debug({title: '----compile-build-production ' + language + ':', minimal: true}))
            .pipe(dataPlugin(function (file) {
                return loadedDataFile
            }))
            .pipe(dataPlugin(function (file) {
                return fileClassification({file, theme, project, language, dataFile});
            }))
            .pipe(jade({
                basedir: 'pug'
            }))
            .pipe(flatten())
            .pipe(batchReplace(replaceForNormalizeLinks))
            .pipe(replace('........', '../../../../../'))
            .pipe(replace('<link rel="stylesheet" href="../../../../../tools/css/tools.css"/>', ''))
            .pipe(replace('<script src="../../../../../tools/js/tools-bundle.js" type="text/javascript"></script>', ''))
            .pipe(gulp.dest('./build-staging/html/' + realPath))
            .pipe(gulp.dest('./build-production/html/' + realPath))
            .pipe(replace('content="index, follow"', 'content="noindex, nofollow"'))
            .pipe(gulp.dest('./build-staging/html-no-index-no-follow/' + realPath))
            .pipe(gulp.dest('./build-production/html-no-index-no-follow/' + realPath))
    };
}

gulp.task('compile-build-production-agency-general-general-en', compileProductionPug({theme:'agency', project: 'general', language:'en', dataFile: DB.data.agency.general.en}));
gulp.task('compile-build-production-agency-general-ipaadvisory-en', compileProductionPug({theme:'agency', project: 'ipaadvisory', language:'en', dataFile: DB.data.agency.ipaadvisory.en}));
gulp.task('compile-build-production-agent-general-en', compileProductionPug({theme:'agent', project: 'general', language:'en', dataFile: DB.data.agent.general.en}));
// gulp.task('compile-build-production-agent-greenapple-en', compileProductionPug({theme:'agent', project: 'greenapple', language:'en', dataFile: DB.data.agent.greenapple.en}));

// ================================================ Pug
// ------------------------------------------------ Sass
gulp.task('sass-local', function (cb) {
    return gulp.src('./css/style.scss')
    .pipe(plumber())
    .pipe(debug({title: 'compile "sass-local:', minimal: true}))
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass({
        errLogToConsole: true,
        outputStyle: 'expanded'
    }))
    .pipe(autoprefixer({
        browsers: ['last 2 versions', 'safari 5', 'ie 11', 'opera 12.1', 'ios 6', 'android 4'],
        cascade: false
    }))
    .pipe(sourcemaps.write('/maps'))
    .pipe(gulp.dest('./css/'))
    .pipe(browserSync.stream());
});


gulp.task('sass-staging', function (cb) {
    gulp.src('./css/style.scss')
    .pipe(plumber())
    .pipe(debug({title: 'compile "sass-staging:', minimal: true}))
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass({
        errLogToConsole: true,
        outputStyle: 'expanded'
    }))
    .pipe(autoprefixer({
        browsers: ['last 2 versions', 'safari 5', 'ie 11', 'opera 12.1', 'ios 6', 'android 4'],
        cascade: false
    }))
    .pipe(sourcemaps.write('/maps'))
    .pipe(replace('../', S3_BUCKET_STAGING_NO_HASH))
    .pipe(replace('sourceMappingURL=', 'sourceMappingURL=' + S3_BUCKET_STAGING_NO_HASH + 'css/'))
    .pipe(gulp.dest('./build-staging/css/'));

    cb();
});

gulp.task('sass-production', function (cb) {
    return gulp.src('./css/style.scss')
    .pipe(plumber())
    .pipe(debug({title: '----sass production:', minimal: true}))
    .pipe(sassGlob())
    .pipe(sass())
    .pipe(autoprefixer({
        browsers: ['last 2 versions', 'safari 5', 'opera 12.1', 'ios 7', 'android 4'],
        cascade: false
    }))
    .pipe(cleanCSS({compatibility: 'ie11'}))
    .pipe(batchReplace(replaceForNormalizeLinks))
    .pipe(replace('........', '../../'))
    .pipe(gulp.dest('./build-staging/css/'))
    .pipe(gulp.dest('./build-production/css/'))

});
// ================================================ Sass
// ------------------------------------------------ Js
//Old version
// gulp.task('webpack-sourcemap', function (cb) {
//     return gulp.src('./js/main.js')
//     .pipe(plumber())
//     .pipe(webpack({
//         module: {
//             loaders: [
//                 {
//                     test: /\.js$/, exclude: /node_modules/, loader: "babel-loader", query: {
//                         presets: ['es2015']
//                     }
//                 }],
//         },
//         entry: './js/main.js', output: {
//             filename: './js/bundle.js'
//         }, devtool: 'inline-source-map'
//     }))
//     .pipe(gulp.dest('./'));
// });


gulp.task('webpack-sourcemap', function(cb) {
    return gulp.src('./js/main.js')
    .pipe(plumber())
    .pipe(webpackstream({
        entry: {
            app: './js/main.js',
        },
        output: {
            filename: 'bundle.js',
        },
        devtool: 'inline-source-map',
        module: {
            rules: [{
                test: /\.js$/, // include .js files
                exclude: /node_modules/, // exclude any and all files in the node_modules folder
                use: {
                    loader: 'babel-loader',
                    options: {
                        "presets": [
                            [
                                "@babel/preset-env",
                                {
                                    "targets": {"ie": "11"}
                                }
                            ]
                        ]
                    }
                }
            }],
        },
    }))
    .pipe(gulp.dest('./js/'));
});

gulp.task('webpack', function(cb) {
    return gulp.src('./js/main.js')
    .pipe(plumber())
    .pipe(webpackstream({
        entry: {
            app: './js/main.js',
        },
        output: {
            filename: 'bundle.js',
        },
        module: {
            rules: [{
                test: /\.js$/, // include .js files
                exclude: /node_modules/, // exclude any and all files in the node_modules folder
                use: {
                    loader: 'babel-loader',
                    options: {
                        "presets": [
                            [
                                "@babel/preset-env",
                            ]
                        ]
                    }
                }
            }],
        },
    }))
    .pipe(gulp.dest('./js/'));
});

gulp.task('webpack-sourcemap-st', function(cb) {
    return gulp.src('./js/main.js')
    .pipe(plumber())
    .pipe(webpackstream({
        entry: {
            app: './js/main.js',
        },
        output: {
            filename: 'bundle.js',
        },
        devtool: 'inline-source-map',
        module: {
            rules: [{
                test: /\.js$/, // include .js files
                exclude: /node_modules/, // exclude any and all files in the node_modules folder
                use: {
                    loader: 'babel-loader',
                    options: {
                        "presets": [
                            [
                                "@babel/preset-env",
                            ]
                        ]
                    }
                }
            }],
        },
    }))
    .pipe(gulp.dest('./js/'));
});


gulp.task('bundle-js-staging', (function (cb) {
    gulp.src('./js/bundle.js')
    .pipe(debug({title: '----bundle js staging:', minimal: true}))
    .pipe(replace('momentDateStampObject', moment().format("YYYY/MM/DD/ HH-mm-ss")))
    .pipe(gulp.dest('./build-staging/js/'));

    cb();
}));


gulp.task('bundle-js-production', (function (cb) {
    return gulp.src('./js/bundle.js')
    .pipe(debug({title: '----bundle-js-production:', minimal: true}))
    .pipe(replace('momentDateStampObject', moment().format("YYYY/MM/DD/ HH-mm-ss")))
    .pipe(uglify())
    .pipe(gulp.dest('./build-staging/js/'))
    .pipe(gulp.dest('./build-production/js/'));

}));
// ================================================ Js
// ------------------------------------------------ Hash + S3 for PRODUCTION

let hashSrcData = {
    build_dir: "",
    src_path: "",
    verbose: true,
    regex: /(href|src|srcset)\s*=\s*(?:(")([^"]*)|(')([^']*)|([^'"\s>]+))|url\s*\((?:(")([^"]+)|(')([^']+)|([^'"\)]+))/ig,
    exts: [".js", ".css", ".jpg", ".jpeg", ".png", ".gif", ".webp", ".svg", ".pdf", ".ico", ".ttf", ".woff", ".mp3", ".ogg", ".ogv", ".mp4", ".webm", ".zip", ".tar", ".gz", ".bz2", "mp4"]
}

gulp.task('hash-and-s3-html-staging', function (cb) {
    hashSrcData.build_dir = "./build-staging/html/*.html";
    hashSrcData.src_path = "./build-staging/html/*.html";

    return gulp.src('./build-staging/html/**/*.html')
    .pipe(plumber())
    .pipe(debug({title: '----hash and s3 html staging:', minimal: true}))
    .pipe(hash_src(hashSrcData))
    .pipe(replace('../../../../../', S3_BUCKET_STAGING))
    .pipe(version(versionConfig))
    .pipe(gulp.dest('./build-staging/html/'));
});

gulp.task('hash-and-s3-html-staging-no-index-no-follow', function (cb) {
    hashSrcData.build_dir = "./build-staging/html-no-index-no-follow/*.html";
    hashSrcData.src_path = "./build-staging/html-no-index-no-follow/*.html";

    return gulp.src('./build-staging/html-no-index-no-follow/**/*.html')
    .pipe(plumber())
    .pipe(debug({title: '----hash and s3 html staging:', minimal: true}))
    .pipe(hash_src(hashSrcData))
    .pipe(replace('../../../../../', S3_BUCKET_STAGING))
    .pipe(version(versionConfig))
    .pipe(gulp.dest('./build-staging/html-no-index-no-follow/'));
});


gulp.task('hash-and-s3-css-staging', function (cb) {
    hashSrcData.build_dir = "./build-staging/css/style.css";
    hashSrcData.src_path = "./build-staging/css/style.css";

    return gulp.src('./build-staging/css/style.css')
    .pipe(plumber())
    .pipe(debug({title: '----hash and s3 bundle.js staging:', minimal: true}))
    // .pipe(hash_src(hashSrcData))
    .pipe(replace('../../', S3_BUCKET_STAGING))
    .pipe(gulp.dest('./build-staging/css/'));
});

gulp.task('hash-and-s3-js-staging', function (cb) {
    hashSrcData.build_dir = "./build-staging/js/bundle.js";
    hashSrcData.src_path = "./build-staging/js/bundle.js";

    return gulp.src('./build-staging/js/bundle.js')
    .pipe(plumber())
    .pipe(debug({title: '----hash and s3 style.css staging:', minimal: true}))
    // .pipe(hash_src(hashSrcData))
    .pipe(replace('../../../../../', S3_BUCKET_STAGING))
    .pipe(gulp.dest('./build-staging/js/'));
});

gulp.task('hash-and-s3-html-production', function (cb) {
    hashSrcData.build_dir = "./build-production/html/*.html";
    hashSrcData.src_path = "./build-production/html/*.html";

    return gulp.src('./build-production/html/**/*.html')
    .pipe(plumber())
    .pipe(debug({title: '----hash and s3 html production:', minimal: true}))
    .pipe(hash_src(hashSrcData))
    .pipe(replace('../../../../../', S3_BUCKET_PRODUCTION))
    .pipe(version(versionConfig))
    .pipe(gulp.dest('./build-production/html/'));
});

gulp.task('hash-and-s3-html-production-no-index-no-follow', function (cb) {
    hashSrcData.build_dir = "./build-production/html-no-index-no-follow/*.html";
    hashSrcData.src_path = "./build-production/html-no-index-no-follow/*.html";

    return gulp.src('./build-production/html-no-index-no-follow/**/*.html')
    .pipe(plumber())
    .pipe(debug({title: '----hash and s3 html production:', minimal: true}))
    .pipe(hash_src(hashSrcData))
    .pipe(replace('../../../../../', S3_BUCKET_PRODUCTION))
    .pipe(version(versionConfig))
    .pipe(gulp.dest('./build-production/html-no-index-no-follow/'));
});

gulp.task('hash-and-s3-css-production', function (cb) {
    hashSrcData.build_dir = "./build-production/css/style.css";
    hashSrcData.src_path = "./build-production/css/style.css";
    return gulp.src('./build-production/css/style.css')
    .pipe(plumber())
    .pipe(debug({title: '----hash and s3 bundle.js production:', minimal: true}))
    // .pipe(hash_src(hashSrcData))
    .pipe(replace('../../', S3_BUCKET_PRODUCTION))
    .pipe(gulp.dest('./build-production/css/'));
});

gulp.task('hash-and-s3-js-production', function (cb) {
    hashSrcData.build_dir = "./build-production/js/bundle.js";
    hashSrcData.src_path = "./build-production/js/bundle.js";
    return gulp.src('./build-production/js/bundle.js')
    .pipe(plumber())
    .pipe(debug({title: '----hash and s3 style.css production:', minimal: true}))
    // .pipe(hash_src(hashSrcData))
    .pipe(replace('../../../../../', S3_BUCKET_PRODUCTION))
    .pipe(gulp.dest('./build-production/js/'));
});
// ================================================ Hash + S3 for PRODUCTION
// ------------------------------------------------ Move images, plugins, fonts
gulp.task('move-images-to-staging', (function (cb) {
    return gulp.src('./images/**/*')
    .pipe(debug({title: '----move images to staging:', minimal: true}))
    .pipe(gulp.dest('./build-staging/images'));

}));

gulp.task('move-css-plugins-to-staging', (function (cb) {
    return gulp.src('./css/plugins/**/*')
    .pipe(debug({title: '----move css plugins to staging:', minimal: true}))
    .pipe(gulp.dest('./build-staging/css/plugins'));

}));

gulp.task('move-js-plugins-to-staging', (function (cb) {
    return gulp.src('./js/plugins/**/*')
    .pipe(debug({title: '----move js plugins to staging:', minimal: true}))
    .pipe(gulp.dest('./build-staging/js/plugins'));

}));

gulp.task('move-fonts-to-staging', (function (cb) {
    return gulp.src('./fonts/**/*')
    .pipe(debug({title: '----move fonts to staging:', minimal: true}))
    .pipe(gulp.dest('./build-staging/fonts'));
}));

gulp.task('move-files-to-staging', (function (cb) {
    return gulp.src('./files/**/*')
    .pipe(debug({title: '----move files to staging:', minimal: true}))
    .pipe(gulp.dest('./build-staging/files'));
}));

gulp.task('move-images-to-production', (function (cb) {
    return gulp.src('./images/**/*')
    .pipe(debug({title: '----move images to production:', minimal: true}))
    .pipe(gulp.dest('./build-staging/images'))
    .pipe(gulp.dest('./build-production/images'));
}));

gulp.task('move-css-plugins-to-production', (function (cb) {
    return gulp.src('./css/plugins/**/*')
    .pipe(debug({title: '----move css plugins to production:', minimal: true}))
    .pipe(gulp.dest('./build-staging/css/plugins'))
    .pipe(gulp.dest('./build-production/css/plugins'));

}));

gulp.task('move-js-plugins-to-production', (function (cb) {
    return gulp.src('./js/plugins/**/*')
    .pipe(debug({title: '----move js plugins to production:', minimal: true}))
    .pipe(gulp.dest('./build-staging/js/plugins'))
    .pipe(gulp.dest('./build-production/js/plugins'));

}));

gulp.task('move-fonts-to-production', (function (cb) {
    return gulp.src('./fonts/**/*')
    .pipe(debug({title: '----move fonts to production:', minimal: true}))
    .pipe(gulp.dest('./build-staging/fonts'))
    .pipe(gulp.dest('./build-production/fonts'));
}));

gulp.task('move-files-to-production', (function (cb) {
    return gulp.src('./files/**/*')
    .pipe(debug({title: '----move files to production:', minimal: true}))
    .pipe(gulp.dest('./build-staging/files'))
    .pipe(gulp.dest('./build-production/files'));
}));

// ================================================ Move images, plugins, fonts
// ------------------------------------------------------------------------------------------------------ Watcher

function watchPug({theme='', project='', language='', dataFile='', handleBarsFile=''}) {
    let watchData = require(dataFile);
    let watchHandleBarsData = require(handleBarsFile);

    console.log('// ------------------------------------------------ DATA',dataFile.resolve);
    return function () {
        let realPath = `${theme ? theme + "/" : ""}${project ? project + "/" : ""}${language ? language + "/" : ""}`;
        console.log('theme', theme);
        console.log('project', project);
        console.log('language', language);
        console.log('dataFile', dataFile);
        console.log('realPath', realPath);

        return gulp.watch('./pug/all/' + realPath + '**/*.pug').on("change", function (file) {
            delete require.cache[require.resolve(dataFile)];
                watchData = require(dataFile);

                return gulp.src(file.path)
                    .pipe(plumber())
                    .pipe(debug({title: '----compile-local on change' + language + ':', minimal: true}))
                    .pipe(dataPlugin(function (file) {
                        return watchData
                    }))
                    .pipe(dataPlugin(function (file) {
                        return fileClassification({file, theme, project, language, dataFile});
                    }))
                    .pipe(jade({
                        pretty: true,
                        basedir: 'pug'
                    }))
                    .pipe(handlebars(watchHandleBarsData))
                    .pipe(flatten())
                    .pipe(replace(language + "$#.html", 'index.html'))
                    .pipe(replace(language + "$#", ''))
                    .pipe(replace("/" + language + "/", ''))
                    .pipe(batchReplace(replaceForNormalizeLinks))
                    .pipe(replace('........', '../'))
                    .pipe(replace('href="/"', 'href="/local/en"'))
                    .pipe(gulp.dest('./local/'))
                    .pipe(browserSync.stream());
        });

    };
}

// gulp.task('watch-pug-agency-general-en', watchPug('agency', 'en', DB.data.agency.general.en, DB.handlebar.agency.general.en));
gulp.task('watch-pug-agency-general-en', watchPug({theme:'agency', project: 'general', language:'en', dataFile: DB.data.agency.general.en, handleBarsFile: DB.handlebar.agency.general.en}));



//Add watchComponent
function watchComponent(language, projectPath) {
    return function () {
        var extraPath = projectPath || ''
        return gulp.watch('./pug/' + extraPath + 'components/**/*', ['sequence-compile-pug-local-' + language]);
    };
}

gulp.task('watch-component-en', watchComponent('en'));
gulp.task('watch-component-agent-all', watchComponent('en', 'all/agency/'));
gulp.task('watch-component-agent-en', watchComponent('en', 'all/agency/en/'));



//Add watchTemplate
function watchTemplate(language) {
    return function () {
        return gulp.watch('./pug/template/**/*.jade', ['sequence-compile-pug-local-' + language]);
    };
}

gulp.task('watch-template-en', watchTemplate('en'));




function watchData(language) {
    return function () {
        return gulp.watch(['pug/data/**/*.js','./pug/all/agency/' + language + '/data/**/*.js'], ['sequence-compile-pug-local-' + language]);
    };
}

gulp.task('watch-data-en', watchData('en'));


function watchHandelbars(language) {
    return function () {
        return gulp.watch('./pug/all/agency/' + language + '/data/handleBarsData.js', ['sequence-compile-pug-local-' + language]);
    };
}

gulp.task('watch-handelbars-en', watchHandelbars('en'));


gulp.task('watch-css-and-js', function () {
    gulp.watch('./js/*/**/*.js', ['sequence-watch-webpack-sourcemap']);
    gulp.watch('./js/main.js', ['sequence-watch-webpack-sourcemap']);
    gulp.watch('./css/**/*.scss', ['sass-local']);
});

// ====================================================================================================== Watcher
// ------------------------------------------------------------------------------------------------------ Sequence

gulp.task('sequence-js-st', function (cb) {
    gulpSequence('webpack-sourcemap', 'bundle-js-staging')(cb);
});

gulp.task('sequence-watch-webpack-sourcemap', function (cb) {
    gulpSequence('webpack-sourcemap', 'server-reload')(cb);
});

gulp.task('sequence-compile-pug-local-en', function (cb) {
    gulpSequence('cleanNodeCache', 'compile-local-agency-general-en', 'server-reload')(cb);
    });

gulp.task('sequence-compile-pug-local-agency-general-en', function (cb) {
    gulpSequence('cleanNodeCache', 'compile-local-agency-general-en', 'server-reload')(cb);
});

gulp.task('move-all-to-staging', function (cb) {
    gulpSequence('move-images-to-staging', 'move-css-plugins-to-staging', 'move-js-plugins-to-staging', 'move-fonts-to-staging', 'move-files-to-staging')(cb);
});

gulp.task('move-all-to-production', function (cb) {
    gulpSequence('move-images-to-production', 'move-css-plugins-to-production', 'move-js-plugins-to-production', 'move-fonts-to-production', 'move-files-to-production')(cb);
});

gulp.task('hash-and-s3-all', function (cb) {
    gulpSequence('hash-and-s3-html-production', 'hash-and-s3-html-production-no-index-no-follow', 'hash-and-s3-css-production', 'hash-and-s3-js-production')(cb);
});


// ====================================================================================================== Sequence
// ------------------------------------------------------------------------------------------------------ Main Tasks

gulp.task('default', gulpSequence(
    'clean:all',
    'webpack-sourcemap',
    'sass-local',
    'compile-local-agency-general-en',
    'watch-pug-agency-general-en',
    'watch-template-en',
    'watch-component-en',
    'watch-component-agent-all',
    'watch-component-agent-en',
    'watch-data-en',
    'watch-handelbars-en',
    'watch-css-and-js',
    'server',
    'build-notification',
    // 'slack-gulp'
));

//watch-pug-agency-general-en
//watch-pug-agency-greenapple-en
//watch-pug-agent-general-en
//watch-pug-agent-greenapple-en


gulp.task('st', gulpSequence(
    'clean:all',
    'cleanNodeCache',
    'webpack-sourcemap-st',
    'bundle-js-staging',
    'sass-staging',
    'compile-build-staging-agency-general-general-en',
    'compile-build-staging-agency-general-ipaadvisory-en',
    'compile-build-staging-agent-general-en',
    //'compile-build-staging-agent-greenapple-en',
    'timestamp-create-staging',
    'move-all-to-staging',
    'server-staging',
    'build-notification',
    'rename-delete'
    //'slack-gulp-st'
));

gulp.task('js', gulpSequence(
    'clean:all',
    'cleanNodeCache',
    'webpack',
    'bundle-js-production',
    'sass-local',
    'sass-production',
    'timestamp-create-production',
    'hash-and-s3-all',
    'move-all-to-production',
));

gulp.task('pt', gulpSequence(
    'clean:all',
    'cleanNodeCache',
    'webpack',
    'bundle-js-production',
    'sass-local',
    'sass-production',
    'compile-build-production-agency-general-general-en',
    'compile-build-production-agency-general-ipaadvisory-en',
    'compile-build-production-agent-general-en',
    //'compile-build-production-agent-greenapple-en',
    'timestamp-create-production',
    'hash-and-s3-all',
    'move-all-to-production',
    'build-notification',
    'rename-delete',
    // 'slack-gulp-pt'
));
//produciton minified without sourcemaps with hash

// ====================================================================================================== Main Tasks