const AWS = require('aws-sdk');

AWS.config.update({ region: 'ap-southeast-1' });
const ddb = new AWS.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    
    // create entryId based on timestamp for uniqueness
    const entryId = Date.now();
    console.log('Received event (', entryId, '): ', event);
    
    // get parameters from request
    const requestEvent = (typeof event == "string") ? JSON.parse(event) : event;
    const requestBody = (requestEvent && requestEvent.body)? requestEvent.body : {};
    const email = requestBody.email;
    const phone = requestBody.phone;
    console.log('Finding entry for email=', email, ' or phone=', phone, '.');
    
    // define table name based on environment
    // prod: http://xxxxxxxx.manulife.sg/make-sense-of-your-finances
    const prodDomain = "manulife.sg";
    const prodTableName = "ml-campaign2";
    var tableName = "ml-campaign2-staging"; // staging
    try { 
        var hostOrigin = (requestEvent && requestEvent.headers) ? requestEvent.headers.origin : "";
    	if (hostOrigin && hostOrigin.indexOf(prodDomain) > -1) {
    		tableName = prodTableName;
    	}
    	console.log('Finding entry from table=', tableName, ' for origin=', hostOrigin, '.');
    } catch (err) {
        console.log("Domain not found in headers.origin");    
    }
    
    // create filter params for scan
    var params = {};
    
    if (isEmpty(email) && isEmpty(phone) ) { returnResponse(400, 'param(s) cannot be empty', context.awsRequestId, callback) } 
    if (!isEmpty(email) && !isEmpty(phone)) {
        var params = {
            ExpressionAttributeValues: {
                ":v1": email,
                ":v2": phone
            },
            TableName: tableName,
            FilterExpression: "email = :v1 OR phone = :v2"
        };
    } else if (!isEmpty(email)) {
        var params = {
            ExpressionAttributeValues: {
                ":v1": email
            },
            TableName: tableName,
            FilterExpression: "email = :v1"
        };
    } else if (!isEmpty(phone)) {
        var params = {
            ExpressionAttributeValues: {
                ":v1": phone
            },
            TableName: tableName,
            FilterExpression: "phone = :v1"
        };
    }
    
    if (!isEmptyObject(params)) {
        ddb.scan(params, function(err, data) {
            if (err) {
                console.log("Error", err);
                errorResponse(err.message, context.awsRequestId, callback);
            }
            else {
                console.log("Scan results", data);
                if (data.Count > 0) {
                    returnResponse(409, "duplicated entry", context.awsRequestId, callback);
                }
                else {
                    recordEntry(entryId, email, phone, tableName).then(() => {
                        // You can use the callback function to provide a return value from your Node.js
                        // Lambda functions. The first parameter is used for failed invocations. The
                        // second parameter specifies the result data of the invocation.

                        // Because this Lambda function is called by an API Gateway proxy integration
                        // the result object must use the following structure.
                        returnResponse(200, "success", context.awsRequestId, callback);
                    }).catch((err) => {
                        console.error(err);

                        // If there is an error during processing, catch it and return
                        // from the Lambda function successfully. Specify a 500 HTTP status
                        // code and provide an error message in the body. This will provide a
                        // more meaningful error response to the end client.
                        errorResponse(err.message, context.awsRequestId, callback);
                    });
                }
            }
        });
    }
};

function isEmpty(value) {
    return (typeof value === 'undefined' || value == null || value.length === 0);
}

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function recordEntry(entryId, email, phone, tableName) {
    return ddb.put({
        TableName: tableName,
        Item: {
            entryId: entryId,
            email: email,
            phone: phone,
            requestTime: new Date().toISOString(),
        },
    }).promise();
}

function errorResponse(errorMessage, awsRequestId, callback) {
    callback(null, {
        statusCode: 500,
        body: JSON.stringify({
            Error: errorMessage,
            Reference: awsRequestId,
            Message: "error"
        }),
        //headers: {
        //    'Access-Control-Allow-Origin': '*',
        //},
    });
}

function returnResponse(statusCode, message, awsRequestId, callback) {
    callback(null, {
        statusCode: statusCode,
        body: JSON.stringify({
            Reference: awsRequestId,
            Message: message
        }),
        //headers: {
        //    'Access-Control-Allow-Origin': '*',
        //},
    });
}