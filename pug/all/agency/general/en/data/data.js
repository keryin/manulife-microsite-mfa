let data = require(__base + 'pug/data/data.js');
console.log('agency/en/data.js');

//for making changes you must modify data, not overwrite it
data.insurance_guides_index.box1 =  {
    title: "Unclaimed Proceeds List",
    context: "Search from a list of unclaimed proceeds from LIA website.",
    link: "https://www.lia.org.sg/consumers/unclaimed-proceeds/list"
};


module.exports = data;
