var data = {
    companyName: '[companyName]',
    phones: {
        WORK: "[0731876868]",
        MOBILE: "081-213-814-382",
        HOME_FAX: "02976582748",
        HOME: "4324982304",

    },
    fullAddress: "2200 Western Avenue, Suite 200 Seattle, WA 98121",
    address: "tel aviv",
    jobTitle: '[jobTitle]',
    emailAddress: "nirbenyasuperlongemailfortest@gmail.com",
    parentBUUser: {
        imageUrl: "../images/general/user-image.jpg",
        firstName: '[firstName]',
        lastName: '[lastName]',
        externalSysId: 'ABX200034457'
    },

    businessUnitStreet: "2200 Western Avenue, Suite 200",
    businessUnitZipCode: "98121",
    businessUnitCity: "Seattle, WA",


    social: {
        Facebook: "//www.facebook.com",
        Instagram: "//www.instagram.com",
        Twitter: "//www.twitter.com",
        LinkedIn: "//www.linkedin.com",
        GooglePlus: "//www.googleplus.com",
        Youtube: '//www.youtube.com'
    },
    currentBu: {
        businessUnit:{
            BranchName: "Branch Name",
            address: "2200 Western Avenue, Suite 200"
        }
    },
    openingHours: {
        SUNDAY: [
            {
                fromHour: "08:00",
                toHour: '10:00'
            },
            {
                fromHour: "14:00",
                toHour: '18:00'
            }
        ],
        MONDAY: [
            {
                fromHour: "08:00",
                toHour: '10:00'
            },
            {
                fromHour: "14:00",
                toHour: '18:00'
            }
        ],
        TUESDAY: [
            {
                fromHour: "08:00",
                toHour: '10:00'
            },
            {
                fromHour: "14:00",
                toHour: '18:00'
            }
        ],
        WEDNESDAY: [
            {
                fromHour: "08:00",
                toHour: '10:00'
            },
            {
                fromHour: "14:00",
                toHour: '18:00'
            }
        ],
        THURSDAY: [
            {
                fromHour: "08:00",
                toHour: '10:00'
            },
            {
                fromHour: "14:00",
                toHour: '18:00'
            }
        ],
        FRIDAY: [
            {
                fromHour: "08:00",
                toHour: '10:00'
            },
            {
                fromHour: "14:00",
                toHour: '18:00'
            }
        ],
        SATURDAY: [
            {
                fromHour: "08:00",
                toHour: '10:00'
            },
            {
                fromHour: "14:00",
                toHour: '18:00'
            }
        ],
    },
    members: [
        {
            imageUrl: "../images/who_we_are/member.jpg",
            jobTitle: "Chief Customer Officer & Regional Head of Bancassurance Marketing",
            firstName: "Maayan",
            lastName: "Savir",
            email: [{address: "nirbenya@gmail.com"}, {address: "nirbenya@gmail.com"}],
            mobilePhone: "081-213-814-382"
        }

    ]
};

module.exports = data;