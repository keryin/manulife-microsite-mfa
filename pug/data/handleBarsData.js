let data = {
    websiteMainUser: {
        displayName: "Manulife Agency",
        firstName: "Yali",
        lastName: "Fishman",
        jobTitle: "Novia's Agent",
        workPhone: "0352791288",
        mobilePhone: "0352791288",
        emails: "demo@demo.demo"
    },

    currentBu: {
        businessUnit: {
            BranchName: "Branch Name",
            address: 'Geelong Wealth Centre: 61 Leather Street, Breakwater, Victoria 3219.',
            facebookAddress: 'https://google.com',
            twitterAddress: 'https://google.com',
        },
        users: [
            {imageUrl: 'https://placekitten.com/635/300', oriasNumber: "1", firstName: 'Kitten', lastName: 'Catson', jobTitle: 'Main Cat', facebookAddress: 'facebookAddress', linkedinAddress: 'linkedinAddress', emails: ['email-1'], sirenNumber: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus tortor facilisis ex elementum euismod. Curabitur commodo mattis sapien, eget laoreet nunc auctor in. Etiam nec est cursus, ullamcorper dolor in, commodo risus. Cras sodales lacus mi, vel accumsan libero finibus id."},
            {imageUrl: 'https://placekitten.com/400/200', firstName: 'Oscar', lastName: 'MousensteinB', jobTitle: 'Food quality tester', facebookAddress: 'facebookAddress'},
            {imageUrl: 'https://placekitten.com/200/400', oriasNumber: "3", firstName: 'Kitten', lastName: 'McKitten', jobTitle: 'Junior Cat', linkedinAddress: 'linkedinAddress', emails: ['email-1'], sirenNumber: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus tortor facilisis ex elementum euismod. Curabitur commodo mattis sapien, eget laoreet nunc auctor in. Etiam nec est cursus, ullamcorper dolor in, commodo risus. Cras sodales lacus mi, vel accumsan libero finibus id."},
            {imageUrl: 'https://placekitten.com/400/200', firstName: 'Oscar', lastName: 'MousensteinC', jobTitle: 'Food quality tester', facebookAddress: 'facebookAddress'},
            {imageUrl: 'https://placekitten.com/400/400', oriasNumber: "2", firstName: 'Tomas', lastName: 'Fishbites', jobTitle: 'Human Relations Manager', facebookAddress: 'facebookAddress', linkedinAddress: 'linkedinAddress', emails: ['email-1']},
            {imageUrl: 'https://placekitten.com/775/500', oriasNumber: "2", firstName: 'Katty', lastName: 'Catson', jobTitle: '', sirenNumber: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus tortor facilisis ex elementum euismod. Curabitur commodo mattis sapien, eget laoreet nunc auctor in. Etiam nec est cursus, ullamcorper dolor in, commodo risus. Cras sodales lacus mi, vel accumsan libero finibus id."},
            {imageUrl: 'https://placekitten.com/400/200', firstName: 'Oscar', lastName: 'MousensteinA', jobTitle: 'Food quality tester', facebookAddress: 'facebookAddress'},
        ]

    },
    documents: [
        {name: 'a_somename.pdf', path: 'https://s3-eu-west-1.amazonaws.com/studio.gf.staging/files-placeholders/document.pdf'},
        {name: 'a_othername.pdf', path: 'https://s3-eu-west-1.amazonaws.com/studio.gf.staging/files-placeholders/document.pdf'},
        {name: 'a_othername two.pdf', path: 'https://s3-eu-west-1.amazonaws.com/studio.gf.staging/files-placeholders/document.pdf'},
        {name: 'b_somename.pdf', path: 'https://s3-eu-west-1.amazonaws.com/studio.gf.staging/files-placeholders/document.pdf'},
        {name: 'b_othername.pdf', path: 'https://s3-eu-west-1.amazonaws.com/studio.gf.staging/files-placeholders/document.pdf'},
        {name: 'b_othername.pdf', path: 'https://s3-eu-west-1.amazonaws.com/studio.gf.staging/files-placeholders/document.pdf'},
        {name: 'b_othername.pdf', path: 'https://s3-eu-west-1.amazonaws.com/studio.gf.staging/files-placeholders/document.pdf'},
        {name: 'b_othername.pdf', path: 'https://s3-eu-west-1.amazonaws.com/studio.gf.staging/files-placeholders/document.pdf'},
    ],
    businessUnitStyle: {
        colorPalette: {
            primary: "#0f1034",
            accents: {
                color1: "#29bfbd",
                color2: "orange",
                color3: "slateblue",
                color4: "yellow",
            },

        },
        colorPalette: {
            primary: "rgba(256,0,0,0.70)",
            accents: {
                color1: "rgba(0,256,0,0.70)",
                color2: "rgba(0,0,256,0.70)",
                color3: "orange",
                color4: "white",
            },

        },
        logos: {
            web: "https://dummyimage.com/160x100/00ff00/ffffff"
        }
    },
};

module.exports = data;