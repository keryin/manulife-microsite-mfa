console.log('data.js');

var data = {

    testMethodOtherServices: function() {
        console.log('data------------------------------------------------ \n\n', data);
        console.log('data.services------------------------------------------------ \n\n', data.services);
        console.log('data.services.retirement------------------------------------------------ \n\n', data.services.retirement);
        console.log('data.services.keys------------------------------------------------ \n\n', data.services.keys);
        return data.services.signature;
    },

    servicesBoxes: {
        yourpersonalneeds: {
            title: "Your Personal Needs",
            content: "Protect yourself and provide for your loved ones.",
            image: "../images/services/Personal-Needs.jpg",
            href: 'your-personal-needs'
        },
        yourbusinessneeds: {
            title: "Your Business Needs",
            content: "Reap and enjoy the fruits of your labour.",
            image: "../images/services/Business-Needs.jpg",
            href: 'your-business-needs'
        },
        servicescategory: [
            {
                title: "Manulife Signature",
                content: "Well-protection by a premium suite of insurance solutions catering to your unique needs.",
                image: "../images/box/products/landing-signature.jpg",
                href: 'signature'
            },
            {
                title: "Life Insurance",
                content: "Provide for your loved ones in times of need.",
                image: "../images/box/products/landing-life.jpg",
                href: 'life'
            },
            {
                title: "Health Protection",
                content: "Ensures you have financial support when you need medical and hospital care.",
                image: "../images/box/products/landing-health.jpg",
                href: 'health'
            },
            {
                title: "Disciplined Saving",
                content: "Helps plan your finances for your dreams to come true.",
                image: "../images/box/products/landing-save.jpg",
                href: 'save'
            },
            {
                title: "Investment Plans",
                content: "Helps capitalise on investment opportunities and put you on the path to achieving your financial goals",
                image: "../images/box/products/landing-invest.jpg",
                href: 'invest'
            },
        ],
    },

    services: {
        yourpersonalneeds: [
            {
                title: "Health",
                content: "Guard against costly medical expenses and loss of income, should an illness strike.",
                image: "../images/services/Health.jpg",
                href: 'health'
            },
            {
                title: "Savings and Investment",
                content: "Make your money work harder for you with different savings and investment options.",
                image: "../images/services/Save-Investment.jpg",
                href: 'savings-and-investment'
            },
            {
                title: "Family Protection",
                content: "Plan ahead for life’s unexpected events, so you and your family can enjoy peace of mind.",
                image: "../images/services/Family-Protection.jpg",
                href: 'family-protection'
            },
            {
                title: "Children’s Education Funding",
                content: "Pave the way for your children’s future by taking care of their tertiary education funding.",
                image: "../images/services/Child-Education.jpg",
                href: 'child-education-funding'
            },
            {
                title: "Home Protection",
                content: "Protect this important lifetime asset in the event of unfortunate circumstances.",
                image: "../images/services/Home-Protection.jpg",
                href: 'home-protection'
            },
            {
                title: "Retirement Planning",
                content: "Together, we develop a comprehensive retirement plan so you can enjoy your golden years.",
                image: "../images/services/Retirement-Planning.jpg",
                href: 'retirement-planning'
            },
            {
                title: "Legacy Planning",
                content: "Protect and distribute the wealth you have built, and ensure your loved ones are taken care of.",
                image: "../images/services/Legacy-Planning.jpg",
                href: 'legacy-planning'
            },
        ],

        yourbusinessneeds: [
            {
                title: "Business Succession Planning",
                content: "A comprehensive business plan ensures the continued success of your company.",
                image: "../images/services/Business-Succession-Planning.jpg",
                href: 'business-succession-planning'
            },
            {
                title: "Employee Benefit Schemes",
                content: "Take care of your team so you are able to attract and retain the best talent.",
                image: "../images/services/Employee-Benefit-Schemes.jpg",
                href: 'employee-benefit-schemes'
            },
            {
                title: "Keyman Protection",
                content: "Protect your business by cushioning the possible financial impact of losing a key executive.",
                image: "../images/services/Keyman-Protection.jpg",
                href: 'keyman-protection'
            },
            {
                title: "Credit Protection Planning",
                content: "Prevent disruptions to your business and facilitate a smooth transition during contingencies.",
                image: "../images/services/Credit-Protection-Planning.jpg",
                href: 'credit-protection-planning'
            },
        ],
        retirement: [
            {
                image: "../images/box/products/box-retire-ready-plus-ii.jpg",
                title: "RetireReady Plus II",
                context: "A retirement plan that offers you the option to receive Guaranteed Monthly Income for life.",
                href: "retire-ready-plus",
                list: [
                    "Receive up to an additional 100% of yourGuaranteed Monthly Income depending on the severity",
                    "Get a lump sum payout in the event of the life insured's retrenchment",
                    "In times of need, put your premium payment on hold while your policy stays in force"
                ]
            }
        ],
        investment: [
            {
                image: "../images/box/products/box-manuinvestduo.jpg",
                title: "ManuInvest Duo",
                context: "Accumulate your wealth while protecting your lifestyle",
                href: "manuinvestduo",
                list: [
                    "Get a boost to your investment with a higher Welcome Bonus when you choose a higher coverage and longer Minimum Investment Period",
                    "Choose your protection amount to up to 100x of your first year premium for death, terminal illness and total and permanent disability",
                    "100% of your regular basic premiums paid will be put to purchase fund units"
                ]
            },
            {
                image: "../images/box/products/box-invest-ready-wealth.jpg",
                title: "InvestReady Wealth (II)",
                context: "Over 100 funds available in 1 investment-linked plan",
                href: "invest-ready-wealth",
                list: [
                    "Get a head-start with a welcome bonus in the first policy year in the form of additional units",
                    "Enjoy yearly loyalty bonus after the end of your chosen Minimum Investment Period (MIP)",
                    "Build a portfolio that meets your financial goals with over 100 funds to pick from and invest in"
                ]
            },
            // {
            //     image: "../images/box/products/box-invest-ready-protect.jpg",
            //     title: "InvestReady Protect",
            //     context: "A whole-life regular-premium investment-linked plan that combines investments and protection",
            //     href: "invest-ready-protect",
            //     list: [
            //         "Get a head-start with a welcome bonus in the first policy year in the form of additional units",
            //         "Enjoy loyalty bonus every 5 years throughout the policy term",
            //         "Build a portfolio that meets your financial goals with over 100 funds to pick from and invest in"
            //     ]
            // },
            {
                image: "../images/box/products/box-manu-link-investor.jpg",
                title: "Manulink Investor",
                context: "A single premium investment-linked insurance plan with no insurance charges or policy fees",
                href: "manu-link-investor",
                customClass: "flex-col-xlg-6",
                list: [
                    "100% of your premium paid is put to purchase fund(s) units at the offer price",
                    "Choose to pay via cash or your Supplementary Retirement Scheme (SRS), or CPF-OA",
                    "Start from as low as S$5,000"
                ]
            },
            {
                image: "../images/box/products/box-manu-link-enrich.jpg",
                title: "Manulink Enrich",
                context: "A regular premium investment-linked insurance plan from S$100 a month",
                href: "manu-link-enrich",
                customClass: "flex-col-xlg-6",
                list: [
                    "100% of your regular basic premiums paid are put to purchase fund(s) units",
                    "Choose from 3 different plan options to suit your protection needs",
                    "Get invested from as low as S$100 per month"
                ]
            }
        ],
        insurance: [
            {
                image: "../images/box/products/box-service-1.jpg",
                title: "ManuProtect Term",
                context: "Receive a lump sum payout upon death or terminal illness with this term insurance plan",
                href: "manu-protect-term",
                list: [
                    "Flexibility in choosing your coverage term",
                    "Be covered against death and terminal illness",
                    "Automatic policy renewals till age 85, regardless of your health condition"
                ]
            },
            {
                title: "ManuProtect Decreasing",
                context: "A plan with decreasing coverage that allows you to choose your interest rate and policy term",
                href: "manu-protect-decreasing",
                list: [
                    "Choice of policy terms and interest rates",
                    "Fixed premiums on basic plan for your entire policy term",
                    "Additional protection with a selection of riders"
                ]
            },
            // {
            //     title: "Manulife ReadyIncome",
            //     context: "Receive a yearly income stream with this life insurance plan",
            //     href: "manulife-ready-income",
            //     list: [
            //         "Guaranteed yearly income from end of policy year 2 to age 99",
            //         "Freedom to transfer the policy to your child when he/she turns age 18",
            //         "Premium waiver upon total and permanent disability while your policy continues"
            //     ]
            // },
            {
                title: "Ready LifeIncome",
                context: "Provides you with annual payouts that last beyond one generation",
                href: "ready-lifeincome",
                list: [
                    "Enjoy guaranteed and non-guaranteed yearly income up to age 120",
                    "Get a premium waiver upon total and permanent disability",
                    "Allows you to provide a &ldquo;gift of love&rdquo; to your loved ones"
                ]
            },
            {
                title: "ManuCare",
                context: "Covers hospitalisation expenses in private and public hospitals in Singapore",
                href: "manu-care",
                list: [
                    "Reimburses your bill from the first dollar, with no co-insurance and deductibles",
                    "Option to select a plan that fully complements your needs: Executive, Deluxe, Premier or Elite",
                    "Renew every year up to age 74 with no health questions asked, and no medical evidence required"
                ]
            },
            {
                title: "ReadyProtect",
                context: "Accident plan for you and your family from S$2 a week",
                href: "ready-protect",
                list: [
                    "S$50,000 to S$1 million coverage for accidental death and dismemberment",
                    "Covers 21 infectious diseases, including Zika virus and hand, foot and mouth disease",
                    "2X payout if the accidental death and dismemberment happened on public transport"
                ]
            },
            {
                title: "Ready CompleteCare",
                context: "Arm yourself with a Critical Illness plan to cover you again and again",
                href: "ready-complete-care",
                list: [
                    "Covers all stages of Critical Illnesses for 106 conditions",
                    "Free basic health check every 2 years",
                    "Optional \"cover me again\" feature covers you again and again, up to 900%"
                ]
            },
        ],
        termlife: [
            {
                image: "../images/box/products/box-manu-protect-term.jpg",
                title: "ManuProtect Term",
                context: "Receive a lump sum payout upon death or terminal illness with this term insurance plan",
                href: "manu-protect-term",
                list: [
                    "Flexibility in choosing your coverage term",
                    "Be covered against death and terminal illness",
                    "Automatic policy renewals till age 85, regardless of your health condition"
                ]
            },
            {
                image: "../images/box/products/box-manu-protect-decreasing.jpg",
                title: "ManuProtect Decreasing",
                context: "A plan with decreasing coverage that allows you to choose your interest rate and policy term",
                href: "manu-protect-decreasing",
                list: [
                    "Choice of policy terms and interest rates",
                    "Fixed premiums on basic plan for your entire policy term",
                    "Additional protection with a selection of riders"
                ]
            }
        ],
        wholelife: [       
            {
                image: "../images/box/products/box-life-ready-plus.jpg",
                title: "LifeReady Plus",
                context: "Be covered at every stage of life",
                href: "life-ready-plus",
                list: [
                    "Be covered against death, terminal illness, and total and permanent disability, with an option to enchance your protection up to 5X",
                    "Enjoy upfront premium discounts for the first 2 policy years",
                    "Continue to enjoy the premium discounts after policy year 2, if you meet our health targets"
                ]
            },  
            // {
            //     image: "../images/box/products/box-manulife-ready-income.jpg",
            //     title: "Manulife ReadyIncome",
            //     context: "Receive a yearly income stream with this life insurance plan",
            //     href: "manulife-ready-income",
            //     list: [
            //         "Guaranteed yearly income from end of policy year 2 to age 99",
            //         "Freedom to transfer the policy to your child when he/she turns age 18",
            //         "Premium waiver upon total and permanent disability while your policy continues"
            //     ]
            // },
            {
                image: "../images/box/products/box-ready-lifeincome.jpg",
                title: "Ready LifeIncome",
                context: "Provides you with annual payouts that last beyond one generation",
                href: "ready-lifeincome",
                list: [
                    "Enjoy guaranteed and non-guaranteed yearly income up to age 120",
                    "Get a premium waiver upon total and permanent disability",
                    "Allows you to provide a &ldquo;gift of love&rdquo; to your loved ones"
                ]
            },
        ],
        medicalandhospitalisation: [
            {
                image: "../images/box/products/box-manu-care.jpg",
                title: "ManuCare",
                context: "Covers hospitalisation expenses in private and public hospitals in Singapore",
                href: "manu-care",
                list: [
                    "Reimburses your bill from the first dollar, with no co-insurance and deductibles",
                    "Option to select a plan that fully complements your needs: Executive, Deluxe, Premier or Elite",
                    "Renew every year up to age 74 with no health questions asked, and no medical evidence required"
                ]
            }
        ],
        accident: [
            {
                image: "../images/box/products/box-ready-protect.jpg",
                title: "ReadyProtect",
                context: "Accident plan for you and your family from S$2 a week",
                href: "ready-protect",
                list: [
                    "S$50,000 to S$1 million coverage for accidental death and dismemberment",
                    "Covers 21 infectious diseases, including Zika virus and hand, foot and mouth disease",
                    "2X payout if the accidental death and dismemberment happened on public transport"
                ]
            }
        ],
        criticalillness: [
            {
                image: "../images/box/products/box-ready-complete-care.jpg",
                title: "Ready CompleteCare",
                context: "Arm yourself with a Critical Illness plan to cover you again and again",
                href: "ready-complete-care",
                list: [
                    "Covers all stages of Critical Illnesses for 106 conditions",
                    "Free basic health check every 2 years",
                    "Optional \"cover me again\" feature covers you again and again, up to 900%"
                ]
            },
            {
                image: "../images/box/products/box-critical-selectcare.jpg",
                title: "Critical SelectCare",
                context: "Purchasing a protection plan after 40 years old can be difficult.",
                href: "critical-selectcare",
                list: [
                    "Be covered for selected critical illnesses and additional coverage for special conditions up to S$25,000 for each condition",
                    "Simply answer 3 questions, with no medical check-ups",
                    "This plan is available even to those with existing health conditions - such as high blood pressure, high blood sugar, high cholesterol or diabetes"
                ]
            },
        ],
        maternity: [
            {
                image: "../images/box/products/box-ready-mummy.jpg",
                title: "ReadyMummy",
                context: "The first maternity plan to cover mental wellness. Be ready with ReadyMummy.",
                href: "ready-mummy",
                list: [
                    "Covers the child against 24 congenital illnesses at 100% of the sum insured.",
                    "Offers the mother and child daily cash benefit at 1% of the sum insured for each day of hospital stay (up to 30 days).",
                    "Covers the mother for psychotherapy treatment at 10% of the sum insured."
                ]
            }
        ],
        savingsplans: [
            {
                image: "../images/box/products/box-manulifespring.jpg",
                title: "Manulife Spring",
                context: "12-year endowment plan with 100% capital return at policy maturity.",
                href: "manulifespring",
                list: [
                    "Get 100% of your capital return, upon policy maturity",
                    "Receive total potential return of up to 3.32% p.a. over 12 years",
                    "Use them to offset future premiums from end of 3rd or 6th policy year; or receive yearly payout for liquidity; or accumulate with Manulife at a non-guaranteed interest rate"
                ]
            },
            {
                image: "../images/box/products/box-ready-builder.jpg",
                title: "ReadyBuilder",
                context: "A safe and stable plan that allows you financial flexibility when you need it",
                href: "ready-builder",
                list: [
                    "Allows you to withdraw from cash value",
                    "Wealth accumulation continuity",
                    "Choice of premium periods to suit your needs"
                ]
            },
            {
                image: "../images/box/products/box-manulife-goal.jpg",
                title: "Manulife Goal 5",
                context: "Rev up the growth of your funds with Manulife Goal 5, as you zoom towards your life targets.",
                href: "manulife-goal",
                list: [
                    "Start from as low as S$10,000 via cash or your Supplementary Retirement Scheme (SRS)",
                    "Be covered against death at 101% of your single premium",
                    "Guaranteed acceptance with no health questions asked"
                ]
            },
            {
                image: "../images/box/products/box-manu-wealth-secure.jpg",
                title: "ManuWealth Secure",
                context: "Pay for 2 or 5 years and receive a stream of guaranteed yearly payouts",
                href: "manu-wealth-secure",
                customClass: "flex-col-xlg-6",
                list: [
                    "Enjoy guaranteed yearly payouts at 5% of your sum insured",
                    "Get 100% of your capital back upon policy maturity",
                    "Receive higher returns upon maturity through potential bonuses"
                ]
            },
            {
                image: "../images/box/products/box-ready-payout-plus.jpg",
                title: "ReadyPayout Plus",
                context: "Receive a guaranteed cash benefit every year with this endowment plan",
                href: "ready-payout-plus",
                list: [
                    "Receive guaranteed cash benefit that increases from 2% to 10% of your sum insured",
                    "Receive a lump sum maturity benefit at the end of your policy term",
                    "Premium waiver upon total and permanent disability"
                ]
            },
        ],
        education: [
            {
                image: "../images/box/products/box-manulife-educate.jpg",
                title: "Manulife Educate",
                context: "Receive a stream of guaranteed income payouts to see your child through university",
                href: "manulife-educate",
                list: [
                    "Receive 6 guaranteed yearly income payouts before and when your child reaches the chosen Payout Age",
                    "Choose between the Payout Age of 18 or 20 that best suits your child’s educational milestones",
                    "Your child is covered against death and terminal illness"
                ]
            }
        ],
        financial: [
            {
                image: "../images/box/products/box-manulifespring.jpg",
                title: "Manulife Spring",
                context: "12-year endowment plan with 100% capital return at policy maturity.",
                href: "manulifespring",
                list: [
                    "Get 100% of your capital return, upon policy maturity",
                    "Receive total potential return of up to 3.32% p.a. over 12 years",
                    "Use them to offset future premiums from end of 3rd or 6th policy year; or receive yearly payout for liquidity; or accumulate with Manulife at a non-guaranteed interest rate"
                ]
            },
            {
                image: "../images/box/products/box-ready-builder.jpg",
                title: "ReadyBuilder",
                context: "A safe and stable plan that allows you financial flexibility when you need it",
                href: "ready-builder",
                list: [
                    "Allows you to withdraw from cash value",
                    "Wealth accumulation continuity",
                    "Choice of premium periods to suit your needs"
                ]
            },
            {
                image: "../images/box/products/box-manulife-goal.jpg",
                title: "Manulife Goal 5",
                context: "Rev up the growth of your funds with Manulife Goal 5, as you zoom towards your life targets.",
                href: "manulife-goal",
                list: [
                    "Start from as low as S$10,000 via cash or your Supplementary Retirement Scheme (SRS)",
                    "Be covered against death at 101% of your single premium",
                    "Guaranteed acceptance with no health questions asked"
                ]
            },
            {
                image: "../images/box/products/box-manu-wealth-secure.jpg",
                title: "ManuWealth Secure",
                context: "Pay for 2 or 5 years and receive a stream of guaranteed yearly payouts",
                href: "manu-wealth-secure",
                customClass: "flex-col-xlg-6",
                list: [
                    "Enjoy guaranteed yearly payouts at 5% of your sum insured",
                    "Get 100% of your capital back upon policy maturity",
                    "Receive higher returns upon maturity through potential bonuses"
                ]
            },
            {
                image: "../images/box/products/box-ready-payout-plus.jpg",
                title: "ReadyPayout Plus",
                context: "Receive a guaranteed cash benefit every year with this endowment plan",
                href: "ready-payout-plus",
                list: [
                    "Receive guaranteed cash benefit that increases from 2% to 10% of your sum insured",
                    "Receive a lump sum maturity benefit at the end of your policy term",
                    "Premium waiver upon total and permanent disability"
                ]
            },
            {
                title: "InvestReady Wealth (II)",
                context: "Over 100 funds available in 1 investment-linked plan",
                href: "invest-ready-wealth",
                list: [
                    "Get a head-start with a welcome bonus in the first policy year in the form of additional units",
                    "Enjoy yearly loyalty bonus after the end of your chosen Minimum Investment Period (MIP)",
                    "Build a portfolio that meets your financial goals with over 100 funds to pick from and invest in"
                ]
            },
            // {
            //     title: "InvestReady Protect",
            //     context: "A whole-life regular-premium investment-linked plan that combines investments and protection",
            //     href: "invest-ready-protect",
            //     list: [
            //         "Get a head-start with a welcome bonus in the first policy year in the form of additional units",
            //         "Enjoy loyalty bonus every 5 years throughout the policy term",
            //         "Build a portfolio that meets your financial goals with over 100 funds to pick from and invest in"
            //     ]
            // },
            {
                image: "../images/box/products/box-manu-link-investor.jpg",
                title: "Manulink Investor",
                context: "A single premium investment-linked insurance plan with no insurance charges or policy fees",
                href: "manu-link-investor",
                list: [
                    "100% of your premium paid is put to purchase fund(s) units at the offer price",
                    "Choose to pay via cash or your Supplementary Retirement Scheme (SRS), or CPF-OA",
                    "Start from as low as S$5,000"
                ]
            },
            {
                title: "Manulink Enrich",
                context: "A regular premium investment-linked insurance plan from S$100 a month",
                href: "manu-link-enrich",
                list: [
                    "100% of your regular basic premiums paid are put to purchase fund(s) units",
                    "Choose from 3 different plan options to suit your protection needs",
                    "Get invested from as low as S$100 per month"
                ]
            }
        ],
        signature: [
            {
                image: "../images/box/products/box-signature-income-ii.jpg",
                title: "Signature Income (II)",
                context: "Receive a lifetime of monthly income with this single premium life insurance plan",
                href: "signature-income",
                customClass: "flex-col-xlg-6",
                list: [
                    "Enjoy a lifetime coverage on death and terminal illness, up to age 120",
                    "Enjoy guaranteed and non-guaranteed monthly income up to age 120",
                    "Guaranteed acceptance with no health questions asked"
                ]
            },
            {
                image: "../images/box/products/box-heirloom.jpg",
                title: "Heirloom (VI)",
                context: "Leave a lasting legacy to your loved ones with this USD universal life insurance plan",
                href: "heirloom",
                list: [
                    "High insurance cover with cash value accumulation",
                    "Helps with legacy planning and ensures business continuity",
                    "Provides estate liquidity and helps to diversify your portfolio"
                ]
            },
            {
                image: "../images/box/products/box-signature-life.jpg",
                title: "Signature Life",
                context: "You want stability in your legacy plan. We’ll give you more. Your trusted partner to grow and preserve your legacy.",
                href: "signature-life",
                customClass: "flex-col-xlg-6",
                list: [
                    "One-time payment for a lifetime coverage on death and terminal illness",
                    "Choice of your preferred currency Singapore Dollars or U.S. Dollars",
                    "Guaranteed cash value of at least 80% of your single premium from Day 1"
                ]
            },
            {
                image: "../images/box/products/box-signature-indexed-universal-life.jpg",
                title: "Signature Indexed Universal Life",
                context: "A stepping stone to accumulate wealth, leveraging on an Index Account.",
                href: "signature-indexed-universal-life",
                customClass: "flex-col-xlg-6",
                list: [
                    "Leaving a legacy for your loved ones",
                    "Each additional potential higher returns from the Index Account that is tied to reputable international indices",
                    "Flexible premium payment"
                ]
            },
        ],
    },


    articles: {
        retirement: [
            {
                image: "../images/box/box-article-8.jpg",
                title: "Plan your retirement early, Learn how much of a difference it makes",
                href: "planning-earlier-retirement",
                date: "16 Jan 2017",
                class: "card-article card-article-default",
                tag: "Retirement",
                tagData: "retirement",
            },
            {
                image: "../images/box/box-article-retire-early-need-plan-man-with-paper-laptop.jpg",
                title: "Can I save less if I don't retire?",
                href: "i-dont-want-to-retire",
                date: "17 January 2017",
                class: "card-article card-article-default",
                tag: "Retirement",
                tagData: "retirement",
            },
            {
                image: "../images/box/box-article-how-do-millennials-approach-retirement.jpg",
                title: "How do millennials approach retirement?",
                href: "how-do-millennials-approach-retirement",
                date: "19 January 2017",
                class: "card-article card-article-default",
                tag: "Retirement",
                tagData: "retirement",
            },
            {
                image: "../images/box/box-article-6.jpg",
                title: "Turbo boosting your retirement, Auto or Manual Transmission?",
                href: "automatic-vs-manual-transmission-turbo-boosting-your-pension",
                date: "21 January 2017",
                class: "card-article card-article-default",
                tag: "Retirement",
                tagData: "retirement",
            },
            {
                image: "../images/box/box-article-6-factors-retirement-plan.png",
                title: "6 factors to think about when building your retirement fund",
                href: "the-6-factors-to-think-about-when-building-your-retirement-fund",
                number: "6",
                date: "24 January 2017",
                class: "card-article card-article-color",
                tag: "Retirement",
                tagData: "retirement",
            },
            {
                image: "../images/box/box-article-5-steps-help-parents-retirement.png",
                title: "5 Steps to help your parents plan their retirement",
                href: "the-5-steps-to-help-your-parents-plan-their-retirement",
                number: "5",
                date: "25 January 2017",
                class: "card-article card-article-color",
                tag: "Retirement",
                tagData: "retirement",
            },
            {
                image: "../images/box/box-article-6-retirement-planning-mistakes-to-avoid.jpg",
                title: "6 retirement planning mistakes to avoid",
                href: "6-retirement-planning-mistakes-to-avoid",
                number: "6",
                date: "30 January 2017",
                class: "card-article card-article-color",
                tag: "Retirement",
                tagData: "retirement",
            },
            {
                image: "../images/box/box-article-why-should-retirement-planning-start-as-early-as-possible.jpg",
                title: "Why should retirement planning start as early as possible?",
                href: "why-should-retirement-planning-start-as-early-as-possible",
                date: "31 January 2017",
                class: "card-article card-article-default",
                tag: "Retirement",
                tagData: "retirement",
            },
            {
                image: "../images/box/box-article-why-gig-workers-should-focus-on-retirement-savings.jpg",
                title: "Why gig workers should focus on retirement savings",
                href: "why-gig-workers-should-focus-on-retirement-savings",
                date: "9 Febuary 2017",
                class: "card-article card-article-default",
                tag: "Retirement",
                tagData: "retirement",
            },
            {
                image: "../images/box/box-faceapp.jpg",
                title: "The Reality Behind FaceApp’s #AgeChallenge – Are You Ready to Embrace the Older You?",
                href: "the-real-behind-faceapp",
                date: "24 September 2019",
                class: "card-article card-article-default",
                tag: "Retirement",
                tagData: "retirement",
            },
        ],

        financial: [
            {
                image: "../images/box/box-article-financial-planner-advice.jpg",
                title: "The importance of a financial planner",
                href: "financial-planner-advice",
                date: "9 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-can-i-afford-a-bigger-place.jpg",
                title: "Can I afford a bigger place?",
                href: "can-i-afford-a-bigger-place",
                date: "10 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-is-leaving-money-in-a-regular-savings-account-a-bad-idea.jpg",
                title: "When you leave your money in a regular savings account...",
                href: "is-leaving-money-in-a-regular-savings-account-a-bad-idea",
                date: "11 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-5.jpg",
                title: "I want to start a family, does having children change my retirement plan?",
                href: "starting-a-new-family-4-changes-consider-financial-planning",
                date: "11 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-keep-savings-accessible.jpg",
                title: "How can I keep my savings accessible?",
                href: "keep-savings-accessible",
                date: "12 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-what-should-i-do-with-the-bonus-i-just-received.jpg",
                title: "What should I do with the bonus I just received?",
                href: "what-should-i-do-with-the-bonus-i-just-received",
                date: "13 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-am-i-saving-less-than-most-singaporeans.jpg",
                title: "Am I saving less than most Singaporeans?",
                href: "am-i-saving-less-than-most-singaporeans",
                date: "15 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-beat-the-fine-print-in-insurance.jpg",
                title: "Beat the fine print in insurance",
                href: "beat-the-fine-print-in-insurance",
                date: "18 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-buying-first-home-apartment.jpg",
                title: "6 steps to buying your first home",
                href: "the-6-steps-to-buying-your-first-home",
                number: "6",
                date: "20 January 2017",
                class: "card-article card-article-color",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-5-unconventional-ways-fight-inflation-1.jpg",
                title: "5 unconventional ways to protect yourself against inflation",
                href: "the-5-unconventional-ways-to-protect-yourself-against-inflation",
                number: "5",
                date: "22 January 2017",
                class: "card-article card-article-color",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-5-things-before-meet-financial.jpg",
                title: "5 things to know before meeting your Financial Representative",
                href: "the-5-things-to-know-before-meeting-your-financial-representative",
                number: "5",
                date: "23 January 2017",
                class: "card-article card-article-color",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-financial-planning-101-guide.jpg",
                title: "Financial Planning - a 101 guide",
                href: "financial-planning-101-guide",
                date: "26 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-how-to-inflation-proof-your-savings.jpg",
                title: "How to inflation proof your savings",
                href: "how-to-inflation-proof-your-savings",
                date: "27 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-self-care-guide-for-mothers.jpg",
                title: "Self-Care Guide for Mothers",
                href: "self-care-guide-for-mothers",
                date: "28 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-5-everyday-items-that-cost-more-than-term-life-insurance.jpg",
                title: "5 everyday items that cost more than term life insurance",
                href: "5-everyday-items-that-cost-more-than-term-life-insurance",
                date: "29 January 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-4-unconventional-investments-that-can-make-your-life-better.jpg",
                title: "4 unconventional investments that can make your life better",
                href: "4-unconventional-investments-that-can-make-your-life-better",
                date: "1 February 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-fathers-day-special-financial-planning-tips-from-dads.jpg",
                title: "Financial Planning Tips from Dads",
                href: "fathers-day-special-financial-planning-tips-from-dads",
                date: "2 February 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-building-financial-discipline-in-singapore-your-4-step-guide.jpg",
                title: "Your Guide to Building Financial Discipline",
                href: "building-financial-discipline-in-singapore-your-4-step-guide",
                date: "3 February 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-world-cup.jpg",
                title: "Financial Lessons You can Learn from Watching the World Cup",
                href: "financial-lessons-you-can-learn-from-watching-the-world-cup",
                date: "4 February 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-how-to-bridge-your-critical-illness-protection-gap.jpg",
                title: "How to bridge your Critical Illness protection gap",
                href: "how-to-bridge-your-critical-illness-protection-gap",
                date: "5 February 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-planning-ahead-in-the-gig-economy.jpg",
                title: "Planning Ahead in the Gig Economy",
                href: "planning-ahead-in-the-gig-economy",
                date: "6 February 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-how-the-gig-economy-is-shaping-the-future-of-work.jpg",
                title: "How the gig economy is shaping the future of work",
                href: "how-the-gig-economy-is-shaping-the-future-of-work",
                date: "7 February 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-having-financial-safeguard-when-critical-illness-strikes.jpg",
                title: "Having financial safeguard when critical illness strikes",
                href: "having-financial-safeguard-when-critical-illness-strikes",
                date: "8 February 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-wedding-planning-guide-a-reality-check.jpg",
                title: "Guide to Wedding Banquet Planning: A Reality Check",
                href: "wedding-planning-guide-a-reality-check",
                date: "10 Febuary 2017",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-financial-planning-skills.jpg",
                title: "3 Things to Consider When Levelling Up Your Financial Planning Skills",
                href: "financial-planning-skills",
                date: "21 April 2019",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-how-to-save-money-in-3-unconventional-steps.jpg",
                title: "How To Save Money in 3 Unconventional Steps",
                href: "how-to-save-money-in-3-unconventional-steps",
                date: "22 April 2019",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-getting-real-about-baby-blues.jpg",
                title: "Getting real about baby blues",
                href: "getting-real-about-baby-blues",
                date: "11 July 2019",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-article-know-what-you-have.jpg",
                title: "Start your Financial Planning right: Know what you have",
                href: "know-what-you-have",
                date: "26 August 2019",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-refocusing-your-everyday-worries.jpg",
                title: "Refocusing Your Everyday Worries",
                href: "refocusing-your-everyday-worries",
                date: "24 September 2019",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-nine-most-important-things-to-consider-when-planning-for-2020.jpg",
                title: "The Nine Most Important Things to Consider When Planning for 2020",
                href: "nine-most-important-things-to-consider-when-planning-for-2020",
                date: "30 December 2019",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-finding-your-financial-path.jpg",
                title: "Finding Your Financial Path in the First Thirty-One Days of the New Year",
                href: "finding-your-financial-path",
                date: "15 January 2020",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-spring-cleaning-before-chinese-new-year.jpg",
                title: "8 Ways to Spring Clean Your Finances before Chinese New Year",
                href: "8-ways-to-spring-clean-your-finances-before-chinese-new-year",
                date: "21 January 2020",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-tips-for-financial-security-from-inspirational-women.jpg",
                title: "Tips for Financial Security from Inspirational Women",
                href: "tips-for-financial-security-from-inspirational-women",
                date: "11 May 2020",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-investing-strategies-for-every-speed.jpg",
                title: "Investing Strategies for Every Speed",
                href: "investing-strategies-for-every-speed",
                date: "26 May 2020",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-see-how-the-savings-grow.jpg",
                title: "Fast or Slow, See How the Savings Grow",
                href: "see-how-the-savings-grow",
                date: "26 May 2020",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-easy-ways-to-identify-your-money-personality.png",
                title: "Easy Ways to Identify Your Money Personality",
                href: "easy-ways-to-identify-your-money-personality",
                date: "17 June 2020",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
            {
                image: "../images/box/box-simple-tips-for-creating-sustainable-financial-habits-for-successful-women.png",
                title: "Simple Tips for Creating Sustainable Financial Habits for Successful Women",
                href: "simple-tips-for-creating-sustainable-financial-habits-for-successful-women",
                date: "17 June 2020",
                class: "card-article card-article-default",
                tag: "Financial Planning",
                tagData: "financial",
            },
        ],
        education: [
            {
                image: "../images/box/box-article-giving-my-kids-the-best-shot-at-perusing-their-dreams.jpg",
                title: "Plan ahead for your child's future",
                href: "giving-my-kids-the-best-shot-at-perusing-their-dreams",
                date: "14 January 2017",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-article-avoiding-assumptions-about-ci-insurance.jpg",
                title: "Avoiding assumptions about Critical Illness (CI) insurance",
                href: "avoiding-assumptions-about-ci-insurance",
                date: "26 August 2019",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-know-your-body.jpg",
                title: "Managing Stress Starts With Knowing Your Body",
                href: "know-your-body",
                date: "8 October 2019",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-ways-to-reduce-stress.jpg",
                title: "10 Ways to Reduce Stress",
                href: "10-ways-to-reduce-stress",
                date: "24 October 2019",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },     
            {
                image: "../images/box/box-the-beginners-meditation-checklist.jpg",
                title: "The Beginner's Meditation Checklist",
                href: "the-beginners-meditation-checklist",
                date: "7 November 2019",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-do-this-not-that.jpg",
                title: "Do This Not That: 8 Ways to Avoid Wasting Time & Money This Holiday Season",
                href: "do-this-not-that",
                date: "3 December 2019",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-free-gifts-to-give-this-holiday-season.jpg",
                title: "Here’s Your Ten (Mostly) Free Gifts to Give This Holiday Season",
                href: "free-gifts-to-give-this-holiday-season",
                date: "17 December 2019",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-5-ways-to-stamp-out-stress-snacking-at-work.jpg",
                title: "5 Ways To Stamp Out Stress-Snacking At Work",
                href: "5-ways-to-stamp-out-stress-snacking-at-work",
                date: "13 January 2020",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-five-easy-ways-to-change-your-life.jpg",
                title: "Five Easy Ways to Change Your Life from Stressful to Successful",
                href: "five-easy-ways-to-change-your-life-from-stressful-to-successful",
                date: "26 March 2020",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-five-tips-for-preparing-for-unexpected-circumstances.jpg",
                title: "Are You Ready? Five Tips for Preparing for Unexpected Circumstances",
                href: "five-tips-for-preparing-for-unexpected-circumstances",
                date: "26 March 2020",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-3-ways-to-better-prepare-ourselves-against-accidental-death.jpg",
                title: "3 Ways To Better Prepare Ourselves Against Accidental Death",
                href: "3-ways-to-better-prepare-ourselves-against-accidental-death",
                date: "26 March 2020",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },      
            {
                image: "../images/box/box-covid-19-benefits-at-a-glance.jpg",
                title: "COVID-19 Benefits At A Glance",
                href: "covid-19-benefits-at-a-glance",
                date: "26 March 2020",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            }, 
            {
                image: "../images/box/box-five-ways-to-teach-children-financial-responsibility.jpg",
                title: "Five Ways to Teach Children the Importance of Financial Responsibility",
                href: "five-ways-to-teach-children-the-importance-of-financial-responsibility",
                date: "20 April 2020",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            },  
            {
                image: "../images/box/box-how-to-stay-active-and-healthy-while-at-home.jpg",
                title: "How to Stay Active and Healthy While at Home",
                href: "how-to-stay-active-and-healthy-while-at-home",
                date: "20 April 2020",
                class: "card-article card-article-default",
                tag: "Education",
                tagData: "education",
            }, 
            {
                image: "../images/box/box-critical-illness-insurance-for-elderly-parents.jpg",
                title: "How to Choose a Critical Illness Plan for Elderly Parents",
                href: "critical-illness-insurance-for-elderly-parents",
                date: "5 May 2020",
                class: "card-article card-article-color",
                tag: "Education",
                tagData: "education",
            }, 
            {
                image: "../images/box/box-is-critical-illness-insurance-necessary.jpg",
                title: "5 Reasons Why Skipping Critical Illness Insurance is a Big Mistake",
                href: "is-critical-illness-insurance-necessary",
                date: "5 May 2020",
                class: "card-article card-article-color",
                tag: "Education",
                tagData: "education",
            }, 
            {
                image: "../images/box/box-do-i-need-a-personal-accident-plan-if-i-work-in-an-office.jpeg",
                title: "Do I Need a Personal Accident Plan If I Work in an Office?",
                href: "do-i-need-a-personal-accident-plan-if-i-work-in-an-office",
                date: "26 May 2020",
                class: "card-article card-article-color",
                tag: "Education",
                tagData: "education",
            },
            {
                image: "../images/box/box-do-i-need-an-early-stage-critical-illness-plan.jpeg",
                title: "Do I Need an Early-Stage Critical Illness Plan?",
                href: "do-i-need-an-early-stage-critical-illness-plan",
                date: "26 May 2020",
                class: "card-article card-article-color",
                tag: "Education",
                tagData: "education",
            },
        ],        
        news: [         
            {
                image: "../images/box/news/coverage-covid-19.jpg",
                title: "Manulife Plan Coverage of COVID-19",
                href: "manulife-plan-coverage-covid-19",
                date: "10 February 2020",
                class: "card-article card-article-color",
                tag: "News",
                tagData: "news",
            },           
            {
                image: "../images/box/news/covid-19-support-fund.jpg",
                title: "COVID-19: Special S$1,000,000 support fund for our customers",
                href: "covid-19-special-s1million-support-fund-for-our-customers",
                date: "17 February 2020",
                class: "card-article card-article-color",
                tag: "News",
                tagData: "news",
            },           
            {
                image: "../images/box/news/manulife-survey-used-up-savings.jpg",
                title: "Manulife survey reveals that 1 out of 3 critical illness patients have used up most or all their savings",
                href: "manulife-survey-1-out-of-3-critical-illness-patients-have-used-up-most-or-all-their-savings",
                date: "17 February 2020",
                class: "card-article card-article-color",
                tag: "News",
                tagData: "news",
            },           
            {
                image: "../images/box/news/new-covid-benefits.jpg",
                title: "New Benefits To Cover You For Covid-19",
                href: "new-benefits-to-cover-you-for-covid-19",
                date: "12 May 2020",
                class: "card-article card-article-default",
                tag: "News",
                tagData: "news",
            },   
        ],
    },

    seminars: {
        seminarevent: [            
            {
                image: "../images/seminars/career-opportunity/career-opportunity-2020-box.jpg",
                title: "Career Opportunity Program",
                href: "career-opportunity-program-2020",
                content: "Unleash your full potential with Manulife Financial Adviser. Join us at Career Opportunity Program to find out more.",
                location: "Botanivo, 50 Cluny Park Road (level 2), Singapore Botanic Gardens, 257488",
                date: "20 February 2020, Thursday, 6:00PM",
                class: "card-seminars card-article-default",
                // class: "card-seminars card-article-default card-no-location card-new-notice card-no-link card-no-date card-title-hashtag",
            },
            {
                image: "../images/seminars/career-opportunity/career-opportunity-program-box.jpg",
                title: "Career Opportunity Program 2019",
                href: "career-opportunity-program-2019",
                content: "Be part of the best and brightest! Join us at MFA COP to find out more.",
                location: "Food for Thought 93 Stamford Rd, #01-04/05 National Museum of Singapore, Singapore 178897",
                date: "20 November 2019, Wednesday, 6:00PM",
                class: "card-seminars card-article-default",
                // class: "card-seminars card-article-default card-no-location card-new-notice card-no-link card-no-date card-title-hashtag",
            },
            // {
            //     image: "../images/seminars/Seminar-MostRewardingCareer.jpg",
            //     title: "#IFoundTheMostRewardingCareer",
            //     // href: "#",
            //     content: "How will you know when you’ve found yours? When you enjoy the journey (and not just the results)!",
            //     location: "Manulife Financial Advisers",
            //     date: "Wed, Feb 20, 7.00pm",
            //     class: "card-seminars card-article-default card-new-notice card-no-link card-title-hashtag",
            // },
            {
                image: "../images/seminars/img9-364x205.jpg",
                title: "Your health is wealth",
                href: "your-health-is-wealth",
                content: "So how can you protect both valuable assets? Find out in this free event!",
                location: "Amara Sanctuary Resort, Sentosa",
                date: "27 Jul 2019, Sat, 10am - 1pm",
                class: "card-seminars card-article-default",
            },
        ],
    },

    insurance_guides_index: {
        box1: {
            title: "Unclaimed Proceeds List",
            context: "Search from a list of unclaimed proceeds from LIA website.",
            link: "https://www.lia.org.sg/consumers/unclaimed-proceeds/list"
        },
        box2: {
            title: "Code of Life Insurance Practice (Eng)",
            context: "This guide aims to give you an idea of the practices used by the life insurance industry in Singapore. Life insurers, their representatives and representatives of financial advisers are operating with the stated guidelines, to ensure that the standard of practice within the industry is maintained for everyone concerned.",
            pdf: "https://www.manulife.com.sg/resources/LIA_COLIP_english_jan_2018.pdf"
        },
        box3: {
            title: "Code of Life Insurance Practice (Chn) - 人寿保险行为守则",
            context: "This guide aims to give you an idea of the practices used by the life insurance industry in Singapore. Life insurers, their representatives and representatives of financial advisers are operating with the stated guidelines, to ensure that the standard of practice within the industry is maintained for everyone concerned.",
            pdf: "https://www.manulife.com.sg/resources/LIA_COLIP_chinese_jan_2018.pdf"
        },
        box4: {
            title: "Guide to Life Insurance (Eng)",
            context: "This guide describes the various types of life insurance products and distribution channels and provides general information about life insurance. It gives you the basic information you need so you can discuss your insurance needs with a financial adviser (FA) representative.",
            pdf: "https://www.manulife.com.sg/resources/GI_Life_Insurance.pdf"
        },
        box5: {
            title: "Guide to Life Insurance (Chn) - 人寿保险指南",
            context: "This guide describes the various types of life insurance products and distribution channels and provides general information about life insurance. It gives you the basic information you need so you can discuss your insurance needs with a financial adviser (FA) representative.",
            pdf: "https://www.manulife.com.sg/resources/GI_Life_Insurance_Chinese.pdf"
        },
        box6: {
            title: "Accidental Death Coverage",
            context: "This document outlines the terms of the accidental death benefit that covers the life insured. The benefit applies to both new policy applications and reinstated policies.",
            pdf: "https://www.manulife.com.sg/resources/Accidental-Death-Coverage.pdf"
        },
        box7: {
            title: "Guide to Health Insurance (Eng)",
            context: "This guide provides general information on health insurance and the various types that may meet your needs. It gives you the information you should have before you buy any health insurance policy or discuss your needs with a financial adviser (FA) representative.",
            pdf: "https://www.manulife.com.sg/resources/GI_Health_Insurance_EN.pdf"
        },
        box8: {
            title: "Guide to Health Insurance (Chn) - 健康保险指南",
            context: "This guide provides general information on health insurance and the various types that may meet your needs. It gives you the information you should have before you buy any health insurance policy or discuss your needs with a financial adviser (FA) representative.",
            pdf: "https://www.manulife.com.sg/resources/GI_Health_Insurance_CN.pdf"
        },
        box9: {
            title: "Guide to Investment-Linked Insurance Plans",
            context: "This guide provides general information about what you should know before purchasing an investment-linked insurance plan – from key features and benefits and how they work to investment returns and fees and charges.",
            pdf: "https://www.manulife.com.sg/resources/GI_Investment_Linked_Insurance_Plan.pdf"
        },
        box10: {
            title: "Guide to Participating Policies (Eng)",
            context: "This guide provides you with general information about how a participating life insurance policy works. It gives you the information you should know before you purchase any participating policy or a \"with profits\" policy.",
            pdf: "https://www.manulife.com.sg/resources/Guide-to-Participating-Policies-2016-En.pdf"
        },
        box11: {
            title: "Guide to Participating Policies (Chn) - 分红保单指南",
            context: "This guide provides you with general information about how a participating life insurance policy works. It gives you the information you should know before you purchase any participating policy or a \"with profits\" policy.",
            pdf: "https://www.manulife.com.sg/resources/Guide-to-Participating-Policies-2016-Cn.pdf"
        },
        box12: {
            title: "Understanding Common Reporting Standard (CRS)",
            context: "This guide aims to help you understand the a set of new information and reporting requirements for financial institutions known as the Common Reporting Standard (\"CRS\").",
            link: "https://www.manulife.com.sg/common-reporting-standard.html"
        },
        box13: {
            title: "Understanding Bonus Determination in Manulife Participating Fund",
            context: "To help you understand our participating fund and how bonuses are determined, we are pleased to provide you with this supplementary guide. Please read in conjunction with \"Your Guide to Participating Policy\" published by the Life Insurance Association.",
            pdf: "https://www.manulife.com.sg/resources/GM_Understanding_Bonus_Determination.pdf"
        },
        box14: {
            title: "Guide to the Nomination of Insurance Nominees (Eng)",
            context: "This guide outlines how the Insurance Nomination Law, which came into effect on 1 September 2009, applies to various types of policies and the steps in making a trust or revocable nomination.",
            pdf: "https://www.manulife.com.sg/resources/GM_Nomination_of_Insurance_Nominees_2013_EN.pdf"
        },
        box15: {
            title: "Guide to the Nomination of Insurance Nominees (Chn) - 保险受益人提名指南",
            context: "This guide outlines how the Insurance Nomination Law, which came into effect on 1 September 2009, applies to various types of policies and the steps in making a trust or revocable nomination.",
            pdf: "https://www.manulife.com.sg/resources/GM_Nomination_of_Insurance_Nominees_2013_CN.pdf"
        },
        box16: {
            title: "Prepare for the \"What If's\" in Life (Eng)",
            context: "This brochure provides general information about life insurance protection and what the consumers should look out for.",
            pdf: "https://www.manulife.com.sg/resources/Prepare-for-the-What-ifs-in-Life_En.pdf"
        },
        box17: {
            title: "Prepare for the \"What If's\" in Life (Chn) - 为人生中的“不时之需”做好准备",
            context: "This brochure provides general information about life insurance protection and what the consumers should look out for.",
            pdf: "https://www.manulife.com.sg/resources/Prepare-for-the-What-ifs-in-Life_Cn.pdf"
        },
        box18: {
            title: "Guide to Policy Owners' Protection Scheme (Life Insurance) (Eng)\n",
            context: "The Policy Owners' Protection (\"PPF\") Scheme protects policy owners in the event a life or general insurer which is a PPF Scheme member fails. The PPF Scheme provides 100% protection for the guaranteed benefits of your life insurance policies up to the applicable caps. PPF Scheme is administered by the Singapore Deposit Insurance Corporation (\"SDIC\").",
            pdf: "https://www.manulife.com.sg/resources/GI_Policyholders_Protection_Scheme_EN.pdf"
        },
        box19: {
            title: "Guide to Policy Owners' Protection Scheme (Life Insurance) (Chn) - 保单持有人保障计划 (人寿保险)",
            context: "The Policy Owners' Protection (\"PPF\") Scheme protects policy owners in the event a life or general insurer which is a PPF Scheme member fails. The PPF Scheme provides 100% protection for the guaranteed benefits of your life insurance policies up to the applicable caps. PPF Scheme is administered by the Singapore Deposit Insurance Corporation (\"SDIC\").",
            pdf: "https://www.manulife.com.sg/resources/GI_Policyholders_Protection_Scheme_CN.pdf"
        },
        box20: {
            title: "Register of Insured Policies",
            context: "Manulife (Singapore) is a PPF Scheme member and all life insurance policies (including riders) issued by Manulife (Singapore) are covered under this Scheme. Click to access our Register of Insured Policies.",
            pdf: "https://www.manulife.com.sg/resources/Register_of_Insured_Policies.pdf"
        },
        box21: {
            title: "CPF reforms affecting CPFIS",
            context: "This document provide frequently asked questions on CPF Reforms that affects CPFIS.",
            pdf: "https://www.manulife.com.sg/resources/GF_FAQ_CPF_Reforms_Affecting_CPFIS.pdf"
        },
        box22: {
            title: "Participating Par Fund Report 2016",
            context: "Your participating policy shares in the profits and losses of the Participating Fund (“Par Fund”). We are pleased to provide you with your Par Fund Statement for 2016. In this statement, we share with you important information about the Par Fund's performance in 2016. We also share an overview of the asset allocation of the Par Fund as well as its future outlook.",
            pdf: "https://www.manulife.com.sg/resources/Par-Fund-Statement-2016.pdf"
        }
    }


};


module.exports = data;

delete require.cache[require.resolve(__base + 'pug/data/data.js')];