# MFA

Manulife Microsite for MFA (Manulife Financial Advisers) is a set of websites for Manulife managed by the Backoffice CMS. The code in this repository builds the set of pages to be deployed to Backoffice, as well as the assets to be deployed to AWS S3. This readme will show the reader how to build the websites and deploy the assets to AWS S3. For how to upload the pages to Backoffice, refer to the documentation below.

Backoffice (staging):
https://backoffice.manulife-fa.xyz/

Documentation on Backoffice: 
https://unionsg-my.sharepoint.com/:f:/g/personal/huijun_union_sg/EmA3fYyWTslFpqgkT1Xh4XYBcxFUm8K2iVC_WupQpMijzg?e=vdyl6g

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Install node package manager: https://nodejs.org/en/
Install node version manager (if using a different version of node): https://github.com/creationix/nvm
Install the correct version of node (if using a different version of node)
```
nvm install 10
```
Install gulp-cli globally:
```
npm install --global gulp-cli
```
For Window Installation refer:
```
https://www.taniarascia.com/how-to-install-and-use-node-js-and-npm-mac-and-windows/
```
Install AWS CLI (for staging/production): https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

Configure AWS CLI (for staging/production)
```
aws configure
```
### Installing

Install project dependecies:
```
npm install
```
Install versioning (Update versioning of css and js file when compile)
```
npm i gulp-version-number
```
Run gulp

To build the project locally:
```
nvm use 10 (if using a different version of node)
gulp
```
If fail to run node-sass use 
```
npm install node-sass@4.12.0
```

## Deployment
Update S3_BUCKET_PRODUCTION to the staging/production 
```
const S3_BUCKET_PRODUCTION = '//s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/staging/mls-mfa/';
```
To build the project for staging/projection:
```
gulp pt
```
*the author uses gulp pt (gulp production) for both production and staging because in a previous version of the code, gulp st (gulp staging) did not work correctly and the author has not tested gulp st subsequently

To upload the project to staging/production:

Upload the pages to Backoffice from the build-production/html folder (refer to Backoffice documentation above) 
Upload the assets to AWS S3 staging/production as appropriate:
```
cd build-production
aws s3 cp css s3://studio.gf.manulife.digital/manulife-website/staging/mls-mfa/css --recursive --acl public-read
aws s3 cp js s3://studio.gf.manulife.digital/manulife-website/staging/mls-mfa/js --recursive --acl public-read
aws s3 cp images s3://studio.gf.manulife.digital/manulife-website/staging/mls-mfa/images --recursive --acl public-read
aws s3 cp resources s3://studio.gf.manulife.digital/manulife-website/staging/mls-mfa/resources --recursive --acl public-read
```
You're done!

*be careful when uploading to production, as you will override existing production assets with the same name. versioning of the styles and scripts has not been implemented as of the time of writing 24/6/2020