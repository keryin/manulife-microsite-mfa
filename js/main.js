console.log('momentDateStampObject');
window.timeStampsAreForever = { js : 'momentDateStampObject' };
console.time('measure-time-main-js');

import './components/functions.js';
import './components/video-srcset.js';
//custom functions
import form_submission from './components/form-submission.js';
import isMobileUtility from './utilities/is-mobile';
import show_more from './components/show-more.js';
import fixedHtmlErr from './components/fix-titles';

import contactBox from './components/contact-box-functions.js';
import isBannerVisible from './components/check-banner-visible.js';
import datetimepicker from './components/date-time-picker.js';
import rating from './components/rating.js';
import solutionTab from './components/solution-tab.js';
import worldcup from './components/worldcup.js';
import lucky_draw_quiz from './components/lucky-draw-quiz.js';
import luckyDrawQuizForm from './components/lucky-draw-quiz-form.js';
import goforit from './components/goforit.js';
// import make_sense_of_your_finances from './components/make-sense-of-your-finances.js';
import share_your_feels from './components/share-your-feels.js';
import gift_and_greetings_form from './components/tale-of-gift.js';
import manulife_critical_illness from './components/manulife-critical-illness.js';
import campaign_form from './components/campaign-form.js';
import women_we_know_form from './components/women-we-know-form.js';
import contest_redemption from './components/redemption.js';
import tortoise_and_hare_form from './components/tortoise-and-hare-form.js';
import communityscircuitbuilder_form from './components/communityscircuitbuilder-form.js';

//elements

import googleMap from './components/google-map';
// import meetingForm from './components/meeting-form';
// import dateTimePicker from './components/date-time-picker';
// import chat from './components/chat';

$(document).ready(function () {
    // ------------------------------------------------ Variables
    const windowBreakpointMobileMaxWidth = 767;
    const windowBreakpointTabletMaxWidth = 991;
    let isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
    let weAreInsideIframe = window.self !== window.top;

    // fix for BO preview mode
    // href with '/' are needed for 2 levels of path (like in products and articles) but this break preview
    if(weAreInsideIframe){
        [...document.links].forEach(function(link) {
            // console.log('link', link.getAttribute("href"));
            if(link.getAttribute("href").charAt(0) === "/" && link.getAttribute("href").length > 1){
                link.setAttribute("href", link.getAttribute("href").substr(1));
            }
        });
    }

    let contactUserName = $(".box-user-name");
    let contactJobtitle = contactUserName.find("p");

    contactJobtitle.html(function (i, html) {
        return html.replace(/&nbsp;/g,' ');
    });

    //object-fit normilizer for IE11
    if (isIE11) {

        //add Object assign polyfill
        if (!Object.assign) {
            Object.defineProperty(Object, 'assign', {
                enumerable: false,
                configurable: true,
                writable: true,
                value: function(target) {
                    'use strict';
                    if (target === undefined || target === null) {
                        throw new TypeError('Cannot convert first argument to object');
                    }

                    var to = Object(target);
                    for (var i = 1; i < arguments.length; i++) {
                        var nextSource = arguments[i];
                        if (nextSource === undefined || nextSource === null) {
                            continue;
                        }
                        nextSource = Object(nextSource);

                        var keysArray = Object.keys(Object(nextSource));
                        for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
                            var nextKey = keysArray[nextIndex];
                            var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
                            if (desc !== undefined && desc.enumerable) {
                                to[nextKey] = nextSource[nextKey];
                            }
                        }
                    }
                    return to;
                }
            });
        }

        //append Promise polyfill
        !function(e,n){"object"==typeof exports&&"undefined"!=typeof module?n():"function"==typeof define&&define.amd?define(n):n()}(0,function(){"use strict";function e(e){var n=this.constructor;return this.then(function(t){return n.resolve(e()).then(function(){return t})},function(t){return n.resolve(e()).then(function(){return n.reject(t)})})}function n(){}function t(e){if(!(this instanceof t))throw new TypeError("Promises must be constructed via new");if("function"!=typeof e)throw new TypeError("not a function");this._state=0,this._handled=!1,this._value=undefined,this._deferreds=[],u(e,this)}function o(e,n){for(;3===e._state;)e=e._value;0!==e._state?(e._handled=!0,t._immediateFn(function(){var t=1===e._state?n.onFulfilled:n.onRejected;if(null!==t){var o;try{o=t(e._value)}catch(f){return void i(n.promise,f)}r(n.promise,o)}else(1===e._state?r:i)(n.promise,e._value)})):e._deferreds.push(n)}function r(e,n){try{if(n===e)throw new TypeError("A promise cannot be resolved with itself.");if(n&&("object"==typeof n||"function"==typeof n)){var o=n.then;if(n instanceof t)return e._state=3,e._value=n,void f(e);if("function"==typeof o)return void u(function(e,n){return function(){e.apply(n,arguments)}}(o,n),e)}e._state=1,e._value=n,f(e)}catch(r){i(e,r)}}function i(e,n){e._state=2,e._value=n,f(e)}function f(e){2===e._state&&0===e._deferreds.length&&t._immediateFn(function(){e._handled||t._unhandledRejectionFn(e._value)});for(var n=0,r=e._deferreds.length;r>n;n++)o(e,e._deferreds[n]);e._deferreds=null}function u(e,n){var t=!1;try{e(function(e){t||(t=!0,r(n,e))},function(e){t||(t=!0,i(n,e))})}catch(o){if(t)return;t=!0,i(n,o)}}var c=setTimeout;t.prototype["catch"]=function(e){return this.then(null,e)},t.prototype.then=function(e,t){var r=new this.constructor(n);return o(this,new function(e,n,t){this.onFulfilled="function"==typeof e?e:null,this.onRejected="function"==typeof n?n:null,this.promise=t}(e,t,r)),r},t.prototype["finally"]=e,t.all=function(e){return new t(function(n,t){function o(e,f){try{if(f&&("object"==typeof f||"function"==typeof f)){var u=f.then;if("function"==typeof u)return void u.call(f,function(n){o(e,n)},t)}r[e]=f,0==--i&&n(r)}catch(c){t(c)}}if(!e||"undefined"==typeof e.length)throw new TypeError("Promise.all accepts an array");var r=Array.prototype.slice.call(e);if(0===r.length)return n([]);for(var i=r.length,f=0;r.length>f;f++)o(f,r[f])})},t.resolve=function(e){return e&&"object"==typeof e&&e.constructor===t?e:new t(function(n){n(e)})},t.reject=function(e){return new t(function(n,t){t(e)})},t.race=function(e){return new t(function(n,t){for(var o=0,r=e.length;r>o;o++)e[o].then(n,t)})},t._immediateFn="function"==typeof setImmediate&&function(e){setImmediate(e)}||function(e){c(e,0)},t._unhandledRejectionFn=function(e){void 0!==console&&console&&console.warn("Possible Unhandled Promise Rejection:",e)};var l=function(){if("undefined"!=typeof self)return self;if("undefined"!=typeof window)return window;if("undefined"!=typeof global)return global;throw Error("unable to locate global object")}();"Promise"in l?l.Promise.prototype["finally"]||(l.Promise.prototype["finally"]=e):l.Promise=t});



        if($('.page-type-worldcup').length > 0){
            $('.cover-image-wrapper .property-visible-mobile').remove();
        }

        $(".hero__background video").addClass("ie-fix");

        $('.large-solution-container .card-image, .cover-image-wrapper, .card-gold .card-image, .card-full-image .card-image').each(function () {
            var $container = $(this),
                imgUrl = $container.find('img').prop('src');
            if (imgUrl) {
                $container
                .css('backgroundImage', 'url(' + imgUrl + ')')
                .addClass('block-object-fit')
                .find('img')
                .css('visibility','hidden');
            }
        });
    }
    fixedHtmlErr.init();

    if(window.CMS_RUNTIME) {
        window.chatAvailability.requestChatAvailability((data) =>{
            if (data) {
                //add some special online text
                console.log('status-user-is-online');
                $('.box-user-img').addClass('status-user-is-online');
                $('[open-chat]').attr('open-chat','');
                $('.text-user-accessible, .block-contact-chat .panel-theme-dark .panel-text').text("I'm available to chat now.");

            } else {
                console.log('user is offline');
                $('.text-user-accessible, .block-contact-chat .panel-theme-dark .panel-text').text("I am currently unavailable, please leave your details in the contact form or via chat and I will get back to you.");
                $('[open-chat]').addClass('status-disabled');
                $(document).on('click', '[open-chat]', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                });
            }
        });
    }

    let phoneCodeOptions = `<option data-countrycode="GB" title="+44" value="44">(+44) UK</option><option data-countrycode="US" title="+1" value="1">(+1) USA</option><option data-countrycode="DZ" title="+213" value="213">(+213) Algeria</option><option data-countrycode="AD" title="+376" value="376">(+376) Andorra</option><option data-countrycode="AO" title="+244" value="244">(+244) Angola</option><option data-countrycode="AI" title="+1264" value="1264">(+1264) Anguilla</option><option data-countrycode="AG" title="+1268" value="1268">(+1268) Antigua &amp; Barbuda</option><option data-countrycode="AR" title="+54" value="54">(+54) Argentina</option><option data-countrycode="AM" title="+374" value="374">(+374) Armenia</option><option data-countrycode="AW" title="+297" value="297">(+297) Aruba</option><option data-countrycode="AU" title="+61" value="61">(+61) Australia</option><option data-countrycode="AT" title="+43" value="43">(+43) Austria</option><option data-countrycode="AZ" title="+994" value="994">(+994) Azerbaijan</option><option data-countrycode="BS" title="+1242" value="1242">(+1242) Bahamas</option><option data-countrycode="BH" title="+973" value="973">(+973) Bahrain</option><option data-countrycode="BD" title="+880" value="880">(+880) Bangladesh</option><option data-countrycode="BB" title="+1246" value="1246">(+1246) Barbados</option><option data-countrycode="BY" title="+375" value="375">(+375) Belarus</option><option data-countrycode="BE" title="+32" value="32">(+32) Belgium</option><option data-countrycode="BZ" title="+501" value="501">(+501) Belize</option><option data-countrycode="BJ" title="+229" value="229">(+229) Benin</option><option data-countrycode="BM" title="+1441" value="1441">(+1441) Bermuda</option><option data-countrycode="BT" title="+975" value="975">(+975) Bhutan</option><option data-countrycode="BO" title="+591" value="591">(+591) Bolivia</option><option data-countrycode="BA" title="+387" value="387">(+387) Bosnia Herzegovina</option><option data-countrycode="BW" title="+267" value="267">(+267) Botswana</option><option data-countrycode="BR" title="+55" value="55">(+55) Brazil</option><option data-countrycode="BN" title="+673" value="673">(+673) Brunei</option><option data-countrycode="BG" title="+359" value="359">(+359) Bulgaria</option><option data-countrycode="BF" title="+226" value="226">(+226) Burkina Faso</option><option data-countrycode="BI" title="+257" value="257">(+257) Burundi</option><option data-countrycode="KH" title="+855" value="855">(+855) Cambodia</option><option data-countrycode="CM" title="+237" value="237">(+237) Cameroon</option><option data-countrycode="CA" title="+1" value="1">(+1) Canada</option><option data-countrycode="CV" title="+238" value="238">(+238) Cape Verde Islands</option><option data-countrycode="KY" title="+1345" value="1345">(+1345) Cayman Islands</option><option data-countrycode="CF" title="+236" value="236">(+236) Central African Republic</option><option data-countrycode="CL" title="+56" value="56">(+56) Chile</option><option data-countrycode="CN" title="+86" value="86">(+86) China</option><option data-countrycode="CO" title="+57" value="57">(+57) Colombia</option><option data-countrycode="KM" title="+269" value="269">(+269) Comoros</option><option data-countrycode="CG" title="+242" value="242">(+242) Congo</option><option data-countrycode="CK" title="+682" value="682">(+682) Cook Islands</option><option data-countrycode="CR" title="+506" value="506">(+506) Costa Rica</option><option data-countrycode="HR" title="+385" value="385">(+385) Croatia</option><option data-countrycode="CU" title="+53" value="53">(+53) Cuba</option><option data-countrycode="CY" title="+90392" value="90392">(+90392) Cyprus North</option><option data-countrycode="CY" title="+357" value="357">(+357) Cyprus South</option><option data-countrycode="CZ" title="+42" value="42">(+42) Czech Republic</option><option data-countrycode="DK" title="+45" value="45">(+45) Denmark</option><option data-countrycode="DJ" title="+253" value="253">(+253) Djibouti</option><option data-countrycode="DM" title="+1809" value="1809">(+1809) Dominica</option><option data-countrycode="DO" title="+1809" value="1809">(+1809) Dominican Republic</option><option data-countrycode="EC" title="+593" value="593">(+593) Ecuador</option><option data-countrycode="EG" title="+20" value="20">(+20) Egypt</option><option data-countrycode="SV" title="+503" value="503">(+503) El Salvador</option><option data-countrycode="GQ" title="+240" value="240">(+240) Equatorial Guinea</option><option data-countrycode="ER" title="+291" value="291">(+291) Eritrea</option><option data-countrycode="EE" title="+372" value="372">(+372) Estonia</option><option data-countrycode="ET" title="+251" value="251">(+251) Ethiopia</option><option data-countrycode="FK" title="+500" value="500">(+500) Falkland Islands</option><option data-countrycode="FO" title="+298" value="298">(+298) Faroe Islands</option><option data-countrycode="FJ" title="+679" value="679">(+679) Fiji</option><option data-countrycode="FI" title="+358" value="358">(+358) Finland</option><option data-countrycode="FR" title="+33" value="33">(+33) France</option><option data-countrycode="GF" title="+594" value="594">(+594) French Guiana</option><option data-countrycode="PF" title="+689" value="689">(+689) French Polynesia</option><option data-countrycode="GA" title="+241" value="241">(+241) Gabon</option><option data-countrycode="GM" title="+220" value="220">(+220) Gambia</option><option data-countrycode="GE" title="+7880" value="7880">(+7880) Georgia</option><option data-countrycode="DE" title="+49" value="49">(+49) Germany</option><option data-countrycode="GH" title="+233" value="233">(+233) Ghana</option><option data-countrycode="GI" title="+350" value="350">(+350) Gibraltar</option><option data-countrycode="GR" title="+30" value="30">(+30) Greece</option><option data-countrycode="GL" title="+299" value="299">(+299) Greenland</option><option data-countrycode="GD" title="+1473" value="1473">(+1473) Grenada</option><option data-countrycode="GP" title="+590" value="590">(+590) Guadeloupe</option><option data-countrycode="GU" title="+671" value="671">(+671) Guam</option><option data-countrycode="GT" title="+502" value="502">(+502) Guatemala</option><option data-countrycode="GN" title="+224" value="224">(+224) Guinea</option><option data-countrycode="GW" title="+245" value="245">(+245) Guinea - Bissau</option><option data-countrycode="GY" title="+592" value="592">(+592) Guyana</option><option data-countrycode="HT" title="+509" value="509">(+509) Haiti</option><option data-countrycode="HN" title="+504" value="504">(+504) Honduras</option><option data-countrycode="HK" title="+852" value="852">(+852) Hong Kong</option><option data-countrycode="HU" title="+36" value="36">(+36) Hungary</option><option data-countrycode="IS" title="+354" value="354">(+354) Iceland</option><option data-countrycode="IN" title="+91" value="91">(+91) India</option><option data-countrycode="ID" title="+62" value="62">(+62) Indonesia</option><option data-countrycode="IR" title="+98" value="98">(+98) Iran</option><option data-countrycode="IQ" title="+964" value="964">(+964) Iraq</option><option data-countrycode="IE" title="+353" value="353">(+353) Ireland</option><option data-countrycode="IL" title="+972" value="972">(+972) Israel</option><option data-countrycode="IT" title="+39" value="39">(+39) Italy</option><option data-countrycode="JM" title="+1876" value="1876">(+1876) Jamaica</option><option data-countrycode="JP" title="+81" value="81">(+81) Japan</option><option data-countrycode="JO" title="+962" value="962">(+962) Jordan</option><option data-countrycode="KZ" title="+7" value="7">(+7) Kazakhstan</option><option data-countrycode="KE" title="+254" value="254">(+254) Kenya</option><option data-countrycode="KI" title="+686" value="686">(+686) Kiribati</option><option data-countrycode="KP" title="+850" value="850">(+850) Korea North</option><option data-countrycode="KR" title="+82" value="82">(+82) Korea South</option><option data-countrycode="KW" title="+965" value="965">(+965) Kuwait</option><option data-countrycode="KG" title="+996" value="996">(+996) Kyrgyzstan</option><option data-countrycode="LA" title="+856" value="856">(+856) Laos</option><option data-countrycode="LV" title="+371" value="371">(+371) Latvia</option><option data-countrycode="LB" title="+961" value="961">(+961) Lebanon</option><option data-countrycode="LS" title="+266" value="266">(+266) Lesotho</option><option data-countrycode="LR" title="+231" value="231">(+231) Liberia</option><option data-countrycode="LY" title="+218" value="218">(+218) Libya</option><option data-countrycode="LI" title="+417" value="417">(+417) Liechtenstein</option><option data-countrycode="LT" title="+370" value="370">(+370) Lithuania</option><option data-countrycode="LU" title="+352" value="352">(+352) Luxembourg</option><option data-countrycode="MO" title="+853" value="853">(+853) Macao</option><option data-countrycode="MK" title="+389" value="389">(+389) Macedonia</option><option data-countrycode="MG" title="+261" value="261">(+261) Madagascar</option><option data-countrycode="MW" title="+265" value="265">(+265) Malawi</option><option data-countrycode="MY" title="+60" value="60">(+60) Malaysia</option><option data-countrycode="MV" title="+960" value="960">(+960) Maldives</option><option data-countrycode="ML" title="+223" value="223">(+223) Mali</option><option data-countrycode="MT" title="+356" value="356">(+356) Malta</option><option data-countrycode="MH" title="+692" value="692">(+692) Marshall Islands</option><option data-countrycode="MQ" title="+596" value="596">(+596) Martinique</option><option data-countrycode="MR" title="+222" value="222">(+222) Mauritania</option><option data-countrycode="YT" title="+269" value="269">(+269) Mayotte</option><option data-countrycode="MX" title="+52" value="52">(+52) Mexico</option><option data-countrycode="FM" title="+691" value="691">(+691) Micronesia</option><option data-countrycode="MD" title="+373" value="373">(+373) Moldova</option><option data-countrycode="MC" title="+377" value="377">(+377) Monaco</option><option data-countrycode="MN" title="+976" value="976">(+976) Mongolia</option><option data-countrycode="MS" title="+1664" value="1664">(+1664) Montserrat</option><option data-countrycode="MA" title="+212" value="212">(+212) Morocco</option><option data-countrycode="MZ" title="+258" value="258">(+258) Mozambique</option><option data-countrycode="MN" title="+95" value="95">(+95) Myanmar</option><option data-countrycode="NA" title="+264" value="264">(+264) Namibia</option><option data-countrycode="NR" title="+674" value="674">(+674) Nauru</option><option data-countrycode="NP" title="+977" value="977">(+977) Nepal</option><option data-countrycode="NL" title="+31" value="31">(+31) Netherlands</option><option data-countrycode="NC" title="+687" value="687">(+687) New Caledonia</option><option data-countrycode="NZ" title="+64" value="64">(+64) New Zealand</option><option data-countrycode="NI" title="+505" value="505">(+505) Nicaragua</option><option data-countrycode="NE" title="+227" value="227">(+227) Niger</option><option data-countrycode="NG" title="+234" value="234">(+234) Nigeria</option><option data-countrycode="NU" title="+683" value="683">(+683) Niue</option><option data-countrycode="NF" title="+672" value="672">(+672) Norfolk Islands</option><option data-countrycode="NP" title="+670" value="670">(+670) Northern Marianas</option><option data-countrycode="NO" title="+47" value="47">(+47) Norway</option><option data-countrycode="OM" title="+968" value="968">(+968) Oman</option><option data-countrycode="PW" title="+680" value="680">(+680) Palau</option><option data-countrycode="PA" title="+507" value="507">(+507) Panama</option><option data-countrycode="PG" title="+675" value="675">(+675) Papua New Guinea</option><option data-countrycode="PY" title="+595" value="595">(+595) Paraguay</option><option data-countrycode="PE" title="+51" value="51">(+51) Peru</option><option data-countrycode="PH" title="+63" value="63">(+63) Philippines</option><option data-countrycode="PL" title="+48" value="48">(+48) Poland</option><option data-countrycode="PT" title="+351" value="351">(+351) Portugal</option><option data-countrycode="PR" title="+1787" value="1787">(+1787) Puerto Rico</option><option data-countrycode="QA" title="+974" value="974">(+974) Qatar</option><option data-countrycode="RE" title="+262" value="262">(+262) Reunion</option><option data-countrycode="RO" title="+40" value="40">(+40) Romania</option><option data-countrycode="RU" title="+7" value="7">(+7) Russia</option><option data-countrycode="RW" title="+250" value="250">(+250) Rwanda</option><option data-countrycode="SM" title="+378" value="378">(+378) San Marino</option><option data-countrycode="ST" title="+239" value="239">(+239) Sao Tome &amp; Principe</option><option data-countrycode="SA" title="+966" value="966">(+966) Saudi Arabia</option><option data-countrycode="SN" title="+221" value="221">(+221) Senegal</option><option data-countrycode="CS" title="+381" value="381">(+381) Serbia</option><option data-countrycode="SC" title="+248" value="248">(+248) Seychelles</option><option data-countrycode="SL" title="+232" value="232">(+232) Sierra Leone</option><option data-countrycode="SG" title="+65" value="65" selected="">(+65) Singapore</option><option data-countrycode="SK" title="+421" value="421">(+421) Slovak Republic</option><option data-countrycode="SI" title="+386" value="386">(+386) Slovenia</option><option data-countrycode="SB" title="+677" value="677">(+677) Solomon Islands</option><option data-countrycode="SO" title="+252" value="252">(+252) Somalia</option><option data-countrycode="ZA" title="+27" value="27">(+27) South Africa</option><option data-countrycode="ES" title="+34" value="34">(+34) Spain</option><option data-countrycode="LK" title="+94" value="94">(+94) Sri Lanka</option><option data-countrycode="SH" title="+290" value="290">(+290) St. Helena</option><option data-countrycode="KN" title="+1869" value="1869">(+1869) St. Kitts</option><option data-countrycode="SC" title="+1758" value="1758">(+1758) St. Lucia</option><option data-countrycode="SD" title="+249" value="249">(+249) Sudan</option><option data-countrycode="SR" title="+597" value="597">(+597) Suriname</option><option data-countrycode="SZ" title="+268" value="268">(+268) Swaziland</option><option data-countrycode="SE" title="+46" value="46">(+46) Sweden</option><option data-countrycode="CH" title="+41" value="41">(+41) Switzerland</option><option data-countrycode="SI" title="+963" value="963">(+963) Syria</option><option data-countrycode="TW" title="+886" value="886">(+886) Taiwan</option><option data-countrycode="TJ" title="+7" value="7">(+7) Tajikstan</option><option data-countrycode="TH" title="+66" value="66">(+66) Thailand</option><option data-countrycode="TG" title="+228" value="228">(+228) Togo</option><option data-countrycode="TO" title="+676" value="676">(+676) Tonga</option><option data-countrycode="TT" title="+1868" value="1868">(+1868) Trinidad &amp; Tobago</option><option data-countrycode="TN" title="+216" value="216">(+216) Tunisia</option><option data-countrycode="TR" title="+90" value="90">(+90) Turkey</option><option data-countrycode="TM" title="+7" value="7">(+7) Turkmenistan</option><option data-countrycode="TM" title="+993" value="993">(+993) Turkmenistan</option><option data-countrycode="TC" title="+1649" value="1649">(+1649) Turks &amp; Caicos Islands</option><option data-countrycode="TV" title="+688" value="688">(+688) Tuvalu</option><option data-countrycode="UG" title="+256" value="256">(+256) Uganda</option><option data-countrycode="UA" title="+380" value="380">(+380) Ukraine</option><option data-countrycode="AE" title="+971" value="971">(+971) United Arab Emirates</option><option data-countrycode="UY" title="+598" value="598">(+598) Uruguay</option><option data-countrycode="UZ" title="+7" value="7">(+7) Uzbekistan</option><option data-countrycode="VU" title="+678" value="678">(+678) Vanuatu</option><option data-countrycode="VA" title="+379" value="379">(+379) Vatican City</option><option data-countrycode="VE" title="+58" value="58">(+58) Venezuela</option><option data-countrycode="VN" title="+84" value="84">(+84) Vietnam</option><option data-countrycode="VG" title="+84" value="84">(+1284) Virgin Islands - British</option><option data-countrycode="VI" title="+84" value="84">(+1340) Virgin Islands - US</option><option data-countrycode="WF" title="+681" value="681">(+681) Wallis &amp; Futuna</option><option data-countrycode="YE" title="+969" value="969">(+969) Yemen (North</option><option data-countrycode="YE" title="+967" value="967">(+967) Yemen (South</option><option data-countrycode="ZM" title="+260" value="260">(+260) Zambia</option><option data-countrycode="ZW" title="+263" value="263">(+263) Zimbabwe</option>`;
    console.log('$(".placeholder-for-phone-code-options")', $(".placeholder-for-phone-code-options"));

    if($('.selectpicker-phone-code').length > 0){
        $('.selectpicker-phone-code').append($(phoneCodeOptions));
        $('.selectpicker-phone-code').addClass('selectpicker');
        if((typeof $('.selectpicker-phone-code').selectpicker !== "undefined")) {
            $('.selectpicker-phone-code').selectpicker();
        }
    }
    // ------------------------------------------------ Enable bootstrap select

    form_submission.init();
    isMobileUtility.init();
    contactBox.init();
    isBannerVisible.init();
    if(window.self === window.top) {
        datetimepicker.init();
    }
    solutionTab.init();
    worldcup.init();
    lucky_draw_quiz.init();
    luckyDrawQuizForm.init();
    goforit.init();
    // make_sense_of_your_finances.init();
    share_your_feels.init();
    gift_and_greetings_form.init();
    manulife_critical_illness.init();
    campaign_form.init();
    women_we_know_form.init();
    contest_redemption.init();
    tortoise_and_hare_form.init();
    communityscircuitbuilder_form.init();

    if(window.isMobile.any()){
        $('html').addClass('page-device-mobile');

        //only for testing of carousel bug
        if(isMobile.iPhone()){
            $('html').addClass('page-device-mobile-ip');
            // var iHeight = window.screen.height;
            // var iWidth = window.screen.width;
            // if (iWidth === 375 && iHeight === 667) {
            //     $('html').addClass('page-device-mobile-ip');
            // }
            // else if (iWidth === 414 && iHeight === 736) {
            //     $('html').addClass('page-device-mobile-ip');
            // }
            // else if (iWidth === 320 && iHeight === 568) {
            //     $('html').addClass('page-device-mobile-ip');
            // }
        }
    }



    // ------------------------------------------------------------------------------------------------------ Carousel
    // ------------------------------------------------ Pagination was changed and now it is object, so I removed pagination null and add object

    function swiperConstructor(className, customOption) {
        let swiper = $(className);
        let swiperNav = swiper.parent(".carousel-slider");
        let swiperArrowLeft = swiperNav.find('.swiper-button-prev');
        let swiperArrowRight = swiperNav.find('.swiper-button-next');
        let swiperPagination = swiperNav.find('.swiper-pagination');
        let options = customOption || {};

        console.log('swiperPagination', swiperPagination);


        //configuration
        let swiperMaxSize = 5;


        let swiperSize = $(className).find('.swiper-slide').not('.status-hidden').length;
        console.log('swiperSize', swiperSize);
        //check status
        let swiperExist = $(className).length > 0;

        let swiperHaveMultipleChilds = $(className).find('.swiper-slide').length > 1;

        let swiperHaveNormalSize = swiperSize < swiperMaxSize;


        if(className == '.review-box-carousel-slider .swiper-rating-list'){
            swiperHaveNormalSize = false;
        }

        // To remove pagination in review-box carousel
        // if( className !== '.block-service-carousel' ){
        //     if(swiperHaveNormalSize){
        //         swiperArrowLeft.remove();
        //         swiperArrowRight.remove();
        //     } else {
        //         swiperPagination.remove()
        //     }
        // }






            let swiperConfiguration = {
                // touchEventsTarget: 'wrapper',
                simulateTouch: true,
                speed: 600,
                slidesPerView: 1,
                spaceBetween: 15,
                effect: 'slide',
                // Swiper card effect
                // effect: 'coverflow',
                // coverflowEffect: {
                //     rotate: 50,
                //     stretch: 0,
                //     depth: 100,
                //     modifier: 1,
                //     slideShadows : true,
                // },

                grabCursor: true,
                // setWrapperSize: true,
                // centeredSlides: true,
                // slidesPerView: 'auto',                

                onDestroy: function () {
                    $(this.pagination).html('');
                },
                onInit: function (swiper) {
                    // swiper.slideTo(0);
                    // swiper.setWrapperTranslate(0);
                },
                // onSlideChangeEnd: function (swiper) {}
            };

    //     if($('.page-device-mobile-ip').length > 0){
    // swiperConfiguration.effect = 'slide';
    //     }




        if(className == '.review-box-load-more .swiper-container' || className == '.review-box-carousel-slider .swiper-rating-list'){
            //swiperConfiguration.coverflowEffect.slideShadows = false;
        }

        if(className == '.review-box-carousel-slider .swiper-rating-list'){
            swiperConfiguration.slidesPerView = 1;
            swiperConfiguration.spaceBetween = 40;
        }



        if(className == '.block-service-carousel'){
            // swiperConfiguration.slidesPerView = 2;
            // swiperConfiguration.slidesPerGroup = 2;
            // swiperConfiguration.spaceBetween = 15;
            // swiperConfiguration.effect = 'slide';
        }






        if (swiperExist) {
            //Expand swiper functionality if swiper element exist on page
            console.log('swiper exist');


            if (swiperArrowLeft.length > 0) {
                swiperConfiguration.navigation = {};
                swiperConfiguration.navigation.prevEl = swiperArrowLeft;
            }

            if (swiperArrowRight.length > 0) {
                swiperConfiguration.navigation.nextEl = swiperArrowRight;
            }

            if (swiperPagination.length > 0) {
                swiperConfiguration.pagination = {
                    el : swiperPagination,
                    type: 'bullets',
                    clickable: 'true'
                };
            }



        }
        console.log('options', options);
        let combinedConfiguration = Object.assign({}, swiperConfiguration, options);


        if (swiperExist && swiperHaveMultipleChilds) {
            console.log('combinedConfiguration', combinedConfiguration);
            return new Swiper(swiper, combinedConfiguration);

        }
    }


    let anySwiperElement = $('.swiper-container');
    let swiperExistAndVisible = anySwiperElement && anySwiperElement.is(':visible');
    let thisIsMobileDevice = isMobile.any();
    let screenWidth = window.outerWidth;
    let thisIsMobileOrTabletBySize = screenWidth < windowBreakpointTabletMaxWidth;
    let swiperBannerTop = '';



    function destroyMainCarousel() {
        swiperBannerTop.destroy();
    }


    function createMainCarousel() {
        console.log('createMainCarousel');
        swiperBannerTop = swiperConstructor('.swiper-container');
    }

    function createRatingCarousel() {
        console.log('createRatingCarousel', createRatingCarousel);

        if($('.review-box-load-more').length > 0){
            //window.swiperBannerRating = swiperConstructor('.review-box-load-more .swiper-container');
            window.swiperBannerRating = swiperConstructor('.review-box-load-more .swiper-container', {
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: 'true'
                },
            });
        }
    }



    window.createCarouselFromRatingPage = function () {
        if (thisIsMobileDevice) {
            createRatingCarousel();
            console.log('create main carousel from rating')
        }

        if ($('.review-box-carousel-slider').length > 0 && swiperExistAndVisible) {
            $('.review-box-carousel-slider .swiper-rating-list').parent().addClass('status-force-enable-swiper');
            let swiperHomeRating = swiperConstructor('.review-box-carousel-slider .swiper-rating-list');
        }
    }


    if (thisIsMobileDevice && swiperExistAndVisible) {
        //disable for inedx and review page
        if($('.review-box-load-more').length == 0 && $('.review-box-carousel-slider').length == 0) {
            createMainCarousel();
            console.log('create main carousel')
        }
    }

    if($('.carousel-slider-container').length > 0) {
        console.log("create carousel slider");
        var swiperServices = swiperConstructor('.carousel-slider-container', {
            spaceBetween: 15,
            slidesPerView: 3,
            // Enable grab slider
            // touchRatio: 0,
            // grabCursor: false,
            slidesPerGroup: 1,
            coverflowEffect: {
                rotate: 0,
                depth: 0,
                slideShadows: false,
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    spaceBetween: 30,
                },
                991: {
                    slidesPerView: 2,
                    spaceBetween: 15,
                },
                1199: {
                    slidesPerView: 3,
                },
            }
        })
        
        setTimeout(function(){
            // Check if target has class .page-location-in-bo (Backoffice purpose)
            // If yes, destroy slider for editing purpose
            if($("html").hasClass("page-location-in-bo") && $(".awards-carousel").hasClass("status-force-enable-swiper")) {
                function destroyMainCarousel() {
                    swiperServices.destroy();
                };
                destroyMainCarousel();
                console.log("Destroy carousel slider in awards page for editing purpose");
            }
        }, 1000);
    }

    if( $('.review-box-carousel-slider').length > 0 ){

        console.log('// ------------------------------------------------------------------------------------------------------ DEMOCarousel');
        var swiperServices = swiperConstructor('.block-service-carousel', {
            spaceBetween: 15,
            slidesPerView: 4,
            slidesPerGroup: 4,
            coverflowEffect: {
                rotate: 0,
                depth: 0,
                slideShadows: false,
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    spaceBetween: 30,
                    slidesPerGroup: 1,
                },
                991: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                    spaceBetween: 15,
                },
                1199: {
                    slidesPerView: 3,
                    slidesPerGroup: 3,
                },
            }
        })

    }




    // //HOTFIX for footer
    // $('.copyright').insertBefore($('.footer-social'));
    // $('.footer-social').hide();


    // ------------------------------------------------ Accordion behavioiur



    // ------------------------------------------------ Events
    let $tagButtons = $('.button-tag');
    let $tagSidemenuLinks = $('.article-type-tag');
    let $articlesTags = $('.card-article');
    let $allarticleTags = $('.button-tag-all-select');

    let selectedTags = {
        alltopic: false,
        news: false,
        retirement: false,
        financial: false,
        education: false
    };

    console.log('selectedTags', selectedTags);

    function updateArticleTags(tag){
        console.log('selectedTags[tag] is', selectedTags[tag]);
        console.log('All selectedTags', selectedTags);
        console.log('// ------------------------------------------------ updateArticleTags', tag);


        selectedTags[tag] = selectedTags[tag] ? false : true;

        //update all buttons classes by selectedTags
        $tagButtons.each(function () {
            var thisElementTag = $(this).attr('data-tag');
            var thisElementSelected = selectedTags[thisElementTag];

            if(thisElementSelected){
                $(this).addClass('status-tag-selected');
            } else {
                $(this).removeClass('status-tag-selected');
            }
        })

        //update all sidemenu titles by selectedTags
        $tagSidemenuLinks.each(function () {
            var thisElementTag = $(this).attr('data-tag');
            var thisElementSelected = selectedTags[thisElementTag];

            if(thisElementSelected){
                $(this).addClass('status-tag-selected').parent().addClass('status-menu-active');
                $(this).next().slideDown( "slow", function() {});
            } else {
                $(this).removeClass('status-tag-selected').parent().removeClass('status-menu-active');
                $(this).next().slideUp( "slow", function() {});
            }
        });



        $articlesTags.each(function () {
            var thisElementTag = $(this).attr('data-tag');
            var thisElementSelected = selectedTags[thisElementTag];

            if(thisElementSelected){
                $(this).fadeIn( "slow", "swing", function() {});
            } else {
                $(this).fadeOut( "slow", "swing", function() {})
            }
        });

        $articlesTags.promise().done(function() {
            $('body').removeClass('status-unmovable');
        });
    }

    $(document).on('click', '.button-tag, .article-type-tag', function() {
        var thisTag = $(this).attr('data-tag');
        $('body').addClass('status-unmovable');
        $('.block-articles').removeClass('custom-show-less');
        $('.block-articles .show-more').hide();
        updateArticleTags(thisTag);

        if(thisTag === "alltopic"){        
            $tagButtons.not(this).addClass('status-tag-selected');
            $allarticleTags.removeClass('status-tag-selected');
            $articlesTags.fadeIn( "slow", "swing", function() {});
            $('.block-articles').addClass('custom-show-less');
            $('.block-articles .show-more').show();

            // Reset tag selected
            for(var i in selectedTags) {
                if (Object.hasOwnProperty.call(selectedTags, i)) {
                    selectedTags[i] = false;
                }
            }
            console.log('Updated All selectedTags', selectedTags);
        }

        //show-hide articles by tag
        //select sidemenu tag
        //show-hide related to tag links
    });

    if($('.block-articles').hasClass('custom-show-less')){
        let $showHideButton = $('.block-articles .show-more');
        let $blockArticlesList = $(".block-articles");
        let articlesIsHidden = true;

        function showArticles(){
            $showHideButton.text('Hide');
            $blockArticlesList.addClass('custom-show-more');
            articlesIsHidden = false;
        }
        function hideArticles(){
            $showHideButton.text('Show more');
            $blockArticlesList.removeClass('custom-show-more');
            articlesIsHidden = true;
        }

        $('.block-articles .show-more').click(function (e) {
            e.preventDefault();
            articlesIsHidden ? showArticles() : hideArticles();
        });
    };

    
// ================================================ Events

    //Click on careers page join us icon
    $(document).on('click', '#button-join-us', function() {
        openThisAccordion($('.section-our-openings').find('.block-accordion-item-title'), scrollToCareerForm);
    });

    function scrollToCareerForm(){
        scrollToElement($('#form-career-form'));
    }


    // ------------------------------------------------ Events

    // ------------------------------------------------------------------------------------------------------ REFACTOR THIS ACCORDION AND ASIDE SECTION
    $(document).on('click', '.block-accordion-item-title', function(event) {
        event.stopPropagation();
        if($(this).parent().hasClass('accordion-mobile-only')){
            if(window.isMobile.any()){
                if(thisAccordionIsOpen($(this))){
                    closeThisAccordion($(this));
                } else {
                    openThisAccordion($(this));
                }
            }
        } else {
            if(thisAccordionIsOpen($(this))){
                closeThisAccordion($(this));
            } else {
                openThisAccordion($(this));
            }
        }

    });

    function closeThisAccordion($target){
        console.log('down');

        $target.next().slideUp( "slow", function() {
            let thisAccordionItem = $target.parent();
            thisAccordionItem.addClass('status-closed');
        });
    }

    //closeAllAccordion
    function closeAllAccordion(){
        $('.accordion-item').filter(":not('.status-closed')").each(function () {
           $(this).find('.accordion-item-content').slideUp( "slow", function() {
               $(this).parent().addClass('status-closed');
           });
        });
    }

    function closeAllOtherAccordionAndScroll($targetElement){
        if(isMobile.any()){
            openOrCloseAllAccordion($targetElement);
        } else {
            if($('.accordion-item').length = $('.accordion-mobile-only').length){
                scrollToElement($targetElement);
            } else {
                openOrCloseAllAccordion($targetElement);
            }
        }

    }

    function openOrCloseAllAccordion($targetElement) {
        console.log('openOrCloseAllAccordion - rating');
        let $allOtherAccordions = $('.accordion-item').not($targetElement).filter(":not('.status-closed')").find('.accordion-item-content');
        $allOtherAccordions.slideUp( "slow", function() {
            $(this).parent().addClass('status-closed');
        });

        $allOtherAccordions.promise().done(function() {
            if(thisAccordionIsOpen($targetElement.children('.block-accordion-item-content'))){
                scrollToElement($targetElement);
            } else {
                openThisAccordion($targetElement.children('.block-accordion-item-title'), scrollToElement($targetElement));
            }
        });
    }


    //openThisAccordion
    function openThisAccordion($target, callback){
        $target.next().slideDown( "slow", function() {
            let thisAccordionItem = $target.parent();
            thisAccordionItem.removeClass('status-closed');
            if(callback){
                callback()
            }
        });
    }
    //openAllAccordion
    function openAllAccordion(){
        $('.accordion-item').filter(".status-closed").each(function () {
            $(this).find('.accordion-item-content').slideDown( "slow", function() {
                $(this).parent().removeClass('status-closed');
            });
        });
    }
    //this accordion is open
    function thisAccordionIsOpen($target){
        return $target.next().is(':visible');
    }



    $(document).on('click', 'aside a[data-target]', function(event) {
        let targetElementId = $(this).attr('data-target');
        let $targetElement = $(targetElementId);

        closeAllOtherAccordionAndScroll($targetElement);


    });

    function scrollToElement($target, callback){
        console.log(' $target',  $target);
        console.log('$target.offset().top', $target.offset().top);
        $('html, body').animate({
            scrollTop: $target.offset().top
        }, 1000, function() {
            if(callback){
                callback();
            }
        });
    }



    $(document).on('click', '.form-offer-option', function(event) {
        $(this).addClass('status-selected');
        $('.form-offer-question').slideUp( "slow", function() {
            event.stopPropagation();
        });
        $('.form-offer-answer').slideDown( "slow", function() {
            event.stopPropagation();
        });


        return false;
    });

    // $(document).on('click', '.button-let-me-help', function(event) {
    //     $(this).addClass('status-selected');
    //     $('.form-offer-green-question').slideUp( "slow", function() {
    //         event.stopPropagation();
    //     });
    //     $('.form-offer-green-answer').slideDown( "slow", function() {
    //         event.stopPropagation();
    //     });

    //     return false;
    // });

    $(document).on('click', '.first-level-menu', function(event) {
        let menuIsOpened = $('.status-menu-opened').length > 0;
        let asideClosedButtonVisible = $('.aside-closed-button').is(':visible');
        let plusButtonWasClicked = $(event.target).hasClass('aside-closed-button');


        if(asideClosedButtonVisible) {
            if (menuIsOpened) {
                if (plusButtonWasClicked) {

                    $('.first-level-menu-content').hide();
                    $(".first-level-menu").animate({
                        width: '60px'
                    }, 'fast', function () {

                        $('.aside-closed-title').show();

                        $('.aside-closed-button').animate({
                            right: '20px'
                        }, 'fast', function () {
                            $('body').removeClass('status-menu-opened');
                        });

                    });

                    $("main").animate({left: '0'}, 'fast', function () {});
                    $(".contact-box-desktop").animate({right: '30px'}, 'fast', function () {});
                    $(".contact-box-desktop-mini.status-position-bottom").animate({right: '30px'}, 'fast', function () {});
                } else {
                    console.log('aside will close only if you click small button')
                }

            } else {

                $(".first-level-menu").animate({
                    width: '280px'
                }, 'fast', function () {
                    $('.aside-closed-title').hide();

                    $('.aside-closed-button').animate({
                        right: '-10px'
                    }, 'fast', function () {
                        $('body').addClass('status-menu-opened');
                    });

                    $('.first-level-menu-content').show()
                });

                $("main").animate({left: '220px'}, 'fast', function () {});
                $(".contact-box-desktop").animate({right: '250px'}, 'fast', function () {});
                $(".contact-box-desktop-mini.status-position-bottom").animate({right: '250px'}, 'fast', function () {});

            }
        }

    });


    // ------------------------------------------------------------------------------------------------------ REFACTOR THIS ACCORDION AND ASIDE SECTION


    // ------------------------------------------------ Articles Cards dotdotdot
    String.prototype.trunc =
        function( n, useWordBoundary ){
            if (this.length <= n) { return this; }
            var subString = this.substr(0, n-1);
            return (useWordBoundary
                ? subString.substr(0, subString.lastIndexOf(' '))
                : subString) + "...";
        };

    if($('.card-article-wrapper').length > 0 && !isMobile.any() && $('.page-name-not-found').length === 0){

        $('.card-article-wrapper').each(function(){
            let $cardWrapper = $(this);

            let cardWrapperHeight = $cardWrapper.height();
            let cardTitleElementsHeight = $cardWrapper.find('.title').outerHeight();
            let cardTitleElementsPosition = $cardWrapper.find('.title').position();
            let cardTitleElementsText = $cardWrapper.find('.title').text();

            $cardWrapper.find('.title').text($cardWrapper.find('.title').text().trunc(60, true))
        })

    }
    // ================================================ End Articles Cards dotdotdot

    // ------------------------------------------------------------------------------------------------------ Aside menu discovery pages active tracking by href
    if($('aside').is(':visible')) {
        let asideLinks = $('.side-bar a[href], .aside-with-tags a[href]');
        let currentPagePath = window.location.pathname.replace('.html', '').replace('/local/', '').split('/').pop();
        console.log('currentPagePath', currentPagePath);

        // if ($('.page-type-articles').length > 0) {
        //     $('.side-bar a[href], .aside-discovery a[href]').filter("[href='insights']").parents("li").addClass('status-current-page');
        // }

        // Update active navigation menu for sidebar
        if ($('.page-type-articles').length > 0) {
            $('.side-bar a[href]').filter("[href='/insights']").parents("li").addClass('status-current-page');
        }

        if ($('.page-type-services, .page-type-products').length > 0) {
            $('.side-bar a[href]').filter("[href='/services']").addClass('status-current-page');
        }

        // if ($('.page-name-discovery').length > 0) {
        //     updateArticleTags('alltopic');
        //     updateArticleTags('retirement');
        //     updateArticleTags('financial');
        //     updateArticleTags('education');
        // }


        asideLinks.each(function () {
            let currentHref = this.getAttribute("href").split('/').pop().replace(".html", "");

            if (currentHref == currentPagePath) {
                console.log('======this is current page href', currentHref);
                $(this).parent('li').addClass('status-current-page');
                // $(this).parents('.first-level-menu-list-item').addClass('status-menu-active');

                var thisTag = $(this).parents('.first-level-menu-list-item').find('.article-type-tag').attr('data-tag');

                if( thisTag ){
                    $('body').addClass('status-unmovable');
                    updateArticleTags(thisTag);
                }

            }
        });
    }











    // ------------------------------------------------ MegaMenu for mobile only
    //create social icons using handelbarsDataPrivatObject.social data and then implement them to header

    if(isMobile.any()) {
        if($("#menu").length > 0) {

            let $menu = $("#menu").mmenu({
                extensions: ["shadow-panels", "position-right", "fx-panels-slide-100", "border-none", "fullscreen"],
                navbars: [{
                    height: 2,
                    content: [
                        `<header>
  <div class="mobile-menu-container">
    <div class="container">
      <div class="row">
        <div class="hamburger"><a id='button-close-menu'><div class="icon icon-mmenu-close"></div></a></div>
        <div class="col-xs-12 logo-container"><a href="/"><div class="icon icon-mmenu-logo"></div></a></div>
      </div>
    </div>
  </div>
</header>`,],

                }, {
                    //second navbar
                    content: ["prev"]
                }, {
                    position: "bottom",
                    content: [
                        `<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h3>${window.handelbarsDataPrivatObject.companyName}</h3>
      </div>
    </div>
    <div class="footer-legal-and-social">
      <div class="row">
        <div class="col-xs-12">
          <ul class="footer-social">
          
         </ul>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12">
          <ul class="footer-legal-section">
            <li><a href="term-of-use">Terms & conditions</a></li>
            <li><a href="fair-dealing-policy">Fair dealing policy</a></li>
            <li><a href="insurance-guides-and-useful-links">Insurance guides & useful links</a></li>
            <li><a href="statement-of-personal-data-protection">Statement of personal data protection</a></li>
          </ul>
        </div>
        <div class="col-xs-12 block-footer-copyright">
          <p class="copyright">©2019 Manulife (Singapore) Pte Ltd</p>
        </div>
      </div>
    </div>
  </div>
</footer>`,],

                }],
                setSelected: true,
            }, {});

            let API = $menu.data("mmenu");
            let menuWasClosedFromSubmenu = false;
            let currentMenuTrackerId = 'mm-1';

            $("#button-close-menu").click(function () {
                API.close();
            });

            $("#button-open-menu").click(function () {
                API.open();
            });

            //Dirty hack to remove first navigation with back button on step 1,
            //closePanel:finish not work for some reason
            API.bind("open", function () {
                if (currentMenuTrackerId == 'mm-1') {
                    $('body').addClass('status-first-mmenu-open');
                }
            });
            API.bind("close", function () {
                if (currentMenuTrackerId != 'mm-1') {
                    $('body').removeClass('status-first-mmenu-open');
                }
            });


            API.bind("openPanel", function ($panel) {
                console.log("This panel is now opening: #" + $panel.attr("id"));
                currentMenuTrackerId = $panel.attr("id");

                if (currentMenuTrackerId == 'mm-1') {
                    console.log('first-menu add class');
                    $('body').addClass('status-first-mmenu-open');
                } else {
                    console.log('not first-menu remove class');
                    $('body').removeClass('status-first-mmenu-open');
                }
            });
            API.bind("closePanel", function ($panel) {
                console.log("This panel is now closePanel: #" + $panel.attr("id"));
            });


        }
    }



    rating.init();

    console.timeEnd('measure-time-main-js');
    show_more.init();
});

// const props = {
//     address: $("footer .company_address").text(),
//     mapIdSelector: "#google_map",
//     mapId: "google_map",
//     mapButtons: ".popup-address-link",
//     mapBusinessName: "Agencia Generali Seguros,",
//     mapBusinessCountry: "",
// };


if(typeof google !== 'undefined'){
    google.maps.event.addDomListener(window, 'load', googleMap.init());
}
