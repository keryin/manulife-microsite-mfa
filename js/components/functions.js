/*******************************************
 FUNCTIONS
 ******************************************/
import meetingForm from './meeting-form';
/*****************detect which kind of device does the user use********/

var functions = {


    isMobile: ()=> {
        return window.innerWidth < 767;
    },

    isTablet : () => {
        return window.innerWidth >= 768 && window.innerWidth < 1150;
    },

    isDesktop(){
      return  window.innerWidth >= 1150
    },


    getMobileOperatingSystem: ()=> {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }, isInterntExplorer: function () {

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
        {
            return true;
        } else  // If another browser, return 0
        {
            return false;
        }

        return false;
    },

    submitMeetingForm(){
        meetingForm.submitForm();
    },
    makeDarkBackground(){
        $(".l-dark_background").show();
    },
    makeNormalBackground(){
        $(".l-dark_background").hide();
    }
};

for (var prop in functions) {
    if (!window[prop]) {
        window[prop] = functions[prop];
    }
}
module.exports = functions;

