let isBannerVisible = {

    init: function () {
        this.isContactBannerVisible();
    },

    isContactBannerVisible: function () {

        $.fn.isVisible = function () {



            // Am I visible?
            // Height and Width are not explicitly necessary in visibility detection, the bottom, right, top and left are the
            // essential checks. If an image is 0x0, it is technically not visible, so it should not be marked as such.
            // That is why either width or height have to be > 0.
            // if ($('.page-name-about-management').length > 0 || $('.knowledge-base').length > 0 || $('.page-name.contact-us').length > 0) {
            //     var banner = 1;
            // } else {
            //
            // }
            if(this[0]) {
                var banner = this[0].getBoundingClientRect();

                return (
                    (banner.height > 0 || banner.width > 0) &&
                    banner.bottom >= 0 &&
                    banner.right >= 0 &&
                    banner.top <= (window.innerHeight || document.documentElement.clientHeight) &&
                    banner.left <= (window.innerWidth || document.documentElement.clientWidth)
                );
            }
        };

        var lastScrollTop = 0;

        $(window).on('load resize scroll', function () {
            if($('.contact-box').length === 0){
                return false
            }

            var currentScrollTop = $(this).scrollTop();
            var coverImgHeight = $(".cover-image-wrapper").outerHeight();
            var footerHeight = $('footer').innerHeight();
            
            if (currentScrollTop > coverImgHeight || !($('.contact-box-desktop').isVisible())) {
                $('.contact-box-desktop-mini').fadeIn().addClass("show-contact-box");
            } else {
                $('.contact-box-desktop-mini').fadeOut().removeClass("show-contact-box");
            }

            // if ($('.contact-box-desktop').isVisible()) {
            //     $('.contact-box-desktop-mini').fadeOut().removeClass("show-contact-box");
            // } else {
            //     /*if (currentScrollTop > lastScrollTop){
            //         // downscroll code
            //         if($(window).scrollTop() + $(window).height() > $(document).height() - footerHeight) {
            //             $('.contact-box-desktop-mini').addClass('status-position-bottom');
            //             $('.contact-box-desktop-mini')[0].style.right = $('.contact-box-desktop')[0].style.right;
            //         } else {
            //             $('.contact-box-desktop-mini').removeClass('status-position-bottom');
            //             $('.contact-box-desktop-mini')[0].style.right = '';

            //         }
            //     } else {
            //         // upscroll code
            //         if($(window).scrollTop() + $(window).height() > $(document).height() - footerHeight) {
            //             $('.contact-box-desktop-mini').addClass('status-position-bottom');
            //             $('.contact-box-desktop-mini')[0].style.right = $('.contact-box-desktop')[0].style.right;
            //         } else {
            //             $('.contact-box-desktop-mini').removeClass('status-position-bottom');
            //             $('.contact-box-desktop-mini')[0].style.right = '';
            //         }
            //     }*/
            //     lastScrollTop = currentScrollTop;


            //     $('.contact-box-desktop-mini').fadeIn().addClass("show-contact-box");

            // }
        });

    }
};

module.exports = isBannerVisible;
