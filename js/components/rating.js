function notEmpty(inputElement) {
    return inputElement.val() !== "";
}

function changeErrorMessageText(elClass, text) {
    let $el = $(elClass);
    $el.text(text);
}

function scrollToElement($target, callback){
    console.log(' $target',  $target);
    console.log('$target.offset().top', $target.offset().top);
    $('html, body').animate({
        scrollTop: $target.offset().top
    }, 1000, function() {
        if(callback){
            callback($target);
        }
    });
}



function getRatingItemDOM(dataObject, index) {

    return `<div class="rating-list-item swiper-slide col-xs-12 col-md-6 col-lg-4" id="rating-item-${index}">
  <div class="rating-list-item-content">
    <div class="block-rating-list-item-body">
      <div class="rating-list-item-text">${dataObject.review}</div>
    </div>
    <div class="block-rating-list-item-header">
      <div class="rating-list-item-name">${dataObject.name}</div>
      <div class="rating-list-item-date">${dataObject.date}</div>
    </div>
    <div class="block-rating-list-item-footer property-rate-${dataObject.rating}">
      <div class="rating-list-item-face">
        <div class="rating-list-item-face">
          <div class="icon icon-rating-face"></div>
        </div>
      </div>
      <div class="rating-list-item-stars">
        <div class="rating-list-item-star rating-star"><span class="fa fa-star"></span></div>
        <div class="rating-list-item-star rating-star"><span class="fa fa-star"></span></div>
        <div class="rating-list-item-star rating-star"><span class="fa fa-star"></span></div>
        <div class="rating-list-item-star rating-star"><span class="fa fa-star"></span></div>
        <div class="rating-list-item-star rating-star"><span class="fa fa-star"></span></div>
      </div>
    </div>
  </div>
</div>`

}

// var contactBoxElement = $('.contact-box-data');
let ratingBlockAverage = $('.block-average-rating');
let ratingAverageNumber = $('#average-rating-number-score');
let ratingAverageAmount = $('#average-rating-amount');
let ratingAverageStatisticScore1 = $('#average-rating-statistic-score-1');
let ratingAverageStatisticScore2 = $('#average-rating-statistic-score-2');
let ratingAverageStatisticScore3 = $('#average-rating-statistic-score-3');
let ratingAverageStatisticScore4 = $('#average-rating-statistic-score-4');
let ratingAverageStatisticScore5 = $('#average-rating-statistic-score-5');
let ratingCardStars = $('#rating-card-stars .rating-star');






var rating = {

    init: function () {
        if($('.review-box').length > 0) {
            this.getData();
            this.initForm();
        }



    },

    initForm: function () {
        let currentForm = $('#form-review-form');
        let currentFormExist = currentForm.length > 0;
        let reviewformType  = 'review';

        if(currentFormExist) {
            let currentFormSubmit = $('#button-rate-us');
            let currentFormThank = $('#form-rate-us-thank');

            let curentFullName = currentForm.find('.input-fullname');
            let curentNote = currentForm.find('.input-note');

            let curentFullNameValue = curentFullName.val();
            let curentNoteValue = curentNote.val();
            let curentRatingValue = $('#rating-card-face');



            currentFormSubmit.click(function (event) {


                //put all required input error
                curentFullName.addClass("status-error");
                if(currentForm.find('.error-label-fullname').length === 0){
                    curentFullName.parents('.form-field-inner').append('<div class="error-label error-label-fullname"></div>')
                }

                curentNote.addClass("status-error");
                if(currentForm.find('.error-label-review').length === 0){
                    curentNote.parents('.form-field-inner').append('<div class="error-label error-label-review"></div>')
                }


                if (notEmpty(curentNote)) {
                    curentNote.removeClass('status-error');
                    //also remove or hide error message
                } else {
                    //add error message about [EMPTY FIELD]
                    changeErrorMessageText('.error-label-review', 'Review is required')
                }


                if (notEmpty(curentFullName)) {
                    curentFullName.removeClass('status-error');
                    //also remove or hide error message
                } else {
                    //add error message about [EMPTY FIELD]
                    changeErrorMessageText('.error-label-fullname', 'Full Name is required')
                }


                //if no error - send message to server
                if ($(currentForm).find('.status-error').length === 0) {

                    let curentFullNameValue = curentFullName.val() || '';
                    let curentNoteValue = curentNote.val() || '';
                    //let curentRatingValue = parseInt($('.rating-card-controll').attr('class').slice(-1), 10);
                    let curentDate = moment().format('DD MMMM YYYY');


                    //reset current form
                    $(currentForm).find('input, textarea').val('');
                    //show thank you and hide form

                    //window.rating.rateBusiness(curentFullNameValue, '', curentNoteValue, curentRatingValue, function (response) {
                    window.rating.rateBusiness(curentFullNameValue, '', curentNoteValue, function (response) {
                        // $(currentForm).hide();
                        // $(currentFormThank).show();
                        console.log('Rating was Send')
                    });

                    $(currentForm).hide();
                    $(currentFormThank).show();
                }
            });

            ratingCardStars.click(function () {
                let clickedStarScore = ratingCardStars.index(this) + 1;

                $(this).parents('.rating-card-controll').removeClass(function (index, className) {
                    return (className.match (/\bproperty-rate-\S+/g) || []).join(' ');
                }).addClass('property-rate-' + clickedStarScore)
            })

        }
    },

    getData: function () {
        var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;

        // For local check of rating function
        // window.demoRating = [{name: "Lin", date: "08/05/2018", target_name: "", rating: 4, review: "Eng Beow was really helpful and patient, I'll definitely work with him again Eng Beow was really helpful and patient, I'll definitely work with him again Eng Beow was really helpful and patient, I'll definitely work with him again"},{name: "Lin", date: "08/05/2018", target_name: "", rating: 4, review: "Eng Beow was really helpful and patient, I'll definitely work with him again Eng Beow was really helpful and patient, I'll definitely work with him again Eng Beow was really helpful and patient, I'll definitely work with him again"}, {name: "Yulia", date: "08/05/2018", target_name: "", rating: 5, review: "Got an amazing experience planning my retirement plan. Thanks so much! Got an amazing experience planning my retirement plan. Thanks so much! Got an amazing experience planning my retirement plan. Thanks so much!"}, {name: "Julia", date: "08/05/2018", target_name: "", rating: 5, review: "Thank you for a great experience!"}, {name: "Julia", date: "08/05/2018", target_name: "", rating: 5, review: "Thank you for a great experience!"}, {name: "Julia", date: "08/05/2018", target_name: "", rating: 5, review: "Thank you for a great experience!"}, {name: "Julia", date: "08/05/2018", target_name: "", rating: 5, review: "Thank you for a great experience!"},{name: "Lin", date: "08/05/2018", target_name: "", rating: 4, review: "Eng Beow was really helpful and patient, I'll definitely work with him again Eng Beow was really helpful and patient, I'll definitely work with him again Eng Beow was really helpful and patient, I'll definitely work with him again"}];
        // if(window.demoRating){
        //     alert('you enabled local rating function, disable it before upload to server');
        //     window.rating = {};
        //     window.rating.requestRatingList = function(callback) {
        //         callback(window.demoRating);
        //     };
        // }

        if (window.rating) {

                window.rating.requestRatingList(function (ratingsArray) {
                    window.reviewsData = ratingsArray;
                    // window.reviewsData = window.ratingData;
                    let ratingWasReceived = ratingsArray.length > 0;


                    if (!ratingWasReceived) {
                        console.log('no rating was received');
                        $('.review-box .show-more').text('Be the first one to review me');
                    } else if (ratingWasReceived) {
                        //update html status
                        $('html').addClass('status-rating-loaded');
                        $('.section-review-wrapper').addClass('review-box-show');
                        $('.review-box').addClass('review-box-show');
                        //update buttons on home page
                        $('.review-box .button-see-all-review').text('See all Reviews');

                        //update form title on review page
                        let pointer = $('.page-theme-agency').length > 0 ? 'us' : 'me';
                        console.log('pointer', pointer);
                        let ratingFormTitleText = `Give ${pointer} some <span>love:</span>`;
                        $('.controll-rating-form h2').html(ratingFormTitleText);

                        let pointer2 = $('.page-theme-agency').length > 0 ? 'Our' : 'My';
                        console.log('pointer2', pointer2);
                        let ratingListTitleText = `${pointer2} happy <span>customers:</span>`;
                        $('.controll-rating-list h2').html(ratingListTitleText);

                        //show elements on rating page
                        $('.controll-rating-average, .controll-rating-list').fadeIn( "medium", "swing", function() {});

                        //restructure data from rating object
                        console.log('reviewsData', reviewsData);
                        let arrayWithAllRating = [];
                        let ratingSum = 0;
                        let amountOfReviews = reviewsData.length;


                        for( var i = 0; i < reviewsData.length; i++ ){
                            console.log(reviewsData[i]);
                            arrayWithAllRating.push(reviewsData[i].rating);
                            ratingSum += parseInt( reviewsData[i].rating, 10 );

                            // Show maximum 3 ratings only in services and home page
                            if($(".review-box").hasClass("review-box-load-more") && reviewsData.length > 3){
                                reviewsData.length = 3 
                            }

                            //generate customer review html
                            $('.block-rating-list').append(getRatingItemDOM(reviewsData[i], i));
                        }

                        if(reviewsData.length < 7){
                            $('.section-rating-list .custom-show-less').removeClass('custom-show-less');
                            $('.section-rating-list .show-more-overlay, .section-rating-list .show-more').hide();
                        }
                        
                        if ($("rating-list-item")) {

                            console.log("rating is there!");
                            var minimized_elements = $('.rating-list-item-text');
                            var minimize_character_count = 200;


                            minimized_elements.each(function () {
                                var t = $(this).text();
                                if (t.length < minimize_character_count) return;
                                if($('.review-box-carousel-slider').length > 0){
                                    //special behaviour on index page rating carousel
                                    //on click it should lead you to rating/review/testimonials page, wait for rating upload, scroll to rating, open it
                                    $(this).html(
                                        t.slice(0, minimize_character_count) + '<span>... </span><a href="#" class="more-on-rating-page"> Read More ></a>'
                                    )
                                } else {
                                    //normal read more-less behaviour on rating/review/testimonials page
                                    $(this).html(
                                        t.slice(0, minimize_character_count) + '<span>... </span><a href="#" class="more"> Read More ></a>' +
                                        '<span style="display:none;">' + t.slice(minimize_character_count, t.length) + ' <a href="#" class="less"> Hide ></a></span>'
                                    );
                                }

                            });


                            $('a.more-on-rating-page').click(function (event) {
                                event.preventDefault();
                                let currentRatingElementId = $(this).parents('.rating-list-item').attr('id');
                                let ratingPageUrl = '';
                                if(window.location.search === ""){
                                    ratingPageUrl = window.location.href + 'review' + '?target-review-id=' + currentRatingElementId;
                                } else {
                                    let creanURL = window.location.href.replace(window.location.search, '');
                                    let urlSearchParam = window.location.search;
                                    ratingPageUrl = creanURL + 'review' + urlSearchParam + '&target-review-id=' + currentRatingElementId;

                                    console.log('creanURL', creanURL);
                                    console.log('urlSearchParam', urlSearchParam);
                                }
                                console.log('ratingPageUrl', ratingPageUrl);
                                window.location.href = ratingPageUrl;
                            });


                            $('a.more', minimized_elements).click(function (event) {
                                event.preventDefault();
                                $(this).hide().prev().hide();
                                $(this).next().show();
                            });


                            $('a.less', minimized_elements).click(function (event) {
                                event.preventDefault();
                                $(this).parent().hide().prev().show().prev().show();
                            });
                        }

                        console.log('arrayWithAllRating', arrayWithAllRating);
                        console.log('ratingSum', ratingSum);
                        console.log('amountOfReviews', amountOfReviews);
                        let ratingAverage = Math.round(ratingSum/amountOfReviews * 10) / 10;
                        console.log('ratingAverage', ratingAverage);

                        //put averageNumberToPage
                        ratingAverageNumber.text(ratingAverage);

                        //put ratingAverageAmount
                        ratingAverageAmount.text(amountOfReviews);
                        //create class by averageRating
                        let rateClassForAverage = 'property-rate-' + Math.round((ratingAverage));
                        console.log('rateClassForAverage', rateClassForAverage);

                        //add class to averageBlock
                        ratingBlockAverage.addClass(rateClassForAverage);


                        //get and put average data number by score
                        //put score class

                        let countRatingFor1 = arrayWithAllRating.filter(function(x){return x==0 || x==1}).length;
                        let countRatingFor2 = arrayWithAllRating.filter(function(x){return x==2}).length;
                        let countRatingFor3 = arrayWithAllRating.filter(function(x){return x==3}).length;
                        let countRatingFor4 = arrayWithAllRating.filter(function(x){return x==4}).length;
                        let countRatingFor5 = arrayWithAllRating.filter(function(x){return x==5}).length;

                        ratingAverageStatisticScore1.text(countRatingFor1);
                        ratingAverageStatisticScore2.text(countRatingFor2);
                        ratingAverageStatisticScore3.text(countRatingFor3);
                        ratingAverageStatisticScore4.text(countRatingFor4);
                        ratingAverageStatisticScore5.text(countRatingFor5);


                        let $showHideButton = $('.section-rating-list .show-more');
                        let $blockRatingList = $(".block-rating-list-wrapper");
                        let ratingIsHidden = true;

                        function showReviews(){
                            $showHideButton.text('Hide');
                            $blockRatingList.addClass('custom-show-more');
                            ratingIsHidden = false;
                        }
                        function hideReviews(){
                            $showHideButton.text('Show all Reviews');
                            $blockRatingList.removeClass('custom-show-more');
                            ratingIsHidden = true;
                        }

                        $('.section-rating-list .show-more').click(function (e) {
                            e.preventDefault();
                            ratingIsHidden ? showReviews() : hideReviews();
                        });

                        //generate customer review html
                        window.createCarouselFromRatingPage();

                        function getUrlVars() {
                            var vars = {};
                            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                                vars[key] = value;
                            });
                            return vars;
                        }

                        function swipeIfNeededAndShow($target){
                            $target.find('.more').click();
                            if(typeof swiperBannerRating !== 'undefined'){
                                swiperBannerRating.slideTo($target.index());
                            }
                        }

                        if($('.review-box').length > 0){
                            //check if there is scrollTo parameter in url, if so, scroll-open
                            //on mobile scroll, scroll-swipe-to-element-open
                            let targetElementId = getUrlVars()['target-review-id'];
                            console.log('targetElementId', targetElementId);



                            if(targetElementId){
                                let $targetElement = $('#' + targetElementId);
                                if($targetElement.is(':hidden')){
                                    showReviews();
                                }
                                scrollToElement($targetElement, swipeIfNeededAndShow);
                            }
                        }

                    }
                });
            }


        }
    // }
};


module.exports = rating;