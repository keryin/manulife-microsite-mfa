const goforit = {
  init: function() {
    var that = this;
    if ($().slick && $().validate) {
      that.initSlickBanner();
      that.initCharacterCounter();
      that.initForm();
    }
  },

  initSlickBanner() {
    $('.banner-body').slick({
      autoplay: true,
      dots: false,
      infinite: true,
      autoplaySpeed: 3000,
      fade: true,
      arrows: false,
      cssEase: 'linear'
    });

    $(".banner-body div").addClass("active");
  },

  initCharacterCounter() {
    $.fn.characterCounter = function(options) {

      var settings = $.extend({
          max     : 100,
          opacity : ".8",
          color : "#363642",
          textArea: false
      }, options);
  
      return this.each( function() {
  
        $(this).wrap('<div class="character-wrap"></div>');
        $(this).after('<span class="remaining"></span>');
  
        // This will write the input's value on database
        var value = $(this).val().length;
              //var result = settings.max - value;
              var result = value + "/" + settings.max;
              $(this).next('.remaining').text(result);
  
              // This is counter
              //$(this).keyup(function() {
              $(this).on('input', function() {
                var value = $(this).val().length;
                //var result = settings.max - value;
                var result = value + "/" + settings.max;
                $(this).next('.remaining').text(result);
              });
  
              // Css
              $(this).css("padding-right", "35px");
              $(this).parent('.character-wrap').css("position", "relative");
              $(this).next('.remaining').css({
          'position' : 'absolute',
          'opacity' : settings.opacity,
          'color' : settings.color,
          'right' : '10px'
        });
  
              // textArea
        if (settings.textArea == false) {
          $(this).next('.remaining').css({
            'top' : '50%',
            'transform' : 'translateY(-50%)'
          });
        } else {
          $(this).next('.remaining').css({
            'bottom' : '10px',
          });
        }
  
      });
    }
  },

  initForm() {
    var $btnSubmit = $('.btn-submit');
    var $btnBack = $('.btn-back');
    var $btnNext = $('.btn-next');
    var $inputCategory = $('input[name="category"]');
    var $goal = $('#goal');
    var $form = $('#form-digital-stars');

    // Character Counter
    $goal.characterCounter({
      max: 200,
      textArea: true
    });

    $goal.on('input', function(){
      stepValidation(1);
    });

    // TnC
    $('.tnc-toggle').on('click', function(){
      $('.tnc-content').slideToggle();
      $(this).parent().toggleClass('open');
    });

    $('.tnc-open').on('click', function(){
      if (!$('.tnc-toggle').parent().hasClass('open')) {
        $('.tnc-content').slideToggle();
        $('.tnc-toggle').parent().toggleClass('open');
      }

      $('html, body').animate({
        scrollTop: $('.tnc-toggle').offset().top
      }, 1000);
    });

    $('.tnc-open-modal').on('click', function(){
      if (!$('.tnc-toggle').parent().hasClass('open')) {
        $('.tnc-content').slideToggle();
        $('.tnc-toggle').parent().toggleClass('open');
      }

      $('html, body').animate({
        scrollTop: $('.tnc-toggle').offset().top
      }, 1000);
    });

    // button is bugged on chrome incognito with document.ready 
    // (although this script is already wrapped inside a document.ready in the first place...)
    $(document).ready( function() { 
      // Agree button
      $('.btn-lets-do-it').on('click', function() {

        $('.btn-lets-do-it').hide();
        $('.progressbar-container').fadeIn();
        $('.button-container').fadeIn();
        $('.steps').removeClass('open');
        $('.step-one').fadeIn().addClass('open');
        $('.progressbar').find('[data-step="1"]').addClass('active');
        $('.intro-container').addClass('ua');
        $('.form-container').addClass('ua');
        $('.home-page-section').addClass('ua');

        stepValidation(1);

        $('html, body').animate({
          scrollTop: $('.form-container').offset().top - 50
        }, 1000);
      });
    });

    // Category
    $('.category-box').on('click', function(){
      var category = $(this).data('category');

      $('.category-box').removeClass('selected');
      $('.category-box').removeClass('show-result');
      $('.category-box .bar').hide("slide", { direction: "left" }, 1000);
      $('.category-box .percentage').fadeOut(1000);
      
      $(this).addClass('selected');
      $('.category-box').addClass('show-result');
      $('.category-box .bar').show("slide", { direction: "left" }, 1000);
      $('.category-box .percentage').fadeIn(1000);

      $inputCategory.val(category);

      stepValidation(1);
    });

    // Back Button
    $btnBack.on('click', function(){
      var currentStep = $('.steps.open').data('step');
      var $prevStep = $('.steps.open').prev();

      $btnSubmit.hide();
      $btnNext.show();

      if(currentStep == 1){
        $('.btn-lets-do-it').fadeIn();
        $('.progressbar-container').hide();
        $('.button-container').hide();
        $('.steps').removeClass('open').hide();
        $('.intro-container').removeClass('ua');
        $('.form-container').removeClass('ua');
        $('.home-page-section').removeClass('ua');

        $('html, body').animate({
          scrollTop: 0
        }, 1000);
      }else{
        $('.steps').removeClass('open').hide();
        $prevStep.fadeIn().addClass('open');

        $('html, body').animate({
          scrollTop: $('.form-container').offset().top - 50
        }, 1000);
      }

      $('.progressbar > li').removeClass('active');
      $('.progressbar').find('[data-step="' + $prevStep.data('step') + '"]').addClass('active');

      stepValidation($prevStep.data('step'));
    });

    // Next Button
    $btnNext.on('click', function(){
      var currentStep = $('.steps.open').data('step');
      var $nextStep = $('.steps.open').next();

      $('.btn-back').text("Back");
      $('.btn-next').text("Next");
      $('.steps').removeClass('open').hide();
      $nextStep.fadeIn().addClass('open');

      if(currentStep == 1){
        $(this).hide();
        $btnSubmit.css('display', 'inline-block');
      } else{
        $(this).show();
        $btnSubmit.css('display', 'none');
      }

      $('.progressbar > li').removeClass('active');
      $('.progressbar').find('[data-step="' + $nextStep.data('step') + '"]').addClass('active');

      stepValidation($nextStep.data('step'));

      $('html, body').animate({
        scrollTop: $('.form-container').offset().top - 50
      }, 1000);
    });

    // Submit button
    $btnSubmit.on('click', function(){
      if($form.valid()){
        window.onSubmit();
      }
    });

    // Form Validation
    $form.validate({
      rules: {
        first_name: "required",
        last_name: "required",
        salutation: "required",
        life_stage: "required",
        phone: {
          required: true,
          digits: true
        },
        email: {
          required: true,
          email: true
        }
      },
      errorContainer: ".js-form-error-message-container",
      showErrors: function(errorMap, errorList) {
        var errorMessageLabel = $(".js-form-error-message-container label");
        var errorMessage = "Missing required fields.";
        errorMessageLabel.text(errorMessage);
        this.defaultShowErrors();
      }
    });

    function stepValidation(step){
      switch(step){
        case 1:
          if($goal.val() == ''){
            $btnNext.attr('disabled', true);
          }else{
            $btnNext.attr('disabled', false);
          }
          break;
      }
    }
  }
};

module.exports = goforit;