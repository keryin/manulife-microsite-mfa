function parse_query_string(query) {
    var vars = query.split("&");
    var query_string = {};
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        var key = decodeURIComponent(pair[0]);
        var value = decodeURIComponent(pair[1]);
        // If first entry with this name
        if (typeof query_string[key] === "undefined") {
            query_string[key] = decodeURIComponent(value);
            // If second entry with this name
        } else if (typeof query_string[key] === "string") {
            var arr = [query_string[key], decodeURIComponent(value)];
            query_string[key] = arr;
            // If third or later entry with this name
        } else {
            query_string[key].push(decodeURIComponent(value));
        }
    }
    return query_string;
}

//reset forms for local
// window.submitForm = function(){
//     console.log(arguments)
// }





let worldcup = {

    init: function () {
        if($('.page-type-worldcup').length > 0) {
            window.dataLayer = window.dataLayer || [];
            this.initForm();
        }

        $('#go-to-terms').click(function () {
            scrollToElement($('.section-title-main'), quizFinished);
        })


        function scrollToElement($target, callback){
            console.log(' $target',  $target);
            console.log('$target.offset().top', $target.offset().top);
            $('html, body').animate({
                scrollTop: $target.offset().top
            }, 1000, function() {
                if(callback){
                    callback();
                }
            });
        }

        function scrollToTerms() {
            scrollToElement($('.block-accordion-item-title'))
        }


        $(document).on('click', '#go-to-terms', function(event) {
            event.preventDefault();
            event.stopPropagation();
            openThisAccordion($('.block-accordion-item-title'), scrollToTerms);
        });


        //openThisAccordion
        function openThisAccordion($target, callback){
            $target.next().slideDown( "slow", function() {
                let thisAccordionItem = $target.parent();
                thisAccordionItem.removeClass('status-closed');
                if(callback){
                    callback()
                }
            });
        }


        $(document).on('click', '.button-close-banner', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parents('.banner-worldcup').fadeOut( "slow", "swing", function() {});
        });




    },

    initForm: function () {
        let thisIsWorldcup = $('.page-name-worldcup').length > 0;
        let thisIsWorldcupFriend = $('.page-name-worldcup-friend').length > 0;
        let thisIsWorldcupThank = $('.page-name-worldcup-thanks').length > 0;
        let thisIsWorldcupFriendThank = $('.page-name-worldcup-friend-thank').length > 0;

        var query_string = document.location.search.replace('?', '');
        var searchParam = parse_query_string(query_string);
        let contactIdCode = searchParam.gfToken || searchParam.refCode;


        console.log('gfToken', searchParam.gfToken);
        console.log('refCode', searchParam.refCode);
        console.log('thisIsWorldcup', thisIsWorldcup);
        console.log('thisIsWorldcupFriend', thisIsWorldcupFriend);
        console.log('thisIsWorldcupThank', thisIsWorldcupThank);
        console.log('thisIsWorldcupFriendThank', thisIsWorldcupFriendThank);


        const formWorldcup = $("#form-worldcup");
        const formWorldcupSubmit = $("#button-submit-worldcup");
        const $generateLink = $('.generate-link .text');


        // if(thisIsWorldcupThank){
        //     $('#button-copy-from-link').tooltipster({
        //         animation: 'fade',
        //         delay: 0,
        //         timer: 150,
        //         trigger: 'click'
        //     });
        //     new ClipboardJS('#button-copy-from-link');
        //
        //
        //
        //     if(localStorage.shareLinkForWorldcup) {
        //         $generateLink.text(localStorage.shareLinkForWorldcup);
        //     }
        // }
        if(thisIsWorldcupThank){
            $('#button-copy-from-link').tooltipster({
                animation: 'fade',
                delay: 0,
                timer: 150,
                trigger: 'click'
            });
            new ClipboardJS('#button-copy-from-link');
            $generateLink.text(document.location.href.replace('-thanks', ''));


            // $('#link-share-on-wechat').attr('data-share', document.location.href.replace('-thanks', ''))
            $('#link-share-on-whatsapp').attr('data-url', document.location.href.replace('-thanks', ''))
            $('#link-share-on-twitter').attr('data-url', document.location.href.replace('-thanks', ''))
            // $('#link-share-on-instagram').attr('data-share', document.location.href.replace('-thanks', ''))
            $('#link-share-on-facebook').attr('data-url', document.location.href.replace('-thanks', ''))
            $('#link-share-on-linkedin').attr('data-url', document.location.href.replace('-thanks', ''))

        }


        const formInputs = {
            firstName: ".input-firstname",
            lastName: ".input-lastname",
            email: ".input-email",
            phone: ".input-phone",
            contactMethod: '.input-method',
            type: '.input-type',
            date: '.input-date',
            time: '.input-time',
            note: '.input-note',
        };




        function worldcupForm(formObject){
            let currentForm = $("#form-worldcup");
            let currentFormExist = currentForm.length > 0;

            if(currentFormExist){
                let currentFormSubmit = $('#button-submit-worldcup');


                let curentFirstName = currentForm.find(formInputs.firstName);
                let curentLastName = currentForm.find(formInputs.lastName);
                let curentEmail = currentForm.find(formInputs.email);
                let curentPhone = currentForm.find(formInputs.phone);
                let curentContactMethod = currentForm.find(formInputs.contactMethod);
                let curentType = currentForm.find(formInputs.type).filter('select');
                let curentNote = currentForm.find(formInputs.note);


                let curentDate = currentForm.find(formInputs.date);
                let curentTime = currentForm.find(formInputs.time);



                let curentGoal = $('.input-goal');



                currentFormSubmit.click(function (event) {
                    console.log('click on form')


                    //put all required input error
                    curentGoal.addClass("status-error");
                    curentFirstName.addClass("status-error");
                    curentLastName.addClass("status-error");
                    curentEmail.addClass("status-error");
                    curentPhone.addClass("status-error");




                    $('.input-method-terms').not('.status-selected').addClass("status-error");
                    $('.input-method-terms.status-selected').removeClass('status-error');



                    //check each input for error and remove error

                    if (notEmpty(curentGoal)) {
                        curentGoal.removeClass('status-error');
                    }
                    if (notEmpty(curentFirstName)) {
                        curentFirstName.removeClass('status-error');
                    }

                    if (notEmpty(curentLastName)) {
                        curentLastName.removeClass('status-error');
                    }

                    if (notEmpty(curentEmail) && isEmail(curentEmail)) {
                        curentEmail.removeClass('status-error');
                    }

                    if (notEmpty(curentPhone) && isPhoneNubmer(curentPhone)) {
                        curentPhone.removeClass('status-error');
                    }

                    //If date was selected time must be selected





                    //if no error - send message to server
                    if ($(currentForm).find('.status-error').length === 0) {

                        let curentFirstNameValue = curentFirstName.val().charAt(0).toUpperCase() + curentFirstName.val().slice(1) || '';
                        let curentFirstNameMessage = curentFirstNameValue != '' ? '\nFirst Name: ' + curentFirstNameValue  : '';

                        let curentLastNameValue =  curentLastName.val().charAt(0).toUpperCase() + curentLastName.val().slice(1) || '';
                        let curentLastNameMessage = curentLastNameValue != '' ? '\nLast Name: ' + curentLastNameValue  : '';

                        let curentEmailValue = curentEmail.val() || '';
                        let curentEmailMessage = curentEmailValue != '' ? '\nEmail: ' + curentEmailValue  : '';

                        let curentPhoneValue = curentPhone.val() || '';
                        let currentPhoneMessage = curentPhoneValue != '' ? '\nPhone: ' + curentPhoneValue  : '';

                        let curentTypeValue = curentType.val() || '';
                        let currentTypeMessage = curentTypeValue != '' ? '\nI would like to talk about: ' + curentTypeValue  : '';

                        let curentNoteValue = curentNote.val() || '';
                        let currentNoteMessage = curentNoteValue != '' ? '\nMessage: ' + curentNoteValue  : '';


                        let curentContactMethodValue = curentContactMethod.hasClass('status-selected') ? curentContactMethod.filter('.status-selected').text().trim() : '';
                        let curentContactMethodMessage = curentContactMethodValue != '' ? '\nPrefered contact method: ' + curentContactMethodValue  : '';

                        let curentDateValue = curentDate.val() || '';
                        let curentTimeValue = (curentTime.val() && curentTime.val() != 'Time') ? curentTime.val() : '';

                        console.log('curentTypeValue', curentTypeValue);
                        console.log('curentTimeValue', curentTimeValue);

                        let dateAndTimeWasSet = (curentDateValue != '' && curentTimeValue != '');
                        console.log('dateAndTimeWasSet', dateAndTimeWasSet);
                        let curentDateAndTimeMessage = '';
                        let curentDateAndTimeMessageForThank = '';


                        let currentMessageFromClient = '';
                        currentMessageFromClient = `${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage} ${currentPhoneMessage} ${curentContactMethodMessage}`;
                        currentMessageFromClient += `\n\nThis message was received from ${$(currentForm).attr('id')} on page ${document.location.href.split('/').pop()}`;


                        //None it our forms
                        let city = null;
                        let zipcode = null;


                        let agreeToTerms = $('.input-method-terms').hasClass('status-selected') ? true : false;
                        let agreeToMarketing = $('.input-method-marketing').hasClass('status-selected') ? true : false;

                        if($('.input-method-terms').has('.status-selected')) {

                        }



                        // if(thisIsWorldcup) {
                        //
                        //     let linkToShare = document.location.origin + '/worldcup-friend?refCode=' + contactIdCode;
                        //     localStorage.shareLinkForWorldcup = linkToShare;
                        //     console.log('linkToShare', localStorage.shareLinkForWorldcup);
                        //
                        //
                        //     window.submitForm({
                        //         formType: 'worldcup',
                        //         payload: {
                        //             linkToShare: linkToShare,
                        //             formTagWorldcup: true,
                        //             answer: $('.input-goal').val()
                        //         },
                        //         contactInfo: {
                        //             emails: [{address: curentEmailValue}],
                        //             phones: [{phoneNumber: curentPhoneValue, phoneTypeId: 'MOBILE'}],
                        //             tags: ['World Cup - known contact'],
                        //             firstName: curentFirstNameValue,
                        //             lastName: curentLastNameValue
                        //         },
                        //     });
                        //
                        //     $(currentForm).find('input, textarea').val('');
                        //     window.location.href = window.location.origin + '/worldcup-thank';
                        // }

                        if(thisIsWorldcup) {
                            let linkToShare = document.location.origin + '/worldcup?refCode=' + contactIdCode;
                            let tagsForWorldcup = ['World Cup'];
                            if(!agreeToMarketing){
                                tagsForWorldcup = ['World Cup', 'Unsubscribe from Updates']
                            }

                            let textToChat = `${curentFirstNameValue} ${curentLastNameValue} entered the World Cup raffle.
                            Email: ${curentEmailValue}
                            Mobile: ${curentPhoneValue}
                            Answer: ${$('.input-goal').val()}
                            Agree to the terms and conditions: ${agreeToTerms}
                            Agree to be contacted by and receive marketing materials: ${agreeToMarketing}`;



                            localStorage.shareLinkForWorldcup = linkToShare;


                            console.log('linkToShare', localStorage.shareLinkForWorldcup);
                            console.log('agreeToTerms', agreeToTerms);
                            console.log('agreeToMarketing', agreeToMarketing);

                            if(typeof localStorage.worldcupFormWasFilled !== 'undefined'){

                                console.log('form create new user');
                                window.submitForm({
                                    formType: 'worldcup',
                                    payload: {
                                        emails: curentEmailValue,
                                        phones: curentPhoneValue,
                                        tags: tagsForWorldcup,
                                        firstName: curentFirstNameValue,
                                        lastName: curentLastNameValue,
                                        refId: contactIdCode,
                                        formTagWorldcup: true,
                                        answer: $('.input-goal').val(),
                                        agreeToTerms: agreeToTerms,
                                        agreeToMarketingUpdates: agreeToMarketing
                                    },
                                    contactInfo: {
                                        emails: [{address: curentEmailValue}],
                                        phones: [{phoneNumber: curentPhoneValue, phoneTypeId: 'MOBILE'}],
                                        tags: tagsForWorldcup,
                                        firstName: curentFirstNameValue,
                                        lastName: curentLastNameValue,
                                    },
                                    forceCreateContact: true,
                                    message: { text: textToChat },
                                }).then(function(value) {
                                    console.log(value);
                                    $(currentForm).find('input, textarea').val('');

                                    window.dataLayer.push({
                                        'event': 'worldCupSent',
                                        'eventCallback': function() {
                                            console.log("Event has fired");
                                            window.location.href = window.location.origin + '/worldcup-thanks';
                                        }
                                    });
                                });

                            } else {
                                console.log('form update user');

                                window.submitForm({
                                    formType: 'worldcup',
                                    payload: {
                                        emails: curentEmailValue,
                                        phones: curentPhoneValue,
                                        tags: tagsForWorldcup,
                                        firstName: curentFirstNameValue,
                                        lastName: curentLastNameValue,
                                        refId: contactIdCode,
                                        formTagWorldcup: true,
                                        answer: $('.input-goal').val(),
                                        agreeToTerms: agreeToTerms,
                                        agreeToMarketingUpdates: agreeToMarketing,

                                    },
                                    contactInfo: {
                                        emails: [{address: curentEmailValue}],
                                        phones: [{phoneNumber: curentPhoneValue, phoneTypeId: 'MOBILE'}],
                                        tags: tagsForWorldcup,
                                        firstName: curentFirstNameValue,
                                        lastName: curentLastNameValue,
                                    },
                                    message: { text: textToChat },
                                    // forceCreateContact: true,
                                }).then(function(value) {
                                    console.log(value);
                                    localStorage.worldcupFormWasFilled = true;
                                    $(currentForm).find('input, textarea').val('');

                                    window.dataLayer.push({
                                        'event': 'worldCupSent',
                                        'eventCallback': function() {
                                            console.log("Event has fired");
                                            window.location.href = window.location.origin + '/worldcup-thanks';
                                        }
                                    });


                                });


                            }



                        }
                    }

                    return false;
                });
            }

        }

        worldcupForm();

        //remove error on focus
        $(document).on('focus', "form input, form textarea, form select", function () {
            $(this).removeClass('status-error');
        });

        $('.input-method').click(function () {
            $(this).removeClass('status-error');
        });

        //allow only digits in phone field
        $('.input-phone').keyup(function () {
            this.value = this.value.replace(/[^0-9\+.]/g,'');
        });

        function isEmail(email) {
            let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email.val());
        }

        function isPhoneNubmer(phone) {
            let regex = /^\(?([0-9]{2})\)?[-. ]?([0-9]{2})[-. ]?([0-9]{4})$/;
            return regex.test(phone.val());
        }

        function notEmpty(inputElement) {
            return inputElement.val() !== "";
        }

    }

};



module.exports = worldcup;