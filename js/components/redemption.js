const contest_redemption = {

    init: function() {
        var that = this;
        that.submitFromAndValidation();
    },

    submitFromAndValidation(){
        $("#WWKredemption-name").focusin(function() {
            $('#WWKredemption-name').removeClass("status-error");
            $('#WWKredemption-name').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#WWKredemption-phone").focusin(function() {
            $('#WWKredemption-phone').removeClass("status-error");
            $('#WWKredemption-phone').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#WWKredemption-email").focusin(function() {
            $('#WWKredemption-email').removeClass("status-error");
            $('#WWKredemption-email').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#WWKredemption-phone").on('input', function(){
            if (this.value.length > 8) {
                this.value = this.value.slice(0,8); 
            }
        });
 
        // Get an instance of `PhoneNumberUtil`
        const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

        $("#WWKredemption-phone").on('change', function () {
            var phoneValue = $(this).val();
            if(phoneValue.length < 8) {
                $('#WWKredemption-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#WWKredemption-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Invalid phone format');
            } 

            var phoneNumber = '+65' + phoneValue;
            var number = phoneUtil.parseAndKeepRawInput(phoneNumber, 'SG');
            var isvalidNumber = phoneUtil.isValidNumberForRegion(number, 'SG');

            if(!isvalidNumber) {
                $('#WWKredemption-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#WWKredemption-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Invalid phone format');
            }
        });

        $('#WWKredemption-name').keyup(function () {
            this.value = this.value.replace(/[^A-Za-z+ -]/g,'');
        });

        $('#WWKredemption-name').on('change', function () {
            this.value = this.value.replace(/ $/,"");
        });

        $('#WWKAcknowledge').click(function(){
            if($(this).is(':checked')){
                $('#WWKAcknowledge').parents('.news-latter-opt-option').removeClass("check-false");
            } else {
                $('#WWKAcknowledge').parents('.news-latter-opt-option').addClass("check-false");
            }
        });

        var moment = require('moment-timezone');
     
        $("#button-submit-WWKredemption").click(function(e) {
            e.preventDefault();  
            var check_fullname = $('#WWKredemption-name').val();
            var check_phone = $('#WWKredemption-phone').val();
            var check_email = $('#WWKredemption-email').val();
            var check_acknowledge_checkbox = $('#form-WWKredemption').find('#WWKAcknowledge');
            var check_marketing_checkbox = $('#form-WWKredemption').find('#WWKredemptionNewsLetter');
            var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.)+(?:[A-z]{2}|com|org|net|gov|mil|edu|biz|info|mobi|name|aero|jobs|museum)\b$/;
            var reg_name = /^(?:[A-Za-z]+[ -])*[A-Za-z]+$/;

            if(check_fullname == '' && !reg_name.test(check_fullname)){
                $('#WWKredemption-name').addClass("status-error");
                if(!$('.error-label-name').length){
                    $('#WWKredemption-name').parents('.form-field-inner').append('<div class="error-label error-label-name"></div>');
                };
                $('.error-label-name').text('Name is required');
            };

            if(check_phone == ''){
                $('#WWKredemption-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#WWKredemption-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Phone is required');             
            };

            if (check_email == '') {   
                $('#WWKredemption-email').addClass("status-error");
                if(!$('.error-label-email').length){
                    $('#WWKredemption-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                }
                $('.error-label-email').text('Email is required');
            } else {
                if(!reg_email.test(check_email)){                    
                    $('#WWKredemption-email').addClass("status-error");
                    if(!$('.error-label-email').length){
                        $('#WWKredemption-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                    }                    
                    $('.error-label-email').text('Invalid email format');
                }
            };

            if(check_acknowledge_checkbox.is(":not(:checked)")){
                check_acknowledge_checkbox.parents('.news-latter-opt-option').addClass("check-false");
            };        
            
            var phoneNumber = '+65' + check_phone;
            var number = phoneUtil.parseAndKeepRawInput(phoneNumber, 'SG');
            var isvalidNumber = phoneUtil.isValidNumberForRegion(number, 'SG'); 

            if(!isvalidNumber) {
                $('#WWKredemption-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#WWKredemption-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Invalid phone format');
            }
 
            if((!$('.status-error').length) && check_fullname != "" && check_email != "" && check_phone != "" && isvalidNumber && check_acknowledge_checkbox.is(":checked")){    
                function capitalize(s){
                    return s.toLowerCase().replace( /\b./g, function(a){ 
                        return a.toUpperCase(); 
                    });
                };

                var $phone = $('#WWKredemption-phone');
                var $email = $('#WWKredemption-email');
                var formTitle = $('.form-title').text().replace(/ $/,"");
                var formTitleType = formTitle.replace(/ /g,"-").replace(/&/g,"and").toLowerCase();
                var submitDateTime = moment().tz('Asia/Singapore').format('DD/MM/YYYY HH:mm:ss');
                var submitDate = moment().tz('Asia/Singapore').format('DD/MM/YYYY');
                var phone = $phone.val();
                var email = $email.val().toLowerCase();
                var name = $('#WWKredemption-name').val();
                var fullname = capitalize(name);
                var acknowledge_consent_bool = false;
                var acknowledge_consent = "No";
                var marketing_consent_bool = false;
                var marketing_consent = "No";
        
                if(check_marketing_checkbox.is(":checked")){
                    marketing_consent_bool = true;
                    marketing_consent = "Yes";
                }

                if(check_acknowledge_checkbox.is(":checked")){
                    acknowledge_consent_bool = true;
                    acknowledge_consent = "Yes";
                }

                console.log("Name: "+fullname+" \n" + "Phone: "+phone+" \n" + "Email: "+email+" \n" + "Date/Time of Submission: "+submitDateTime+" \n");
                console.log("Acknowledge Consent: " + acknowledge_consent+" \n" + "Marketing Consent: " + marketing_consent);
    
                var payload = {
                    "name": fullname,
                    "phone": phone,
                    "email": email,  
                    "formtitle": formTitle,
                    "AcknowledgeConsent": acknowledge_consent,
                    "MarketingConsent": marketing_consent,
                    "SubmitDate": submitDate,
                    "SubmitDateTime": submitDateTime
                }
                        
                var text =
                    "Incentive Acknowledgement Submission\n" +
                    "Name: "+fullname+" \n" +
                    "Phone: "+phone+" \n" +
                    "Email: "+email+" \n" + 
                    "Acknowledgement: "+acknowledge_consent+" \n" + 
                    "Marketing Consent: "+marketing_consent+" \n" + 
                    "Date/Time of Submission: "+submitDateTime+" \n";
                
                window.submitForm({
                    formType: formTitleType,
                    payload: payload,
                    contactInfo: {
                        firstName: fullname,
                        emails: [{
                            address: email,
                        }],
                        phones: [{   
                            phoneNumber: phone,
                            phoneTypeId: "MOBILE",
                        }],
                        tags: [formTitleType],
                        opt_in: {
                            is_email_allowed: marketing_consent_bool,
                            is_sms_allowed: marketing_consent_bool,
                        }
                    },                                        
                    forceCreateContact: true,
                    message: {
                        text: text
                    }
                }).then(() => {
                    $('#WWKredemption-name').val("");
                    $('#WWKredemption-phone').val("");
                    $('#WWKredemption-email').val("");
                    $('#WWKAcknowledge').prop('checked', false);
                    $('#WWKredemptionNewsLetter').prop('checked', false);
                    
                    console.log("Information submitted to Backoffice.");
                    $('#form-WWKredemption').fadeOut();
                    setTimeout(() => {    
                        $('#form-thank-WWKredemption').fadeIn();    
                    }, 500);
                });
            };  
        });
    },

};

module.exports = contest_redemption;
