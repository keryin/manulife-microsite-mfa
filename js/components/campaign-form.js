const campaign_form = {

    init: function() {
        var that = this;
        that.showQuesFlow();
        that.showTermsCondition();
        that.submitFromAndValidation();

        $(document).on('click', '.button-close-banner', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $('.campaign-sticky-banner').fadeOut( "slow", "swing", function() {});
        });
    },

    showQuesFlow() {      
        function updateImage(srcD, srcT, srcM) {
            let imgSrc = "https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/production/mfa/images/campaigns/";
            let isIE11 = !!window.MSInputMethodContext && !!document.documentMode;    
            let imgURLDesktop = imgSrc+srcD;              
            let imgURLTablet = imgSrc+srcT;            
            let imgURLMobile = imgSrc+srcM;            
                                                     
            
            $(".cover-image-wrapper.hide-mobile img").attr("src", imgURLDesktop);
            $(".cover-image-wrapper.hide-tablet img").attr("src", imgURLTablet);
            $(".cover-image-wrapper.hide-desktop img").attr("src", imgURLMobile);

            if (isIE11) {
                $('.cover-image-wrapper.hide-mobile').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLDesktop + ')')
                });
                $('.cover-image-wrapper.hide-tablet').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLTablet + ')')
                });
                $('.cover-image-wrapper.hide-desktop').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLMobile + ')')
                });
            }     
        };

        $('.button-show-campaign-question').on('click', function(e){
            // $('.banner-top-caption').addClass('d-none');
            updateImage('be-unbroken/manulife-unbroken-Q1-1920x526.jpg', 'be-unbroken/manulife-unbroken-Q1-1600x800.jpg', 'be-unbroken/manulife-unbroken-Q1-800x400.jpg');
            $('.campaign-form-title-wrapper').fadeOut();
            $('.block-progress-bar').fadeIn();

            setTimeout(() => {
                $(".question-item:first-child").addClass("status-active");
                $('html, body').animate({
                    scrollTop: 0
                }, 1000);
            }, 500);        
        });
    },

    showTermsCondition() {
        $(document).ready(function() {
            if (window.location.href.indexOf("tnc") > -1) {
                $("#term-modal").fadeIn("slow");
                $("body").css("overflow", "hidden");
                window.history.pushState('', 'Title', ' ');
            }
        });

        $('.showTerms').on('click', function(e){
            $("#term-modal").fadeIn("slow");
            $("body").css("overflow", "hidden");
        });

        $('.showConsent').on('click', function(e){
            $("#consent-modal").fadeIn("slow");
            $("body").css("overflow", "hidden");
        });

        $('.close-modal-btn').on('click', function(e){
            $("#term-modal").fadeOut("slow");
            $("#consent-modal").fadeOut("slow");
            $("body").css("overflow", "");
        });
    },

    submitFromAndValidation(){
        function showResult(){
            var formTitle = $('.form-title').text().replace(/ $/,"");
            var formTitleType = formTitle.replace(/ /g,"-").replace(/&/g,"and").toLowerCase();
            var resultScoreMark = $('.result-score-mark').text();        
            var currentLink = window.location.href.split(formTitleType)[0];
            var currentLinkForm = currentLink + formTitleType;
            var resultLink, resultTitle, resultShareDescrip;
            // var resultTitle = [$('.result-1').find('.title').text(),
            // $('.result-2').find('.title').text(),
            // $('.result-3').find('.title').text()];

            if($('#be-unbroken-form').length){
                resultLink = [currentLinkForm + "-moderate", currentLinkForm + "-very-prepared", currentLinkForm + "-least-prepared"]; 
                resultTitle = ['Stand to win a $10 Starbucks voucher by completing the Be #Unbroken Survey. T&Cs apply.'];
                resultShareDescrip = ['Stand to win a $10 Starbucks voucher by completing the Be #Unbroken Survey. T&Cs apply.'];
            }

            if(resultScoreMark == 1) {
                addthis.update('share', 'url', resultLink[0]); 
                addthis.update('share', 'title', resultTitle[0]);
                addthis.update('share', 'description', resultShareDescrip[0]);
                addthis.url = resultLink[0]; 
            } else if(resultScoreMark == 2) {
                addthis.update('share', 'url', resultLink[1]); 
                addthis.update('share', 'title', resultTitle[0]);
                addthis.update('share', 'description', resultShareDescrip[0]);
                addthis.url = resultLink[1]; 
            } else if(resultScoreMark == 3) {
                addthis.update('share', 'url', resultLink[2]); 
                addthis.update('share', 'title', resultTitle[0]);
                addthis.update('share', 'description', resultShareDescrip[0]);
                addthis.url = resultLink[2]; 
            }
        };

        function updateImage(srcD, srcT, srcM) {
            let imgSrc = "https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/production/mfa/images/campaigns/";
            let isIE11 = !!window.MSInputMethodContext && !!document.documentMode;    
            let imgURLDesktop = imgSrc+srcD;              
            let imgURLTablet = imgSrc+srcT;            
            let imgURLMobile = imgSrc+srcM;            
                                                     
            
            $(".cover-image-wrapper.hide-mobile img").attr("src", imgURLDesktop);
            $(".cover-image-wrapper.hide-tablet img").attr("src", imgURLTablet);
            $(".cover-image-wrapper.hide-desktop img").attr("src", imgURLMobile);

            if (isIE11) {
                $('.cover-image-wrapper.hide-mobile').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLDesktop + ')')
                });
                $('.cover-image-wrapper.hide-tablet').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLTablet + ')')
                });
                $('.cover-image-wrapper.hide-desktop').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLMobile + ')')
                });
            }     
        };

        $("#campaign-form-name").focusin(function() {
            $('#campaign-form-name').removeClass("status-error");
            $('#campaign-form-name').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#campaign-form-phone").focusin(function() {
            $('#campaign-form-phone').removeClass("status-error");
            $('#campaign-form-phone').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#campaign-form-email").focusin(function() {
            $('#campaign-form-email').removeClass("status-error");
            $('#campaign-form-email').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#campaign-form-phone").on('input', function(){
            if (this.value.length > 8) {
                this.value = this.value.slice(0,8); 
            }
        });
 
        // Get an instance of `PhoneNumberUtil`
        const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

        $("#campaign-form-phone").on('change', function () {
            var phoneNumber = '+65' + $(this).val();
            var number = phoneUtil.parseAndKeepRawInput(phoneNumber, 'SG');
            var isvalidNumber = phoneUtil.isValidNumberForRegion(number, 'SG');

            if(!isvalidNumber) {
                $('#campaign-form-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#campaign-form-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Invalid phone format');
            }
        });

        $('#campaign-form-name').keyup(function () {
            this.value = this.value.replace(/[^A-Za-z+ -]/g,'');
        });

        $('#campaign-form-name').on('change', function () {
            this.value = this.value.replace(/ $/,"");
        });

        $('#surveyNewsLetter').click(function(){
            if($(this).is(':checked')){
                $('#surveyNewsLetter').parents('.news-latter-opt-option').removeClass("check-false");
            } else {
                $('#surveyNewsLetter').parents('.news-latter-opt-option').addClass("check-false");
            }
        });

        $.getJSON("https://api.ipify.org?format=jsonp&callback=?", function (data) {
            if (data) {
                var campaignUserIP = window.location.href + '&ip=' + data.ip;                
                $("#ExternalId").val(campaignUserIP);
            }
        });

        var moment = require('moment-timezone');
        var checkDate = moment().tz('Asia/Singapore').format('YYYY-MM-DD');
        
        $(document).ready(function () {
            var getLocalDate = localStorage.getItem('campaignGetThisDate');
            if (getLocalDate !== checkDate) {
                localStorage.removeItem('campaignGetThisDate');
                localStorage.removeItem('campaignSubmitCount');
            }
        });
     
        $("#button-submit-campaign-form").click(function(e) {
            e.preventDefault();  
            var check_fullname = $('#campaign-form-name').val();
            var check_phone = $('#campaign-form-phone').val();
            var check_email = $('#campaign-form-email').val();
            var check_marketing_checkbox = $('#form-campaign-form').find('.general-checkbox');
            var check_IP = $("#ExternalId").val();
            var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.)+(?:[A-z]{2}|com|org|net|gov|mil|edu|biz|info|mobi|name|aero|jobs|museum)\b$/;
            var reg_name = /^(?:[A-Za-z]+[ -])*[A-Za-z]+$/;

            if(check_fullname == '' && !reg_name.test(check_fullname)){
                $('#campaign-form-name').addClass("status-error");
                if(!$('.error-label-name').length){
                    $('#campaign-form-name').parents('.form-field-inner').append('<div class="error-label error-label-name"></div>');
                };
                $('.error-label-name').text('Name is required');
            };

            if(check_phone == ''){
                $('#campaign-form-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#campaign-form-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Phone is required');             
            };
            
            if (check_email == '') {   
                $('#campaign-form-email').addClass("status-error");
                if(!$('.error-label-email').length){
                    $('#campaign-form-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                }
                $('.error-label-email').text('Email is required');
            } else {
                if(!reg_email.test(check_email)){                    
                    $('#campaign-form-email').addClass("status-error");
                    if(!$('.error-label-email').length){
                        $('#campaign-form-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                    }                    
                    $('.error-label-email').text('Invalid email format');
                }
            };

            if(check_marketing_checkbox.is(":not(:checked)")){
                check_marketing_checkbox.parents('.news-latter-opt-option').addClass("check-false");
            };

            var getSubmitCount = localStorage.getItem('campaignSubmitCount');

            if(+getSubmitCount >= 10){
                console.log("Submitted more than 10 times");
                $('#button-submit-campaign-form').addClass('disable-btn');
                $(".multiplesubmission").html('You’ve submitted multiple times, please try again later.<br/>Alternatively, you may contact us at {{websiteMainUser.mainEmail}}.').show();
            }            
            
            var phoneNumber = '+65' + check_phone;
            var number = phoneUtil.parseAndKeepRawInput(phoneNumber, 'SG');
            var isvalidNumber = phoneUtil.isValidNumberForRegion(number, 'SG'); 

            if(!isvalidNumber) {
                $('#campaign-form-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#campaign-form-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Invalid phone format');
            }

            // if((!$('.status-error').length) && check_fullname != "" && check_email != "" && check_phone != "" && reg_email.test(check_email) && isvalidNumber && check_marketing_checkbox.is(":checked") && (+getSubmitCount < 10)){  
            if((!$('.status-error').length) && check_fullname != "" && check_email != "" && check_phone != "" && reg_email.test(check_email) && isvalidNumber && check_marketing_checkbox.is(":checked") && (+getSubmitCount < 10)){    
                showResult();
                $("#loadingscreen").show();
                var addThisDate = moment().tz('Asia/Singapore').format('YYYY-MM-DD');
    
                localStorage.setItem("campaignUserIP", check_IP); 
                if (getSubmitCount) {
                    getSubmitCount = +getSubmitCount + 1
                } else {
                    getSubmitCount = '1'
                }
                
                localStorage.setItem('campaignSubmitCount', getSubmitCount);
                localStorage.setItem('campaignGetThisDate', addThisDate);

                // Failed submission due to unknown error
                setTimeout(function(){ 
                    if(!$('#button-submit-campaign-form').hasClass("disable-btn")){
                        $('.block-quiz-result .block-button').hide();
                        $("#loadingscreen").hide();
                        $(".submission-error").text('Opps something went wrong, please try submitting again. ').show();
                        console.log('Setimeout: Opps something went wrong, please try submitting again. ')
                        $('#button-submit-campaign-form').addClass("disable-btn");
                    }
                }, 5000);

                function capitalize(s){
                    return s.toLowerCase().replace( /\b./g, function(a){ 
                        return a.toUpperCase(); 
                    });
                };

                var $phone = $('#campaign-form-phone');
                var $email = $('#campaign-form-email');
                var formTitle = $('.form-title').text().replace(/ $/,"");
                var formTitleType = formTitle.replace(/ /g,"-").replace(/&/g,"and").toLowerCase();
                var submitDateTime = moment().tz('Asia/Singapore').format('DD/MM/YYYY HH:mm:ss');
                var submitDate = moment().tz('Asia/Singapore').format('DD/MM/YYYY');
                var phone = $phone.val();
                var email = $email.val().toLowerCase();
                var name = $('#campaign-form-name').val();
                var fullname = capitalize(name);
                let quizSummaryMessage = 'Survey Summary:\n';
                let finalResult = $('.score-active').find('.title').text();

                $('.question-item').each(function (index) {
                    quizSummaryMessage += `${$(this).find('.title').text()}\nAnswer: ${$(this).find('.answer-option.status-selected span').text().replace('<', 'less than ')}\n`
                });

                var hashed_phone = sha256(phone);
                var hashed_email = sha256(email);
                var marketing_consent_bool = false;
                var marketing_consent = "No";
                var eligibleStatus = "Eligible: No";
                var url = window.location.href;
                var splitURL = url.split("/");
                var webLink = splitURL[2];
                
                var object = new Object();
                object.email = hashed_email;
                object.phone = hashed_phone;
                object.weblink = webLink;
                var myJsonData = JSON.stringify(object);
        
                if(check_marketing_checkbox.is(":checked")){
                    marketing_consent_bool = true;
                    marketing_consent = "Yes";
                }

                console.log(hashed_phone,hashed_email);
                console.log(quizSummaryMessage+"\n" + "My Final Result: \n"+finalResult);
                console.log("Name: "+fullname+" \n" + "Phone: "+phone+" \n" + "Email: "+email+" \n" + "IP: "+check_IP+" \n" + "Date/Time of Submission: "+submitDateTime+" \n");
                console.log("Marketing Consent: " + marketing_consent);
                console.log(myJsonData);
                function sha256(ascii) {
                    function rightRotate(value, amount) {
                        return (value>>>amount) | (value<<(32 - amount));
                    };
                    var mathPow = Math.pow;
                    var maxWord = mathPow(2, 32);
                    var lengthProperty = 'length'
                    var i, j; 
                    var result = ''
                    var words = [];
                    var asciiBitLength = ascii[lengthProperty]*8;
                    var hash = sha256.h = sha256.h || [];
                    var k = sha256.k = sha256.k || [];
                    var primeCounter = k[lengthProperty];
                    var isComposite = {};
    
                    for (var candidate = 2; primeCounter < 64; candidate++) {
                        if (!isComposite[candidate]) {
                            for (i = 0; i < 313; i += candidate) {
                                isComposite[i] = candidate;
                            }
                            hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
                            k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
                        }
                    }
                
                    ascii += '\x80'
                    while (ascii[lengthProperty]%64 - 56) ascii += '\x00' 
                    for (i = 0; i < ascii[lengthProperty]; i++) {
                        j = ascii.charCodeAt(i);
                        if (j>>8) return; 
                        words[i>>2] |= j << ((3 - i)%4)*8;
                    }
    
                    words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
                    words[words[lengthProperty]] = (asciiBitLength)
    
                    for (j = 0; j < words[lengthProperty];) {
                        var w = words.slice(j, j += 16); 
                        var oldHash = hash;
                        hash = hash.slice(0, 8);
                        
                        for (i = 0; i < 64; i++) {
                            var i2 = i + j;
                            var w15 = w[i - 15], w2 = w[i - 2];
                            var a = hash[0], e = hash[4];
                            var temp1 = hash[7]
                                + (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
                                + ((e&hash[5])^((~e)&hash[6])) // ch
                                + k[i]
                                + (w[i] = (i < 16) ? w[i] : (
                                        w[i - 16]
                                        + (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
                                        + w[i - 7]
                                        + (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
                                    )|0
                                );
                            var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
                                + ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj
                            hash = [(temp1 + temp2)|0].concat(hash); 
                            hash[4] = (hash[4] + temp1)|0;
                        }
                    
                        for (i = 0; i < 8; i++) {
                            hash[i] = (hash[i] + oldHash[i])|0;
                        }
                    }
                    
                    for (i = 0; i < 8; i++) {
                        for (j = 3; j + 1; j--) {
                            var b = (hash[i]>>(j*8))&255;
                            result += ((b < 16) ? 0 : '') + b.toString(16);
                        }
                    }
                    return result;
                };

                function formatErrorMessage(jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        return ('Status Code: ' + jqXHR.status + ', Error Message: Not connected. Please verify your network connection. ');
                    } else if (jqXHR.status == 404) {
                        return ('Status Code: ' + jqXHR.status + ', Error Message: The requested page not found. [404]');
                    } else if (jqXHR.status == 500) {
                        return ('Status Code: ' + jqXHR.status + ', Error Message: Internal Server Error [500].');
                    } else if (exception === 'parsererror') {
                        return ('Exception: parsererror, Error Message: Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        return ('Exception: timeout, Error Message: Time out error.');
                    } else if (exception === 'abort') {
                        return ('Exception: abort, Error Message: Ajax request aborted.');
                    } else {
                        return ('Error Message: Uncaught Error. ' + jqXHR.responseText);
                    }
                }
    
                $.ajax({
                    type: 'POST',
                    url: 'https://qte7t17013.execute-api.ap-southeast-1.amazonaws.com/ml-campaign5/submit',
                    data: myJsonData,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        var jsonBody = (data.body) ? data.body : "";
                        var body = (jsonBody) ? JSON.parse(jsonBody) : "";
                        var msg = (body) ? body.Message : "";
                        var entryId = (body) ? body.entryId : "";
                        var participantNo = (body) ? body.ParticipantNo : "";
                        var webParticipantNo = (body) ? body.WebParticipantNo : "";

                        if (data) {
                            if (data.statusCode == 200) {
                                console.log('Response:', msg);
                                // console.log('entryId', entryId);
                                // console.log('participantNo', participantNo);
                                // console.log('webParticipantNo', webParticipantNo);    
                
                                var payload = {
                                    "name": fullname,
                                    "phone": phone,
                                    "email": email,  
                                    "formtitle": formTitle,
                                    "participantno": participantNo,
                                    "webParticipantNo": webParticipantNo,
                                    "Summary": quizSummaryMessage, 
                                    "FinalResult": finalResult, 
                                    "MarketingConsent": marketing_consent,
                                    "SubmitDateTime": submitDate,
                                    "IP": check_IP,
                                    "webURL": url
                                }
                                        
                                var text =
                                    "You have 30 days from "+submitDate+" to get in touch with this prospect.\n" +
                                    "Index number of lead: "+webParticipantNo+" \n" +
                                    "Name: "+fullname+" \n" +
                                    "Phone: "+phone+" \n" +
                                    "Email: "+email+" \n" + 
                                    ""+quizSummaryMessage+
                                    "Result: "+finalResult+" \n" +
                                    "Marketing Consent: "+marketing_consent+" \n" + 
                                    "Date/Time of Submission: "+submitDate+" \n" +                        
                                    "URL: "+ url+" \n";
                
                                    try {
                                        window.submitForm({
                                            formType: formTitleType,
                                            payload: payload,
                                            contactInfo: {
                                                firstName: fullname,
                                                emails: [{
                                                    address: email,
                                                }],
                                                phones: [{   
                                                    phoneNumber: phone,
                                                    phoneTypeId: "MOBILE",
                                                }],
                                                tags: [formTitleType],
                                                opt_in: {
                                                    is_email_allowed: marketing_consent_bool,
                                                    is_sms_allowed: marketing_consent_bool,
                                                }
                                            },                                        
                                            forceCreateContact: true,
                                            message: {
                                                text: text
                                            }
                                        }).then(() => {
                                            try {
                                                $('.block-quiz-result .block-button').hide();
                                                $("#loadingscreen").hide();
                                                $('#campaign-form-name').val("");
                                                $('#campaign-form-phone').val("");
                                                $('#campaign-form-email').val("");
                                                $('#surveyNewsLetter').prop('checked', false);
                                                updateImage('be-unbroken/manulife-unbroken-thank-you-1920x526.jpg', 'be-unbroken/manulife-unbroken-thank-you-1600x800.jpg', 'be-unbroken/manulife-unbroken-thank-you-800x400.jpg');

                                                console.log("Information submitted to Backoffice. Go to result page.");
                                                $('#form-campaign-form').fadeOut();
                                                $('.score-active').fadeOut();
                                                setTimeout(() => {    
                                                    $('#form-thank-campaign-form').fadeIn();                                
                                                    $('html, body').animate({
                                                        scrollTop: 0
                                                    }, 1000);
                                                }, 500);
                                            } catch (err) {
                                                $('.block-quiz-result .block-button').hide();
                                                $("#loadingscreen").hide();
                                                $('#button-submit-campaign-form').addClass("disable-btn");
                                                $(".submission-error").text('Sorry, we fail to submit your information. Please try again later.').show();
                                                console.log("Failed to go thank you page.");
                                                dataLayer.push({
                                                    'event': 'error',
                                                    // the message that you show in your console.log
                                                    'errorMessage': 'Failed to go thank you page.'
                                                });
                                            }
                                        });
                                    }
                                    catch(err) {
                                        $('.block-quiz-result .block-button').hide();
                                        $("#loadingscreen").hide();
                                        $('#button-submit-campaign-form').addClass("disable-btn");
                                        $(".submission-error").text('Opps something went wrong, please try submitting again. ').show();
                                        console.log("submission to gefen error.");
                                        dataLayer.push({
                                            'event': 'error',
                                            // the message that you show in your console.log
                                            'errorMessage': 'submission to gefen error.'
                                        });

                                        var objectdelete = new Object();
                                        objectdelete.email = hashed_email;
                                        objectdelete.phone = hashed_phone;
                                        var myDeleteData = JSON.stringify(objectdelete);

                                        $.ajax({
                                            type: 'POST',
                                            url: 'https://qte7t17013.execute-api.ap-southeast-1.amazonaws.com/ml-campaign5/delete',
                                            data: myDeleteData,
                                            contentType: 'application/json; charset=utf-8',
                                            dataType: 'json',
                                            success: function (data) {
                                                var jsonBody = (data.body) ? data.body : "";
                                                var body = (jsonBody) ? JSON.parse(jsonBody) : "";
                                                var msg = (body) ? body.Message : "";

                                                if (data) {
                                                    if (data.statusCode == 200) {
                                                        console.log('Response:', msg);
                                                    } else {
                                                        console.log('Status Code ' + data.statusCode + ' Error Message: ' + msg);                                                        
                                                        dataLayer.push({
                                                            'event': 'error',
                                                            // the message that you show in your console.log
                                                            'errorMessage': 'Status Code ' + data.statusCode + ' Error Message: ' + msg
                                                        });
                                                    }
                                                } else {
                                                    console.log("Invalid data return by API Delete.");
                                                    console.log('Status Code ' + data.statusCode + ' Error Message: ' + msg);      
                                                    dataLayer.push({
                                                        'event': 'error',
                                                        // the message that you show in your console.log
                                                        'errorMessage': 'Status Code ' + data.statusCode + ' Error Message: ' + msg
                                                    });
                                                }
                                            },
                                            error: function(xhr, err) {
                                                var errorMessage = formatErrorMessage(xhr, err);
                                                console.log(errorMessage);  
                                                console.log('Error in API Post delete.');         
                                                dataLayer.push({
                                                    'event': 'error',
                                                    // the message that you show in your console.log
                                                    'errorMessage': errorMessage
                                                });
                                            }
                                        });
                                    } 

                            } else {
                                $('.block-quiz-result .block-button').hide();
                                $("#loadingscreen").hide();
                                $('#button-submit-campaign-form').addClass("disable-btn");                                
                                $(".submission-error").html("Oops, looks like you've already submitted an entry.<br/>Only one (1) entry is allowed per person.").show();
                                console.log('Status Code ' + data.statusCode + ' Error Message: ' + msg);                                                      
                                dataLayer.push({
                                    'event': 'error',
                                    // the message that you show in your console.log
                                    'errorMessage': 'Status Code ' + data.statusCode + ' Error Message: ' + msg
                                });
                            }
                        } else {
                            $('.block-quiz-result .block-button').hide();
                            $("#loadingscreen").hide();
                            $('#button-submit-campaign-form').addClass("disable-btn");
                            $(".submission-error").text('Opps something went wrong, please try submitting again. ').show();
                            console.log("Invalid data return by API.");                                                
                            console.log('Status Code ' + data.statusCode + ' Error Message: ' + msg);                                                 
                            dataLayer.push({
                                'event': 'error',
                                // the message that you show in your console.log
                                'errorMessage': 'Status Code ' + data.statusCode + ' Error Message: ' + msg
                            });
                        }
                    },
                    error: function(xhr, err) {
                        var errorMessage = formatErrorMessage(xhr, err);                        
                        console.log('Error with API POST');  
                        console.log(errorMessage);           
                        dataLayer.push({
                            'event': 'error',
                            // the message that you show in your console.log
                            'errorMessage': errorMessage
                        });
                    }
                });
            };  
        });
    },

};

module.exports = campaign_form;
