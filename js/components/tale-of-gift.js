const tale_of_gift_form = {

    init: function() {
        var that = this;
        that.showQuesFlow();
        that.showTermsCondition();
        that.submitFromAndValidation();

        $(document).on('click', '.button-close-banner', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $('.campaign-sticky-banner').fadeOut( "slow", "swing", function() {});
        });
    },

    showQuesFlow() {
        $('.button-show-gift-question').on('click', function(e){
            $('.tale-gift-title-wrapper').fadeOut();
            setTimeout(() => {
                $(".question-item:first-child").addClass("status-active");
                if(window.innerWidth > 600){                    
                    $('html, body').animate({
                        scrollTop: $('.status-active').offset().top - 80
                    }, 1000);
                } else {
                    $('html, body').animate({
                        scrollTop: $('.status-active').offset().top - 30
                    }, 1000);
                }
            }, 500);        
        });
    },

    showTermsCondition() {
        $(document).ready(function() {
            if (window.location.href.indexOf("tnc") > -1) {
                $("#term-modal").fadeIn("slow");
                $("body").css("overflow", "hidden");
                window.history.pushState('', 'Title', ' ');
            }
        });

        $('.showTerms').on('click', function(e){
            $("#term-modal").fadeIn("slow");
            $("body").css("overflow", "hidden");
        });

        $('.showConsent').on('click', function(e){
            $("#consent-modal").fadeIn("slow");
            $("body").css("overflow", "hidden");
        });

        $('.close-modal-btn').on('click', function(e){
            $("#term-modal").fadeOut("slow");
            $("#consent-modal").fadeOut("slow");
            $("body").css("overflow", "");
        });
    },

    submitFromAndValidation(){
        function showResult(){
            var resultScoreMark = $('.result-score-mark').text();        
            var currentLink = window.location.href.split('a-tale-of-gifts-and-greetings')[0];
            var resultLink = [currentLink + "a-tale-of-gifts-and-greetings-santa", 
            currentLink + "a-tale-of-gifts-and-greetings-reindeer", 
            currentLink + "a-tale-of-gifts-and-greetings-elf"]; 
            var resultTitle = ["Ho! Ho! Ho! I'm a Santa! How about you? Take the quiz: ",
            "I'm Rudolph the Reindeer! How about you? Take the quiz: ",
            "I'm a Christmas Elf! How about you? Take the quiz: "];
            var resultShareDescrip = ["Christmas season is almost upon us and you know what that means – office parties, Secret Santa exchanges, feasting and shopping for gifts! Tell us how you would respond to these holiday season scenarios and receive a $10 Starbucks voucher if you are one of the first 1000 eligible participants! T&Cs apply."]

            if(resultScoreMark == 1) {
                addthis.update('share', 'url', resultLink[0]); 
                addthis.update('share', 'title', resultTitle[0]);
                addthis.update('share', 'description', resultShareDescrip[0]);
                addthis.url = resultLink[0]; 
            } else if(resultScoreMark == 2) {
                addthis.update('share', 'url', resultLink[1]); 
                addthis.update('share', 'title', resultTitle[1]);
                addthis.update('share', 'description', resultShareDescrip[0]);
                addthis.url = resultLink[0]; 
            } else if(resultScoreMark == 3) {
                addthis.update('share', 'url', resultLink[2]); 
                addthis.update('share', 'title', resultTitle[2]);
                addthis.update('share', 'description', resultShareDescrip[0]);
                addthis.url = resultLink[0]; 
            }
        };

        $("#tale-gift-name").focusin(function() {
            $('#tale-gift-name').removeClass("status-error");
            $('#tale-gift-name').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#tale-gift-phone").focusin(function() {
            $('#tale-gift-phone').removeClass("status-error");
            $('#tale-gift-phone').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#tale-gift-email").focusin(function() {
            $('#tale-gift-email').removeClass("status-error");
            $('#tale-gift-email').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#tale-gift-phone").on('input', function(){
            if (this.value.length > 8) {
                this.value = this.value.slice(0,8); 
            }
        });
 
        // Get an instance of `PhoneNumberUtil`
        const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

        $("#tale-gift-phone").on('change', function () {
            var phoneNumber = '+65' + $(this).val();
            var number = phoneUtil.parseAndKeepRawInput(phoneNumber, 'SG');
            var isvalidNumber = phoneUtil.isValidNumberForRegion(number, 'SG');

            if(!isvalidNumber) {
                $('#tale-gift-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#tale-gift-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Invalid phone format');
            }
        });

        $('#tale-gift-name').keyup(function () {
            this.value = this.value.replace(/[^A-Za-z+ -]/g,'');
        });

        $('#tale-gift-name').on('change', function () {
            this.value = this.value.replace(/ $/,"");
        });

        $('#surveyNewsLetter').click(function(){
            if($(this).is(':checked')){
                $('#surveyNewsLetter').parents('.news-latter-opt-option').removeClass("check-false");
            } else {
                $('#surveyNewsLetter').parents('.news-latter-opt-option').addClass("check-false");
            }
        });

        $.getJSON("https://api.ipify.org?format=jsonp&callback=?", function (data) {
            if (data) {
                var giftUserIP = window.location.href + '&ip=' + data.ip;                
                $("#ExternalId").val(giftUserIP);
            }
        });

        var moment = require('moment-timezone');
        var checkDate = moment().tz('Asia/Singapore').format('YYYY-MM-DD');
        
        $(document).ready(function () {
            var getLocalDate = localStorage.getItem('giftGetThisDate');
            if (getLocalDate !== checkDate) {
                localStorage.removeItem('giftGetThisDate');
                localStorage.removeItem('giftSubmitCount');
            }
        });
     
        $("#button-submit-tale-gift").click(function(e) {
            e.preventDefault();  
            var check_fullname = $('#tale-gift-name').val();
            var check_phone = $('#tale-gift-phone').val();
            var check_email = $('#tale-gift-email').val();
            var check_marketing_checkbox = $('#form-tale-gift').find('.general-checkbox');
            var check_IP = $("#ExternalId").val();
            var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.)+(?:[A-z]{2}|com|org|net|gov|mil|edu|biz|info|mobi|name|aero|jobs|museum)\b$/;
            var reg_name = /^(?:[A-Za-z]+[ -])*[A-Za-z]+$/;
            var phoneNumber = '+65' + check_phone;
            var number = phoneUtil.parseAndKeepRawInput(phoneNumber, 'SG');
            var isvalidNumber = phoneUtil.isValidNumberForRegion(number, 'SG');

            if(!isvalidNumber) {
                $('#tale-gift-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#tale-gift-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Invalid phone format');
            }

            if(check_fullname == '' && !reg_name.test(check_fullname)){
                $('#tale-gift-name').addClass("status-error");
                if(!$('.error-label-name').length){
                    $('#tale-gift-name').parents('.form-field-inner').append('<div class="error-label error-label-name"></div>');
                };
                $('.error-label-name').text('Name is required');
            };
            
            if (check_email == '') {   
                $('#tale-gift-email').addClass("status-error");
                if(!$('.error-label-email').length){
                    $('#tale-gift-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                }
                $('.error-label-email').text('Email is required');
            } else {
                if(!reg_email.test(check_email)){                    
                    $('#tale-gift-email').addClass("status-error");
                    if(!$('.error-label-email').length){
                        $('#tale-gift-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                    }                    
                    $('.error-label-email').text('Invalid email format');
                }
            };

            if(check_phone == ''){
                $('#tale-gift-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#tale-gift-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Phone is required');             
            };

            if(check_marketing_checkbox.is(":not(:checked)")){
                check_marketing_checkbox.parents('.news-latter-opt-option').addClass("check-false");
            };

            var getSubmitCount = localStorage.getItem('giftSubmitCount');

            // if(+getSubmitCount >= 10){
            //     console.log("Submitted more than 10 times");
            //     $('#button-submit-tale-gift').addClass('disable-btn');
            //     $(".multiplesubmission").html('You’ve submitted multiple times, please try again later.<br/>Alternatively, you may contact us at {{websiteMainUser.mainEmail}}.').show();
            // }

            // if((!$('.status-error').length) && check_fullname != "" && check_email != "" && check_phone != "" && reg_email.test(check_email) && isvalidNumber && check_marketing_checkbox.is(":checked") && (+getSubmitCount < 10)){  
            if((!$('.status-error').length) && check_fullname != "" && check_email != "" && check_phone != "" && reg_email.test(check_email) && isvalidNumber && check_marketing_checkbox.is(":checked")){    
                showResult();
                $("#loadingscreen").show();
                var addThisDate = moment().tz('Asia/Singapore').format('YYYY-MM-DD');
    
                localStorage.setItem("giftUserIP", check_IP); 
                // if (getSubmitCount) {
                //     getSubmitCount = +getSubmitCount + 1
                // } else {
                //     getSubmitCount = '1'
                // }
                
                localStorage.setItem('giftSubmitCount', getSubmitCount);
                localStorage.setItem('giftGetThisDate', addThisDate);

                // Failed submission due to unknown error
                setTimeout(function(){ 
                    if(!$('#button-submit-tale-gift').hasClass("disable-btn")){
                        $('.block-quiz-result .block-button').hide();
                        $("#loadingscreen").hide();
                        $(".submission-error").text('Sorry, there was an error. Please try again later.').show();
                        $('#button-submit-tale-gift').addClass("disable-btn");
                    }
                }, 4000);

                function capitalize(s){
                    return s.toLowerCase().replace( /\b./g, function(a){ 
                        return a.toUpperCase(); 
                    });
                };

                var $phone = $('#tale-gift-phone');
                var $email = $('#tale-gift-email');
                var formTitle = $('.form-title').text().replace(/ $/,"");
                var formTitleType = formTitle.replace(/ /g,"-").replace(/&/g,"and").toLowerCase();
                var submitDateTime = moment().tz('Asia/Singapore').format('YYYY-MM-DD HH:mm:ss');
                var phone = $phone.val();
                var email = $email.val().toLowerCase();
                var name = $('#tale-gift-name').val();
                var fullname = capitalize(name);
                let quizSummaryMessage = 'Survey Summary:\n';
                let finalResult = $('.score-active').find('.title').text();

                $('.question-item').each(function (index) {
                    quizSummaryMessage += `${$(this).find('.title').text()}\nAnswer: ${$(this).find('.answer-option.status-selected span').text().replace('<', 'less than ')}\n`
                });

                var hashed_phone = sha256(phone);
                var hashed_email = sha256(email);
                var marketing_consent_bool = false;
                var marketing_consent = "No";
                var eligibleStatus = "Eligible: No";
                
                var object = new Object();
                object.email = hashed_email;
                object.phone = hashed_phone;
                var myJsonData = JSON.stringify(object);
        
                if(check_marketing_checkbox.is(":checked")){
                    marketing_consent_bool = true;
                    marketing_consent = "Yes";
                }

                console.log(hashed_phone,hashed_email);
                console.log(quizSummaryMessage+"\n" + "My Final Result: \n"+finalResult);
                console.log("Name: "+fullname+" \n" + "Phone: "+phone+" \n" + "Email: "+email+" \n" + "IP: "+check_IP+" \n" + "Date/Time of Submission: "+submitDateTime+" \n");
                console.log("Marketing Consent: " + marketing_consent);
                console.log(myJsonData);
                function sha256(ascii) {
                    function rightRotate(value, amount) {
                        return (value>>>amount) | (value<<(32 - amount));
                    };
                    var mathPow = Math.pow;
                    var maxWord = mathPow(2, 32);
                    var lengthProperty = 'length'
                    var i, j; 
                    var result = ''
                    var words = [];
                    var asciiBitLength = ascii[lengthProperty]*8;
                    var hash = sha256.h = sha256.h || [];
                    var k = sha256.k = sha256.k || [];
                    var primeCounter = k[lengthProperty];
                    var isComposite = {};
    
                    for (var candidate = 2; primeCounter < 64; candidate++) {
                        if (!isComposite[candidate]) {
                            for (i = 0; i < 313; i += candidate) {
                                isComposite[i] = candidate;
                            }
                            hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
                            k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
                        }
                    }
                
                    ascii += '\x80'
                    while (ascii[lengthProperty]%64 - 56) ascii += '\x00' 
                    for (i = 0; i < ascii[lengthProperty]; i++) {
                        j = ascii.charCodeAt(i);
                        if (j>>8) return; 
                        words[i>>2] |= j << ((3 - i)%4)*8;
                    }
    
                    words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
                    words[words[lengthProperty]] = (asciiBitLength)
    
                    for (j = 0; j < words[lengthProperty];) {
                        var w = words.slice(j, j += 16); 
                        var oldHash = hash;
                        hash = hash.slice(0, 8);
                        
                        for (i = 0; i < 64; i++) {
                            var i2 = i + j;
                            var w15 = w[i - 15], w2 = w[i - 2];
                            var a = hash[0], e = hash[4];
                            var temp1 = hash[7]
                                + (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
                                + ((e&hash[5])^((~e)&hash[6])) // ch
                                + k[i]
                                + (w[i] = (i < 16) ? w[i] : (
                                        w[i - 16]
                                        + (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
                                        + w[i - 7]
                                        + (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
                                    )|0
                                );
                            var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
                                + ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj
                            hash = [(temp1 + temp2)|0].concat(hash); 
                            hash[4] = (hash[4] + temp1)|0;
                        }
                    
                        for (i = 0; i < 8; i++) {
                            hash[i] = (hash[i] + oldHash[i])|0;
                        }
                    }
                    
                    for (i = 0; i < 8; i++) {
                        for (j = 3; j + 1; j--) {
                            var b = (hash[i]>>(j*8))&255;
                            result += ((b < 16) ? 0 : '') + b.toString(16);
                        }
                    }
                    return result;
                };

                var payload = {
                    "name": fullname,
                    "phone": phone,
                    "email": email,  
                    "formtitle": formTitle,
                    "Summary": quizSummaryMessage, 
                    "FinalResult": finalResult, 
                    "MarketingConsent": marketing_consent,
                    "SubmitDateTime": submitDateTime,
                    "IP": check_IP,
                }
                        
                var text =
                    ""+fullname+" has submitted a form for "+formTitle+".\n" +
                    "You have 30 days from "+submitDateTime+" to get in touch with them!\n" +
                    "Name: "+fullname+" \n" +
                    "Phone: "+phone+" \n" +
                    "Email: "+email+" \n" + 
                    ""+quizSummaryMessage+
                    "My Final Result: "+finalResult+" \n" +
                    "Date/Time of Submission: "+submitDateTime+" \n" +     
                    "Please note that you have 30 days from "+submitDateTime+" to call this contact!";

                    try {
                        window.submitForm({
                            formType: formTitleType,
                            payload: payload,
                            contactInfo: {
                                firstName: fullname,
                                emails: [{
                                    address: email,
                                }],
                                phones: [{   
                                    phoneNumber: phone,
                                    phoneTypeId: "MOBILE",
                                }],
                                tags: [formTitleType],
                                opt_in: {
                                    is_email_allowed: marketing_consent_bool,
                                    is_sms_allowed: marketing_consent_bool,
                                }
                            },                                        
                            forceCreateContact: true,
                            message: {
                                text: text
                            }
                        }).then(() => {
                            try {
                                $('.block-quiz-result .block-button').hide();
                                $("#loadingscreen").hide();
                                $('#tale-gift-name').val("");
                                $('#tale-gift-phone').val("");
                                $('#tale-gift-email').val("");
                                check_marketing_checkbox.prop('checked', false);

                                console.log("Information submitted to Backoffice. Go to result page.");
                                $('#form-tale-gift').fadeOut();
                                $('.score-active').fadeOut();
                                setTimeout(() => {    
                                    $('#form-thank-tale-gift').fadeIn();                                
                                    $('html, body').animate({
                                        scrollTop: $('.block-quiz-form').offset().top - 100
                                    }, 1000);
                                }, 500);
                            } catch (err) {
                                $('.block-quiz-result .block-button').hide();
                                $("#loadingscreen").hide();
                                $('#button-submit-tale-gift').addClass("disable-btn");
                                $(".submission-error").text('Sorry, there was an error. Please try again later.').show();
                                console.log("Failed to go thank you page.");
                            }
                        });
                    }
                    catch(err) {
                        $('.block-quiz-result .block-button').hide();
                        $("#loadingscreen").hide();
                        $('#button-submit-tale-gift').addClass("disable-btn");
                        $(".submission-error").text('Sorry, there was an error. Please try again later.').show();
                        console.log("submission to gefen error.");
                    } 
    
                // $.ajax({
                //     type: 'POST',
                //     url: 'https://aa8t23a7r9.execute-api.ap-southeast-1.amazonaws.com/ml-campaign4/submit',
                //     data: myJsonData,
                //     contentType: 'application/json; charset=utf-8',
                //     dataType: 'json',
                //     success: function (data) {
                //         if (data) {
                //             var jsonBody = (data.body) ? data.body : "";
                //             var body = (jsonBody) ? JSON.parse(jsonBody) : "";
                //             var msg = (body) ? body.Message : "";

                //             if (data.statusCode == 200) {
                //                 console.log(msg);
                
                //                 var payload = {
                //                     "name": fullname,
                //                     "phone": phone,
                //                     "email": email,  
                //                     "formtitle": formTitle,
                //                     "Summary": quizSummaryMessage, 
                //                     "FinalResult": finalResult, 
                //                     "MarketingConsent": marketing_consent,
                //                     "SubmitDateTime": submitDateTime,
                //                     "IP": check_IP,
                //                 }
                                        
                //                 var text =
                //                     ""+fullname+" has submitted a form for "+formTitle+".\n" +
                //                     "You have 30 days from "+submitDateTime+" to get in touch with them!\n" +
                //                     "Name: "+fullname+" \n" +
                //                     "Phone: "+phone+" \n" +
                //                     "Email: "+email+" \n" + 
                //                     eligibleStatus + " \n" + 
                //                     ""+quizSummaryMessage+
                //                     "My Final Result: "+finalResult+" \n" +
                //                     "Marketing Consent: "+marketing_consent+" \n" + 
                //                     "Date/Time of Submission: "+submitDateTime+" \n" +                        
                //                     "IP: "+ check_IP+" \n" + 
                //                     "Please note that you have 30 days from "+submitDateTime+" to call this contact!";
                
                //                     try {
                //                         window.submitForm({
                //                             formType: formTitleType,
                //                             payload: payload,
                //                             contactInfo: {
                //                                 firstName: fullname,
                //                                 emails: [{
                //                                     address: email,
                //                                 }],
                //                                 phones: [{   
                //                                     phoneNumber: phone,
                //                                     phoneTypeId: "MOBILE",
                //                                 }],
                //                                 tags: [formTitleType, eligibleStatus],
                //                                 opt_in: {
                //                                     is_email_allowed: marketing_consent_bool,
                //                                     is_sms_allowed: marketing_consent_bool,
                //                                 }
                //                             },                                        
                //                             forceCreateContact: true,
                //                             message: {
                //                                 text: text
                //                             }
                //                         }).then(() => {
                //                             try {
                //                                 $('.block-quiz-result .block-button').hide();
                //                                 $("#loadingscreen").hide();
                //                                 $('#tale-gift-name').val("");
                //                                 $('#tale-gift-phone').val("");
                //                                 $('#tale-gift-email').val("");
                //                                 $('#surveyNewsLetter').prop('checked', false);

                //                                 console.log("Information submitted to Backoffice. Go to result page.");
                //                                 $('#form-tale-gift').fadeOut();
                //                                 $('.score-active').fadeOut();
                //                                 setTimeout(() => {    
                //                                     $('#form-thank-tale-gift').fadeIn();                                
                //                                     $('html, body').animate({
                //                                         scrollTop: $('.block-quiz-form').offset().top - 100
                //                                     }, 1000);
                //                                 }, 500);
                //                             } catch (err) {
                //                                 $('.block-quiz-result .block-button').hide();
                //                                 $("#loadingscreen").hide();
                //                                 $('#button-submit-tale-gift').addClass("disable-btn");
                //                                 $(".submission-error").text('Sorry, there was an error. Please try again later.').show();
                //                                 console.log("Failed to go thank you page.");
                //                             }
                //                         });
                //                     }
                //                     catch(err) {
                //                         $('.block-quiz-result .block-button').hide();
                //                         $("#loadingscreen").hide();
                //                         $('#button-submit-tale-gift').addClass("disable-btn");
                //                         $(".submission-error").text('Sorry, there was an error. Please try again later.').show();
                //                         console.log("submission to gefen error.");
                //                     } 

                //             } else {
                //                 $('.block-quiz-result .block-button').hide();
                //                 $("#loadingscreen").hide();
                //                 $('#button-submit-tale-gift').addClass("disable-btn");                                
                //                 $(".submission-error").html("Oops, looks like you've already submitted an entry.<br/>Only one (1) entry is allowed per person.").show();
                //                 console.log("Duplicate entry.");
                //             }
                //         } else{
                //             $('.block-quiz-result .block-button').hide();
                //             $("#loadingscreen").hide();
                //             $('#button-submit-tale-gift').addClass("disable-btn");
                //             $(".submission-error").text('Sorry, there was an error. Please try again later.').show();
                //             console.log("Invalid data return by API.");
                //         }
                //     },
                //     error: function (data) {
                //         $('.block-quiz-result .block-button').hide();
                //         $("#loadingscreen").hide();
                //         $('#button-submit-tale-gift').addClass("disable-btn");
                //         $(".submission-error").text('Sorry, there was an error. Please try again later.').show();
                //         console.log("Error with API POST.");   
                //     }
                // }); 
            };  
        });
    },

};

module.exports = tale_of_gift_form;
