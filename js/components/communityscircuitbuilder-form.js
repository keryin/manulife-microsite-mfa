const communityscircuitbuilder_form = {

    init: function() {
        var that = this;
        that.showPage();
        that.submitFromAndValidation();
    },

    showPage() {        
        // function shareContent(){
        //     addthis.update('share', 'title', 'Hey, Manulife is recruiting and I think you’d be perfect for the job. Check the Manulife Circuit Builder to find out more!');
        //     addthis.update('share', 'description', 'Hey, Manulife is recruiting and I think you’d be perfect for the job. Check the Manulife Circuit Builder to find out more!');
        // }

        $('.button-show-cb-page2').on('click', function(e){
            $('.circuit_builder-page2').attr('style', 'position: relative;').fadeIn("slow");
            $('.circuit_builder-page2-result').attr('style', 'position: absolute;').fadeOut("slow");
            $('.circuit_builder-home-page').attr('style', 'position: absolute; display: none !important;').fadeOut("slow");
            $('.circuit_builder-page3').attr('style', 'position: absolute;').fadeOut("slow");
            $('.campaign-form-content-button').addClass('hide-campaign-form-button');
            $('.contact-box-desktop-mini').addClass('hideButton');
            if($('.circuit_builder-pop-up-content').hasClass('showPopUp')){
                $('.circuit_builder-pop-up-content').removeClass('showPopUp').fadeOut("slow");
            }
        });

        $('.cbWhatisThisPopUp').on('click', function(e){   
            $(this).parents('.circuit_builder-pop-up-wrapper').addClass('showPopUp');

            if($('.circuit_builder-pop-up-content').hasClass('showPopUp')){
                $('.circuit_builder-pop-up-content').attr('style', 'position: absolute; display: none !important;').removeClass('showPopUp').fadeOut("slow");
            } 

            $('#cbWhatisThis').attr('style', 'position: relative;').fadeIn(500, function(){
                $(this).toggleClass('showPopUp');
            });
        });

        $('.cbJoinUsPopUp').on('click', function(e){     
            $(this).parents('.circuit_builder-pop-up-wrapper').addClass('showPopUp');

            if($('.circuit_builder-pop-up-content').hasClass('showPopUp')){
                $('.circuit_builder-pop-up-content').attr('style', 'position: absolute; display: none !important;').removeClass('showPopUp').fadeOut("slow");
            }
            
            $('#cbJoinUs').attr('style', 'position: relative;').fadeIn(500, function(){
                $(this).toggleClass('showPopUp');
            });
        });

        $('.circuit_builder-close-btn').on('click', function(e){
            $('.cbJoinUsPopUp').parents('.circuit_builder-pop-up-wrapper').removeClass('showPopUp');
            $('.cbWhatisThisPopUp').parents('.circuit_builder-pop-up-wrapper').removeClass('showPopUp');
            $(this).parents('.circuit_builder-pop-up-content').attr('style', 'position: absolute; display: none !important;').removeClass('showPopUp').hide();
        });

        // $('.button-cb-invite').on('click', function(e){    
        //     shareContent();

        //     if($('.circuit_builder-pop-up-content').hasClass('showPopUp')){
        //         $('.circuit_builder-pop-up-content').removeClass('showPopUp').fadeOut("slow");
        //     }

        //     $('#cbInviteMessage').fadeToggle(500, function(){
        //         $(this).toggleClass('showPopUp');
        //     });
        // });

        // $('.button-show-cb-copy-invite').on('click', function(e){
        //     var copyText = document.getElementById("cbInviteMessageText");
        //     copyText.select();
        //     copyText.setSelectionRange(0, 99999)
        //     document.execCommand("copy");
        //     alert("Copied the message: " + copyText.value);
        // });

        function showResult(a){
            var resultTitle = ['The Influencer', 'The Analyst', 'The Planner' ,'The Innovator'];
            var resultContent = ['You are the people’s champion. Skilled in the ability to instantly connect and engage with those around you makes it easy for you to win their attention and trust.',
            'Your analytic abilities mean you implement lateral thinking, helping you better analyze situations to draw effective solutions. ', 
            'Careful and calculative for all the right reasons, your number crunching skills are what makes you a highly sought after and trusted source of information' ,
            'Your passion for technology fuels your everyday efficiency, helping you to stay connected, well-informed and at the top of your game.'];
            
            if(a == 1) {
                $('#cbSkilssTitle').text(resultTitle[0]);
                $('#cbSkilssDesc').text(resultContent[0]);
                $('#cvSkillsResult').find('label').text(resultTitle[0]);
                $('#cvSkillsResult').find('label').attr('data-value', resultTitle[0]);
            } else if(a == 2) {
                $('#cbSkilssTitle').text(resultTitle[1]);
                $('#cbSkilssDesc').text(resultContent[1]);
                $('#cvSkillsResult').find('label').text(resultTitle[1]);
                $('#cvSkillsResult').find('label').attr('data-value', resultTitle[1]);
            } else if(a == 3) {
                $('#cbSkilssTitle').text(resultTitle[2]);
                $('#cbSkilssDesc').text(resultContent[2]);
                $('#cvSkillsResult').find('label').text(resultTitle[2]);
                $('#cvSkillsResult').find('label').attr('data-value', resultTitle[2]);
            } else if(a == 4) {
                $('#cbSkilssTitle').text(resultTitle[3]);
                $('#cbSkilssDesc').text(resultContent[3]);
                $('#cvSkillsResult').find('label').text(resultTitle[3]);
                $('#cvSkillsResult').find('label').attr('data-value', resultTitle[3]);
            }
        }

        $('.button-show-cb-analysis').on('click', function(e){
            $('.contact-box-desktop-mini').removeClass('hideButton');
            $('.circuit_builder-page2-result').attr('style', 'position: relative;').fadeIn("slow");
            $('.circuit_builder-page2').attr('style', 'position: absolute;').fadeOut("slow");
            if($(this).hasClass('button-show-cb-influencer')){
                showResult(1);
            } else if($(this).hasClass('button-show-cb-analyst')){
                showResult(2);
            } else if($(this).hasClass('button-show-cb-planner')){
                showResult(3);
            } else if($(this).hasClass('button-show-cb-innovator')){
                showResult(4);
            }

            if($('.circuit_builder-pop-up-content').hasClass('showPopUp')){
                $('.circuit_builder-pop-up-content').removeClass('showPopUp').hide();
            }
        });
        
        $('.button-show-cb-page3').on('click', function(e){
            $('.circuit_builder-page3').attr('style', 'position: relative;').fadeIn("slow");
            $('.circuit_builder-page2-result').attr('style', 'position: absolute;').fadeOut("slow");
            $('.circuit_builder-page2').attr('style', 'position: absolute;').fadeOut("slow");
            $('.circuit_builder-page4').attr('style', 'position: absolute;').fadeOut("slow");
            if($('.circuit_builder-pop-up-content').hasClass('showPopUp')){
                $('.circuit_builder-pop-up-content').removeClass('showPopUp').fadeOut("slow");
            }
        });

        // $('.button-show-cb-page4').on('click', function(e){
        //     if(!$('.status-error').length){
        //         $('.circuit_builder-page4').attr('style', 'position: relative;').fadeIn("slow");
        //         $('.circuit_builder-page3').attr('style', 'position: absolute;').fadeOut("slow");
        //     } 
        // });

        $('.button-show-cb-lastpage').on('click', function(e){            
            $('.campaign-form-content-button').removeClass('hide-campaign-form-button').attr('style', 'position: relative;').fadeIn("slow");
            $('.circuit_builder-lastpage').attr('style', 'position: relative;').fadeIn("slow");
            $('.circuit_builder-page3').attr('style', 'position: absolute;').fadeOut("slow");
        });

        $('.cb-select-label').on('click', function() {
            if($('.cb-select ul').hasClass('showDropDown')) {
                $('.cb-select ul').removeClass('showDropDown').hide();
            }

            $(this).parent().find('ul').toggle().toggleClass('showDropDown');
        });

        $('.cb-select ul li').on('click', function() {
            var label = $(this).parent().parent().children('label');
            label.attr('data-value', $(this).attr('data-value'));
            label.html($(this).html());
        
            $(this).parent().children('.selected').removeClass('selected');
            $(this).addClass('selected');
            $(this).parent().removeClass('showDropDown').hide();
        });

        $(document).mouseup(function(e) {
            var cbSelect = $('.cb-select');

            if (!cbSelect.is(e.target) && cbSelect.has(e.target).length === 0) {
                cbSelect.find('ul').removeClass('showDropDown').hide();
            }
        });
    },

    submitFromAndValidation(){             
        function shareContent(){
            addthis.update('share', 'title', 'Hey, Manulife is recruiting and I think you’d be perfect for the job. Check the Manulife Circuit Builder to find out more!');
            addthis.update('share', 'description', 'Hey, Manulife is recruiting and I think you’d be perfect for the job. Check the Manulife Circuit Builder to find out more!');
        }

        function showCareerTip(){
            var careerTips = ['Send thank-you notes to everyone who helped with your job search – you never know when you’ll need to reach out to those contacts again and be sure to be available to return the favour and help them if need be.',
                'You may want to reach out to someone in a more senior role and ask them to be a mentor who can help you learn the ropes. You should also consider taking someone for coffee to help you learn more about their area, especially if you are going to be working with them or are interested in that career for yourself. ',
                ' In the early days and weeks in your new job, focus on listening, learning and asking questions. Take lots of notes — about the organization, your responsibilities, your manager’s expectations, and other peoples’ names and positions. Keep in mind that jotting down details with a pen and paper, rather than a laptop, can help you <u><a href="https://www.scientificamerican.com/article/a-learning-secret-don-t-take-notes-with-a-laptop/" target="_blank">remember more</a></u>  – but do what works best for your work style.',
                'Set yourself up for success by staying positive can reduce stress and can even <u><a href="http://www.mayoclinic.org/healthy-lifestyle/stress-management/in-depth/positive-thinking/art-20043950" target="_blank">improve your health</a></u>, and people prefer to work with someone upbeat. Sooner than you think, you’ll be settled into your new role — and perhaps helping new employees through their transition.'];
            var careerTip = careerTips[Math.floor(Math.random() * careerTips.length)];
            $('#cbCareerTip').html(careerTip);
        }

        $("#circuit_builder-form-name").focusin(function() {
            $('#circuit_builder-form-name').removeClass("status-error");
            $('#circuit_builder-form-name').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#circuit_builder-form-phone").focusin(function() {
            $('#circuit_builder-form-phone').removeClass("status-error");
            $('#circuit_builder-form-phone').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#circuit_builder-form-email").focusin(function() {
            $('#circuit_builder-form-email').removeClass("status-error");
            $('#circuit_builder-form-email').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#circuit_builder-form-qualification").change(function() {
            $('#circuit_builder-form-qualification').parents('.select-dropdown').removeClass("status-error");
            $('#circuit_builder-form-qualification').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#circuit_builder-form-citizen").change(function() {
            $('#circuit_builder-form-citizen').parents('.select-dropdown').removeClass("status-error");
            $('#circuit_builder-form-citizen').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#circuit_builder-form-phone").on('input', function(){
            if (this.value.length > 8) {
                this.value = this.value.slice(0,8); 
            }
        });

        $("#circuit_builder-form-phone").keyup(function () {
            this.value = this.value.replace(/[^0-9\.]+/g,'');
        });
 
        // Get an instance of `PhoneNumberUtil`
        const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

        $("#circuit_builder-form-phone").on('change', function () {
            var phoneValue = $(this).val();
            if(phoneValue.length < 8) {
                $('#circuit_builder-form-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#circuit_builder-form-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Invalid phone format');
            } 
            
            var phoneNumber = '+65' + phoneValue;
            var number, isvalidNumber;

            if(phoneValue !== ''){
                number = phoneUtil.parseAndKeepRawInput(phoneNumber, 'SG');
                isvalidNumber = phoneUtil.isValidNumberForRegion(number, 'SG');
            }

            if(!isvalidNumber) {
                $('#circuit_builder-form-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#circuit_builder-form-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                };   
                $('.error-label-phone').text('Invalid phone format');
            }
        });

        $('#circuit_builder-form-name').keyup(function () {
            this.value = this.value.replace(/[^A-Za-z+ -]/g,'');
        });

        $('#circuit_builder-form-name').on('change', function () {
            this.value = this.value.replace(/ $/,"");
        });

        let filesFromCareersForm = [];
        //Careers page form
        function filesUpload(uploadInput) {

            $(uploadInput).on("change", _uploadFiles);

            function _uploadFiles(e) {
                let clientFiles = e.target.files;
                if(clientFiles.length <= 0) {
                    return
                }
                window.uploadFilesToAWS(clientFiles, ({fileName, progressEvent}) => {
                    // Process bar here:
                    const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    console.log(fileName, `${percentCompleted}%`);
                }).then((result) => {

                    filesFromCareersForm = result;
                    $('.uploaded-cv-link').remove();
                    $(uploadInput).parents('.block-button').after('<a class="link-icon link-icon-file uploaded-cv-link"><span>' + filesFromCareersForm[0].fileName + '</span><i class="fa fa-trash action-remove-file" aria-hidden="true"></i></a>');

                    console.log('filesFromCareersForm', filesFromCareersForm);
                    // _submitFile(files);
                }).catch((error) => {
                    // if got error handle here.
                    console.log(error)
                });
            }

        };

        filesUpload("#button-cb-upload-cv");

        // ------------------------------------------------ Events

        $(document).on('click', '.action-remove-file', function() {
            filesFromCareersForm = [];
            $(this).parent().remove();
        });

        $.getJSON("https://api.ipify.org?format=jsonp&callback=?", function (data) {
            if (data) {
                var campaignUserIP = window.location.href + '&ip=' + data.ip;                
                $("#ExternalId").val(campaignUserIP);
            }
        });

        var moment = require('moment-timezone');
     
        $("#button-submit-circuit_builder-form").click(function(e) {
            e.preventDefault();  
            shareContent();
            var check_fullname = $('#circuit_builder-form-name').val();
            var check_phone = $('#circuit_builder-form-phone').val();
            var check_email = $('#circuit_builder-form-email').val();
            var check_Qualification = $('#circuit_builder-form-qualification option:selected').val();
            var check_Citizen = $('#circuit_builder-form-citizen option:selected').val();
            var check_marketing_checkbox = $('#form-circuit_builder-form').find('.general-checkbox');
            var check_IP = $("#ExternalId").val();
            var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.)+(?:[A-z]{2}|com|org|net|gov|mil|edu|biz|info|mobi|name|aero|jobs|museum)\b$/;
            var reg_name = /^(?:[A-Za-z]+[ -])*[A-Za-z]+$/;

            if(check_fullname == '' && !reg_name.test(check_fullname)){
                $('#circuit_builder-form-name').addClass("status-error");
                if(!$('.error-label-name').length){
                    $('#circuit_builder-form-name').parents('.form-field-inner').append('<div class="error-label error-label-name"></div>');
                };
                $('.error-label-name').text('Name is required');
            };

            if(check_phone == ''){
                $('#circuit_builder-form-phone').addClass("status-error");
                if(!$('.error-label-phone').length){
                    $('#circuit_builder-form-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Phone is required');             
            };
            
            if (check_email == '') {   
                $('#circuit_builder-form-email').addClass("status-error");
                if(!$('.error-label-email').length){
                    $('#circuit_builder-form-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                }
                $('.error-label-email').text('Email is required');
            } else {
                if(!reg_email.test(check_email)){                    
                    $('#circuit_builder-form-email').addClass("status-error");
                    if(!$('.error-label-email').length){
                        $('#circuit_builder-form-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                    }                    
                    $('.error-label-email').text('Invalid email format');
                }
            };

            if(check_Qualification == 'default') {
                $('#circuit_builder-form-qualification').parents('.select-dropdown').addClass("status-error");
                if(!$('.error-label-select').length){
                    $('#circuit_builder-form-qualification').parents('.form-field-inner').append('<div class="error-label error-label-select"></div>');
                };
                $('.error-label-select').text('Please select your qualification');
            }

            if(check_Citizen == 'default') {
                $('#circuit_builder-form-citizen').parents('.select-dropdown').addClass("status-error");
                if(!$('.error-label-select-citizen').length){
                    $('#circuit_builder-form-citizen').parents('.form-field-inner').append('<div class="error-label error-label-select-citizen"></div>');
                };
                $('.error-label-select-citizen').text('Please select your citizen');
            }

            if(check_marketing_checkbox.is(":not(:checked)")){
                check_marketing_checkbox.parents('.news-latter-opt-option').addClass("check-false");
            };      
            
            var phoneNumber = '+65' + check_phone;
            var number, isvalidNumber;

            if(check_phone == ''){
                if(!$('.error-label-phone').length){
                    $('#circuit_builder-form-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                }   
                $('.error-label-phone').text('Phone is required');
            } else if(check_phone !== ''){
                number = phoneUtil.parseAndKeepRawInput(phoneNumber, 'SG');
                isvalidNumber = phoneUtil.isValidNumberForRegion(number, 'SG');

                if(!isvalidNumber) {
                    $('#circuit_builder-form-phone').addClass("status-error");
                    if(!$('.error-label-phone').length){
                        $('#circuit_builder-form-phone').parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>');
                    }   
                    $('.error-label-phone').text('Invalid phone format');
                }
            }     

            // if((!$('.status-error').length) && check_fullname != "" && check_email != "" && check_phone != "" && reg_email.test(check_email) && isvalidNumber && check_marketing_checkbox.is(":checked") && (+getSubmitCount < 10)){  
            if((!$('.status-error').length) && check_fullname != "" && check_email != "" && check_phone != "" && reg_email.test(check_email) && isvalidNumber){    
                showCareerTip();
                $("#loadingscreen").show();            

                function capitalize(s){
                    return s.toLowerCase().replace( /\b./g, function(a){ 
                        return a.toUpperCase(); 
                    });
                };

                var $phone = $('#circuit_builder-form-phone');
                var $email = $('#circuit_builder-form-email');
                var formTitle = $('.form-title').text().replace(/ $/,"");
                var formTitleType = formTitle.replace(/ /g,"-").replace(/&/g,"and").toLowerCase();
                var submitDateTime = moment().tz('Asia/Singapore').format('DD/MM/YYYY HH:mm:ss');
                var submitDate = moment().tz('Asia/Singapore').format('DD/MM/YYYY');
                var phone = $phone.val();
                var email = $email.val().toLowerCase();
                var name = $('#circuit_builder-form-name').val();
                var fullname = capitalize(name);
                let cvQualification = $('#circuit_builder-form-qualification option:selected').val();
                let cvCitizen = $('#circuit_builder-form-citizen option:selected').val();

                var cvReferredBy = $('#cvReferredBy').find('label').attr('data-value');
                var cvEmployStatus = $('#cvEmployStatus').find('label').attr('data-value');
                var cvOpportunities = $('#cvOpportunities').find('label').attr('data-value');
                var cvLaunchCareer = $('#cvLaunchCareer').find('label').attr('data-value');
                var cvSkillsResult = $('#cvSkillsResult').find('label').attr('data-value');
                var cvSkillsDetails = $('#cvSkillsDetails').find('label').attr('data-value');
                var cvContactDetails = $('#cvContactDetails').find('label').attr('data-value');
                // var cvAdditionalText = $('#cvAdditionalText').val();

                var cvParagraph1 = 'Dear Sir/Mdm';
                var cvParagraph2 = 'I ' + cvReferredBy + ' submit my application as part of Manulife’s Circuit Builder to find out more about becoming a Financial Services Consultant/Financial Consultant.';
                var cvParagraph3 = 'I am ' + cvEmployStatus + ' and would like to explore ' + cvOpportunities + ' that will help ' + cvLaunchCareer + ' professionally.';
                var cvParagraph4= 'I feel that my persona as ' + cvSkillsResult +  ' that highlights my ' + cvSkillsDetails + ' adds to my potential to excel in the role and be a great contribution to your team of esteemed agents.';
                var cvParagraph5= 'I am looking for a dynamic organization that gives me the opportunity to use my strengths and I look forward to hearing from you with regards to my application via ' + cvContactDetails;
                var fullParagraph = cvParagraph1 + '\n' + cvParagraph2 + '\n' + cvParagraph3 + '\n' + cvParagraph4 + '\n' + cvParagraph5;
                var url = window.location.href;


                console.log("Name: "+fullname+" \n" + "Phone: "+phone+" \n" + "Email: "+email+" \n" + "IP: "+check_IP+" \n" + "Date/Time of Submission: "+submitDateTime+" \n");
                console.log(fullParagraph);

                var payload = {
                    "name": fullname,
                    "phone": phone,
                    "email": email,  
                    "cvCitizen": cvCitizen,
                    "cvQualification": cvQualification,
                    "files": filesFromCareersForm,
                    "cvParagraph1": cvParagraph1,
                    "cvParagraph2": cvParagraph2,
                    "cvParagraph3": cvParagraph3,
                    "cvParagraph4": cvParagraph4,
                    "cvParagraph5": cvParagraph5,
                    // "cvAdditionalText": cvAdditionalText,
                    "formtitle": formTitle,
                    "SubmitDate": submitDate,
                    "SubmitDateTime": submitDateTime,
                    "IP": check_IP,
                    "webURL": url
                }
                        
                var text =
                    "I submitted a career form on your website - Manulife Circuit Builder, here are all the details: \n" +
                    "Name: "+fullname+" \n" +
                    "Phone: "+phone+" \n" +
                    "Email: "+email+" \n" + 
                    "Are you:: "+cvCitizen+" \n" + 
                    "Qualification: "+cvQualification+" \n" + 
                    "Date/Time of Submission: "+submitDateTime+" \n" +                        
                    "URL: "+ url+" \n" +
                    fullParagraph +" \n";

                try {
                    window.submitForm({
                        formType: formTitleType,
                        payload: payload,
                        contactInfo: {
                            firstName: fullname,
                            emails: [{
                                address: email,
                            }],
                            phones: [{   
                                phoneNumber: phone,
                                phoneTypeId: "MOBILE",
                            }],
                            tags: [formTitleType],
                            opt_in: {
                                is_email_allowed: true,
                                is_sms_allowed: true,
                            }
                        },                                        
                        forceCreateContact: true,
                        message: {
                            text: text
                        }
                    }).then(() => {
                        try {
                            window.dataLayer.push({
                                'event': 'submit form successfully'
                            });
                            
                            $('html, body').animate({
                                scrollTop: 0
                            }, 1000);

                            $("#loadingscreen").hide();
                            $('#circuit_builder-form-name').val("");
                            $('#circuit_builder-form-phone').val("");
                            $('#circuit_builder-form-email').val("");
                            
                            console.log("Information submitted to Backoffice. Go to result page.");
                            $('.circuit_builder-lastpage').attr('style', 'position: absolute;').fadeOut();
                            setTimeout(() => {            
                                $('.contact-box-desktop-mini').addClass('hideButton');                        
                                $('.circuit_builder-thankyou').fadeIn();    
                            }, 500);
                        } catch (err) {
                            $("#loadingscreen").hide();
                            $('#button-submit-circuit_builder-form').addClass("disable-btn");
                            $(".submission-error").text('Sorry, we fail to submit your information. Please try again later.').show();
                            console.log("Failed to go thank you page.");
                        }
                    }).catch(function(error) {
                        $("#loadingscreen").hide();
                        $('#button-submit-circuit_builder-form').addClass("disable-btn");
                        $(".submission-error").text('Sorry, we fail to submit your information. Please try again later.').show();
                        console.log("Fail to submit to backoffice.");
                    });
                }
                catch(err) {                    
                    $("#loadingscreen").hide();
                    $('#button-submit-circuit_builder-form').addClass("disable-btn");
                    $(".submission-error").text('Opps something went wrong, please try submitting again. ').show();
                    console.log("submission to gefen error.");
                } 
            };  
        });
    },

};

module.exports = communityscircuitbuilder_form;
