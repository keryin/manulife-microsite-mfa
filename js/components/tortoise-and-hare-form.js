const tortoise_and_hare_form = {

    init: function() {
        var that = this;
        that.showArticles();
        that.showTermsCondition();
        that.submitFromAndValidation();

        $(document).on('click', '.button-close-banner', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $('.campaign-sticky-banner').fadeOut( "slow", "swing", function() {});
        });
    },

    showArticles() {
        function updateImage(srcD, srcT, srcM) {
            let imgSrc = "https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/staging/images/campaigns/tortoise-and-hare/";
            let isIE11 = !!window.MSInputMethodContext && !!document.documentMode;    
            let imgURLDesktop = imgSrc+srcD;              
            let imgURLTablet = imgSrc+srcT;            
            let imgURLMobile = imgSrc+srcM;            
                                                     
            
            $(".cover-image-wrapper.hide-mobile img").attr("src", imgURLDesktop);
            $(".cover-image-wrapper.hide-tablet img").attr("src", imgURLTablet);
            $(".cover-image-wrapper.hide-desktop img").attr("src", imgURLMobile);

            if (isIE11) {
                $('.cover-image-wrapper.hide-mobile').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLDesktop + ')')
                });
                $('.cover-image-wrapper.hide-tablet').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLTablet + ')')
                });
                $('.cover-image-wrapper.hide-desktop').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLMobile + ')')
                });
            }     
        };

        $('.button-go-back-title').on('click', function(e){
            $('.campaign-form-title-wrapper').fadeIn("slow");
            updateImage('1380-banner/manulife-tortoise-hare-1380-1A.jpg', '700-banner/manulife-tortoise-hare-700-1A.jpg', '375-banner/manulife-tortoise-hare-375-1A.jpg');    
            $(this).parents('.go-articles').fadeOut("slow");
            if(!$('.accordion-article').hasClass('status-closed')) {
                $('.accordion-article').addClass('status-closed');
                $('.accordion-article').find('.accordion-item-content').hide();
            }

            if($('.input-radio-learn-more').hasClass('selected')){
                $('.input-radio-learn-more').attr("checked", false);
                $('.input-radio-learn-more').removeClass('selected');
            }
        });

        $('.button-go-sign-up-form').on('click', function(e){
            $('.section-form-submission').fadeIn("slow");
            $(this).parents('.go-articles').fadeOut("slow");
        });

        // Save Articles
        $('.button-go-articles-save').on('click', function(e){
            $(".go-articles-save").fadeIn("slow");
            updateImage('1380-banner/manulife-tortoise-hare-1380-1B.jpg', '700-banner/manulife-tortoise-hare-700-1B.jpg', '375-banner/manulife-tortoise-hare-375-1B.jpg');          
            $(this).parents('.campaign-form-title-wrapper').fadeOut("slow");
            $('.form-field-learn-more').find('#tortoisehare-form-savings').addClass('selected').attr("checked", true);
        });

        $('.button-go-save-sign-up').on('click', function(e){
            $('.go-save-sign-up').fadeIn("slow");            
            updateImage('1380-banner/manulife-tortoise-hare-1380-1C.jpg', '700-banner/manulife-tortoise-hare-700-1C.jpg', '375-banner/manulife-tortoise-hare-375-1C.jpg');     
            $(this).parents('.go-articles').fadeOut("slow");
            if(!$('.accordion-article').hasClass('status-closed')) {
                $('.accordion-article').addClass('status-closed');
                $('.accordion-article').find('.accordion-item-content').hide();
            }
        });

        $('.button-go-back-save-articles').on('click', function(e){
            $('.go-articles-save').fadeIn("slow");
            updateImage('1380-banner/manulife-tortoise-hare-1380-1B.jpg', '700-banner/manulife-tortoise-hare-700-1B.jpg', '375-banner/manulife-tortoise-hare-375-1B.jpg');          
            $(this).parents('.go-articles').fadeOut("slow");
            if(!$('.accordion-article').hasClass('status-closed')) {
                $('.accordion-article').addClass('status-closed');
                $('.accordion-article').find('.accordion-item-content').hide();
            }
        });

        // Invest Articles        
        $('.button-go-articles-invest').on('click', function(e){
            $(".go-articles-invest").fadeIn("slow");
            updateImage('1380-banner/manulife-tortoise-hare-1380-2B.jpg', '700-banner/manulife-tortoise-hare-700-2B.jpg', '375-banner/manulife-tortoise-hare-375-2B.jpg');     
            $(this).parents('.campaign-form-title-wrapper').fadeOut("slow");
            $('.form-field-learn-more').find('#tortoisehare-form-investment').addClass('selected').attr("checked", true);
        });

        $('.button-go-back-invest-articles').on('click', function(e){
            $('.go-articles-invest').fadeIn("slow");
            updateImage('1380-banner/manulife-tortoise-hare-1380-2B.jpg', '700-banner/manulife-tortoise-hare-700-2B.jpg', '375-banner/manulife-tortoise-hare-375-2B.jpg');     
            $(this).parents('.go-articles').fadeOut("slow");
        });

        $('.button-go-back-invest-title').on('click', function(e){
            $('.go-invest-title').fadeIn("slow");
            updateImage('1380-banner/manulife-tortoise-hare-1380-2C.jpg', '700-banner/manulife-tortoise-hare-700-2C.jpg', '375-banner/manulife-tortoise-hare-375-2C.jpg');   
            $(this).parents('.go-articles').fadeOut("slow");
        });

        $('.button-go-invest-title').on('click', function(e){
            $('.go-invest-title').fadeIn("slow");
            updateImage('1380-banner/manulife-tortoise-hare-1380-2C.jpg', '700-banner/manulife-tortoise-hare-700-2C.jpg', '375-banner/manulife-tortoise-hare-375-2C.jpg');   
            $(this).parents('.go-articles').fadeOut("slow");
            if(!$('.accordion-article').hasClass('status-closed')) {
                $('.accordion-article').addClass('status-closed');
                $('.accordion-article').find('.accordion-item-content').hide();
            }
        });

        $('.button-go-invest-sign-up').on('click', function(e){
            $('.go-invest-sign-up').fadeIn("slow");
            updateImage('1380-banner/manulife-tortoise-hare-1380-1C.jpg', '700-banner/manulife-tortoise-hare-700-1C.jpg', '375-banner/manulife-tortoise-hare-375-1C.jpg');     
            $(this).parents('.go-articles').fadeOut("slow");
            if(!$('.accordion-article').hasClass('status-closed')) {
                $('.accordion-article').addClass('status-closed');
                $('.accordion-article').find('.accordion-item-content').hide();
            }
        });
    },

    showTermsCondition() {
        $(document).ready(function() {
            if (window.location.href.indexOf("tnc") > -1) {
                $("#term-modal").fadeIn("slow");
                $("body").css("overflow", "hidden");
                window.history.pushState('', 'Title', ' ');
            }
        });

        $('.showTerms').on('click', function(e){
            $("#term-modal").fadeIn("slow");
            $("body").css("overflow", "hidden");
        });

        $('.showConsent').on('click', function(e){
            $("#consent-modal").fadeIn("slow");
            $("body").css("overflow", "hidden");
        });

        $('.close-modal-btn').on('click', function(e){
            $("#term-modal").fadeOut("slow");
            $("#consent-modal").fadeOut("slow");
            $("body").css("overflow", "");
        });
    },

    submitFromAndValidation(){
        function showResult(){
            addthis.update('share', 'title', 'Remember the tale of tortoise and the hare and wondered what happened once the famous race was over? After all, both characters had two totally different approaches to life, so what do you think would be their views on topics like Savings & Insurance? Find out for yourself and while you’re at it, subscribe to my mailing list for more awesome tips on financial planning!');
            addthis.update('share', 'description', 'Remember the tale of tortoise and the hare and wondered what happened once the famous race was over? After all, both characters had two totally different approaches to life, so what do you think would be their views on topics like Savings & Insurance? Find out for yourself and while you’re at it, subscribe to my mailing list for more awesome tips on financial planning!');
        }

        $("#tortoisehare-form-name").focusin(function() {
            $('#tortoisehare-form-name').removeClass("status-error");
            $('#tortoisehare-form-name').parents('.form-field-inner').find('.error-label').text('');
        });

        $("#tortoisehare-form-email").focusin(function() {
            $('#tortoisehare-form-email').removeClass("status-error");
            $('#tortoisehare-form-email').parents('.form-field-inner').find('.error-label').text('');
        });
 
        $('#tortoisehare-form-name').keyup(function () {
            this.value = this.value.replace(/[^A-Za-z+ -]/g,'');
        });

        $('#tortoisehare-form-name').on('change', function () {
            this.value = this.value.replace(/ $/,"");
        });

        $('#surveyNewsLetter').click(function(){
            if($(this).is(':checked')){
                $('#surveyNewsLetter').parents('.news-latter-opt-option').removeClass("check-false");
            } else {
                $('#surveyNewsLetter').parents('.news-latter-opt-option').addClass("check-false");
            }
        });

        $.getJSON("https://api.ipify.org?format=jsonp&callback=?", function (data) {
            if (data) {
                var campaignUserIP = window.location.href + '&ip=' + data.ip;                
                $("#ExternalId").val(campaignUserIP);
            }
        });

        var moment = require('moment-timezone');
        var checkDate = moment().tz('Asia/Singapore').format('YYYY-MM-DD');
        
        $(document).ready(function () {
            var getLocalDate = localStorage.getItem('campaignGetThisDate');
            if (getLocalDate !== checkDate) {
                localStorage.removeItem('campaignGetThisDate');
                localStorage.removeItem('campaignSubmitCount');
            }
        });
     
        $("#button-submit-tortoisehare-form").click(function(e) {
            e.preventDefault();  
            var check_fullname = $('#tortoisehare-form-name').val();
            var check_email = $('#tortoisehare-form-email').val();
            var check_marketing_checkbox = $('#form-tortoisehare-form').find('.general-checkbox');
            var check_IP = $("#ExternalId").val();
            var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.)+(?:[A-z]{2}|com|org|net|gov|mil|edu|biz|info|mobi|name|aero|jobs|museum)\b$/;
            var reg_name = /^(?:[A-Za-z]+[ -])*[A-Za-z]+$/;

            if(check_fullname == '' && !reg_name.test(check_fullname)){
                $('#tortoisehare-form-name').addClass("status-error");
                if(!$('.error-label-name').length){
                    $('#tortoisehare-form-name').parents('.form-field-inner').append('<div class="error-label error-label-name"></div>');
                };
                $('.error-label-name').text('Name is required');
            };
            
            if (check_email == '') {   
                $('#tortoisehare-form-email').addClass("status-error");
                if(!$('.error-label-email').length){
                    $('#tortoisehare-form-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                }
                $('.error-label-email').text('Email is required');
            } else {
                if(!reg_email.test(check_email)){                    
                    $('#tortoisehare-form-email').addClass("status-error");
                    if(!$('.error-label-email').length){
                        $('#tortoisehare-form-email').parents('.form-field-inner').append('<div class="error-label error-label-email"></div>');
                    }                    
                    $('.error-label-email').text('Invalid email format');
                }
            };

            if(check_marketing_checkbox.is(":not(:checked)")){
                check_marketing_checkbox.parents('.news-latter-opt-option').addClass("check-false");
            };

            // var getSubmitCount = localStorage.getItem('campaignSubmitCount');

            // if(+getSubmitCount >= 10){
            //     console.log("Submitted more than 10 times");
            //     $('#button-submit-tortoisehare-form').addClass('disable-btn');
            //     $(".multiplesubmission").html('You’ve submitted multiple times, please try again later.<br/>Alternatively, you may contact us at {{websiteMainUser.mainEmail}}.').show();
            // }          

            // if((!$('.status-error').length) && check_fullname != "" && check_email != "" && check_phone != "" && reg_email.test(check_email) && isvalidNumber && check_marketing_checkbox.is(":checked") && (+getSubmitCount < 10)){  
            if((!$('.status-error').length) && check_fullname != "" && check_email != "" && reg_email.test(check_email) && check_marketing_checkbox.is(":checked")){    
                showResult();
                $("#loadingscreen").show();                
                var addThisDate = moment().tz('Asia/Singapore').format('YYYY-MM-DD');
    
                localStorage.setItem("campaignUserIP", check_IP); 
                // if (getSubmitCount) {
                //     getSubmitCount = +getSubmitCount + 1
                // } else {
                //     getSubmitCount = '1'
                // }
                
                // localStorage.setItem('campaignSubmitCount', getSubmitCount);
                // localStorage.setItem('campaignGetThisDate', addThisDate);

                // Failed submission due to unknown error
                // setTimeout(function(){ 
                //     if(!$('#button-submit-tortoisehare-form').hasClass("disable-btn")){
                        
                //         $("#loadingscreen").hide();
                //         $(".submission-error").text('Opps something went wrong, please try submitting again. ').show();
                //         console.log('Setimeout: Opps something went wrong, please try submitting again. ')
                //         $('#button-submit-tortoisehare-form').addClass("disable-btn");
                //     }
                // }, 5000);

                function capitalize(s){
                    return s.toLowerCase().replace( /\b./g, function(a){ 
                        return a.toUpperCase(); 
                    });
                };

                var $email = $('#tortoisehare-form-email');
                var submitDateTime = moment().tz('Asia/Singapore').format('DD/MM/YYYY HH:mm:ss');
                var submitDate = moment().tz('Asia/Singapore').format('DD/MM/YYYY');
                var email = $email.val().toLowerCase();
                var name = $('#tortoisehare-form-name').val();
                var fullname = capitalize(name);
                let interestedArticles = $("input[name='input-radio-article']:checked").val();
                var formTitle = $('.form-title').text().replace(/ $/,"");
                var fullFormTitle = formTitle.replace(/ /g,"-").replace(/&/g,"and").toLowerCase();
                var formTitleType = fullFormTitle + '-' + interestedArticles.toLowerCase();

                var marketing_consent_bool = false;
                var marketing_consent = "No";
                var url = window.location.href;
                var splitURL = url.split("/");
                var webLink = splitURL[2];
        
                if(check_marketing_checkbox.is(":checked")){
                    marketing_consent_bool = true;
                    marketing_consent = "Yes";
                }

                console.log("Interested articles: \n"+interestedArticles);
                console.log("Name: "+fullname+" \n" + "Email: "+email+" \n" + "IP: "+check_IP+" \n" + "Date/Time of Submission: "+submitDateTime+" \n");
                console.log("Marketing Consent: " + marketing_consent);
                function sha256(ascii) {
                    function rightRotate(value, amount) {
                        return (value>>>amount) | (value<<(32 - amount));
                    };
                    var mathPow = Math.pow;
                    var maxWord = mathPow(2, 32);
                    var lengthProperty = 'length'
                    var i, j; 
                    var result = ''
                    var words = [];
                    var asciiBitLength = ascii[lengthProperty]*8;
                    var hash = sha256.h = sha256.h || [];
                    var k = sha256.k = sha256.k || [];
                    var primeCounter = k[lengthProperty];
                    var isComposite = {};
    
                    for (var candidate = 2; primeCounter < 64; candidate++) {
                        if (!isComposite[candidate]) {
                            for (i = 0; i < 313; i += candidate) {
                                isComposite[i] = candidate;
                            }
                            hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
                            k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
                        }
                    }
                
                    ascii += '\x80'
                    while (ascii[lengthProperty]%64 - 56) ascii += '\x00' 
                    for (i = 0; i < ascii[lengthProperty]; i++) {
                        j = ascii.charCodeAt(i);
                        if (j>>8) return; 
                        words[i>>2] |= j << ((3 - i)%4)*8;
                    }
    
                    words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
                    words[words[lengthProperty]] = (asciiBitLength)
    
                    for (j = 0; j < words[lengthProperty];) {
                        var w = words.slice(j, j += 16); 
                        var oldHash = hash;
                        hash = hash.slice(0, 8);
                        
                        for (i = 0; i < 64; i++) {
                            var i2 = i + j;
                            var w15 = w[i - 15], w2 = w[i - 2];
                            var a = hash[0], e = hash[4];
                            var temp1 = hash[7]
                                + (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
                                + ((e&hash[5])^((~e)&hash[6])) // ch
                                + k[i]
                                + (w[i] = (i < 16) ? w[i] : (
                                        w[i - 16]
                                        + (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
                                        + w[i - 7]
                                        + (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
                                    )|0
                                );
                            var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
                                + ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj
                            hash = [(temp1 + temp2)|0].concat(hash); 
                            hash[4] = (hash[4] + temp1)|0;
                        }
                    
                        for (i = 0; i < 8; i++) {
                            hash[i] = (hash[i] + oldHash[i])|0;
                        }
                    }
                    
                    for (i = 0; i < 8; i++) {
                        for (j = 3; j + 1; j--) {
                            var b = (hash[i]>>(j*8))&255;
                            result += ((b < 16) ? 0 : '') + b.toString(16);
                        }
                    }
                    return result;
                };

                function formatErrorMessage(jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        return ('Status Code: ' + jqXHR.status + ', Error Message: Not connected. Please verify your network connection. ');
                    } else if (jqXHR.status == 404) {
                        return ('Status Code: ' + jqXHR.status + ', Error Message: The requested page not found. [404]');
                    } else if (jqXHR.status == 500) {
                        return ('Status Code: ' + jqXHR.status + ', Error Message: Internal Server Error [500].');
                    } else if (exception === 'parsererror') {
                        return ('Exception: parsererror, Error Message: Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        return ('Exception: timeout, Error Message: Time out error.');
                    } else if (exception === 'abort') {
                        return ('Exception: abort, Error Message: Ajax request aborted.');
                    } else {
                        return ('Error Message: Uncaught Error. ' + jqXHR.responseText);
                    }
                }
    
                var payload = {
                    "name": fullname,
                    // "phone": phone,
                    "email": email,  
                    "formtitle": formTitle,
                    "InterestedArticles": interestedArticles,
                    "MarketingConsent": marketing_consent,
                    "SubmitDate": submitDate,
                    "SubmitDateTime": submitDateTime,
                    "IP": check_IP,
                    "webURL": url
                }
                        
                var text =
                    "You have 30 days from "+submitDate+" to get in touch with this prospect.\n" +
                    "Name: "+fullname+" \n" +
                    // "Phone: "+phone+" \n" +
                    "Email: "+email+" \n" + 
                    "I want to learn more about: "+interestedArticles+ " \n" +
                    "Marketing Consent: "+marketing_consent+" \n" + 
                    "Date/Time of Submission: "+submitDateTime+" \n" +                        
                    "URL: "+ url+" \n";

                try {
                    window.submitForm({
                        formType: formTitleType,
                        payload: payload,
                        contactInfo: {
                            firstName: fullname,
                            emails: [{
                                address: email,
                            }],
                            tags: [fullFormTitle, formTitleType],
                            opt_in: {
                                is_email_allowed: marketing_consent_bool,
                            }
                        },                                        
                        forceCreateContact: true,
                        message: {
                            text: text
                        }
                    }).then(() => {
                        try {                            
                            $('html, body').animate({
                                scrollTop: 0
                            }, 1000);

                            $("#loadingscreen").hide();
                            $('#tortoisehare-form-name').val("");
                            $('#tortoisehare-form-email').val("");
                            $('#surveyNewsLetter').prop('checked', false);
                            
                            console.log("Information submitted to Backoffice. Go to result page.");
                            $('#form-tortoisehare-form').fadeOut();
                            setTimeout(() => {    
                                $('#form-thank-tortoisehare-form').fadeIn();    
                            }, 500);
                        } catch (err) {
                            $("#loadingscreen").hide();
                            $('#button-submit-tortoisehare-form').addClass("disable-btn");
                            $(".submission-error").text('Sorry, we fail to submit your information. Please try again later.').show();
                            console.log("Failed to go thank you page.");
                        }
                    }).catch(function(error) {
                        $("#loadingscreen").hide();
                        $('#button-submit-tortoisehare-form').addClass("disable-btn");
                        $(".submission-error").text('Sorry, we fail to submit your information. Please try again later.').show();
                        console.log("Fail to submit to backoffice.");
                    });
                }
                catch(err) {
                    $("#loadingscreen").hide();
                    $('#button-submit-tortoisehare-form').addClass("disable-btn");
                    $(".submission-error").text('Opps something went wrong, please try submitting again. ').show();
                    console.log("submission to gefen error.");
                } 
            };  
        });
    },

};

module.exports = tortoise_and_hare_form;
