const form_test = {

    init: function() {
        var that = this;
        that.submitFromAndValidation();

        $(document).on('click', '.button-close-banner', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $('.campaign-sticky-banner').fadeOut( "slow", "swing", function() {});
        });
    },

    submitFromAndValidation(){
        $("#syfFullName").focusin(function() {
            $('#syfFullName').parent().removeClass("input-error");
        });

        $("#syfPhone").focusin(function() {
            $('#syfPhone').parent().removeClass("input-error");
        });

        $("#syfEmail").focusin(function() {
            $('#syfEmail').parent().removeClass("input-error");
        });
        
        // $('#syfNewsLatter').change(function(){
        //     if($(this).prop("checked")) {
        //       $("#errorMarketing_checkbox").hide();
        //     }
        // });

        $("#syfPhone").on('input', function(){
            if (this.value.length > 8) {
                this.value = this.value.slice(0,8); 
            }
        });

        $('#syfFullName').keyup(function () {
            this.value = this.value.replace(/[^A-Za-z+ -]/g,'');
        });

        $('#syfNewsLatter').click(function(){
            if($(this).is(':checked')){
                $('#syfNewsLatter').parent().removeClass("input-error");
            } else {
                $('#syfNewsLatter').parent().addClass("input-error");
            }
        });

        $.getJSON("https://api.ipify.org?format=jsonp&callback=?", function (data) {
            if (data) {
                var userIP = window.location.href + '&ip=' + data.ip;                
                $("#ExternalId").val(userIP);
            }
        });

        var getDate = new Date();
        var checkDate = getDate.getFullYear() + '-' + (getDate.getMonth() + 1) + '-' + getDate.getDate();
        var getLocalDate = localStorage.getItem('getThisDate');

        if (getLocalDate < checkDate) {
            localStorage.removeItem('getThisDate')
            localStorage.removeItem('submitCount')
        }
     
        $("#button-submit-share-your-feels").click(function(e) {
            e.preventDefault();  
            var check_fullname = $('#syfFullName').val();
            var check_phone = $('#syfPhone').val();
            var check_email = $('#syfEmail').val();
            var check_marketing_checkbox = $('#syfNewsLatter');
            var check_IP = $("#ExternalId").val();
            var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.)+(?:[A-z]{2}|com|org|net|gov|mil|edu|biz|info|mobi|name|aero|jobs|museum)\b$/;
            // var reg_email = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
            // var reg_email = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-z]{2}|com|org|net|gov|mil|edu|biz|info|mobi|name|aero|jobs|museum)\b/;
            // var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/
            var reg_phone = /^(3|6|8|9)\d{7}$/;
            // var reg_phone = /^[0-9]*$/;
            var reg_name = /^(?:[A-Za-z]+[ -])*[A-Za-z]+$/;

            if(!reg_name.test(check_fullname)){
                $('#syfFullName').parent().addClass("input-error");
            };
            if (!reg_email.test(check_email)) {
                $('#syfEmail').parent().addClass("input-error");
            };
            if((check_phone.length < 8) || (check_phone.length > 8) || (!reg_phone.test(check_phone))){
                $('#syfPhone').parent().addClass("input-error");
            };

            if(check_marketing_checkbox.is(":not(:checked)")){
                $('#syfNewsLatter').parent().addClass("input-error");
            };

            var getSubmitCount = localStorage.getItem('submitCount');

            if(+getSubmitCount >= 5){
                console.log("Submitted more than 5 times");
                $('#button-submit-share-your-feels').addClass('disable-btn');
                $(".double-summission").after('<div class="col-xs-12 unable-submit submission-error" style="display:block;"><p>You have submitted more than 5 times.</p></div>');
            }

            if(check_fullname != "" && check_email != "" && check_phone != "" && 
            check_phone.length == 8 && reg_email.test(check_email) && reg_phone.test(check_phone) && check_marketing_checkbox.is(":checked") && (+getSubmitCount < 5)){   
                var getThisDate = new Date();
                var addThisDate = getThisDate.getFullYear() + '-' + (getThisDate.getMonth() + 1) + '-' + getThisDate.getDate();
    
                localStorage.setItem("userIP", check_IP); 
                if (getSubmitCount) {
                    getSubmitCount = +getSubmitCount + 1
                } else {
                    getSubmitCount = '1'
                }
                localStorage.setItem('submitCount', getSubmitCount);
                localStorage.setItem('getThisDate', addThisDate);

                    $('#button-submit-share-your-feels').prop('disabled', 'disabled');
                    $("#loadingsyf").show();
                    function failedSubmission() {
                        setTimeout(function(){ 
                            $("#loadingsyf").hide();
                            $(".double-summission").after('<div class="col-xs-12 unable-submit submission-error" style="display:block;"><p>Oops, your entry was not submitted. Please try again later.</p></div>');
                            $('#button-submit-share-your-feels').addClass("disable-btn");
                        }, 3000);
                    };

                    if(!$('#button-submit-share-your-feels').hasClass("disable-btn")){
                        failedSubmission();
                    }

                    // failedSubmission();
                    //$("#button-submit-share-your-feels").hide();

                    var $phone = $('#syfPhone');
                    var $email = $('#syfEmail');
                    var phone = $phone.val();
                    var email = $email.val().toLowerCase();
                    var fullname = $('#syfFullName').val();
                    // var ques_q1 = $('#feels-q1 .block-text h5').text();
                    // var ques_q2 = $('#feels-q2 .block-text h5').text();
                    // var ques_q3 = $('#feels-q3 .block-text h5').text();
                    // var ques_q4 = $('#feels-q4 .block-text h5').text();
                    // var ques_q5 = $('#feels-q5 .block-text h5').text();
                    // var data_q1 = $('#feels-q1 .selected-emoji').attr('data-share-score');
                    // var data_q2 = $('#feels-q2 .selected-emoji').attr('data-share-score');
                    // var data_q3 = $('#feels-q3 .selected-emoji').attr('data-share-score');
                    // var data_q4 = $('#feels-q4 .selected-emoji').attr('data-share-score');
                    // var data_q5 = $('#feels-q5 .selected-emoji').attr('data-share-score');  
                    let syfSummaryMessage = '\nQuiz Summary:\n';
                    // let scoreSummary = '';

                    $('.question-card').each(function (index) {
                        syfSummaryMessage += `${index+1}. ${$(this).find('h5').text()}\nAnswer: ${$(this).find('.selected-emoji').attr('data-share-score')}\n`
                        // scoreSummary += `${index+1}) ${$(this).find('.title').text()}\nAnswer: ${$(this).find('.answer-option.status-selected span').text().replace('<', 'less than ')}\n`
                    });              
                    var score_Result = $('#feels-share-result .result-score').text();
                    var hashed_phone = sha256($phone.val());
                    var hashed_email = sha256($email.val().toLowerCase());
                    var marketing_consent_bool = false;
                    var marketing_consent = "No";
                    
                    var object = new Object();
                    object.email = hashed_email;
                    object.phone = hashed_phone;
                    var myJsonData = JSON.stringify(object);
            
                    if(check_marketing_checkbox.is(":checked")){
                        marketing_consent_bool = true;
                        marketing_consent = "Yes";
                    }

                    $(".fullname").text(fullname);
                    $(".phonenumber").text(phone);
                    $(".emailaddress").text(email);


                    console.log(hashed_phone,hashed_email);
                    console.log(syfSummaryMessage);
                    console.log("Name: "+fullname+" \n" + "Phone: "+phone+" \n" + "Email: "+email+" \n" + "IP: "+check_IP+" \n");
                    console.log("Marketing Consent: " + marketing_consent);
                    console.log(myJsonData);
                    function sha256(ascii) {
                        function rightRotate(value, amount) {
                            return (value>>>amount) | (value<<(32 - amount));
                        };
                        var mathPow = Math.pow;
                        var maxWord = mathPow(2, 32);
                        var lengthProperty = 'length'
                        var i, j; 
                        var result = ''
                        var words = [];
                        var asciiBitLength = ascii[lengthProperty]*8;
                        var hash = sha256.h = sha256.h || [];
                        var k = sha256.k = sha256.k || [];
                        var primeCounter = k[lengthProperty];
                        var isComposite = {};
        
                        for (var candidate = 2; primeCounter < 64; candidate++) {
                            if (!isComposite[candidate]) {
                                for (i = 0; i < 313; i += candidate) {
                                    isComposite[i] = candidate;
                                }
                                hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
                                k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
                            }
                        }
                    
                        ascii += '\x80'
                        while (ascii[lengthProperty]%64 - 56) ascii += '\x00' 
                        for (i = 0; i < ascii[lengthProperty]; i++) {
                            j = ascii.charCodeAt(i);
                            if (j>>8) return; 
                            words[i>>2] |= j << ((3 - i)%4)*8;
                        }
        
                        words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
                        words[words[lengthProperty]] = (asciiBitLength)
        
                        for (j = 0; j < words[lengthProperty];) {
                            var w = words.slice(j, j += 16); 
                            var oldHash = hash;
                            hash = hash.slice(0, 8);
                            
                            for (i = 0; i < 64; i++) {
                                var i2 = i + j;
                                var w15 = w[i - 15], w2 = w[i - 2];
                                var a = hash[0], e = hash[4];
                                var temp1 = hash[7]
                                    + (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
                                    + ((e&hash[5])^((~e)&hash[6])) // ch
                                    + k[i]
                                    + (w[i] = (i < 16) ? w[i] : (
                                            w[i - 16]
                                            + (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
                                            + w[i - 7]
                                            + (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
                                        )|0
                                    );
                                var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
                                    + ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj
                                hash = [(temp1 + temp2)|0].concat(hash); 
                                hash[4] = (hash[4] + temp1)|0;
                            }
                        
                            for (i = 0; i < 8; i++) {
                                hash[i] = (hash[i] + oldHash[i])|0;
                            }
                        }
                        
                        for (i = 0; i < 8; i++) {
                            for (j = 3; j + 1; j--) {
                                var b = (hash[i]>>(j*8))&255;
                                result += ((b < 16) ? 0 : '') + b.toString(16);
                            }
                        }
                        return result;
                    };
        
                    var payload = {
                        "name": fullname,
                        "phone": phone,
                        "email": email,  
                        "Summary": syfSummaryMessage, 
                        // ques_q1: data_q1,
                        // ques_q2: data_q2,
                        // ques_q3: data_q3,
                        // ques_q4: data_q4,
                        // ques_q5: data_q5,
                        "Result Score": score_Result,
                        "MarketingConsent": marketing_consent,
                        "IP": check_IP,
                    }
                            
                    var text =
                        ""+fullname+" has submitted a form for Share Your Feels.\n" +
                        "Name: "+fullname+" \n" +
                        "Phone: "+phone+" \n" +
                        "Email: "+email+" \n" + 
                        ""+syfSummaryMessage+" \n" +
                        // ""+ques_q1+" : "+data_q1+" \n" + 
                        // ""+ques_q2+" : "+data_q2+" \n" + 
                        // ""+ques_q3+" : "+data_q3+" \n" + 
                        // ""+ques_q4+" : "+data_q4+" \n" + 
                        // ""+ques_q5+" : "+data_q5+" \n" + 
                        "Result Score: "+score_Result+" \n" + 
                        "Marketing Consent: "+marketing_consent+" \n" +                        
                        "IP: "+ check_IP+" \n";
                    
                    try {
                        window.submitForm({
                            formType: "share-your-feels",
                            payload: payload,
                            contactInfo: {
                                firstName: fullname,
                                emails: [{
                                    address: email,
                                }],
                                phones: [{   
                                    phoneNumber: phone,
                                    phoneTypeId: "MOBILE",
                                }],
                                tags: ["Share Your Feels"],
                                opt_in: {
                                    is_email_allowed: marketing_consent_bool,
                                    is_sms_allowed: marketing_consent_bool,
                                }
                            },                                        
                            forceCreateContact: true,
                            message: {
                                text: text
                            }
                        }).then(() => {
                            $("#loadingsyf").hide();
                            try {
                                $('#syfFullName').val("");
                                $('#syfPhone').val("");
                                $('#syfEmail').val("");
                                $('#syfNewsLatter').prop('checked', false);

                                console.log("Information submitted to Backoffice. Go to result page.");
                                
                                var current_fs, next_fs; //data-content
                                var left, opacity, scale; //data-content properties which we will animate
                                var animating; //flag to prevent quick multi-click glitches    
                                if(animating) return false;
                                animating = true;
                                
                                current_fs = $("#feels-form-wrapper");
                                next_fs = $("#feels-form-wrapper").next();

                                //activate next step on progressbar using the index of next_fs
                                $(".tab-menu li").eq($(".data-content").index(next_fs)).addClass("active").removeClass("disable-tab");
                                
                                //show the next data-content
                                next_fs.addClass("active-data").show(); 
                                current_fs.addClass("hide-data").removeClass("active-data")
                                //hide the current data-content with style
                                current_fs.animate({opacity: 0}, {
                                    step: function(now, mx) {
                                        //as the opacity of current_fs reduces to 0 - stored in "now"
                                        //1. scale current_fs down to 80%
                                        scale = 1 - (1 - now) * 0.2;
                                        //2. bring next_fs from the right(50%)
                                        left = (now * 50)+"%";
                                        //3. increase opacity of next_fs to 1 as it moves in
                                        opacity = 1 - now;
                                        current_fs.css({'transform': 'scale('+scale+')', 'position': "absolute"});
                                        next_fs.css({'left': left, 'opacity': opacity, 'position': "relative"});
                                    }, 
                                    duration: 500, 
                                    complete: function(){
                                        current_fs.hide();
                                        animating = false;
                                    }, 
                                    //this comes from the custom easing plugin
                                    easing: 'easeOutQuint'
                                });
                            } catch (err) {
                                $('#button-submit-share-your-feels').addClass("disable-btn");
                                console.log("Failed to go thank you page.");
                            }
                        });
                    }
                    catch(err) {
                        $("#loadingsyf").hide();
                        $('#button-submit-share-your-feels').addClass("disable-btn");
                        console.log("submission to gefen error.");
                    } 
            };  
        });
    },

};

module.exports = form_test;
