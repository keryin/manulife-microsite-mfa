const manulife_critical_illness = {

    init: function() {
        var that = this;
        that.showVideo();
    },

    showVideo() {
        "use strict";
        var objectFitVideos = require('object-fit-videos');

        $(document).ready(function() {
            if($('.page-name-manulife-critical-illness').length){
                $('.button-watch-video').on('click', '[data-lity]', lity);

                objectFitVideos(document.querySelectorAll(".videos")),
                $(".video").videoSrcset({
                    resize: !0
                })
            }
        });
    },

};

module.exports = manulife_critical_illness;
