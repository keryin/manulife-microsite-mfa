var meetingForm = {


    init: function () {
        this.bind();
        this.handleMeetingDate();
        this.insertHours("00:00", "24:30");

        $('.numbersOnly').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });

    },

    closeMeetingForm: function () {
        //close the meeting form, fires from onclick events from the .close-tag anchor tag inside the confrimation message
        $(".meeting_dropdown").stop().slideUp("slow");
        setTimeout(function () {
            $(".meeting-confirmation").slideToggle();
            $(".form-group").show();
        }, 500)
    },


    bind: function () {

        const self = this;

        $(".close-tag").click(function () {
            meetingForm.closeMeetingForm();
        });

        //this handles the validation of the form
        //if a part of the form is not valid so we color it in red in the isFormValid() function.
        //so here we change the color back to white while typing in the form
        //(we also change the color of the text back to black)
        $(".form-group input").keydown(function () {
            $(this).removeClass('danger');
        });


        /*THANK YOU MESSAGE ATER SCHEDUELE A MEETING*/
        $('.submit_but').click(() => self.sendFormToServer());


        $(".meet_button").click(function (e) {
            console.log("test");
            $(".meeting_dropdown").slideToggle("slow");
            e.preventDefault();
        });


        $(".meeting_form .cancel_but").click(function () {
            $(".meeting_dropdown").stop().slideUp("slow");
        });


    },

    handleMeetingDate: function () {
        //updating the avalible of the agent hours in the dropdown when date is changed
        $("#meetingDate").on("dp.change", function () {

            //gets the date from the datepicker input
            var date = $('#meetingDate').data('date');
            getEuropeDate(date);
            //turning the date to a readable one for converting later with get custom functions that i wrote - bellow
            var finalDate = getMonthInWords(date) + " " + getDayInString(date) + ", " + getYearinString(date) + " 23:15:30";

            var currentDay = new Date(finalDate);
            var weekDay = currentDay.getDay();
            var days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];


            //pulls out the year from the string that holds the date
            function getYearinString(date) {
                var year = date.substring(6, date.length);
                return year;
            }

            //pulls out the day from the string that holds the date
            function getDayInString(date) {
                return date.substring(0, 2);
            }

            //pulls out the month from the string that holds the date
            function getMonthInWords(date) {

                var month = date.substring(3, 5);
                switch (month) {
                    case "01":
                        return "January";
                    case "02":
                        return "February";
                    case "03":
                        return "March";
                    case "04":
                        return "April";
                    case "05":
                        return "May";
                    case "06":
                        return "June";
                    case "07":
                        return "July";
                    case "08":
                        return "August";
                    case "09":
                        return "September";
                    case "10":
                        return "October";
                    case "11":
                        return "November";
                    case "12":
                        return "December";

                }
            }
        });
        const getEuropeDate = (date) => {
            //Editing Format  Date
            let newDate = date.split("/");
            let y = newDate[2];
            let m = newDate[1];
            let d = newDate[0];
            let europeDate = d + "." + m + "." + y;
            $("#meetingDate").val(europeDate);

        };
    },

    //inserts the hours to the html according to the opening hours object
    insertHours: function (fromHour, toHour) {
        $("#meeting-hour").html('');
        var fromInt = parseInt(fromHour.substring(0, 2));
        var toInt = parseInt(toHour.substring(0, 2));
        for (var i = fromInt; i < toInt; i++) {
            if (i < 10) {
                $("#meeting-hour").html($("#meeting-hour").html() + "<option>" + "0" + i + ":00" + "</option>");
                $("#meeting-hour").html($("#meeting-hour").html() + "<option>" + "0" + i + ":30" + "</option>");
                if (i === "10") {
                    $(this).attr('selected', true);
                }

            }
            else {
                $("#meeting-hour").html($("#meeting-hour").html() + "<option>" + i + ":00" + "</option>");
                $("#meeting-hour").html($("#meeting-hour").html() + "<option>" + i + ":30" + "</option>");
                if (i == "10") {
                    $("#meeting-hour").html($("#meeting-hour").html() + "<option selected>" + i + ":00" + "</option>");
                    $("#meeting-hour").html($("#meeting-hour").html() + "<option>" + i + ":30" + "</option>");
                }
            }
        }

    },


    handleCreationOfThankYouMessage: function (date, hour) {
        //changing the thank you message html to show the current day selected

        //if the lead chose a date we will show him the date that was chosen
        if (date) {
            var formattedDate = date.format("DD.MM.YYYY");

            var meetingDetailsString = formattedDate;

            if (hour.includes(':')) {
                meetingDetailsString += " at" + " " + hour + ".";
            }
            $("#meeting-final-time").html(meetingDetailsString);
        }
        else {
            //tranlation: we will contact you shortly
            $(".meeting-confirmation p").html('We will contact you shortly.');
        }
    },

    getMessageSentToBackOffice: function (date, hour,name,lastName,phone,email) {
        let messageToBeSentToTheBackOffice = '';

        if (!date) {
        //coming from the other form
        messageToBeSentToTheBackOffice = `
        Appointment request:
        Name: ${name}
        last Name: ${lastName}
        Phone: ${phone}
        E-Mail: ${email}
        Message: ${document.getElementById('message').value}
        `
        }
        else {
            //coming from the contact banner
            var dateAndTimeOfFormRequest = new Date();
            var dateFormRequest = dateAndTimeOfFormRequest.getDate() + '/' + (dateAndTimeOfFormRequest.getMonth() + 1) + '/' + dateAndTimeOfFormRequest.getFullYear();
            var timeFormRequest = dateAndTimeOfFormRequest.getHours() + ':' + dateAndTimeOfFormRequest.getMinutes();

            messageToBeSentToTheBackOffice = `${name} scheduled a meeting on your website:\n Time and date: ${date.format('DD.MM.YYYY')} - ${hour}\n Name: ${name}\n Last name: ${lastName}\n Phone: ${phone}\n E-mail: ${email}\n Message:  ${document.getElementById('message').value}\n\n Please don't forget to confirm the meeting via email or phone:\n ${name}'s phone: ${phone}\n ${name}'s email: ${email}`;
        }

        console.log(messageToBeSentToTheBackOffice);
        return messageToBeSentToTheBackOffice
    },

    sendFormToServer: function () {
        const self = this;
        if (!self.isFormValid()) {
            return;
        }
        //else
        var name = document.getElementById('first_name').value;
        var lastName = document.getElementById('last_name').value;
        var email = document.getElementById('email').value;
        var phone = document.getElementById('phone').value;
        var city = "";
        var zipcode = "";
        var message = document.getElementById('message').value;

        var date = $('#meetingDate').data("DateTimePicker").date();
        var hour = $('#meeting-hour :selected').text();

        this.handleCreationOfThankYouMessage(date, hour);

        //Get the Time and Date NOw
        var dateAndTimeOfFormRequest = new Date();
        var dateFormRequest = dateAndTimeOfFormRequest.getDate() + '.' + (dateAndTimeOfFormRequest.getMonth() + 1) + '.' + dateAndTimeOfFormRequest.getFullYear();
        var timeFormRequest = dateAndTimeOfFormRequest.getHours() + ':' + dateAndTimeOfFormRequest.getMinutes();


        //Get the message that was left by the lead with the date and time of the meeting and the dropdown select
        var messageFromLead =
       `message: ${dateFormRequest}  at  ${timeFormRequest}
        ${document.getElementById('message').value}`;

         var messageToBeSentToTheBackOffice = this.getMessageSentToBackOffice(date, hour,name,lastName,phone,email);
         console.log(email);

        //sending it to the server
        onLeadSubmit(name,
            lastName,
            email,
            phone,
            city,
            zipcode,
            messageFromLead,
            date && date.format("DD.MM.YYYY"),
            hour,
            messageToBeSentToTheBackOffice);

        // submitNewLead({
        //     firstName: document.getElementById('first_name').value,
        //     lastName: document.getElementById('last_name').value,
        //     email:document.getElementById('email').value,
        //     phone:document.getElementById('phone').value,
        //     message: document.getElementById('message').value,
        //     date: date,
        //     time: hour,
        //     messageFormat: messageToBeSentToTheBackOffice
        // });
        self.resetForm();
        self.showThankYouMessageAndHideForm();



    },

    showThankYouMessageAndHideForm: function () {
        $(".form-group").hide();
        //showing the thank you message
        $(".meeting-confirmation").slideToggle("slow");

    },

    resetForm: function () {
        document.getElementById('first_name').value = '';
        document.getElementById('last_name').value = '';
        document.getElementById('email').value = '';
        document.getElementById('phone').value = '';
        document.getElementById('message').value = '';
        $('#meetingDate').val("") //.data("DateTimePicker").date(null);

    },

    changeStyleOfUnValidInput: function (elm) {
        $(elm).addClass('danger');
    },


    isPhoneValid(phone){
        return /^\d+$/.test(phone);
    },


    isFormValid: function () {
        const firstName = document.getElementById("first_name");
        const email = document.getElementById("email");

        this.handleObligatoryFieldsStyle(firstName, email);
            return firstName.value !== '' &&  (this.validateEmail(email.value) && email.value!== '')

            //return firstName.value !== '' && this.isSecurityLineApproved() && (this.validateEmail(email.value));

    },
    handleObligatoryFieldsStyle: function (firstName , email) {
        //if there is no first name inserted so we COLOR THE INPUT IN RED  phone === '' ||

        if (firstName.value === '') {
            this.changeStyleOfUnValidInput(firstName)
        }
        //if there is no phone number and email inserted, so we color the e-mail in red
        if (!this.validateEmail(email.value)  ) {
            this.changeStyleOfUnValidInput(email);
        }

        // if (!this.isPhoneValid(phone.value)  ) {
        //     this.changeStyleOfUnValidInput(phone);
        // }

    },
    validateEmail: function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }


};


module.exports = meetingForm;


