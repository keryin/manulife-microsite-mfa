const make_sense_of_your_finances = {

    init: function() {
        var that = this;
        var isPageHide = false;  

        that.ButtonControl();
        that.StartGameSectionControl();
        that.QuestionSet_22_To_32();
        that.QuestionSet_33_To_45();
        that.QuestionSet_46_To_60();
        that.AfterFinishedQuestionControl();
        that.submitFromAndValidation();

        $("#tncActive").hide("fast");
        $("#marketing_consent_section").hide("fast");
        $("#start-game-section").hide("fast");
        $(".side-bar-startGame").hide("fast");

        if (window.location.search == "?terms-and-conditions") {
            $("#main-screen").fadeOut("fast");
            $("#start-game-section").fadeOut("fast");
            $("#marketing_consent-section").fadeOut("fast");
            $("#tncActive").fadeIn("fast");
            $("body").scrollTop(0);
        };

        if (window.location.search == "?marketing-consent") {
            $("#main-screen").fadeOut("fast");
            $("#start-game-section").fadeOut("fast");
            $("#tncActive").fadeOut("fast");
            $("#marketing_consent-section").fadeIn("fast");
            $("body").scrollTop(0);
        };

        window.addEventListener('pageshow', function () {  
            if (isPageHide) {  
                $.when(window.location.reload()).done(function () {
                    $('html,body').animate({scrollTop: 0});
                });
            }  
        });  

        window.addEventListener('pagehide', function () {  
            isPageHide = true;  
        });

        $(".re-play-game-btn").click(function() {
            window.location.href = "/make-sense-of-your-finances";
        });
    },

    StartGameSectionControl() {
        $(".section-start-game-anwser").hide("fast");

        $.fn.digits = function(){ 
            return this.each(function(){ 
                $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); //Convert value with comma
            })
        };

        $(".start-game-select-salary").focusin(function() {
            $(this).siblings("#errorSalary").text("");
        });

        $(".start-game-select-age").focusin(function() {
            $(this).siblings("#errorAge").text("");
        });
        
        $(".continueGameBtn").click(function(e) {
            e.preventDefault;
            var check_salary = $(".start-game-select-salary option:selected").val();
            var check_ageRange = $(".start-game-select-age option:selected").val();

            if(check_salary == 0 && check_ageRange != 0){
                $('#errorSalary').text("Please select your annual salary.");
            }else if(check_salary != 0 && check_ageRange == 0){
                $('#errorAge').text("Please select your age group.");
            }else if(check_salary == 0 && check_ageRange == 0){
                $('#errorSalary').text("Please select your annual salary.");
                $('#errorAge').text("Please select your age group.");
            }else if (check_salary != 0 && check_ageRange != 0){
                $('#errorSalary').hide();
                $('#errorAge').hide();

                var salary = $(".start-game-select-salary option:selected").val();
                var ageRange = $(".start-game-select-age option:selected").val();

                $(".annual-salary span").text("$" + salary).digits();
                $(".salary_selected").text("$" + salary).digits();
                $(".ageRange_selected").text(ageRange);
                $(".continueGameBtn").attr("disabled", "disabled");
                $('.start-game-select-salary').prop('disabled', 'disabled');
                $('.start-game-select-age').prop('disabled', 'disabled');
                $(".section-start-game-anwser").show();
                $(".continueGameBtn").removeClass('addHover-Button');

                if(ageRange == '22-32'){
                    setTimeout(function(){ $("#questionSet-22-32_q1").show(); 
                    $('html, body').animate({scrollTop: $("#questionSet-22-32_q1").offset().top - 300}, 1000);}, 500);
                    setTimeout(function(){ $("#questionSet-22-32_q1_answer").show();}, 1000);

                    //Specify Category 23-32 anwser by Array 
                    var cat1_q1_Array_Id = ['22-32_q1_heartland_shop','22-32_q1_supermarkets','22-32_q1_Minimalist','22-32_q1_international_brand'];
                    var cat1_q2_Array_Id = ['once_a_month','2_4_times','5_7_times','8_10_times'];
                    var cat1_q3_Array_Id = ['express_air','economy_air','sensitive_aira','sea'];
                    var cat1_q4_Array_Id = ['bus_trains','taxis','ride_hailing','bus_pooling'];
                    var cat1_q5_Array_Id = ['karaoke','club_pub','movie','game_night'];
                    var cat1_q6_Array_Id = ['food_drinks','shopping','unique_experiences','adventure_sports'];
                    var cat1_q7_Array_Id = ['gym','community_centre','personal_trainer','free_tutorials_online'];
                    var cat1_q8_Array_Id = ['0_1','2_4','5_8','9_up'];
    
                    //Question 1 Looping by Array 
                    jQuery.each(cat1_q1_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_22_32_q1();
                            $(".data_q1").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat1_q1_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 2 Looping by Array 
                    jQuery.each(cat1_q2_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_22_32_q2();
                            $(".data_q2").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat1_q2_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
                
                    //Question 3 Looping by Array 
                    jQuery.each(cat1_q3_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_22_32_q3();
                            $(".data_q3").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat1_q3_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 4 Looping by Array 
                    jQuery.each(cat1_q4_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_22_32_q4();
                            $(".data_q4").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat1_q4_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 5 Looping by Array 
                    jQuery.each(cat1_q5_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_22_32_q5();
                            $(".data_q5").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat1_q5_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 6 Looping by Array 
                    jQuery.each(cat1_q6_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_22_32_q6();
                            $(".data_q6").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat1_q6_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 7 Looping by Array 
                    jQuery.each(cat1_q7_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_22_32_q7();
                            $(".data_q7").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat1_q7_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 8 Looping by Array 
                    jQuery.each(cat1_q8_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_22_32_q8();
                            ResultSectionControl();
                            $(".data_q8").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat1_q8_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Refactor
                    function Contorl_QuestionSet_22_32_q1(){
                        $("#questionSet-22-32_q1_response").show();
                        setTimeout(function(){ $("#questionSet-22-32_q2").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-22-32_q2").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-22-32_q2_answer").show();}, 1000);

                        $("#22-32_q1_heartland_shop").attr("disabled", "disabled");
                        $("#22-32_q1_supermarkets").attr("disabled", "disabled");
                        $("#22-32_q1_Minimalist").attr("disabled", "disabled");
                        $("#22-32_q1_international_brand").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_22_32_q2(){
                        $("#questionSet-22-32_q2_response").show();
                        setTimeout(function(){ $("#questionSet-22-32_q3").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-22-32_q3").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-22-32_q3_answer").show();}, 1000);

                        $("#once_a_month").attr("disabled", "disabled");
                        $("#2_4_times").attr("disabled", "disabled");
                        $("#5_7_times").attr("disabled", "disabled");
                        $("#8_10_times").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_22_32_q3(){
                        $("#questionSet-22-32_q3_response").show();
                        setTimeout(function(){ $("#questionSet-22-32_q4").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-22-32_q4").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-22-32_q4_answer").show();}, 1000);

                        $("#express_air").attr("disabled", "disabled");
                        $("#economy_air").attr("disabled", "disabled");
                        $("#sensitive_aira").attr("disabled", "disabled");
                        $("#sea").attr("disabled", "disabled");
                    };
                    
                    //Refactor
                    function Contorl_QuestionSet_22_32_q4(){
                        $("#questionSet-22-32_q4_response").show();
                        setTimeout(function(){ $("#questionSet-22-32_q5").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-22-32_q5").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-22-32_q5_answer").show();}, 1000);

                        $("#bus_trains").attr("disabled", "disabled");
                        $("#taxis").attr("disabled", "disabled");
                        $("#ride_hailing").attr("disabled", "disabled");
                        $("#bus_pooling").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_22_32_q5(){
                        $("#questionSet-22-32_q5_response").show();
                        setTimeout(function(){ $("#questionSet-22-32_q6").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-22-32_q6").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-22-32_q6_answer").show();}, 1000);

                        $("#karaoke").attr("disabled", "disabled");
                        $("#club_pub").attr("disabled", "disabled");
                        $("#movie").attr("disabled", "disabled");
                        $("#game_night").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_22_32_q6(){
                        $("#questionSet-22-32_q6_response").show();
                        setTimeout(function(){ $("#questionSet-22-32_q7").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-22-32_q7").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-22-32_q7_answer").show();}, 1000);

                        $("#food_drinks").attr("disabled", "disabled");
                        $("#shopping").attr("disabled", "disabled");
                        $("#unique_experiences").attr("disabled", "disabled");
                        $("#adventure_sports").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_22_32_q7(){
                        $("#questionSet-22-32_q7_response").show();
                        setTimeout(function(){ $("#questionSet-22-32_q8").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-22-32_q8").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-22-32_q8_answer").show();}, 1000);
    
                        $("#gym").attr("disabled", "disabled");
                        $("#community_centre").attr("disabled", "disabled");
                        $("#personal_trainer").attr("disabled", "disabled");
                        $("#free_tutorials_online").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_22_32_q8(){
                        $("#0_1").attr("disabled", "disabled");
                        $("#2_4").attr("disabled", "disabled");
                        $("#5_8").attr("disabled", "disabled");
                        $("#9_up").attr("disabled", "disabled");
                    };
    
                    //Result here after answered all question 
                    function ResultSectionControl(){
                        $(".response-after-q8-text span").text("$" + salary).digits();
                        $("#cashflow_response").show();
                        setTimeout(function(){ $("#section-submition").show(); 
                        $('html, body').animate({scrollTop: $("#section-submition").offset().top - 300}, 1000);}, 500);
                        var finalCashFlow = salary;
                        console.log(salary);
                    };
    
                    //Result Calculation
                    function SalaryCalculation(data){
                        salary = (salary- data);
                        salary = parseFloat(salary).toFixed(2);
                        return salary;
                    };
    
                }else if(ageRange == '33-45'){
                    setTimeout(function(){ $("#questionSet-33-45_q1").show(); 
                    $('html, body').animate({scrollTop: $("#questionSet-33-45_q1").offset().top - 300}, 1000);}, 500);
                    setTimeout(function(){ $("#questionSet-33-45_q1_answer").show();}, 1000);

                    //Specify Category 33-45 answer by Array 
                    var cat2_q1_Array_Id = ['hawker_kopitiams','fast_food','hipster_cafe','highEnd_rest'];
                    var cat2_q2_Array_Id = ['mrt_publicBuses','hire_car','own_car','mobility_device'];
                    var cat2_q3_Array_Id = ['both_tuition_classes','abacus_chinese','music_speech_drama','rabot_programming'];
                    var cat2_q4_Array_Id = ['buy_public_housing','buy_private_housing','investment','rent_long_term'];
                    var cat2_q5_Array_Id = ['mobile','mobile_internet','mobile_internet_music_streaming','mobile_internet_music_streaming_pay_tv'];
                    var cat2_q6_Array_Id = ['short_gateways','21_days_trip','staycations','holidays'];
                    var cat2_q7_Array_Id = ['online_store','HDB','heartland_malls','luxury_shopping_mall'];
                    var cat2_q8_Array_Id = ['gym_exercise','fit_exercise','exercise_insurance','insurance_only'];
    
                    //Question 1 Looping by Array 
                    jQuery.each(cat2_q1_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_33_45_q1();
                            $(".data_q1").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat2_q1_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 2 Looping by Array 
                    jQuery.each(cat2_q2_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_33_45_q2();
                            $(".data_q2").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat2_q2_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 3 Looping by Array 
                    jQuery.each(cat2_q3_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_33_45_q3();
                            $(".data_q3").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat2_q3_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 4 Looping by Array 
                    jQuery.each(cat2_q4_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_33_45_q4();
                            $(".data_q4").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat2_q4_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 5 Looping by Array 
                    jQuery.each(cat2_q5_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_33_45_q5();
                            $(".data_q5").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat2_q5_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 6 Looping by Array 
                    jQuery.each(cat2_q6_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_33_45_q6();
                            $(".data_q6").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat2_q6_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 7 Looping by Array 
                    jQuery.each(cat2_q7_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_33_45_q7();
                            $(".data_q7").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat2_q7_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 8 Looping by Array 
                    jQuery.each(cat2_q8_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            var q8_answer = attr_text;
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_33_45_q8();
                            ResultSectionControl();
                            $(".data_q8").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat2_q8_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Refactor
                    function Contorl_QuestionSet_33_45_q1(){
                        $("#questionSet-33-45_q1_response").show();
                        setTimeout(function(){ $("#questionSet-33-45_q2").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-33-45_q2").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-33-45_q2_answer").show();}, 1000);

                        $("#hawker_kopitiams").attr("disabled", "disabled");
                        $("#fast_food").attr("disabled", "disabled");
                        $("#hipster_cafe").attr("disabled", "disabled");
                        $("#highEnd_rest").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_33_45_q2(){
                        $("#questionSet-33-45_q2_response").show();
                        setTimeout(function(){ $("#questionSet-33-45_q3").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-33-45_q3").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-33-45_q3_answer").show();}, 1000);

                        $("#mrt_publicBuses").attr("disabled", "disabled");
                        $("#hire_car").attr("disabled", "disabled");
                        $("#own_car").attr("disabled", "disabled");
                        $("#mobility_device").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_33_45_q3(){
                        $("#questionSet-33-45_q3_response").show();
                        setTimeout(function(){ $("#questionSet-33-45_q4").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-33-45_q4").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-33-45_q4_answer").show();}, 1000);

                        $("#both_tuition_classes").attr("disabled", "disabled");
                        $("#abacus_chinese").attr("disabled", "disabled");
                        $("#music_speech_drama").attr("disabled", "disabled");
                        $("#rabot_programming").attr("disabled", "disabled");
                    };
                    
                    //Refactor
                    function Contorl_QuestionSet_33_45_q4(){
                        $("#questionSet-33-45_q4_response").show();
                        setTimeout(function(){ $("#questionSet-33-45_q5").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-33-45_q5").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-33-45_q5_answer").show();}, 1000);

                        $("#buy_public_housing").attr("disabled", "disabled");
                        $("#buy_private_housing").attr("disabled", "disabled");
                        $("#investment").attr("disabled", "disabled");
                        $("#rent_long_term").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_33_45_q5(){
                        $("#questionSet-33-45_q5_response").show();
                        setTimeout(function(){ $("#questionSet-33-45_q6").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-33-45_q6").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-33-45_q6_answer").show();}, 1000);

                        $("#mobile").attr("disabled", "disabled");
                        $("#mobile_internet").attr("disabled", "disabled");
                        $("#mobile_internet_music_streaming").attr("disabled", "disabled");
                        $("#mobile_internet_music_streaming_pay_tv").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_33_45_q6(){
                        $("#questionSet-33-45_q6_response").show();
                        setTimeout(function(){ $("#questionSet-33-45_q7").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-33-45_q7").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-33-45_q7_answer").show();}, 1000);

                        $("#short_gateways").attr("disabled", "disabled");
                        $("#21_days_trip").attr("disabled", "disabled");
                        $("#staycations").attr("disabled", "disabled");
                        $("#holidays").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_33_45_q7(){
                        $("#questionSet-33-45_q7_response").show();
                        setTimeout(function(){ $("#questionSet-33-45_q8").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-33-45_q8").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-33-45_q8_answer").show();}, 1000);

                        $("#online_store").attr("disabled", "disabled");
                        $("#HDB").attr("disabled", "disabled");
                        $("#heartland_malls").attr("disabled", "disabled");
                        $("#luxury_shopping_mall").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_33_45_q8(){
                        $("#gym_exercise").attr("disabled", "disabled");
                        $("#fit_exercise").attr("disabled", "disabled");
                        $("#exercise_insurance").attr("disabled", "disabled");
                        $("#insurance_only").attr("disabled", "disabled");
                    };
    
                    //Result here after answered all question 
                    function ResultSectionControl(){
                        $(".response-after-q8-text span").text("$" + salary).digits();
                        $("#cashflow_response").show();
                        setTimeout(function(){ $("#section-submition").show(); 
                        $('html, body').animate({scrollTop: $("#section-submition").offset().top - 300}, 1000);}, 500);
                        var finalCashFlow = salary;
                        console.log(salary);
                    };
    
                    //Result Calculation
                    function SalaryCalculation(data){
                        salary = (salary- data);
                        salary = parseFloat(salary).toFixed(2);
                        return salary;
                    };
                }else{
                    $("#questionSet-46-60_q1").show();
                    setTimeout(function(){ $("#questionSet-46-60_q1").show(); 
                    $('html, body').animate({scrollTop: $("#questionSet-46-60_q1").offset().top - 300}, 1000);}, 500);
                    setTimeout(function(){ $("#questionSet-46-60_q1_answer").show();}, 1000);

                    //Specify Category 33-45 answer by Array 
                    var cat3_q1_Array_Id = ['cat3_heartland_shop','cat3_supermarkets','cat3_departmental_stores','cat3_international_brand'];
                    var cat3_q2_Array_Id = ['home_cooked','local_hawker','cater_tingkat_meal','restaurants'];
                    var cat3_q3_Array_Id = ['cat3_q3_below_100','100_140','140_170','above_170'];
                    var cat3_q4_Array_Id = ['below_50','cat3_q4_below_100','below_120','below_150'];
                    var cat3_q5_Array_Id = ['399_500','500_800','800_1200','above_1200'];
                    var cat3_q6_Array_Id = ['cat3_public_bus_trains','cat3_taxis','cat3_ride_hailing','cat3_own_car'];
                    var cat3_q7_Array_Id = ['business_class_flight','5_star_accomodatio','michelin_star_dining','private_tours'];
                    var cat3_q8_Array_Id = ['exercise_healthy_eating','health_supplement','chinese_medicine','medical_check_up'];
    
                    //Question 1 Looping by Array 
                    jQuery.each(cat3_q1_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_46_60_q1();
                            $(".data_q1").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat3_q1_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 2 Looping by Array 
                    jQuery.each(cat3_q2_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_46_60_q2();
                            $(".data_q2").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat3_q2_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });
                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 3 Looping by Array 
                    jQuery.each(cat3_q3_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_46_60_q3();
                            $(".data_q3").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat3_q3_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 4 Looping by Array 
                    jQuery.each(cat3_q4_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_46_60_q4();
                            $(".data_q4").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat3_q4_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 5 Looping by Array 
                    jQuery.each(cat3_q5_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_46_60_q5();
                            $(".data_q5").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat3_q5_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 6 Looping by Array 
                    jQuery.each(cat3_q6_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_46_60_q6();
                            $(".data_q6").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat3_q6_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 7 Looping by Array 
                    jQuery.each(cat3_q7_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_46_60_q7();
                            $(".data_q7").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat3_q7_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Question 8 Looping by Array 
                    jQuery.each(cat3_q8_Array_Id, function(i,id) {
                        $("#"+id).click(function () {
                            var attr_Value = $("#"+id).val();
                            var attr_text = $("#"+id).text().replace(/[>]/gi,'');
                            $(".annual-salary span").text("$" + SalaryCalculation(attr_Value)).digits();
                            Contorl_QuestionSet_46_60_q8();
                            ResultSectionControl();
                            $(".data_q8").text(attr_text); //answer data
                            $("#"+id).css({'background-color':'#fd7570','color':'white'});

                            jQuery.each(cat3_q8_Array_Id, function(i,new_id){
                                $("#"+new_id).removeClass('addHover');
                            });                        });
                        $("#"+id).addClass('addHover');
                    });
    
                    //Refactor
                    function Contorl_QuestionSet_46_60_q1(){
                        $("#questionSet-46-60_q1_response").show();
                        setTimeout(function(){ $("#questionSet-46-60_q2").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-46-60_q2").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-46-60_q2_answer").show();}, 1000);

                        $("#cat3_heartland_shop").attr("disabled", "disabled");
                        $("#cat3_supermarkets").attr("disabled", "disabled");
                        $("#cat3_departmental_stores").attr("disabled", "disabled");
                        $("#cat3_international_brand").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_46_60_q2(){
                        $("#questionSet-46-60_q2_response").show();
                        setTimeout(function(){ $("#questionSet-46-60_q3").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-46-60_q3").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-46-60_q3_answer").show();}, 1000);

                        $("#home_cooked").attr("disabled", "disabled");
                        $("#local_hawker").attr("disabled", "disabled");
                        $("#cater_tingkat_meal").attr("disabled", "disabled");
                        $("#restaurants").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_46_60_q3(){
                        $("#questionSet-46-60_q3_response").show();
                        setTimeout(function(){ $("#questionSet-46-60_q4").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-46-60_q4").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-46-60_q4_answer").show();}, 1000);

                        $("#cat3_q3_below_100").attr("disabled", "disabled");
                        $("#100_140").attr("disabled", "disabled");
                        $("#140_170").attr("disabled", "disabled");
                        $("#above_170").attr("disabled", "disabled");
                    };
                    
                    //Refactor
                    function Contorl_QuestionSet_46_60_q4(){
                        $("#questionSet-46-60_q4_response").show();
                        setTimeout(function(){ $("#questionSet-46-60_q5").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-46-60_q5").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-46-60_q5_answer").show();}, 1000);

                        $("#below_50").attr("disabled", "disabled");
                        $("#cat3_q4_below_100").attr("disabled", "disabled");
                        $("#below_120").attr("disabled", "disabled");
                        $("#below_150").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_46_60_q5(){
                        $("#questionSet-46-60_q5_response").show();
                        setTimeout(function(){ $("#questionSet-46-60_q6").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-46-60_q6").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-46-60_q6_answer").show();}, 1000);

                        $("#399_500").attr("disabled", "disabled");
                        $("#500_800").attr("disabled", "disabled");
                        $("#800_1200").attr("disabled", "disabled");
                        $("#above_1200").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_46_60_q6(){
                        $("#questionSet-46-60_q6_response").show();
                        setTimeout(function(){ $("#questionSet-46-60_q7").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-46-60_q7").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-46-60_q7_answer").show();}, 1000);

                        $("#cat3_public_bus_trains").attr("disabled", "disabled");
                        $("#cat3_taxis").attr("disabled", "disabled");
                        $("#cat3_ride_hailing").attr("disabled", "disabled");
                        $("#cat3_own_car").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_46_60_q7(){
                        $("#questionSet-46-60_q7_response").show();
                        setTimeout(function(){ $("#questionSet-46-60_q8").show(); 
                        $('html, body').animate({scrollTop: $("#questionSet-46-60_q8").offset().top - 300}, 1000);}, 500);
                        setTimeout(function(){ $("#questionSet-46-60_q8_answer").show();}, 1000);

                        $("#business_class_flight").attr("disabled", "disabled");
                        $("#5_star_accomodatio").attr("disabled", "disabled");
                        $("#michelin_star_dining").attr("disabled", "disabled");
                        $("#private_tours").attr("disabled", "disabled");
                    };
    
                    //Refactor
                    function Contorl_QuestionSet_46_60_q8(){
                        $("#exercise_healthy_eating").attr("disabled", "disabled");
                        $("#health_supplement").attr("disabled", "disabled");
                        $("#chinese_medicine").attr("disabled", "disabled");
                        $("#medical_check_up").attr("disabled", "disabled");
                    };
    
                    //Result here after answered all question 
                    function ResultSectionControl(){
                        $(".response-after-q8-text span").text("$" + salary).digits();
                        $("#cashflow_response").show();
                        setTimeout(function(){ $("#section-submition").show(); 
                        $('html, body').animate({scrollTop: $("#section-submition").offset().top - 300}, 1000);}, 500);

                        var finalCashFlow = salary;
                        console.log(salary);
                    };
    
                    //Result Calculation
                    function SalaryCalculation(data){
                        salary = (salary- data);
                        salary = parseFloat(salary).toFixed(2);
                        return salary;
                    };
                }
            }else{
                e.preventDefault();
            }
        });
    },

    submitFromAndValidation(){
        $("#first_name").focusin(function() {
            $(this).siblings("#errorFirstName").text("");
        });

        $("#last_name").focusin(function() {
            $(this).siblings("#errorLastName").text("");
        });

        $("#phone").focusin(function() {
            $(this).siblings("#errorPhone").text("");
        });

        $("#email").focusin(function() {
            $(this).siblings("#errorEmail").text("");
        });

        $('#pdpa_checkbox').change(function(){
            if($(this).prop("checked")) {
                $("#errorPDPA_checkbox").hide();
            }
        });
        
        $('#marketing_checkbox').change(function(){
            if($(this).prop("checked")) {
              $("#errorMarketing_checkbox").hide();
            }
        });

        $("#submitFormBtn").click(function(e) {
            var check_firstname = $('#first_name').val();
            var check_lastname = $('#last_name').val();
            var check_phone = $('#phone').val();
            var check_email = $('#email').val();
            var check_pdpa_checkbox = $('#pdpa_checkbox');
            var check_marketing_checkbox = $('#marketing_checkbox');
            var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/
            var reg_phone = /^[0-9]*$/;

            if(check_firstname == ""){
                $('#errorFirstName').text("Please enter your first name.");
            };
            if(check_lastname == ""){
                $('#errorLastName').text("Please enter your last name.");
            };
            if (!reg_email.test(check_email)) {
                $('#errorEmail').text("Please enter a valid email address.");
            };
            if((check_phone.length < 8) || (!reg_phone.test(check_phone))){
                $('#errorPhone').text("Please enter a valid phone number.");
            };
            if(check_pdpa_checkbox.is(":not(:checked)")){
                $("#errorPDPA_checkbox").show();
                $('#errorPDPA_checkbox').text("Please accept the terms and conditions to continue.");
            };

            if(check_firstname != "" && check_lastname != "" && check_email != "" && check_phone != "" && 
            check_phone.length >= 8 && reg_email.test(check_email) && reg_phone.test(check_phone) && check_pdpa_checkbox.is(":checked")){
                
                e.preventDefault();
                $("#submitFormBtn").removeClass('addHover-Button');
                $('#submitFormBtn').prop('disabled', 'disabled');
                $("#submitFormBtn").hide();
                $(".loading-icon").show();

                var $phone = $('#phone');
                var $email = $('#email');
                var phone = $phone.val();
                var email = $email.val().toLowerCase();
                var firstname = $('#first_name').val();
                var lastname = $('#last_name').val();
                var name = firstname + " " + lastname;
                var finalCashFlow = $('.response-after-q8-text span').text();
                var salary_selected = $('.salary_selected').text();
                var ageRange_selected = $('.ageRange_selected').text();
                var data_q1 = $('.data_q1').text();
                var data_q2 = $('.data_q2').text();
                var data_q3 = $('.data_q3').text();
                var data_q4 = $('.data_q4').text();
                var data_q5 = $('.data_q5').text();
                var data_q6 = $('.data_q6').text();
                var data_q7 = $('.data_q7').text();
                var data_q8 = $('.data_q8').text();
                var hashed_phone = sha256($phone.val());
                var hashed_email = sha256($email.val().toLowerCase());
                var marketing_consent_bool = false;
                var marketing_consent = "No";
                
                var object = new Object();
                object.email = hashed_email;
                object.phone = hashed_phone;
                var myJsonData = JSON.stringify(object);
        
                if(check_marketing_checkbox.is(":checked")){
                    marketing_consent_bool = true;
                    marketing_consent = "Yes";
                }

                console.log(hashed_phone,hashed_email);
                console.log(salary_selected,ageRange_selected,data_q1,data_q2,data_q3,data_q4,data_q5,data_q6,data_q7,data_q8);
                console.log(firstname,lastname,name,phone,email,finalCashFlow);
                console.log(marketing_consent_bool,marketing_consent);

                function sha256(ascii) {
                    function rightRotate(value, amount) {
                        return (value>>>amount) | (value<<(32 - amount));
                    };
                    var mathPow = Math.pow;
                    var maxWord = mathPow(2, 32);
                    var lengthProperty = 'length'
                    var i, j; 
                    var result = ''
                    var words = [];
                    var asciiBitLength = ascii[lengthProperty]*8;
                    var hash = sha256.h = sha256.h || [];
                    var k = sha256.k = sha256.k || [];
                    var primeCounter = k[lengthProperty];
                    var isComposite = {};
    
                    for (var candidate = 2; primeCounter < 64; candidate++) {
                        if (!isComposite[candidate]) {
                            for (i = 0; i < 313; i += candidate) {
                                isComposite[i] = candidate;
                            }
                            hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
                            k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
                        }
                    }
                
                    ascii += '\x80'
                    while (ascii[lengthProperty]%64 - 56) ascii += '\x00' 
                    for (i = 0; i < ascii[lengthProperty]; i++) {
                        j = ascii.charCodeAt(i);
                        if (j>>8) return; 
                        words[i>>2] |= j << ((3 - i)%4)*8;
                    }
    
                    words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
                    words[words[lengthProperty]] = (asciiBitLength)
    
                    for (j = 0; j < words[lengthProperty];) {
                        var w = words.slice(j, j += 16); 
                        var oldHash = hash;
                        hash = hash.slice(0, 8);
                        
                        for (i = 0; i < 64; i++) {
                            var i2 = i + j;
                            var w15 = w[i - 15], w2 = w[i - 2];
                            var a = hash[0], e = hash[4];
                            var temp1 = hash[7]
                                + (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
                                + ((e&hash[5])^((~e)&hash[6])) // ch
                                + k[i]
                                + (w[i] = (i < 16) ? w[i] : (
                                        w[i - 16]
                                        + (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
                                        + w[i - 7]
                                        + (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
                                    )|0
                                );
                            var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
                                + ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj
                            hash = [(temp1 + temp2)|0].concat(hash); 
                            hash[4] = (hash[4] + temp1)|0;
                        }
                    
                        for (i = 0; i < 8; i++) {
                            hash[i] = (hash[i] + oldHash[i])|0;
                        }
                    }
                    
                    for (i = 0; i < 8; i++) {
                        for (j = 3; j + 1; j--) {
                            var b = (hash[i]>>(j*8))&255;
                            result += ((b < 16) ? 0 : '') + b.toString(16);
                        }
                    }
                    return result;
                };
     
                $.ajax({
                    type: 'POST',
                    url: 'https://kw4ixwjpfd.execute-api.ap-southeast-1.amazonaws.com/campaign2/submit',
                    data: myJsonData,
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        if (data) {
                            var jsonBody = (data.body) ? data.body : "";
                            var body = (jsonBody) ? JSON.parse(jsonBody) : "";
                            var msg = (body) ? body.Message : "";

                            if (data.statusCode == 200) {
                                console.log(msg);

                                var payload = {
                                    "name": name,
                                    "phone": phone,
                                    "email": email,   
                                    "Age": ageRange_selected,
                                    "Salary": salary_selected,
                                    "Answer1": data_q1,
                                    "Answer2": data_q2,
                                    "Answer3": data_q3,
                                    "Answer4": data_q4,
                                    "Answer5": data_q5,
                                    "Answer6": data_q6,
                                    "Answer7": data_q7,
                                    "Answer8": data_q8,
                                    "FinalCashFlow": finalCashFlow,
                                    "MarketingConsent": marketing_consent,
                                }
                                        
                                var text =
                                    ""+name+" has submitted a form.\n" +
                                    "Name: "+name+" \n" +
                                    "Phone: "+phone+" \n" +
                                    "Email: "+email+" \n" + 
                                    "Annual Salary: "+salary_selected+" \n" + 
                                    "Age: "+ageRange_selected+" years old\n" + 
                                    "Answer For Question 1 : "+data_q1+" \n" + 
                                    "Answer For Question 2 : "+data_q2+" \n" + 
                                    "Answer For Question 3 : "+data_q3+" \n" + 
                                    "Answer For Question 4 : "+data_q4+" \n" + 
                                    "Answer For Question 5 : "+data_q5+" \n" + 
                                    "Answer For Question 6 : "+data_q6+" \n" + 
                                    "Answer For Question 7 : "+data_q7+" \n" + 
                                    "Answer For Question 8 : "+data_q8+" \n" + 
                                    "Final Cash Flow: "+finalCashFlow+" \n" +
                                    "Marketing Consent: "+marketing_consent+" \n";
                                
                                try {
                                    window.submitForm({
                                        formType: "Make-Sense-of-Your-Finances",
                                        payload: payload,
                                        contactInfo: {
                                            firstName: firstname,
                                            lastName: lastname,
                                            emails: [{
                                                address: email,
                                            }],
                                            phones: [{   
                                                phoneNumber: phone,
                                                phoneTypeId: "MOBILE",
                                            }],
                                            tags: ["Make-Sense-of-Your-Finances"],
                                            opt_in: {
                                                is_email_allowed: marketing_consent_bool,
                                                is_sms_allowed: marketing_consent_bool,
                                            }
                                        },
                                        message: {
                                            text: text
                                        }
                                    }).then(() => {
                                        
                                        try {
                                            $('#first_name').val("");
                                            $('#last_name').val("");
                                            $('#phone').val("");
                                            $('#email').val("");
                                            $('#pdpa_checkbox').prop('checked', false);
                                            $('#marketing_checkbox').prop('checked', false);
                                            $(".start-game-select-salary").prop('selectedIndex',0);
                                            $(".start-game-select-age").prop('selectedIndex',0);
                                            $(".loading-icon").hide();
        
                                            console.log("Information submitted to Backoffice.");
                                            window.location = "/make-sense-of-your-finances-thankyou";
                                        } catch (err) {
                                            $("#submitFormBtn").addClass('addHover-Button');
                                            $('#submitFormBtn').prop('disabled', '');
                                            $("#submitFormBtn").show();
                                            $(".loading-icon").hide();
                                            console.log("Failed to go thank you page.");
                                        }
                                    });
                                }
                                catch(err) {
                                    $("#submitFormBtn").addClass('addHover-Button');
                                    $('#submitFormBtn').prop('disabled', '');
                                    $("#submitFormBtn").show();
                                    $(".loading-icon").hide();
                                    console.log("subbmition to gefen error.");
                                }

                            } else {
                                $("#submitFormBtn").show();
                                $(".loading-icon").hide();
                                $("#duplicate-section").show(); 
                                $("#submitFormBtn").addClass('addHover-Button');
                                $('#submitFormBtn').prop('disabled', '');
                                console.log("Duplicate entry.");
                            }
                        }else{
                            $("#submitFormBtn").addClass('addHover-Button');
                            $('#submitFormBtn').prop('disabled', '');
                            $("#submitFormBtn").show();
                            $(".loading-icon").hide();
                            console.log("Invalid data return by API.");
                        }
                    },
                    error: function (data) {
                        $("#submitFormBtn").addClass('addHover-Button');
                        $('#submitFormBtn').prop('disabled', '');
                        $("#submitFormBtn").show();
                        $(".loading-icon").hide();  
                        console.log("Error with API POST.");   
                    }
                });
            };
        });
    },

    QuestionSet_22_To_32() {
        $("#questionSet-22-32_q1").hide("fast");
        $("#questionSet-22-32_q2").hide("fast");
        $("#questionSet-22-32_q3").hide("fast");
        $("#questionSet-22-32_q4").hide("fast");
        $("#questionSet-22-32_q5").hide("fast");
        $("#questionSet-22-32_q6").hide("fast");
        $("#questionSet-22-32_q7").hide("fast");
        $("#questionSet-22-32_q8").hide("fast");

        $("#questionSet-22-32_q1_answer").hide("fast");
        $("#questionSet-22-32_q2_answer").hide("fast");
        $("#questionSet-22-32_q3_answer").hide("fast");
        $("#questionSet-22-32_q4_answer").hide("fast");
        $("#questionSet-22-32_q5_answer").hide("fast");
        $("#questionSet-22-32_q6_answer").hide("fast");
        $("#questionSet-22-32_q7_answer").hide("fast");
        $("#questionSet-22-32_q8_answer").hide("fast");

        $("#questionSet-22-32_q1_response").hide("fast");
        $("#questionSet-22-32_q2_response").hide("fast");
        $("#questionSet-22-32_q3_response").hide("fast");
        $("#questionSet-22-32_q4_response").hide("fast");
        $("#questionSet-22-32_q5_response").hide("fast");
        $("#questionSet-22-32_q6_response").hide("fast");
        $("#questionSet-22-32_q7_response").hide("fast");
        $("#questionSet-22-32_q8_response").hide("fast");
    },

    QuestionSet_33_To_45() {
        $("#questionSet-33-45_q1").hide("fast");
        $("#questionSet-33-45_q2").hide("fast");
        $("#questionSet-33-45_q3").hide("fast");
        $("#questionSet-33-45_q4").hide("fast");
        $("#questionSet-33-45_q5").hide("fast");
        $("#questionSet-33-45_q6").hide("fast");
        $("#questionSet-33-45_q7").hide("fast");
        $("#questionSet-33-45_q8").hide("fast");

        $("#questionSet-33-45_q1_answer").hide("fast");
        $("#questionSet-33-45_q2_answer").hide("fast");
        $("#questionSet-33-45_q3_answer").hide("fast");
        $("#questionSet-33-45_q4_answer").hide("fast");
        $("#questionSet-33-45_q5_answer").hide("fast");
        $("#questionSet-33-45_q6_answer").hide("fast");
        $("#questionSet-33-45_q7_answer").hide("fast");
        $("#questionSet-33-45_q8_answer").hide("fast");

        $("#questionSet-33-45_q1_response").hide("fast");
        $("#questionSet-33-45_q2_response").hide("fast");
        $("#questionSet-33-45_q3_response").hide("fast");
        $("#questionSet-33-45_q4_response").hide("fast");
        $("#questionSet-33-45_q5_response").hide("fast");
        $("#questionSet-33-45_q6_response").hide("fast");
        $("#questionSet-33-45_q7_response").hide("fast");
        $("#questionSet-33-45_q8_response").hide("fast");
    },

    QuestionSet_46_To_60() {
        $("#questionSet-46-60_q1").hide("fast");
        $("#questionSet-46-60_q2").hide("fast");
        $("#questionSet-46-60_q3").hide("fast");
        $("#questionSet-46-60_q4").hide("fast");
        $("#questionSet-46-60_q5").hide("fast");
        $("#questionSet-46-60_q6").hide("fast");
        $("#questionSet-46-60_q7").hide("fast");
        $("#questionSet-46-60_q8").hide("fast");

        $("#questionSet-46-60_q1_answer").hide("fast");
        $("#questionSet-46-60_q2_answer").hide("fast");
        $("#questionSet-46-60_q3_answer").hide("fast");
        $("#questionSet-46-60_q4_answer").hide("fast");
        $("#questionSet-46-60_q5_answer").hide("fast");
        $("#questionSet-46-60_q6_answer").hide("fast");
        $("#questionSet-46-60_q7_answer").hide("fast");
        $("#questionSet-46-60_q8_answer").hide("fast");

        $("#questionSet-46-60_q1_response").hide("fast");
        $("#questionSet-46-60_q2_response").hide("fast");
        $("#questionSet-46-60_q3_response").hide("fast");
        $("#questionSet-46-60_q4_response").hide("fast");
        $("#questionSet-46-60_q5_response").hide("fast");
        $("#questionSet-46-60_q6_response").hide("fast");
        $("#questionSet-46-60_q7_response").hide("fast");
        $("#questionSet-46-60_q8_response").hide("fast");
    },

    AfterFinishedQuestionControl() {
        $("#cashflow_response").hide("fast");
        $("#section-submition").hide("fast");
        $("#duplicate-section").hide("fast");
        $("#more-info-section").hide("fast");
    },

    ButtonControl() {
        var status;

        $("#playGameBtn").click(function() {
            $("#main-screen").fadeOut();
            $("#tncActive").fadeOut();
            $("#start-game-section").fadeIn();
            $('html, body').scrollTop(0);
            status = "started";
            console.log(status);
        });

        $(".marketing_Consent_Btn").click(function() {
            $("#main-screen").fadeOut();
            $("#start-game-section").fadeOut();
            $("#tncActive").fadeOut();
            $("#marketing_consent_section").fadeIn();
            $('html,body').animate({scrollTop: 0});
        });

        $(".tncBtn").click(function() {
            $("#main-screen").fadeOut();
            $("#start-game-section").fadeOut();
            $("#tncActive").fadeIn();
            $(".side-bar-startGame").fadeIn();
            $("#back-to-game-btn").fadeIn();
            $('html, body').scrollTop(0);
        });

        $(".side-bar-startGame").click(function() {
            $("#tncActive").fadeOut();
            $("#start-game-section").fadeOut();
            $(".side-bar-startGame").fadeOut();
            $("#main-screen").fadeIn();
            $('html, body').scrollTop(0);
        });

        $(".side-bar-tnc").click(function() {
            if(status == "started"){
                $("#tncActive").fadeIn();
                $('html,body').animate({scrollTop: 0});
                $("#main-screen").fadeOut();
                $("#start-game-section").fadeOut();
                $(".side-bar-startGame").fadeOut();
                $('#tncActive').scrollTop(0);
            }else{
                $("#main-screen").fadeOut();
                $("#start-game-section").fadeOut();
                $("#tncActive").fadeIn();
                $(".side-bar-startGame").fadeIn();
                $('html, body').scrollTop(0);
            };
        });

        $("#back-to-game-btn2").click(function() {
            if(status == "started"){
                $("#tncActive").fadeOut();
                $("#marketing_consent_section").fadeOut();
                $(".side-bar-startGame").fadeOut();
                $("#main-screen").fadeOut();
                $("#start-game-section").fadeIn();
                $("html, body").animate({ scrollTop: $(".hidden-div").offset().top - 1300}, 2000);
            }else{
                $("#tncActive").fadeOut();
                $("#marketing_consent_section").fadeOut();
                $("#start-game-section").fadeOut();
                $(".side-bar-startGame").fadeOut();
                $("#main-screen").fadeIn();
                $('html, body').scrollTop(0);
            };
        });

        $("#back-to-game-btn-mobile").click(function() {
            if(status == "started"){
                $("#tncActive").fadeOut();
                $("#marketing_consent_section").fadeOut();
                $(".side-bar-startGame").fadeOut();
                $("#main-screen").fadeOut();
                $("#start-game-section").fadeIn();
                $("html, body").animate({ scrollTop: $(".hidden-div").offset().top - 3300}, 2000);
            }else{
                $("#tncActive").fadeOut();
                $("#marketing_consent_section").fadeOut();
                $("#start-game-section").fadeOut();
                $(".side-bar-startGame").fadeOut();
                $("#main-screen").fadeIn();
                $('html, body').scrollTop(0);
            };

            if (window.location.search == "?terms-and-conditions") {
                var url = window.location.href; 
                url = url.split('?')[0];
                console.log(url);
                window.location.replace(url);
            };
        });

        $("#back-to-game-btn").click(function() {
            if(status == "started"){
                $("#tncActive").fadeOut();
                $("#marketing_consent-section").fadeOut();
                $(".side-bar-startGame").fadeOut();
                $("#main-screen").fadeOut();
                $("#start-game-section").fadeIn();

                if($(document).height() > 2650){ 
                    $("html, body").animate({ scrollTop: $(".hidden-div").offset().top - 1800}, 2000);
                };
            }else{
                $("#tncActive").fadeOut();
                $("#start-game-section").fadeOut();
                $(".side-bar-startGame").fadeOut();
                $("#main-screen").fadeIn();
                $('html, body').scrollTop(0);
            };

            if (window.location.search == "?terms-and-conditions") {
                var url = window.location.href; 
                url = url.split('?')[0];
                console.log(url);
                window.location.replace(url);
            };
        });
    },
};

module.exports = make_sense_of_your_finances;
