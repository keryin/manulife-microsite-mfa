const share_your_feels = {

    init: function() {
        var that = this;
        that.loadingScreen();
        that.stepTabProgress();
        that.showTermsCondition();
        that.quesRangeSlider();
        that.submitFromAndValidation();

        $(document).on('click', '.button-close-banner', function(event) {
            event.preventDefault();
            event.stopPropagation();
            $('.campaign-sticky-banner').fadeOut( "slow", "swing", function() {});
        });
    },

    loadingScreen() {
        function onReady(callback) {
            var intervalId = window.setInterval(function() {
            if (document.getElementById('ShareYourFeelsForm')[0] !== undefined) {
                window.clearInterval(intervalId);
                callback.call(this);
            }
            }, 2000);
        }
          
        function setVisible(selector, visible) {
            document.querySelector(selector).style.display = visible ? 'block' : 'none';
        }

        if($(".page-name-share-your-feels").length){
            onReady(function() {
                setVisible('#loadingsyf', false);
                $("#feels-landing").addClass("active-data");
            });            
        }
    },

    stepTabProgress() {
        var current_fs, next_fs, last_Ques, selected_Emoji; //data-content
        var left, opacity, scale; //data-content properties which we will animate
        var animating; //flag to prevent quick multi-click glitches        
        var finalScore = 0;
        var scoreMark = 0;


        $(".next-btn").click(function(){
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parents(".data-content");
            next_fs = $(this).parents(".data-content").next();
            selected_Emoji = current_fs.find(".selected-emoji");
            last_Ques = $(this).parents(".last-question");

            //activate next step on progressbar using the index of next_fs
            $(".tab-menu li").eq($(".data-content").index(next_fs)).addClass("active").removeClass("disable-tab");
            
            //show the next data-content
            next_fs.addClass("active-data").show(); 
            current_fs.addClass("hide-data").removeClass("active-data")
            //hide the current data-content with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale('+scale+')', 'position': "absolute"});
                    next_fs.css({'left': left, 'opacity': opacity, 'position': "relative"});
                }, 
                duration: 500, 
                complete: function(){
                    current_fs.hide();
                    animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeOutQuint'
            });
            
            // if($("#feels-how-to-play").hasClass("active-data")){
            //     window.setTimeout(function(){            
            //         var demoSliderID = document.getElementById("demoslider");  
            //         var demoSlider = $("#demoslider");    
            //         var demoSliderActive = demoSlider.parents(".active-data");    
            //         var setAnimate = setInterval(sliderAnimate, 50);
            //         function sliderAnimate(){
            //             demoSliderID.stepUp();   
            //             var rangeValue = demoSlider.val();
            //             var thisSliderValue = $("#feels-how-to-play").find(".range-slider__value");
            //             var selectedEmoji = demoSliderActive.find(".selected-emoji");   
            //             var emojiImg = demoSliderActive.find(".emoji-img-"+(+rangeValue));   
            //             thisSliderValue.text(rangeValue);
            //             console.log("rangeValue " + rangeValue);
            //             if(!selectedEmoji.length){
            //                 emojiImg.addClass("selected-emoji");
            //             } else {
            //                 selectedEmoji.removeClass("selected-emoji")
            //             }
            //         };
            //     }, 500);
            // };

            selected_Emoji.each(function () {
                let currentQuestionPoint = parseInt($(this).attr('data-share-score'));
                console.log("currentQuestionPoint = " + currentQuestionPoint);
            });

            if(last_Ques.hasClass("status-activated")){
                console.log('//this is last option in quiz, calculate all points');
                getQuizScore();
                calculateFinalScore();
                updateQuizScoreNumber();
            }
        });

        function getQuizScore() {
            do {
                $('.emoji-img-q.selected-emoji').each(function () {
                    let currentQuestionPoint = parseInt($(this).attr('data-share-score'));
                    finalScore += currentQuestionPoint;
                });
            } while(finalScore == 0 || finalScore == NaN)
        }
    
        function calculateFinalScore() {
            var shareResult = $("#feels-share-result");
            var shareResultContent = shareResult.find(".block-result");
            var resultImage = ["https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/staging/mls-mfa/images/campaigns/share-your-feels/Results_Fantastic.png", 
            "https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/staging/mls-mfa/images/campaigns/share-your-feels/Results_Optimistic.png", 
            "https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/staging/mls-mfa/images/campaigns/share-your-feels/Results_Meh.png", 
            "https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/staging/mls-mfa/images/campaigns/share-your-feels/Results_worried.png", 
            "https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/staging/mls-mfa/images/campaigns/share-your-feels/Results_Frustrated.png"];
            var resultDescription = ["You’re feeling satisfied about how your life is going. You’ve got your finances under control and you feel like a winner. Care to share some tips? 😉", 
            "You’re feeling contented about your life right now and you’re feeling confident you’re on the right track. Good for you! 🙌🏼", 
            "You’re feeling so-so about the way things are going and can’t put a finger on how you feel right now. That’s alright. It happens to everyone. ✌🏼", 
            "We feel you. Life can become challenging and sometimes, worries can keep us up at night. Take a breather. This too shall pass. 🙏🏼", 
            "Sorry to hear that your outlook on life is bleak right now. We hope tomorrow will be a better day. In the meantime, “Just keep swimming!” 🐠"];            
            // var fbShareButton = $(".fb-share-button");
            // var fbSharer = "https://www.facebook.com/sharer/sharer.php?u=";
            // var resultLink = [fbSharer + currentLink + "-fantastic", 
            // fbSharer + currentLink + "-optimistic", 
            // fbSharer + currentLink + "-meh", 
            // fbSharer + currentLink + "-worried", 
            // fbSharer + currentLink + "-frustrated"];      
            var currentLink = window.location.href.replace(/\?/g, "");
            var resultLink = [currentLink + "-fantastic", 
            currentLink + "-optimistic", 
            currentLink + "-meh", 
            currentLink + "-worried", 
            currentLink + "-frustrated"]; 
            var resultTitle = ["Life got me feelin’ FANTASTIC! How about you? Take the survey: ",
            "Life got me feelin’ OPTIMISTIC! How about you? Take the survey: ",
            "Life got me feelin’ MEH. How about you? Take the survey: ",
            "Life got me feelin’ WORRIED. How about you? Take the survey: ",
            "Life got me feeling’ FRUSTRATED. How about you? Take the survey: "]
            var resultShareDescrip = ["Take our quick survey and get a $10 Starbucks Voucher if you’re one of the first 2,000 participants!",
            "Take our quick survey and get a $10 Starbucks Voucher if you’re one of the first 2,000 participants!",
            "Take our quick survey and get a $10 Starbucks Voucher if you’re one of the first 2,000 participants!",
            "Take our quick survey and get a $10 Starbucks Voucher if you’re one of the first 2,000 participants!",
            "Take our quick survey and get a $10 Starbucks Voucher if you’re one of the first 2,000 participants!"]

            if(finalScore < 26 && finalScore >= 21){
                scoreMark = "Fantastic!";
                shareResultContent.append('<div class="block-image"><img src="' + resultImage[0] + '"></div>' 
                + '<div class="block-text"><p>' + resultDescription[0] + '</p></div>');
                // fbShareButton.attr('href', resultLink[0]);
                addthis.update('share', 'url', resultLink[0]); 
                addthis.update('share', 'title', resultTitle[0]);
                addthis.update('share', 'description', resultShareDescrip[0]);
                addthis.url = resultLink[0]; 
            }
            if(finalScore < 21 && finalScore >= 16){
                scoreMark = "Optimistic!";
                shareResultContent.append('<div class="block-image"><img src="' + resultImage[1] + '"></div>' 
                + '<div class="block-text"><p>' + resultDescription[1] + '</p></div>');
                // fbShareButton.attr('href', resultLink[1]);     
                addthis.update('share', 'url', resultLink[1]); 
                addthis.update('share', 'title', resultTitle[1]);
                addthis.update('share', 'description', resultShareDescrip[1]);
                addthis.url = resultLink[1]; 
            }
            if(finalScore < 16 && finalScore >= 11){
                scoreMark = "Meh";
                shareResultContent.append('<div class="block-image"><img src="' + resultImage[2] + '"></div>' 
                + '<div class="block-text"><p>' + resultDescription[2] + '</p></div>');
                // fbShareButton.attr('href', resultLink[2]);
                addthis.update('share', 'url', resultLink[2]); 
                addthis.update('share', 'title', resultTitle[2]);
                addthis.update('share', 'description', resultShareDescrip[2]);
                addthis.url = resultLink[2]; 
            }
            if(finalScore < 11 && finalScore >= 6){
                scoreMark = "Worried";
                shareResultContent.append('<div class="block-image"><img src="' + resultImage[3] + '"></div>' 
                + '<div class="block-text"><p>' + resultDescription[3] + '</p></div>');
                // fbShareButton.attr('href', resultLink[3]);
                addthis.update('share', 'url', resultLink[3]); 
                addthis.update('share', 'title', resultTitle[3]);
                addthis.update('share', 'description', resultShareDescrip[3]);
                addthis.url = resultLink[3]; 
            }
            if(finalScore <= 5){
                scoreMark = "Frustrated";
                shareResultContent.append('<div class="block-image"><img src="' + resultImage[4] + '"></div>' 
                + '<div class="block-text"><p>' + resultDescription[4] + '</p></div>');
                // fbShareButton.attr('href', resultLink[4]);
                addthis.update('share', 'url', resultLink[4]); 
                addthis.update('share', 'title', resultTitle[4]);
                addthis.update('share', 'description', resultShareDescrip[4]);
                addthis.url = resultLink[4]; 
            }
        }

        function updateQuizScoreNumber() {
            console.log("scoreMark = " + scoreMark + " finalScore = " + finalScore);
            $('.result-score').text( scoreMark + " (" + finalScore + ") ");
        }
    },

    showTermsCondition() {
        $('.showTerms').on('click', function(e){
            $("#term-modal").fadeIn("slow");
            $("body").css("overflow", "hidden");
        });

        $('.showConsent').on('click', function(e){
            $("#consent-modal").fadeIn("slow");
            $("body").css("overflow", "hidden");
        });

        $('.close-modal-btn').on('click', function(e){
            $("#term-modal").fadeOut("slow");
            $("#consent-modal").fadeOut("slow");
            $("body").css("overflow", "");
        });


    },

    quesRangeSlider() {     
        $(document).ready(function() {
            var rangeSlider = function(){
                var slider = $('.range-slider'),
                    range = $('.range-slider__range'),
                    value = $('.range-slider__value');
                    
                slider.each(function(){    
                    value.each(function(){
                        var value = $(this).prev().attr('value');
                        $(this).html(value);
                    });

                    range.on('input change', function(){
                        var rangeSlider = $(this).parent('.range-slider');
                        var rangeQuesCard = $(this).parents('.question-card');
                        var currentValue = this.value;
                        var selectedEmoji = rangeSlider.find(".selected-emoji");
                        var thisValue;
                        $(this).next(value).html(currentValue);

                        rangeQuesCard.addClass("status-activated");

                        if(!selectedEmoji.length){
                            thisValue = +currentValue;
                            var thisEmojiImg = rangeSlider.find(".emoji-img-"+thisValue);
                            thisEmojiImg.addClass("selected-emoji");
                        } else {
                            thisValue = +currentValue;
                            var thisEmojiImg = rangeSlider.find(".emoji-img-"+thisValue);
                            selectedEmoji.removeClass("selected-emoji");
                            thisEmojiImg.addClass("selected-emoji");
                        };

                        var quesActived = rangeQuesCard.hasClass("status-activated");
                        var nextBtn = rangeQuesCard.find(".next-btn");

                        if(quesActived){
                            nextBtn.removeClass("disable-btn");
                        } else {
                            nextBtn.addClass("disable-btn");
                        }
                    });
                });
            };
            rangeSlider();

            function clickEmoji(CurrentValue){
                var thisValue = +CurrentValue;
                var emojiImg = ".emoji-img-"+thisValue+"";

                $(emojiImg).on('click', function(e){
                    var dataContent  = $(this).parents(".data-content");
                    var rangeQuesCard = $(this).parents('.question-card');
                    var thisSliderRange = dataContent.find(".range-slider__range");
                    var thisSliderValue = dataContent.find(".range-slider__value");
                    var selectedEmoji = dataContent.find(".selected-emoji");
                    var thisEmojiImg = dataContent.find(emojiImg);

                    rangeQuesCard.addClass("status-activated");
                    thisSliderRange.val(thisValue);
                    thisSliderValue.text(thisValue);

                    var quesActived = rangeQuesCard.hasClass("status-activated");
                    var nextBtn = rangeQuesCard.find(".next-btn");
                    if(quesActived){
                        nextBtn.removeClass("disable-btn");
                    } else {
                        nextBtn.addClass("disable-btn");
                    }

                    if(!thisSliderRange.hasClass("selected-range")){
                        thisSliderRange.addClass("selected-range");
                    };

                    if(!selectedEmoji.length){
                        thisEmojiImg.addClass("selected-emoji");
                    } else {
                        selectedEmoji.removeClass("selected-emoji");
                        thisEmojiImg.addClass("selected-emoji");
                    };
                });
            }

            clickEmoji(1);
            clickEmoji(2);
            clickEmoji(3);
            clickEmoji(4);
            clickEmoji(5);

            function goNearestValue(CurrentValue){
                $(".range-slider__range").on('mouseout mouseleave touchend', function(){
                    var thisValue = +CurrentValue;
                    var thisValuePlusTwo = thisValue + 0.2;
                    var thisValueMinusTwo = thisValue - 0.2;
                    var dataContent  = $(this).parents(".data-content");
                    var rangeQuesCard = $(this).parents('.question-card');
                    var thisSliderRange = dataContent.find(".range-slider__range");
                    var thisSliderValue = dataContent.find(".range-slider__value");
                    var selectedEmoji = dataContent.find(".selected-emoji");

                    if(thisSliderRange.val() == thisValuePlusTwo || thisSliderRange.val() == thisValueMinusTwo){
                        thisSliderRange.val(thisValue);
                        thisSliderValue.text(thisValue);
                        rangeQuesCard.addClass("status-activated");
                        var thisEmojiImg = dataContent.find(".emoji-img-"+thisValue);
                        if(!selectedEmoji.length){
                            thisEmojiImg.addClass("selected-emoji");
                        }

                        var quesActived = rangeQuesCard.hasClass("status-activated");
                        var nextBtn = rangeQuesCard.find(".next-btn");

                        if(quesActived){
                            nextBtn.removeClass("disable-btn");
                        } else {
                            nextBtn.addClass("disable-btn");
                        }
        
                        if(!thisSliderRange.hasClass("selected-range")){
                            thisSliderRange.addClass("selected-range");
                        };
                    }
                });
            }

            goNearestValue(1);
            goNearestValue(2);
            goNearestValue(3);
            goNearestValue(4);
            goNearestValue(5);

            $("#feels-q1 .range-slider").on('click', function(){ 
                $(".dragme").hide();
            });

            $("#feels-q1 .range-slider__range").on('input', function(){ 
                $(".dragme").hide();
            });
        });
    },

    
    submitFromAndValidation(){
        $("#syfFullName").focusin(function() {
            $('#syfFullName').parent().removeClass("input-error");
        });

        $("#syfPhone").focusin(function() {
            $('#syfPhone').parent().removeClass("input-error");
        });

        $("#syfEmail").focusin(function() {
            $('#syfEmail').parent().removeClass("input-error");
        });
        
        // $('#syfNewsLatter').change(function(){
        //     if($(this).prop("checked")) {
        //       $("#errorMarketing_checkbox").hide();
        //     }
        // });

        $("#syfPhone").on('input', function(){
            if (this.value.length > 8) {
                this.value = this.value.slice(0,8); 
            }
        });

        $('#syfFullName').keyup(function () {
            this.value = this.value.replace(/[^A-Za-z+ -]/g,'');
        });

        // $('#syfNewsLatter').click(function(){
        //     if($(this).is(':checked')){
        //         $('#syfNewsLatter').parent().removeClass("input-error");
        //     } else {
        //         $('#syfNewsLatter').parent().addClass("input-error");
        //     }
        // });

        $("#button-submit-share-your-feels").click(function(e) {
            e.preventDefault();  
            var check_fullname = $('#syfFullName').val();
            var check_phone = $('#syfPhone').val();
            var check_email = $('#syfEmail').val();
            var check_marketing_checkbox = $('#share-your-feels-form').find('.general-checkbox');
            var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/
            var reg_phone = /^[0-9]*$/;
            var reg_name = /^(?:[A-Za-z]+[ -])*[A-Za-z]+$/;

            if(!reg_name.test(check_fullname)){
                $('#syfFullName').parent().addClass("input-error");
            };
            if (!reg_email.test(check_email)) {
                $('#syfEmail').parent().addClass("input-error");
            };
            if((check_phone.length < 8) || (check_phone.length > 8) || (!reg_phone.test(check_phone))){
                $('#syfPhone').parent().addClass("input-error");
            };

            if(check_marketing_checkbox.is(":not(:checked)")){
                check_marketing_checkbox.parents('.news-latter-opt-option').addClass("check-false");
            };

            if(check_fullname != "" && check_email != "" && check_phone != "" && 
            check_phone.length == 8 && reg_email.test(check_email) && reg_phone.test(check_phone) && check_marketing_checkbox.is(":checked")){   
                $('#button-submit-share-your-feels').prop('disabled', 'disabled');
                $("#loadingsyf").show();
                function failedSubmission() {
                    setTimeout(function(){ 
                        $("#loadingsyf").hide();
                        $(".double-summission").after('<div class="col-xs-12 unable-submit submission-error" style="display:block;"><p>Oops, your entry was not submitted. Please try again later.</p></div>');
                        $('#button-submit-share-your-feels').addClass("disable-btn");
                    }, 3000);
                };

                if(!$('#button-submit-share-your-feels').hasClass("disable-btn")){
                    failedSubmission();
                }

                var $phone = $('#syfPhone');
                var $email = $('#syfEmail');
                var phone = $phone.val();
                var email = $email.val().toLowerCase();
                var fullname = $('#syfFullName').val();
                let syfSummaryMessage = '\nQuiz Summary:\n';

                $('.question-card').each(function (index) {
                    syfSummaryMessage += `${index+1}. ${$(this).find('h5').text()}\nAnswer: ${$(this).find('.selected-emoji').attr('data-share-score')}\n`
                });              
                var score_Result = $('#feels-share-result .result-score').text();
                var marketing_consent_bool = false;
                var marketing_consent = "No";
        
                if(check_marketing_checkbox.is(":checked")){
                    marketing_consent_bool = true;
                    marketing_consent = "Yes";
                }

                $(".fullname").text(fullname);
                $(".phonenumber").text(phone);
                $(".emailaddress").text(email);


                console.log(syfSummaryMessage);
                console.log("Name: "+fullname+" \n" + "Phone: "+phone+" \n" + "Email: "+email+" \n");
                console.log("Marketing Consent: " + marketing_consent);
                
                var payload = {
                    "name": fullname,
                    "phone": phone,
                    "email": email,  
                    "Summary": syfSummaryMessage, 
                    "Result Score": score_Result,
                    "MarketingConsent": marketing_consent,
                }
                        
                var text =
                    ""+fullname+" has submitted a form for Share Your Feels.\n" +
                    "Name: "+fullname+" \n" +
                    "Phone: "+phone+" \n" +
                    "Email: "+email+" \n" + 
                    ""+syfSummaryMessage+" \n" +
                    "Result Score: "+score_Result+" \n" + 
                    "Marketing Consent: "+marketing_consent+" \n";
                
                try {
                    window.submitForm({
                        formType: "share-your-feels",
                        payload: payload,
                        contactInfo: {
                            firstName: fullname,
                            emails: [{
                                address: email,
                            }],
                            phones: [{   
                                phoneNumber: phone,
                                phoneTypeId: "MOBILE",
                            }],
                            tags: ["Share Your Feels"],
                            opt_in: {
                                is_email_allowed: marketing_consent_bool,
                                is_sms_allowed: marketing_consent_bool,
                            }
                        },                                        
                        forceCreateContact: true,
                        message: {
                            text: text
                        }
                    }).then(() => {
                        $("#loadingsyf").hide();
                        try {
                            $('#syfFullName').val("");
                            $('#syfPhone').val("");
                            $('#syfEmail').val("");
                            check_marketing_checkbox.prop('checked', false);

                            console.log("Information submitted to Backoffice. Go to result page.");
                            
                            var current_fs, next_fs; //data-content
                            var left, opacity, scale; //data-content properties which we will animate
                            var animating; //flag to prevent quick multi-click glitches    
                            if(animating) return false;
                            animating = true;
                            
                            current_fs = $("#feels-form-wrapper");
                            next_fs = $("#feels-form-wrapper").next();

                            //activate next step on progressbar using the index of next_fs
                            $(".tab-menu li").eq($(".data-content").index(next_fs)).addClass("active").removeClass("disable-tab");
                            
                            //show the next data-content
                            next_fs.addClass("active-data").show(); 
                            current_fs.addClass("hide-data").removeClass("active-data")
                            //hide the current data-content with style
                            current_fs.animate({opacity: 0}, {
                                step: function(now, mx) {
                                    //as the opacity of current_fs reduces to 0 - stored in "now"
                                    //1. scale current_fs down to 80%
                                    scale = 1 - (1 - now) * 0.2;
                                    //2. bring next_fs from the right(50%)
                                    left = (now * 50)+"%";
                                    //3. increase opacity of next_fs to 1 as it moves in
                                    opacity = 1 - now;
                                    current_fs.css({'transform': 'scale('+scale+')', 'position': "absolute"});
                                    next_fs.css({'left': left, 'opacity': opacity, 'position': "relative"});
                                }, 
                                duration: 500, 
                                complete: function(){
                                    current_fs.hide();
                                    animating = false;
                                }, 
                                //this comes from the custom easing plugin
                                easing: 'easeOutQuint'
                            });
                        } catch (err) {
                            $('#button-submit-share-your-feels').addClass("disable-btn");
                            console.log("Failed to go thank you page.");
                        }
                    });
                }
                catch(err) {
                    $("#loadingsyf").hide();
                    $('#button-submit-share-your-feels').addClass("disable-btn");
                    console.log("submission to gefen error.");
                }
            };  
        });
     
        // $("#button-submit-share-your-feels").click(function(e) {
        //     e.preventDefault();  
        //     var check_fullname = $('#syfFullName').val();
        //     var check_phone = $('#syfPhone').val();
        //     var check_email = $('#syfEmail').val();
        //     var check_marketing_checkbox = $('#syfNewsLatter');
        //     var reg_email = /^\w+([\\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/
        //     var reg_phone = /^[0-9]*$/;
        //     var reg_name = /^(?:[A-Za-z]+[ -])*[A-Za-z]+$/;

        //     if(!reg_name.test(check_fullname)){
        //         $('#syfFullName').parent().addClass("input-error");
        //     };
        //     if (!reg_email.test(check_email)) {
        //         $('#syfEmail').parent().addClass("input-error");
        //     };
        //     if((check_phone.length < 8) || (check_phone.length > 8) || (!reg_phone.test(check_phone))){
        //         $('#syfPhone').parent().addClass("input-error");
        //     };

        //     if(check_marketing_checkbox.is(":not(:checked)")){
        //         $('#syfNewsLatter').parent().addClass("input-error");
        //     };

        //     if(check_fullname != "" && check_email != "" && check_phone != "" && 
        //     check_phone.length == 8 && reg_email.test(check_email) && reg_phone.test(check_phone) && check_marketing_checkbox.is(":checked")){   
        //         // grecaptcha.reset();
        //         grecaptcha.execute();
        //         var response = grecaptcha.getResponse();
        //         if(response.length == 0) { 
        //             //reCaptcha not verified
        //             console.log("Google recaptcha failed");
        //             // grecaptcha.execute();
        //             // var captchaWidgetId = grecaptcha.render('captcha', {
        //             //         'sitekey' : '6LeCwrUUAAAAAKMydoggpiglghgUWQxPKfiOsMZ-'
        //             // });
        //             // grecaptcha.reset(captchaWidgetId);
        //         } else {
        //             //reCaptch verified

        //             $('#button-submit-share-your-feels').prop('disabled', 'disabled');
        //             $("#loadingsyf").show();
        //             function failedSubmission() {
        //                 setTimeout(function(){ 
        //                     $("#loadingsyf").hide();
        //                     $(".double-summission").after('<div class="col-xs-12 unable-submit submission-error" style="display:block;"><p>Oops, your entry was not submitted. Please try again later.</p></div>');
        //                     $('#button-submit-share-your-feels').addClass("disable-btn");
        //                 }, 3000);
        //             };

        //             if(!$('#button-submit-share-your-feels').hasClass("disable-btn")){
        //                 failedSubmission();
        //             }

        //             // failedSubmission();
        //             //$("#button-submit-share-your-feels").hide();

        //             var $phone = $('#syfPhone');
        //             var $email = $('#syfEmail');
        //             var phone = $phone.val();
        //             var email = $email.val().toLowerCase();
        //             var fullname = $('#syfFullName').val();
        //             let syfSummaryMessage = '\nQuiz Summary:\n';

        //             $('.question-card').each(function (index) {
        //                 syfSummaryMessage += `${index+1}. ${$(this).find('h5').text()}\nAnswer: ${$(this).find('.selected-emoji').attr('data-share-score')}\n`
        //             });              
        //             var score_Result = $('#feels-share-result .result-score').text();
        //             var hashed_phone = sha256($phone.val());
        //             var hashed_email = sha256($email.val().toLowerCase());
        //             var marketing_consent_bool = false;
        //             var marketing_consent = "No";
                    
        //             var object = new Object();
        //             object.email = hashed_email;
        //             object.phone = hashed_phone;
        //             var myJsonData = JSON.stringify(object);
            
        //             if(check_marketing_checkbox.is(":checked")){
        //                 marketing_consent_bool = true;
        //                 marketing_consent = "Yes";
        //             }

        //             $(".fullname").text(fullname);
        //             $(".phonenumber").text(phone);
        //             $(".emailaddress").text(email);


        //             console.log(hashed_phone,hashed_email);
        //             console.log(syfSummaryMessage);
        //             console.log("Name: "+fullname+" \n" + "Phone: "+phone+" \n" + "Email: "+email+" \n");
        //             console.log("Marketing Consent: " + marketing_consent);
        //             console.log(myJsonData);
        //             function sha256(ascii) {
        //                 function rightRotate(value, amount) {
        //                     return (value>>>amount) | (value<<(32 - amount));
        //                 };
        //                 var mathPow = Math.pow;
        //                 var maxWord = mathPow(2, 32);
        //                 var lengthProperty = 'length'
        //                 var i, j; 
        //                 var result = ''
        //                 var words = [];
        //                 var asciiBitLength = ascii[lengthProperty]*8;
        //                 var hash = sha256.h = sha256.h || [];
        //                 var k = sha256.k = sha256.k || [];
        //                 var primeCounter = k[lengthProperty];
        //                 var isComposite = {};
        
        //                 for (var candidate = 2; primeCounter < 64; candidate++) {
        //                     if (!isComposite[candidate]) {
        //                         for (i = 0; i < 313; i += candidate) {
        //                             isComposite[i] = candidate;
        //                         }
        //                         hash[primeCounter] = (mathPow(candidate, .5)*maxWord)|0;
        //                         k[primeCounter++] = (mathPow(candidate, 1/3)*maxWord)|0;
        //                     }
        //                 }
                    
        //                 ascii += '\x80'
        //                 while (ascii[lengthProperty]%64 - 56) ascii += '\x00' 
        //                 for (i = 0; i < ascii[lengthProperty]; i++) {
        //                     j = ascii.charCodeAt(i);
        //                     if (j>>8) return; 
        //                     words[i>>2] |= j << ((3 - i)%4)*8;
        //                 }
        
        //                 words[words[lengthProperty]] = ((asciiBitLength/maxWord)|0);
        //                 words[words[lengthProperty]] = (asciiBitLength)
        
        //                 for (j = 0; j < words[lengthProperty];) {
        //                     var w = words.slice(j, j += 16); 
        //                     var oldHash = hash;
        //                     hash = hash.slice(0, 8);
                            
        //                     for (i = 0; i < 64; i++) {
        //                         var i2 = i + j;
        //                         var w15 = w[i - 15], w2 = w[i - 2];
        //                         var a = hash[0], e = hash[4];
        //                         var temp1 = hash[7]
        //                             + (rightRotate(e, 6) ^ rightRotate(e, 11) ^ rightRotate(e, 25)) // S1
        //                             + ((e&hash[5])^((~e)&hash[6])) // ch
        //                             + k[i]
        //                             + (w[i] = (i < 16) ? w[i] : (
        //                                     w[i - 16]
        //                                     + (rightRotate(w15, 7) ^ rightRotate(w15, 18) ^ (w15>>>3)) // s0
        //                                     + w[i - 7]
        //                                     + (rightRotate(w2, 17) ^ rightRotate(w2, 19) ^ (w2>>>10)) // s1
        //                                 )|0
        //                             );
        //                         var temp2 = (rightRotate(a, 2) ^ rightRotate(a, 13) ^ rightRotate(a, 22)) // S0
        //                             + ((a&hash[1])^(a&hash[2])^(hash[1]&hash[2])); // maj
        //                         hash = [(temp1 + temp2)|0].concat(hash); 
        //                         hash[4] = (hash[4] + temp1)|0;
        //                     }
                        
        //                     for (i = 0; i < 8; i++) {
        //                         hash[i] = (hash[i] + oldHash[i])|0;
        //                     }
        //                 }
                        
        //                 for (i = 0; i < 8; i++) {
        //                     for (j = 3; j + 1; j--) {
        //                         var b = (hash[i]>>(j*8))&255;
        //                         result += ((b < 16) ? 0 : '') + b.toString(16);
        //                     }
        //                 }
        //                 return result;
        //             };
        
        //             $.ajax({
        //                 type: 'POST',
        //                 url: 'https://inys5refjd.execute-api.ap-southeast-1.amazonaws.com/mfa-campaign1/submit',
        //                 data: myJsonData,
        //                 contentType: 'application/json; charset=utf-8',
        //                 dataType: 'json',
        //                 success: function (data) {
        //                     if (data) {
        //                         var jsonBody = (data.body) ? data.body : "";
        //                         var body = (jsonBody) ? JSON.parse(jsonBody) : "";
        //                         var msg = (body) ? body.Message : "";

        //                         if (data.statusCode == 200) {
        //                             console.log(msg);

        //                             var payload = {
        //                                 "name": fullname,
        //                                 "phone": phone,
        //                                 "email": email,  
        //                                 "Summary": syfSummaryMessage, 
        //                                 // ques_q1: data_q1,
        //                                 // ques_q2: data_q2,
        //                                 // ques_q3: data_q3,
        //                                 // ques_q4: data_q4,
        //                                 // ques_q5: data_q5,
        //                                 "Result Score": score_Result,
        //                                 "MarketingConsent": marketing_consent,
        //                             }
                                            
        //                             var text =
        //                                 ""+fullname+" has submitted a form for Share Your Feels.\n" +
        //                                 "Name: "+fullname+" \n" +
        //                                 "Phone: "+phone+" \n" +
        //                                 "Email: "+email+" \n" + 
        //                                 ""+syfSummaryMessage+" \n" +
        //                                 // ""+ques_q1+" : "+data_q1+" \n" + 
        //                                 // ""+ques_q2+" : "+data_q2+" \n" + 
        //                                 // ""+ques_q3+" : "+data_q3+" \n" + 
        //                                 // ""+ques_q4+" : "+data_q4+" \n" + 
        //                                 // ""+ques_q5+" : "+data_q5+" \n" + 
        //                                 "Result Score: "+score_Result+" \n" + 
        //                                 "Marketing Consent: "+marketing_consent+" \n";
                                    
        //                             try {
        //                                 window.submitForm({
        //                                     formType: "share-your-feels",
        //                                     payload: payload,
        //                                     contactInfo: {
        //                                         firstName: fullname,
        //                                         emails: [{
        //                                             address: email,
        //                                         }],
        //                                         phones: [{   
        //                                             phoneNumber: phone,
        //                                             phoneTypeId: "MOBILE",
        //                                         }],
        //                                         tags: ["Share Your Feels"],
        //                                         opt_in: {
        //                                             is_email_allowed: marketing_consent_bool,
        //                                             is_sms_allowed: marketing_consent_bool,
        //                                         }
        //                                     },                                        
        //                                     forceCreateContact: true,
        //                                     message: {
        //                                         text: text
        //                                     }
        //                                 }).then(() => {
        //                                     $("#loadingsyf").hide();
        //                                     try {
        //                                         $('#syfFullName').val("");
        //                                         $('#syfPhone').val("");
        //                                         $('#syfEmail').val("");
        //                                         $('#syfNewsLatter').prop('checked', false);
            
        //                                         console.log("Information submitted to Backoffice. Go to result page.");
                                                
        //                                         var current_fs, next_fs; //data-content
        //                                         var left, opacity, scale; //data-content properties which we will animate
        //                                         var animating; //flag to prevent quick multi-click glitches    
        //                                         if(animating) return false;
        //                                         animating = true;
                                                
        //                                         current_fs = $("#feels-form-wrapper");
        //                                         next_fs = $("#feels-form-wrapper").next();

        //                                         //activate next step on progressbar using the index of next_fs
        //                                         $(".tab-menu li").eq($(".data-content").index(next_fs)).addClass("active").removeClass("disable-tab");
                                                
        //                                         //show the next data-content
        //                                         next_fs.addClass("active-data").show(); 
        //                                         current_fs.addClass("hide-data").removeClass("active-data")
        //                                         //hide the current data-content with style
        //                                         current_fs.animate({opacity: 0}, {
        //                                             step: function(now, mx) {
        //                                                 //as the opacity of current_fs reduces to 0 - stored in "now"
        //                                                 //1. scale current_fs down to 80%
        //                                                 scale = 1 - (1 - now) * 0.2;
        //                                                 //2. bring next_fs from the right(50%)
        //                                                 left = (now * 50)+"%";
        //                                                 //3. increase opacity of next_fs to 1 as it moves in
        //                                                 opacity = 1 - now;
        //                                                 current_fs.css({'transform': 'scale('+scale+')', 'position': "absolute"});
        //                                                 next_fs.css({'left': left, 'opacity': opacity, 'position': "relative"});
        //                                             }, 
        //                                             duration: 500, 
        //                                             complete: function(){
        //                                                 current_fs.hide();
        //                                                 animating = false;
        //                                             }, 
        //                                             //this comes from the custom easing plugin
        //                                             easing: 'easeOutQuint'
        //                                         });
        //                                     } catch (err) {
        //                                         $('#button-submit-share-your-feels').addClass("disable-btn");
        //                                         console.log("Failed to go thank you page.");
        //                                     }
        //                                 });
        //                             }
        //                             catch(err) {
        //                                 $("#loadingsyf").hide();
        //                                 $('#button-submit-share-your-feels').addClass("disable-btn");
        //                                 console.log("submission to gefen error.");
        //                             }

        //                         } else {
        //                             $("#loadingsyf").hide();
        //                             $('.double-summission').addClass("show-double").show();
        //                             $('#button-submit-share-your-feels').addClass("disable-btn");
        //                             console.log("Duplicate entry.");
        //                         }
        //                     }else{
        //                         $("#loadingsyf").hide();
        //                         $('#button-submit-share-your-feels').addClass("disable-btn");
        //                         console.log("Invalid data return by API.");
        //                     }
        //                 },
        //                 error: function (data) {
        //                     $("#loadingsyf").hide();
        //                     $('#button-submit-share-your-feels').addClass("disable-btn");
        //                     console.log("Error with API POST.");   
        //                 }
        //             }); 
        //             // console.log( 'g-recaptcha-response: ' + response );
        //             console.log("Google recaptcha pass"); 
        //         }
        //     };  
        // });
        $("#button-submit-sign-me-up").click(function(e) {
            e.preventDefault(); 
            var signmeupForm = $("#form-sign-me-up");            
            var signmeupThank = $("#form-sign-me-up-thankyou");
            var sUpName = $(".fullname").text();
            var sUpPhone = $(".phonenumber").text();
            var sUpEmail = $(".emailaddress").text();

            console.log("Name: "+sUpName+" \n" + "Phone: "+sUpPhone+" \n" + "Email: "+sUpEmail+" \n");
            
            var payload = {
                "name": sUpName,
                "phone": sUpPhone,
                "email": sUpEmail,
            }
                    
            var text =
                ""+sUpName+" has submitted a form to get a FREE Financial Checkup from you.\n" +
                "Name: "+sUpName+" \n" +
                "Phone: "+sUpPhone+" \n" +
                "Email: "+sUpEmail+" \n";
            
            window.submitForm({
                formType: "share-your-feels-financial-checkup",
                payload: payload,
                contactInfo: {
                    firstName: sUpName,
                    emails: [{
                        address: sUpEmail,
                    }],
                    phones: [{   
                        phoneNumber: sUpPhone,
                        phoneTypeId: "MOBILE",
                    }],
                    tags: ["Financial Checkup"]
                },
                message: {
                    text: text
                }
            }).then(function(value) {
                console.log(value);
                signmeupForm.hide();
                signmeupThank.show();   
            });
        });
    },

};

module.exports = share_your_feels;
