const chat = {

    init: function () {
        this.fixWidthAndHeightProblemOnMobileSafari();
        this.bind();

    },

    bind : function(){

        $('.contact_banner_chat').on('click', function () {
            $('#up2top').addClass('up2top_chat_open')
        })
        $('#chat_close_button').on('click', function () {
            $('#up2top').removeClass('up2top_chat_open')
        })

        //fix for mozilla firefox, activates the chat manually from our js file and not the dynamic-opertaions.js file that is compiled to our site via backoffice ( Oz )
        if(window.openChat){
            $("[open-chat]").on('click', function(event){
                if (!window.isChatOpened) {
                    window.openChat(undefined, true);
                }
            })
        }

    },

    fixWidthAndHeightProblemOnMobileSafari: function () {
        var is_safari = navigator.userAgent.indexOf("Safari") > -1;

        if (isMobile() && is_safari) {
            $("#chat_container").css({
                "width" : window.innerWidth + 'px',
                "height" : window.innerHeight + 'px'
            });

        }
    },




};



module.exports = chat;
