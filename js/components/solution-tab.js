let solutionTab = {

    init: function () {
        this.solutionTabActive();
    },

    solutionTabActive: function () {
        $(document).ready(function () {
            if($('.section-title-tab').length) {
                let titleTabLink = $('.title-tab-content a[href]');
                let currentPagePath = window.location.pathname.replace('.html', '').replace('/local/', '').split('/').pop();
                
                titleTabLink.each(function () {
                    let currentHref = this.getAttribute("href").split('/').pop().replace(".html", "");        
                    if (currentHref == currentPagePath) {
                        console.log('======this is current page href', currentHref);
                        $(this).parent().addClass('active');        
                    }
                });
            };
        });  
    }
};

module.exports = solutionTab;
