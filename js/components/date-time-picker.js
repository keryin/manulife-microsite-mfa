const dateTimePicker = {

    init : function(){


            var disabledDays = this.getDisabledDaysArray();
            var contactBoxMeetingDate = $('.contact-box .select-date-day-input');
            var productMeetingDate = $('.form-product .select-date-day-input');

            contactBoxMeetingDate.each(function () {
                $(this).datetimepicker({
                    format: 'YYYY-MM-DD',
                    minDate: moment(),
                    useCurrent: false,
                    // keepOpen: true,
                    daysOfWeekDisabled: disabledDays,
                    widgetParent: $(this).parents('.form-contact-box-element'),
                    ignoreReadonly: true,

                });
            })

        productMeetingDate.datetimepicker({
            format: 'YYYY-MM-DD',
            minDate: moment(),
            useCurrent: false,
            // keepOpen: true,
            daysOfWeekDisabled: disabledDays,
            ignoreReadonly: true,


        });


        $(document).on('click', '.picker-switch', function(event) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        });

    },


    getDisabledDaysArray : function(){

        var DAYS = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];
        var TIMES = window.openingHours || [];
        var disabledDays = TIMES.length == 0 ? [] : [0,1,2,3,4,5,6];


        var arrayWithoutSameDays = [];
        if(TIMES.length > 0){
            arrayWithoutSameDays.push(TIMES[0])
        }

        for(var i = 1; i < TIMES.length; i++){
            if(!containsThisDay(arrayWithoutSameDays, TIMES[i])){
                arrayWithoutSameDays.push(TIMES[i]);
            }
        }
        TIMES = arrayWithoutSameDays;

        var parsedDays = [];
        for (var j = 0; j < TIMES.length; j++) {
            
            var from = ( typeof TIMES[j].fromHour === "undefined" ? "" : TIMES[j].fromHour.split(':') ),
                to = ( typeof TIMES[j].toHour === "undefined" ? "" : TIMES[j].toHour.split(':') );

            parsedDays[DAYS.indexOf(TIMES[j].day)] =
            {
                fromHour: parseInt(from[0]),
                fromMinute: parseInt(from[1]),
                toHour: parseInt(to[0]),
                toMinute: parseInt(to[1]) + 1
            };


            disabledDays.splice(disabledDays.indexOf(DAYS.indexOf(TIMES[j].day)), 1);
        }





        //checks if there's a variable with the same .day
        function containsThisDay(array,item){
            //loop
            for(var i = 0 ; i < array.length; i++){
                if(array[i].day === item.day){
                    return true;
                }
            }
            return false;
        }



        /* enable this for get the disabled Days */
        //return disabledDays;



    }


    // ------------------------------------------------ Events



// ================================================ Events






}


module.exports = dateTimePicker;