const lucky_draw_quiz = {

    init: function () {
        this.cacheDom();
        this.bind();
    },

    cacheDom: function () {
        this.$startConditions = $('.start-conditions');
        this.$startQuizButton = $('.start-quiz-button');
        this.$sectionQuestions = $('.section-questions');
        this.$terms = $('.page-name-future-cost-of-living-quiz section.section-contact-accordion .block-accordion-item-title');
        this.$accordionItemContent = $('section.section-contact-accordion .accordion-item-content');
        this.$accordionItem = $('section.section-contact-accordion .accordion-item');
        this.$termsLink = $('.terms-link');
        this.$agree = $('.agree');
        this.$cancel = $('.cancel');

    },

    bind: function () {
        let self = this;
        this.$startQuizButton.click(function (e) {
            e.preventDefault();
            self.$startConditions.slideDown('fast', function () {
                self.scrollToElm($(this));
            });
        });

        this.$termsLink.click(function(e){
            e.preventDefault();
            self.$accordionItemContent.slideDown('slow', function () {
                self.$accordionItem.removeClass('status-closed');
                self.scrollToElm(self.$terms);
            });

        });

        this.$agree.click(function (e) {
            e.preventDefault();
            self.$sectionQuestions.slideDown();
            self.$startConditions.slideUp();
            self.$startQuizButton.slideUp();
        });

        this.$cancel.click(function (e) {
            e.preventDefault();
            self.$startConditions.slideUp();
        });

        this.$terms.click(function (e) {
            e.preventDefault();
            setTimeout(function () {
                self.scrollToElm(self.$terms);
            }, 700);

        });
    },

    scrollToElm: function (elm) {
        $('html, body').animate({scrollTop: $(elm).offset().top}, 1000);
    },

};

module.exports = lucky_draw_quiz;