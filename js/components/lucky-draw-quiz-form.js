let luckyDrawQuizForm = {


    init: function () {
        this.bind();

        $('.numbersOnly').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });
    },


    bind: function () {

        const self = this;

        //this handles the validation of the form
        //if a part of the form is not valid so we color it in red in the isFormValid() function.
        //so here we change the color back to white while typing in the form
        //(we also change the color of the text back to black
        $("input").keydown(function () {
            $(this).removeClass('danger');
        });

        $(".form-select-wrapper").change(function () {
            $(".lucky-life-stage >.btn-default").removeClass('danger');
        });


        $('#lucky-button-submit-quiz').click(() => self.sendFormToServer());
    },

    sendFormToServer: function () {
        const self = this;


        if (!self.isFormValid()) {
            return;
        }

        //else
        let payload = {};
        let messageSentToTheBackoffice = "";

        let name = document.getElementById('lucky-first-name').value;
        let lastName = document.getElementById('lucky-last-name').value;
        let email = document.getElementById('lucky-email').value;
        let countryCode = document.getElementById('lucky-country-code').value;
        let phone = document.getElementById('lucky-phone').value;
        let lifeStage = document.getElementById('lucky-life-stage').value;


        payload.first_name = name;
        payload.last_name = lastName;
        payload.email_full = email;
        payload.phone_number = phone;
        payload.country_code = countryCode;
        payload.life_stage = lifeStage;

        const contactInfo = {
            firstName: name,
            lastName: lastName,
            emails: [{address: email}],
            phones: [{phoneNumber: phone, phoneTypeId: 'MOBILE'}],
            tags: ["The Future Cost Of Living"],
        };

        payload.messageSentToTheBackoffice = `${name}  just filled a quiz on your website.\nHere are the details:\nLife stage: ${lifeStage}\nPreferred name: ${name}\nFull name: ${lastName}\nPhone: ${countryCode} ${phone}\nEmail: ${email}\n\nThis message was received from future-cost-of-living-quiz`;

        //window.submitForm({payload});
        window.submitForm({
            formType: "MANULIFE_LUCKY_DRAW_QUIZ",
            payload,
            contactInfo,
            message: {text: payload.messageSentToTheBackoffice}
        }).then(function () {
            window.location.href = window.location.origin + '/contest-quiz-results';
            self.resetForm();
        });


        console.log(
            "first name " + name +
            "\nlast name " + lastName +
            "\nemail " + email +
            "\nphone " + phone +
            "\ncountryCode " + countryCode +
            "\nlifeStage " + lifeStage
        );


        // self.showThankYouMessageAndHideForm();
    },

    showThankYouMessageAndHideForm: function () {
        $(".rad-contact-us-form-wrap").hide();
        //showing the thank you message
        $(".rad-thank-you").slideToggle("slow");

    },

    resetForm: function () {
        document.getElementById('lucky-first-name').value = '';
        document.getElementById('lucky-last-name').value = '';
        document.getElementById('lucky-email').value = '';
        document.getElementById('lucky-phone').value = '';
        document.getElementById('lucky-life-stage').value = '';
    },

    changeStyleOfUnValidInput: function (elm) {
        $(elm).addClass('danger');
    },


    isPhoneValid(phone) {
        return /^\d+$/.test(phone);
    },


    isFormValid: function () {
        const firstName = document.getElementById('lucky-first-name');
        const lastName = document.getElementById('lucky-last-name');
        const email = document.getElementById('lucky-email');
        const phone = document.getElementById('lucky-phone');
        const lifeStage = document.getElementById('lucky-life-stage');

        this.handleObligatoryFieldsStyle(firstName, lastName, email, lifeStage, phone);

        return firstName.value !== '' && lastName.value !== '' && (this.validateEmail(email.value) && email.value !== '' && phone.value !== '' && lifeStage.value !== 'What is your current life stage?*')

    },

    handleObligatoryFieldsStyle: function (firstName, lastName, email, lifeStage, phone) {
        //if there is no first name inserted so we COLOR THE INPUT IN RED  phone === '' ||

        if (firstName.value === '') {
            this.changeStyleOfUnValidInput(firstName)
        }

        if (lastName.value === '') {
            this.changeStyleOfUnValidInput(lastName)
        }

        if (lifeStage.value === 'What is your current life stage?*') {
            this.changeStyleOfUnValidInput($(".lucky-life-stage >.btn-default"))
        }


        //if there is no phone number and email inserted, so we color the e-mail in red
        if (!this.validateEmail(email.value)) {
            this.changeStyleOfUnValidInput(email);
        }

        if (!this.isPhoneValid(phone.value)) {
            this.changeStyleOfUnValidInput(phone);
        }

    },
    validateEmail: function (email) {
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
};

module.exports = luckyDrawQuizForm;


