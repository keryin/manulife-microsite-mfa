const form_submission = {

    init: function() {
        var that = this;
        that.submitEnquiryForm();
    },

    submitEnquiryForm() {
            
    // ================================================ Events

        // ------------------------------------------------ Form Upload
        let filesFromCareersForm = [];
        //Careers page form
        function filesUpload(uploadInput) {

            $(uploadInput).on("change", _uploadFiles);

            function _uploadFiles(e) {
                let clientFiles = e.target.files;
                if(clientFiles.length <= 0) {
                    return
                }
                window.uploadFilesToAWS(clientFiles, ({fileName, progressEvent}) => {
                    // Process bar here:
                    const percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                    console.log(fileName, `${percentCompleted}%`);
                }).then((result) => {

                    filesFromCareersForm = result;
                    $('.uploaded-cv-link').remove();
                    $(uploadInput).parents('.block-button').after('<a class="link-icon link-icon-file uploaded-cv-link"><span>' + filesFromCareersForm[0].fileName + '</span><i class="fa fa-trash action-remove-file" aria-hidden="true"></i></a>');

                    console.log('filesFromCareersForm', filesFromCareersForm);
                    // _submitFile(files);
                }).catch((error) => {
                    // if got error handle here.
                    console.log(error)
                });
            }

        };


        filesUpload("#button-upload-cv");

        // ------------------------------------------------ Events

        $(document).on('click', '.action-remove-file', function() {
            filesFromCareersForm = [];
            $(this).parent().remove();
        });

    // ================================================ Events



        // ------------------------------------------------------------------------------------------------------ Form

        const formProduct = $("#form-product");
        const formSolutions = $("#form-product-solutions");
        const formContactBoxMobile = $("#form-contact-box-mobile");
        const formContactBoxDesktop = $("#form-contact-box-desktop");
        const formContactBoxDesktopMini = $("#form-contact-box-desktop-mini");
        const formContactUs = $("#form-contact-us");
        const formTalkToUs = $("#form-talk-to-us");
        const formPreviousSeminar = $("#form-seminar-form");
        const formSeminarSignUp = $("#form-seminar-signup");

        const formCareers = $("#form-career-form");


        const formQuiz = $("#form-quiz");


        const formProductSubmit = $("#button-submit-product");
        const formSolutionsSubmit = $("#button-submit-product-solutions");
        const formContactBoxMobileSubmit = $("#button-submit-contact-mobile");
        const formContactBoxDesktopSubmit = $("#button-submit-contact-desktop");
        const formContactBoxDesktopMiniSubmit = $("#button-submit-contact-desktop-mini");
        const formContactUsSubmit = $("#button-submit-contact-us");

        const formTalkToUsSubmit = $("#button-submit-talk-to-us");
        const formPreviousSeminarSubmit = $("#button-submit-previous-seminar");
        const formSeminarSignUpSubmit = $("#button-submit-seminar-signup");


        const formCareersSubmit = $("#button-submit-careers");

        const formQuizSubmit = $("#button-submit-quiz");



        const thankProductSubmit = $("#form-product-thank");
        const thankSolutionsSubmit = $("#form-solution-thank");
        const thankContactBoxMobile = $("#form-contact-thank-mobile");
        const thankContactBoxDesktop = $("#form-contact-thank-desktop");
        const thankContactBoxDesktopMini = $("#form-contact-thank-desktop-mini");
        const thankContactUs = $("#form-contact-us-thank");

        const thankTalkToUs = $("#form-talk-to-us-thank");
        const thankPreviousSeminar = $("#form-previous-seminar-thank");
        const thankSeminarSignUp = $("#form-seminar-signup-thank");

        const thankCareers = $("#form-careers-thank");


        const thankQuiz = $(".section-result");






        const formInputs = {
            firstName: ".input-firstname",
            lastName: ".input-lastname",
            email: ".input-email",
            phoneCode: ".select-phone-prefix",
            phone: ".input-phone",
            contactMethod: '.input-method',
            type: '.input-type',
            date: '.input-date',
            time: '.input-time',
            note: '.input-note',
            checkMark: '.general-checkbox'
        };




        initializeForm({
            form: formContactBoxDesktop,
            button: formContactBoxDesktopSubmit,
            thank: thankContactBoxDesktop,
            formType: 'letsmeet'
        });



        initializeForm({
            form: formContactBoxDesktopMini,
            button: formContactBoxDesktopMiniSubmit,
            thank: thankContactBoxDesktopMini,
            formType: 'letsmeet'
        });

        initializeForm({
            form: formContactBoxMobile,
            button: formContactBoxMobileSubmit,
            thank: thankContactBoxMobile,
            formType: 'letsmeet'
        });



        initializeForm({
            form: formProduct,
            button: formProductSubmit,
            thank: thankProductSubmit,
            formType: 'coffeetea'
        });

        initializeForm({
            form: formSolutions,
            button: formSolutionsSubmit,
            thank: thankSolutionsSubmit,
            formType: 'productstopic'
        });

        initializeForm({
            form: formContactUs,
            button: formContactUsSubmit,
            thank: thankContactUs,
            formType: 'contactus'
        });


        initializeForm({
            form: formQuiz,
            button: formQuizSubmit,
            thank: thankQuiz,
            formType: 'quiz'
        });

        initializeForm({
            form: formTalkToUs,
            button: formTalkToUsSubmit,
            thank: thankTalkToUs,
            formType: 'talk-to-us'
        });

        initializeForm({
            form: formPreviousSeminar,
            button: formPreviousSeminarSubmit,
            thank: thankPreviousSeminar,
            formType: 'MFA-seminar'
        });

        initializeForm({
            form: formSeminarSignUp,
            button: formSeminarSignUpSubmit,
            thank: thankSeminarSignUp,
            formType: 'MFA-seminar-register'
        });


        initializeForm({
            form: formCareers,
            button: formCareersSubmit,
            thank: thankCareers,
            formType: 'MFA-career'
        });

        function scrollToElement($target, callback){
            console.log(' $target',  $target);
            console.log('$target.offset().top', $target.offset().top);
            $('html, body').animate({
                scrollTop: $target.offset().top
            }, 1000, function() {
                if(callback){
                    callback();
                }
            });
        }

        function initializeForm(formObject){
            let currentForm = formObject.form;
            let currentFormExist = currentForm.length > 0;

            if(currentFormExist){
                let submitUrl = window.location.href;
                let submitTitle = $('h1.title-main').text();
                let currentFormSubmit = formObject.button;
                let currentFormThank = formObject.thank;

                let curentFirstName = currentForm.find(formInputs.firstName);
                let curentLastName = currentForm.find(formInputs.lastName);
                let curentEmail = currentForm.find(formInputs.email);
                let curentPhoneCode = currentForm.find(formInputs.phoneCode);
                let curentPhone = currentForm.find(formInputs.phone);
                let curentContactMethod = currentForm.find(formInputs.contactMethod);
                let curentType = currentForm.find(formInputs.type).filter('select');
                let curentNote = currentForm.find(formInputs.note);
                let currentCheckBox= currentForm.find(formInputs.checkMark);
                let currentCheckBoxNewsLatter = currentForm.find(".news-latter-opt-option");


                let curentDate = currentForm.find(formInputs.date);
                let curentTime = currentForm.find(formInputs.time);



                currentFormSubmit.click(function (event) {
                    console.log('currentForm', currentForm);
                    console.log('curentDate', curentDate);
                    console.log('curentTime', curentTime);



                    //put all required input error
                    curentFirstName.addClass("status-error");
                    if(currentForm.find('.error-label-firstname').length === 0){
                        curentFirstName.parents('.form-field-inner').append('<div class="error-label error-label-firstname"></div>')
                    }

                    curentLastName.addClass("status-error");
                    if(currentForm.find('.error-label-lastname').length === 0){
                        curentLastName.parents('.form-field-inner').append('<div class="error-label error-label-lastname"></div>')
                    }



                    curentEmail.addClass("status-error");
                    if(currentForm.find('.error-label-email').length === 0){
                        curentEmail.parents('.form-field-inner').append('<div class="error-label error-label-email"></div>')
                    }

                    curentPhone.addClass("status-error");
                    if(currentForm.find('.error-label-phone').length === 0){
                        curentPhone.parents('.form-field-inner').append('<div class="error-label error-label-phone"></div>')
                    }

                    curentNote.addClass("status-error");
                    if(currentForm.find('.error-label-message').length === 0){
                        curentNote.parents('.form-field-inner').append('<div class="error-label error-label-message"></div>')
                    }

                    curentContactMethod.parent(".block-input-method").addClass("status-error");
                    if(currentForm.find('.error-label-contact-method').length === 0){
                        curentContactMethod.parents('.form-field-inner').append('<div class="error-label error-label-contact-method"></div>')
                    }

                    //Worst code in the world, will be fixed soon
                    //check each input for error and remove error

                    if (currentCheckBox.is(':checked')) {
                        currentCheckBoxNewsLatter.removeClass('check-false');
                        //also remove or hide error message
                    } else {
                        //add error message about [EMPTY FIELD]
                        currentCheckBoxNewsLatter.addClass('check-false');
                    }

                    if (notEmpty(curentFirstName)) {
                        curentFirstName.removeClass('status-error');
                        //also remove or hide error message
                    } else {
                        //add error message about [EMPTY FIELD]
                        changeErrorMessageText('.error-label-firstname', 'First Name is required')
                    }


                    if (notEmpty(curentLastName)) {
                        curentLastName.removeClass('status-error');
                        //also remove or hide error message
                    } else {
                        //add error message about [EMPTY FIELD]
                        changeErrorMessageText('.error-label-lastname', 'Last Name is required')
                    }

                    if (notEmpty(curentEmail) && isEmail(curentEmail) ) {
                        curentEmail.removeClass('status-error');
                        //also remove or hide error message
                    } else {
                        //add error message about [EMPTY FIELD]
                        let errorMessageText = notEmpty(curentEmail) && !isEmail(curentEmail) ? 'Please use this format: youremail@example.com' : 'Email is required';
                        changeErrorMessageText('.error-label-email', errorMessageText);
                    }



                    if (notEmpty(curentPhone) && isPhoneNubmer(curentPhone)) {
                        curentPhone.removeClass('status-error');
                        //also remove or hide error message
                    } else {
                        let errorMessageText = notEmpty(curentPhone) && !isPhoneNubmer(curentPhone) ? 'Please use from 8 to 20 digits' : 'Phone is required';
                        changeErrorMessageText('.error-label-phone', errorMessageText);
                    }

                    if (notEmpty(curentNote)) {
                        curentNote.removeClass('status-error');
                        //also remove or hide error message
                    } else {
                        //add error message about [EMPTY FIELD]
                        changeErrorMessageText('.error-label-message', 'Your message is required')
                    }

                    if (curentContactMethod.hasClass('status-selected')) {
                        curentContactMethod.parent(".block-input-method").removeClass('status-error');
                        //also remove or hide error message
                    } else {
                        //add error message about [EMPTY FIELD]
                        changeErrorMessageText('.error-label-contact-method', 'Please select your preferred contact method')
                    }

                    //if date was selected time must be selected
                    //if no error - send message to server
                    if ($(currentForm).find('.status-error').length === 0 && $(currentForm).find('.check-false').length === 0) {

                        let curentWebsiteURL = '\nURL: ' + submitUrl;
                        let curentSubmitTitle = '\nPage Title: ' + submitTitle;

                        let curentFirstNameValue = curentFirstName.val().charAt(0).toUpperCase() + curentFirstName.val().slice(1) || '';
                        let curentFirstNameMessage = curentFirstNameValue != '' ? '\nFirst Name: ' + curentFirstNameValue  : '';

                        let curentLastNameValue =  curentLastName.val().charAt(0).toUpperCase() + curentLastName.val().slice(1) || '';
                        let curentLastNameMessage = curentLastNameValue != '' ? '\nLast Name: ' + curentLastNameValue  : '';

                        let curentEmailValue = curentEmail.val() || '';
                        let curentEmailMessage = curentEmailValue != '' ? '\nEmail: ' + curentEmailValue  : '';


                        let currentPhoneCodeValue = curentPhoneCode.find('.filter-option').text();
                        let curentPhoneValue = curentPhone.val() !== '' ? curentPhone.val() : '';
                        let currentPhoneMessage = curentPhoneValue !== '' ? '\nPhone: ' + currentPhoneCodeValue + curentPhoneValue  : '';

                        console.clear();
                        console.log('curentPhone[0]', curentPhone[0]);
                        console.dir(curentPhone);
                        console.log({
                            "currentPhoneCodeValue" : currentPhoneCodeValue,
                            "curentPhone.val()" : curentPhone.val(),
                            "currentPhoneMessage" : currentPhoneMessage,
                        });

                        let curentTypeValue = curentType.val() || '';
                        let currentTypeMessage = curentTypeValue != '' ? '\nI would like to talk about: ' + curentTypeValue  : '';

                        let curentNoteValue = curentNote.val() || '';
                        let currentNoteMessage = curentNoteValue != '' ? '\nMessage: ' + curentNoteValue  : '';


                        let curentContactMethodValue = curentContactMethod.hasClass('status-selected') ? curentContactMethod.filter('.status-selected').text().trim() : '';
                        let curentContactMethodMessage = curentContactMethodValue != '' ? '\nPreferred contact method: ' + curentContactMethodValue  : '';

                        let curentDateValue = curentDate.val() || '';

                        let curentTimeValue = curentTime.find('option:checked').val() !== 'Time' ? curentTime.find('option:checked').val() : '';

                        let curentSeminarTopicValue = $(".event-main-title").text();
                        let curentSeminarTopicMessage = curentSeminarTopicValue != '' ? '\nRegisted Seminar Event: ' +curentSeminarTopicValue  : '';


                        console.log('curentDate.val()', curentDate.val());
                        console.log('curentSeminarTopicValue', curentSeminarTopicValue);
                        console.log('curentDateValue', curentDateValue);
                        console.log('curentTimeValue', curentTimeValue);
                        console.log('curentTypeValue', curentTypeValue);



                        let curentDateAndTimeMessage = '';
                        let curentDateAndTimeMessageForThank = '';

                        if(curentDateValue !== '' && curentTimeValue !== ''){
                            let dt = moment(curentDateValue, "YYYY-MM-DD HH:mm:ss");
                            let dayName = dt.format('dddd');

                            curentDateAndTimeMessageForThank = `${dayName} ${curentDateValue} ${curentTimeValue}`;
                            curentDateAndTimeMessage = '\nPreferred contact time: ' + curentDateAndTimeMessageForThank;

                            //update thank date
                            currentFormThank.find('.thank-shedule').text(curentDateAndTimeMessageForThank);
                        }


                        let currentMessageFromClient = '';

                        //None it our forms
                        let city = null;
                        let zipcode = null;

                        let thisIsQuizForm = formObject.formType === 'quiz';
                        let thisIsCareersForm = formObject.formType === 'MFA-career';


                        //change to es6 and switch
                        //Separate textarea message and message to BO
                        if(formObject.formType == 'letsmeet'){
                            currentMessageFromClient = `${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage} ${currentPhoneMessage} ${currentTypeMessage} ${curentDateAndTimeMessage} ${curentContactMethodMessage} ${currentNoteMessage}`;
                        }

                        if (formObject.formType == 'coffeetea'){
                            let drinkTypeValue = $('.status-selected [data-drink-type]').attr('data-drink-type');
                            let currentDrinkTypeMessage = drinkTypeValue != '' ? '\nI would like to drink: ' + drinkTypeValue  : '';
                            console.log('$coffeeTeaOption', drinkTypeValue);
                            currentMessageFromClient = `${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage} ${currentPhoneMessage} ${curentContactMethodMessage} ${curentDateAndTimeMessage} ${currentDrinkTypeMessage}`;
                        }

                        if (formObject.formType == 'productstopic'){
                            currentMessageFromClient = `${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage} ${currentPhoneMessage} ${curentContactMethodMessage}`;
                        }
                        
                        if (formObject.formType == 'talk-to-us'){
                            currentMessageFromClient = `${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage} ${currentPhoneMessage}`;
                        }

                        if (formObject.formType == 'MFA-seminar'){
                            currentMessageFromClient = `${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage} ${currentPhoneMessage} ${curentContactMethodMessage}`;
                        }

                        if (formObject.formType == 'MFA-seminar-register'){
                            currentMessageFromClient = `${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage} ${currentPhoneMessage} ${curentContactMethodMessage} ${curentSeminarTopicMessage}`;
                        }


                        //update client message footer
                        console.log('currentForm', currentForm);
                        currentMessageFromClient += `\n\nThis message was received from ${$(currentForm).attr('id')} on page ${document.location.href.split('/').pop()}`;

                        console.log('currentMessageFromClient', currentMessageFromClient);



                        if (thisIsQuizForm){

                            let quizSummaryMessage = '\nQuiz Summary:\n';
                            let scoreSummary = '';

                            $('.question-item').each(function (index) {
                                quizSummaryMessage += `${index+1}) ${$(this).find('.title').text()}\nAnswer: ${$(this).find('.answer-option.status-selected span').text().replace('<', 'less than ')}\n`
                                scoreSummary += `${index+1}) ${$(this).find('.title').text()}\nAnswer: ${$(this).find('.answer-option.status-selected span').text().replace('<', 'less than ')}\n`
                            });


                            quizSummaryMessage += `\nMy final score is: ${finalScore}`;
                            scoreSummary += `\nMy final score is: ${finalScore}`;

                            currentMessageFromClient = `${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage}`;
                            currentMessageFromClient += `\n\nThis message was received from ${$(currentForm).attr('id')} on page ${document.location.href.split('/').pop()}`;


                            let textToChat = `Hello ${handelbarsDataPrivatObject.firstName}\nI filled a quiz on your website, here are all my answers:\n${scoreSummary}\n\nThanks`;


                            console.log('currentMessageFromClient', currentMessageFromClient);
                            console.log('finalScore', finalScore);
                            console.log('scoreMark', scoreMark);
                            console.log('textToChat', textToChat);

                            if(scoreMark == 1){$('.result-1').removeClass('status-hidden')}
                            if(scoreMark == 2){$('.result-2').removeClass('status-hidden')}
                            if(scoreMark == 3){$('.result-3').removeClass('status-hidden')}
                            if(scoreMark == 4){$('.result-4').removeClass('status-hidden')}
                            if(scoreMark == 5){$('.result-5').removeClass('status-hidden')}


                            scrollToElement($('.section-title-main'), quizFinished);


                            window.submitForm({
                                formType: 'quiz',
                                payload: {
                                    formTagQuiz: true,
                                    quizResult: quizSummaryMessage,
                                },
                                contactInfo: {
                                    emails: [{address: curentEmailValue}],
                                    phones: [{phoneNumber: currentPhoneCodeValue + curentPhoneValue, phoneTypeId: 'MOBILE'}],
                                    tags: ['quiz'],
                                    firstName: curentFirstNameValue,
                                    lastName: curentLastNameValue
                                },
                                message: { text: textToChat },
                            });




                            $(currentForm).find('input, textarea').val('');

                        } else if(thisIsCareersForm){

                            let linkedInInputValue = $('.input-linkedin').val() || '';
                            let linkedInInputValueMessage = linkedInInputValue != '' ? '\nLinkedIn: ' + linkedInInputValue  : '';

                            let currentMessageFromClient = `${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage} ${currentPhoneMessage} ${linkedInInputValueMessage}`;
                            currentMessageFromClient += `\n\nThis message was received from ${$(currentForm).attr('id')} on page ${document.location.href.split('/').pop()}`;

                            let textToChat = `Hello ${handelbarsDataPrivatObject.firstName}\nI submitted a career form on your website, here are all the details:\n ${curentFirstNameMessage} ${curentLastNameMessage} ${curentEmailMessage} ${currentPhoneMessage} ${linkedInInputValueMessage} \n\nThanks`;


                            console.log('currentMessageFromClient', currentMessageFromClient);


                            window.submitForm({
                                formType: 'MFA-career',
                                payload: {
                                    formTagCareer: true,
                                    files: filesFromCareersForm,
                                },
                                contactInfo: {
                                    emails: [{address: curentEmailValue}],
                                    phones: [{phoneNumber: currentPhoneCodeValue + curentPhoneValue, phoneTypeId: 'MOBILE'}],
                                    tags: ['career'],
                                    firstName: curentFirstNameValue,
                                    lastName: curentLastNameValue
                                },
                                message: { text: textToChat },
                            });


                            $(currentForm).find('input, textarea').val('');
                            $(currentForm).hide();
                            currentFormSubmit.hide();
                            $(currentFormThank).show();


                        } else{

                            let textToChat = '';
                            let submitFormType = '';
                            let submitFormTags = '';
                            let payloadDataObject = {};

                            //change form type and tag according to
                            // if it contact box/coffeetea/contactus


                            if(formObject.formType == 'letsmeet'){
                                submitFormType = 'letsmeet';
                                submitFormTags = ['Lets Meet'];


                                let dateTimeMessage = '';
                                if(curentDateValue !== '' && curentTimeValue !== ''){
                                    dateTimeMessage = `\nTime and date: ${curentDateValue} ${curentTimeValue}`;
                                }

                                textToChat = `Hello ${handelbarsDataPrivatObject.firstName},\nI scheduled a meeting on your website, here are all the details:\n ${curentFirstNameMessage} ${curentLastNameMessage}${dateTimeMessage}${currentPhoneMessage} ${curentEmailMessage} ${curentContactMethodMessage} ${curentSubmitTitle} ${curentWebsiteURL}`;

                                payloadDataObject = {
                                    contactType: curentContactMethodValue,
                                    date: curentDateValue,
                                    time: curentTimeValue,
                                    "webURL": submitUrl,
                                    "formTitle": submitTitle
                                };
                            }

                            if(formObject.formType == 'talk-to-us'){
                                submitFormType = 'talk-to-us';
                                submitFormTags = ['Talk'];

                                textToChat = `Hello ${handelbarsDataPrivatObject.firstName},\nI submitted a new form on your website, here are all the details:\n ${curentFirstNameMessage} ${curentLastNameMessage} ${currentPhoneMessage} ${curentEmailMessage} ${currentNoteMessage} ${curentSubmitTitle} ${curentWebsiteURL}`;

                                payloadDataObject = {
                                    message: curentNoteValue,
                                    "webURL": submitUrl,
                                    "formTitle": submitTitle
                                };

                            }

                            if(formObject.formType == 'MFA-seminar'){
                                submitFormType = 'MFA-seminar';
                                submitFormTags = ['Events'];
                                textToChat = `Hello ${handelbarsDataPrivatObject.firstName},\nI have subscribed on your website seminars, here are all the details:\n ${curentFirstNameMessage} ${curentLastNameMessage} ${currentPhoneMessage} ${curentEmailMessage}`;
                            }

                            if(formObject.formType == 'MFA-seminar-register'){
                                submitFormType = 'MFA-seminar-register';
                                submitFormTags = ['Events', curentSeminarTopicValue];
                                textToChat = `Hello ${handelbarsDataPrivatObject.firstName},\nI have subscribed on this seminars, ${curentSeminarTopicValue}, here are all the details:\n ${curentFirstNameMessage} ${curentLastNameMessage} ${currentPhoneMessage} ${curentEmailMessage}`;
                            
                                payloadDataObject = {
                                    seminarTopic: curentSeminarTopicValue,
                                };
                            }

                            if(formObject.formType == 'contactus'){
                                submitFormType = 'contactus';
                                submitFormTags = ['Contact Us'];

                                textToChat = `Hello ${handelbarsDataPrivatObject.firstName},\nI submitted a new lead on your website, here are all the details:\n ${curentFirstNameMessage} ${curentLastNameMessage} ${currentPhoneMessage} ${curentEmailMessage} ${curentContactMethodMessage} ${currentNoteMessage} ${curentSubmitTitle} ${curentWebsiteURL}`;

                                payloadDataObject = {
                                    contactType: curentContactMethodValue,
                                    message: curentNoteValue,
                                    "webURL": submitUrl,
                                    "formTitle": submitTitle
                                };

                            }


                            if (formObject.formType == 'coffeetea'){
                                submitFormType = 'coffee-or-tea';
                                submitFormTags = ['Coffee or tea'];

                                let drinkTypeValue = $('.status-selected [data-drink-type]').attr('data-drink-type');
                                let currentDrinkTypeMessage = drinkTypeValue != '' ? '\nI would like to drink: ' + drinkTypeValue  : '';
                                let contactTimeMessage = '';
                                if(curentTimeValue !== ''){
                                    contactTimeMessage = `\nPreferred contact time: ${curentTimeValue}`;
                                }


                                textToChat = `Hello ${handelbarsDataPrivatObject.firstName},\nI scheduled a meeting on your website from 'Coffee or Tea' form, here are all the details:\n ${curentFirstNameMessage} ${curentLastNameMessage} ${currentPhoneMessage} ${curentEmailMessage} ${curentContactMethodMessage} ${contactTimeMessage} ${currentDrinkTypeMessage} ${curentSubmitTitle} ${curentWebsiteURL}`;
                                payloadDataObject = {
                                    time: curentTimeValue,
                                    drinkType: drinkTypeValue,
                                    contactType: curentContactMethodValue,
                                    "webURL": submitUrl,
                                    "formTitle": submitTitle
                                };
                            }

                            if (formObject.formType == 'productstopic'){
                                submitFormType = 'products-topic';
                                submitFormTags = ['products-meeting'];

                                textToChat = `Hello ${handelbarsDataPrivatObject.firstName},\nI scheduled a meeting on your website, here are all the details:\n ${curentFirstNameMessage} ${curentLastNameMessage} ${currentPhoneMessage} ${curentEmailMessage} ${curentContactMethodMessage} ${curentSubmitTitle} ${curentWebsiteURL}`;
                                payloadDataObject = {
                                    contactType: curentContactMethodValue,
                                    "webURL": submitUrl,
                                    "formTitle": submitTitle
                                };
                            }
                            //to store phoneCode
                            payloadDataObject.phoneCode = currentPhoneCodeValue;

                            window.submitForm({
                                formType: submitFormType,
                                payload: payloadDataObject,
                                contactInfo: {
                                    firstName: curentFirstNameValue,
                                    lastName: curentLastNameValue,
                                    emails: [{address: curentEmailValue}],
                                    phones: [{phoneNumber: currentPhoneCodeValue + curentPhoneValue, phoneTypeId: 'MOBILE'}],
                                    tags: submitFormTags,

                                },
                                message: { text: textToChat },
                            }).then(function(value) {
                                console.log(value);
                                //reset current form
                                $(currentForm).find('input, textarea').val('');
                                //show thank you and hide form
                                $(currentForm).hide();
                                currentFormSubmit.hide();
                                $(currentFormThank).show();    
                                if($(".page-name-contact-us").length){                        
                                    $('html, body').animate({scrollTop:0},1000);
                                };
                            });


                        }


                    }

                    return false;
                });
            }
        }

        // function removeError(){
        //     //
        // }

        // function addError(){
        //     //
        // }




        //remove error on focus
        $(document).on('focus', "form input, form textarea, form select", function () {
            $(this).removeClass('status-error');
            $('.news-latter-opt-option').removeClass('check-false');
        });

        if ($('.general-checkbox').is(':checked')) {
            $('.news-latter-opt-option').removeClass('check-false');
        }

        //allow only digits in phone field
        $('.input-phone').keyup(function () {
            this.value = this.value.replace(/[^0-9\+.]/g,'');
        });

        // function onLeadSubmit () {
        //     console.log('onLeadSubmit');
        //     console.log(arguments);
        // }

        function isEmail(email) {
            let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            console.log('email', email);
            console.log('email val', email.val());
            console.log('regex.test(email.val())', regex.test(email.val()));
            return regex.test(email.val());
        }

        function isPhoneNubmer(phone) {
            let regex = /^\(?([0-9]{2,4})\)?[-. ]?([0-9]{2,4})[-. ]?([0-9]{2,4})$/;
            return regex.test(phone.val());
        }

        function notEmpty(inputElement) {
            return inputElement.val() !== "";
        }


        function changeErrorMessageText(elClass, text) {
            let $el = $(elClass);
            $el.text(text);
        }







        $(document).on('click', '.input-method', function(event) {
            var thisMethodAlreadySelected = $(this).hasClass('status-selected');
            if (thisMethodAlreadySelected){
                $(this).removeClass('status-selected');
            } else {
                $(this).addClass('status-selected').siblings().removeClass('status-selected');            
                $(this).parent(".block-input-method").removeClass('status-error');
            }
        });


        // setInterval(function(){
        //     $('#input-date').focus();
        // }, 400);






        // ------------------------------------------------------------------------------------------------------ Quiz Form and Behaviour
        let $quizAnswerOptions = $('.answer-option');
        let $quizQestions = $('.question-item');

        let finalScore = 0;
        let scoreMark = 0;

        function updateImage(srcD, srcT, srcM) {
            let imgSrc = "https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/production/mfa/images/campaigns/";
            let isIE11 = !!window.MSInputMethodContext && !!document.documentMode;    
            let imgURLDesktop = imgSrc+srcD;              
            let imgURLTablet = imgSrc+srcT;            
            let imgURLMobile = imgSrc+srcM;            
                                                     
            
            $(".cover-image-wrapper.hide-mobile img").attr("src", imgURLDesktop);
            $(".cover-image-wrapper.hide-tablet img").attr("src", imgURLTablet);
            $(".cover-image-wrapper.hide-desktop img").attr("src", imgURLMobile);

            if (isIE11) {
                $('.cover-image-wrapper.hide-mobile').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLDesktop + ')')
                });
                $('.cover-image-wrapper.hide-tablet').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLTablet + ')')
                });
                $('.cover-image-wrapper.hide-desktop').each(function () {
                    $(this).css('backgroundImage', 'url(' + imgURLMobile + ')')
                });
            }     
        };

        $(document).on('click', '.answer-option', function(event) {
            var thisOptionAlreadySelected = $(this).hasClass('status-selected');

            if (thisOptionAlreadySelected){
                // $(this).removeClass('status-selected');
            } else {
                $(this).addClass('status-selected').siblings().removeClass('status-selected');
                $('.block-progress-bar .step.step-active').not('.step-activated').removeClass('step-active').addClass('step-activated').next('li').addClass('step-active');
                $(this).parents('.question-item').not('.status-activated').removeClass('status-active').addClass('status-activated').next().addClass('status-active').fadeIn( "medium", function() {
                    if($(this).find('.result-score').length > 0){
                        //this is last option in quiz, calculate all points
                        console.log('//this is last option in quiz, calculate all points');
                        $(this).find('.block-quiz-form').show();
                        getQuizScore();
                        calculateFinalScore();
                        updateQuizScoreNumber();
                        showRelevantAnswer();

                        if($('.page-name-a-tale-of-gifts-and-greetings') || $('.page-type-campaign')){
                            $('.block-progress-bar').hide();
                            $('.block-quiz-result .block-button').fadeIn();
                        }
                    }

                    if($('.page-name-a-tale-of-gifts-and-greetings').length){
                        if(window.innerWidth > 600){
                            $('html, body').animate({
                                scrollTop: $(this).offset().top - 80
                            }, 1000);
                        } else {
                            $('html, body').animate({
                                scrollTop: $(this).offset().top - 30
                            }, 1000);
                        }
                    } else if($('.page-name-be-unbroken').length){
                        // $('html, body').animate({
                        //     scrollTop: 0
                        // }, 1000);    

                        if($('.status-active').attr('data-question') == 2){
                            updateImage('be-unbroken/manulife-unbroken-Q2-1920x526.jpg', 'be-unbroken/manulife-unbroken-Q2-1600x800.jpg', 'be-unbroken/manulife-unbroken-Q2-800x400.jpg');
                        } else if($('.status-active').attr('data-question') == 3){
                            updateImage('be-unbroken/manulife-unbroken-Q3-1920x526.jpg', 'be-unbroken/manulife-unbroken-Q3-1600x800.jpg', 'be-unbroken/manulife-unbroken-Q3-800x400.jpg');
                        } else if($('.status-active').attr('data-question') == 4){
                            updateImage('be-unbroken/manulife-unbroken-Q4-1920x526.jpg', 'be-unbroken/manulife-unbroken-Q4-1600x800.jpg', 'be-unbroken/manulife-unbroken-Q4-800x400.jpg');
                        } else {
                            $('html, body').animate({
                                scrollTop: 0
                            }, 1000); 
                            // updateImage('be-unbroken/manulife-unbroken-1920x526.jpg', 'be-unbroken/manulife-unbroken-1600x800.jpg', 'be-unbroken/manulife-unbroken-800x400.jpg');
                        }
                    } else {
                        scrollToElement($(this));
                    }

                });
            }
        });

        function getQuizScore() {
            $('.answer-option.status-selected').each(function () {
                let currentQuestionPoint = parseInt($(this).attr('data-score'));
                finalScore += currentQuestionPoint;
            });
        }

        function calculateFinalScore() {
            if($('.page-name-quiz').length > 0){
                scoreMark = finalScore > 20 ? 1 : 2;
            } else if ($('.page-name-survey').length > 0){
                if(finalScore >= 80){scoreMark = 1}
                if(finalScore < 80 && finalScore >= 60){scoreMark = 2}
                if(finalScore < 60 && finalScore >= 40){scoreMark = 3}
                if(finalScore < 40 && finalScore >= 20){scoreMark = 4}
                if(finalScore < 20){scoreMark = 5}
            } else if ($('.page-name-a-tale-of-gifts-and-greetings').length){
                if(finalScore <= 3 ){scoreMark = 1}
                if(finalScore > 3 && finalScore <= 6){scoreMark = 2}
                if(finalScore > 6 && finalScore <= 9){scoreMark = 3}
                $('.result-score-mark').text(scoreMark);
            } else if ($('.page-name-be-unbroken').length){
                if(finalScore <= 4 ){
                    scoreMark = 3;    
                    updateImage('be-unbroken/results/manulife-unbroken-results-C.jpg', 'be-unbroken/results/Manulife-unbroken-1600x800-C.jpg', 'be-unbroken/results/manulife-unbroken-800x600-C.jpg');                
                    $('#form-thank-campaign-form').find('.result-score-info-text').text('Did you know that 1 in 2* patients used all or most of their savings on medical treatments?');
                }
                if(finalScore > 4 && finalScore <= 8){
                    scoreMark = 2;
                    updateImage('be-unbroken/results/manulife-unbroken-results-A.jpg', 'be-unbroken/results/Manulife-unbroken-1600x800-A.jpg', 'be-unbroken/results/manulife-unbroken-800x600-A.jpg');     
                    $('#form-thank-campaign-form').find('.result-score-info-text').text('Did you know that almost 100%* of patients that were Contested felt as though they were burdening their loved ones?');
                }
                if(finalScore > 8 && finalScore <= 12){
                    scoreMark = 1;
                    updateImage('be-unbroken/results/manulife-unbroken-results-B.jpg', 'be-unbroken/results/Manulife-unbroken-1600x800-B.jpg', 'be-unbroken/results/manulife-unbroken-800x600-B.jpg');     
                    $('#form-thank-campaign-form').find('.result-score-info-text').text('Did you know that 1 in every 10* patients had to declare bankruptcy due to their critical illness?');
                }
                $('.result-score-mark').text(scoreMark);
            }
        }

        function updateQuizScoreNumber() {
            $('.result-score-number').text(finalScore);
        }

        function showRelevantAnswer() {
            $('.result-score.result-' + scoreMark).removeClass('status-hidden').addClass('score-active');
        }


        function quizFinished() {
            $('.section-questions, .subtitle-main').hide();
            $('.section-result').fadeIn( "slow", "swing", function() {});
        }
    },
};

module.exports = form_submission;
