function scrollToElement($target, callback){
    console.log(' $target',  $target);
    console.log('$target.offset().top', $target.offset().top);
    $('html, body').animate({
        scrollTop: $target.offset().top
    }, 1000, function() {
        if(callback){
            callback();
        }
    });
}

const show_more = {


    init: function () {
        console.log("show more started:");
        this.cacheDom();
        this.bind();

        if (window.self !== window.top) {
            $('html').addClass('page-location-in-bo');
        }
        console.log("page location - BO? " + document.getElementsByClassName("page-location-in-bo").length);
    },


    cacheDom: function () {
        this.$showMore = $('.page-name-about .show-more');
        this.$awardList = $('.awards-list');
        this.$awardTimeLine = $('.awards-timeline');
    },


    bind: function () {
        const self = this;
        //this logic will break in rare case when this section height will be 240px perfectly
        let thereAreMoreAwardsToShow = this.$awardTimeLine.height() === 240;

        if(thereAreMoreAwardsToShow){
            self.$showMore.css('display', 'inline-block');
            self.$showMore.click(function (e) {
                e.preventDefault();

                self.$awardList.toggleClass('show-all-awards');

                if (self.$showMore.text() !== "Read Less") {
                    self.$showMore.text("Read Less");
                } else {
                    self.$showMore.text("Read More");
                    scrollToElement(self.$awardTimeLine);
                }

            });
        }
    }

};

module.exports = show_more;
