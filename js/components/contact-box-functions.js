// var contactBoxElement = $('.contact-box-data');

var contactBoxTabsIcon = $('.tabs-icon');

var contactBox = {

    init: function () {
        this.bind();

    },

    bind: function () {
        contactBoxTabsIcon.click(function (event) {
            // Contact box shows separately based on the tab
            // var contactBoxElement = $(this).parents('.contact-box');

            // Target all contact box - there will have only one content box
            var contactBoxElement = $('.contact-box');
            var contactTabContent = $('.contact-tab-content-desktop-mini');
            var targetTabClass = $(this).attr('data-tab-target');
            let tabIsAlreadySelected = contactBoxElement.hasClass(targetTabClass);
            
            if(tabIsAlreadySelected){
                contactBoxElement.removeClass(targetTabClass);
                contactTabContent.removeClass(targetTabClass);
            } else {
                contactBoxElement.removeClass(function (index, className) {
                    return (className.match (/\bstatus-tab-selected-\S+/g) || []).join(' ');
                }).addClass(targetTabClass)
                contactTabContent.removeClass(function (index, className) {
                    return (className.match (/\bstatus-tab-selected-\S+/g) || []).join(' ');
                }).addClass(targetTabClass)
            }
        })

        // Close Contact box
        $('.button-close-tab').on('click', function () {
            var contactBoxElement = $('.contact-box');
            var targetTabClass = $('.tabs-icon').attr('data-tab-target');
            var contactTabContent = $('.contact-tab-content-desktop-mini');
            contactBoxElement.removeClass(function (index, className) {
                return (className.match (/\bstatus-tab-selected-\S+/g) || []).join(' ');
            }).addClass(targetTabClass)
            contactTabContent.removeClass(function (index, className) {
                return (className.match (/\bstatus-tab-selected-\S+/g) || []).join(' ');
            }).addClass(targetTabClass)
        })

        $('.button-sign-up-solutions').on('click', function () {
            $(".contact-box-desktop-mini").addClass("status-tab-selected-2");
            $(".contact-tab-content-desktop-mini").addClass("status-tab-selected-2");
        })

        $(document).ready(function() {
            if (window.location.href.indexOf("letschat") > -1) {
                $(".contact-box-desktop-mini").addClass("status-tab-selected-2");
                $(".contact-tab-content-desktop-mini").addClass("status-tab-selected-2");
            }
        });
    }
};


module.exports = contactBox;