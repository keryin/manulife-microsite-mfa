let Popup, popup;

/** Defines the Popup class. */
function definePopupClass() {
    console.log('definePopupClass');
    /**
     * A customized popup on the map.
     * @param {!google.maps.LatLng} position
     * @param {!Element} content
     * @constructor
     * @extends {google.maps.OverlayView}
     */
    Popup = function(position, content) {
        this.position = position;

        content.classList.add('popup-bubble-content');

        var pixelOffset = document.createElement('div');
        pixelOffset.classList.add('popup-bubble-anchor');
        pixelOffset.appendChild(content);

        this.anchor = document.createElement('div');
        this.anchor.classList.add('popup-tip-anchor');
        this.anchor.appendChild(pixelOffset);

        // Optionally stop clicks, etc., from bubbling up to the map.
        this.stopEventPropagation();
    };
    // NOTE: google.maps.OverlayView is only defined once the Maps API has
    // loaded. That is why Popup is defined inside initMap().
    Popup.prototype = Object.create(google.maps.OverlayView.prototype);

    /** Called when the popup is added to the map. */
    Popup.prototype.onAdd = function() {
        this.getPanes().floatPane.appendChild(this.anchor);
    };

    /** Called when the popup is removed from the map. */
    Popup.prototype.onRemove = function() {
        if (this.anchor.parentElement) {
            this.anchor.parentElement.removeChild(this.anchor);
        }
    };

    /** Called when the popup needs to draw itself. */
    Popup.prototype.draw = function() {
        var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
        // Hide the popup when it is far out of view.
        var display =
            Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                'block' :
                'none';

        if (display === 'block') {
            this.anchor.style.left = divPosition.x + 'px';
            this.anchor.style.top = divPosition.y + 'px';
        }
        if (this.anchor.style.display !== display) {
            this.anchor.style.display = display;
        }
    };

    /** Stops clicks/drags from bubbling up to the map. */
    Popup.prototype.stopEventPropagation = function() {
        var anchor = this.anchor;
        anchor.style.cursor = 'auto';

        ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
            'pointerdown']
            .forEach(function(event) {
                anchor.addEventListener(event, function(e) {
                    e.stopPropagation();
                });
            });
    };
}

const googleMap = {
    address : window.businessUnitAddress || handelbarsDataPrivatObject.address.fullAddress,


    init: function () {
        if(document.getElementById("google-map") != null) {
            this.activateMap();
            this.changeLinkOfMapAccordingToOperatingSystem();
        }
    },

    changeLinkOfMapAccordingToOperatingSystem: function () {

        var self = this;

        $(".google_maps_link ,.open-map, .popup-address-link").click(function () {

            if (window.isMobile.any()) {
                // if (window.isMobile.iOS()) {
                //     window.open(`maps://maps.google.com/maps?daddr=${self.address}` )
                // } else {
                window.open(`https://maps.google.com/?q=${self.address}` )
                // }
            } else {
                window.open(`https://maps.google.com/?q=${self.address}`)
            }


            return false;
        })
    },

    activateMap : function(){
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var myOptions = {
            zoom: 17,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            },
            navigationControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}]
        };

        definePopupClass();

        var map = new google.maps.Map(document.getElementById("google-map"), myOptions);





        console.log('googleMap.address', googleMap.address);
        if (geocoder) {
            geocoder.geocode({
                'address': googleMap.address,
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                        map.setCenter(results[0].geometry.location);
                        console.log('results[0].geometry.location', results[0].geometry.location);


                        var lat = results[0].geometry.location.lat();
                        var lng = results[0].geometry.location.lng();
                        popup = new Popup(
                            // new google.maps.LatLng(-33.866, 151.196),
                            new google.maps.LatLng(lat, lng),
                            document.getElementById('content')
                        );
                        popup.setMap(map);

                        // var infowindow = new google.maps.InfoWindow({
                        //     content: '<b>' + handelbarsDataPrivatObject.address.fullAddress + '</b>',
                        //     size: new google.maps.Size(150, 50),
                        //
                        // });



                        var marker = new google.maps.Marker({
                            position: results[0].geometry.location,
                            map: map,
                            title: window.businessUnitAddress,
                            icon: 'https://s3-eu-west-1.amazonaws.com/studio.gf.manulife.digital/manulife-website/staging/mls-mfa/images/general/icon-map-pin-arrow-down.png'
                        });



                        /*google.maps.event.addListener(marker, 'click', function () {
                            infowindow.open(map, marker);
                        });*/



                    } else {
                    }
                } else {
                }
            });
        }
    },

}

module.exports = googleMap;